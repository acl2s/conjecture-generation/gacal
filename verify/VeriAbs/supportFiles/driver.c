#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

#define MAX_STATES 100000
#define SOME 0
#define ALL 1

int varvals[1000] ;
int avarvals[1000] ;
int init_varvals[1000] ;
int init_avarvals[1000] ;

struct state { int *vvals; int astate_index; int ival_index; //} ;// to try;
	//190905
	int id; int parentid; } ;// to try;
struct state *states[100000];	//active states?
struct state *estates[100000];	//inactive states - low priority concrete states that are to be explored later as they represent a previously generated abstract state since we are interested in exploring newer and newer abstract states.
struct state *astates[100000];	//abstract states?
struct state *cstates[100000];	//concrete state and the number of abstract states associated with it.
int num_states = 0;
int num_estates = 0;
int num_cstates = 0;
int num_astates = 0;
int num_active_states = 0;
int num_active_astates = 0;
int num_max_cstates = 0;

int parentid = -1;
struct state *poppedst;
struct state *trace[1000];

//#include "var_funcs.c"

int same_state(int s1[], int s2[], int nvars)
{
    int i;
    for (i=0; i< nvars; i++)
        if (s1[i]!=s2[i]) return 0;
    return 1;
}

/*void print_state()
{
    int i;
    printf("Printing state");
    for (i=0; i< num_vars; i++)
    {
        printf("%2d ",varvals[i]);
    }
    printf("\n");
}*/

void print_cstates()
{
    int i,j;
    printf("total states %d \n", num_states);
    printf("total cstates %d \n", num_cstates);
    for (j=0; j< num_cstates; j++)
    {
        printf("%d ", cstates[j]->astate_index);
        for (i=0; i< num_cvars; i++)
        {
            printf("%2d ",cstates[j]->vvals[i]);
        }
        printf("\n");
    }
}

int find_state(struct state *sts[], int n_tot, int n_vars, int vvs[])
{
    int i;
    for (i=0; i<n_tot; i++) {
        if (same_state(sts[i]->vvals, vvs, n_vars)) return i;
    }
    return -1; // not found;
}

// num_estates already incremented
void push_new_estate(int astate_place)
{
    int k;

    estates[num_estates - 1]= (struct state *)
                            calloc(sizeof(struct state),1);
    estates[num_estates - 1]->vvals = (int *) 
                            calloc(sizeof(int), num_vars);
    for (k=0; k< num_vars; k++) 
        estates[num_estates - 1]->vvals[k]=varvals[k];
    estates[num_estates - 1]->ival_index = 0;
    estates[num_estates - 1]->astate_index = astate_place;
    estates[num_estates - 1]->parentid = parentid;
    //190905: will give id at the time of transferring estates.
    return ;
}

// num_states already incremented
// for the falsification witness add the info of the id, parentid, inpval
void push_new_state(int astate_place, int inpValInd)
{
    int k;

    states[num_states-1] = states[num_active_states]; //save the first non active state
    states[num_active_states]= (struct state *)
                            calloc(sizeof(struct state),1);
    states[num_active_states]->vvals = (int *) 
                            calloc(sizeof(int), num_vars);
    for (k=0; k< num_vars; k++) 
        states[num_active_states]->vvals[k]=varvals[k];
    states[num_active_states]->ival_index = inpValInd;
    states[num_active_states]->astate_index = astate_place;
    states[num_active_states]->id = num_states-1;
    states[num_active_states]->parentid = parentid;
    //printf("Pushed state id=%d, parentid=%d, inpval=%d, at num_states=%d ", states[num_active_states]->id, parentid, inpValInd, num_states-1);
    //print_state();
    num_active_states++;
    return ;
}

void push_new_astate()
{
    int k,i;
    i = find_state(cstates, num_cstates, num_cvars, varvals);
    if (i>=0)  //190905: new astate for the same cstate
    {
        cstates[i]->astate_index++;	//190905: an indicator that there are so many astates corresponding to this cstate (concrete state)
        if (cstates[i]->astate_index >= max_cstates)
            num_max_cstates++;
	//190905: no ival_index needed here as this is only an indicator.
    }
    else
    {
        cstates[num_cstates]= (struct state *)
                            calloc(sizeof(struct state),1);
        cstates[num_cstates]->vvals = (int *) 
                            calloc(sizeof(int), num_cvars);
        for (k=0; k< num_cvars; k++) 
            cstates[num_cstates]->vvals[k]=varvals[k];
        cstates[num_cstates]->astate_index=1;
        num_cstates++;
    }
    astates[num_astates]= (struct state *)
                            calloc(sizeof(struct state),1);
    astates[num_astates]->vvals = (int *) 
                            calloc(sizeof(int), num_avars);
    for (k=0; k< num_avars; k++) 
        astates[num_astates]->vvals[k]=avarvals[k];
    astates[num_astates]->ival_index = 0;
    num_astates++;
    num_active_astates++;
    return ;
}

void push_state()
{
    int i,j, astate_found, pushInpInd=0;
    i = find_state(states, num_states, num_vars, varvals);
    if (i>=0) return ; // state exists
    i = find_state(estates, num_estates, num_vars, varvals);
    if (i>=0) return ; // state exists
    j = find_state(astates, num_astates, num_avars, avarvals);
    if (j >= 0)	 //190905: if this is the same astate -> the state is unique but a repeat abstract state.
    {
        num_estates++;		// 190905: add the same astate as an estate and not a state.
        if (num_estates> MAX_STATES) 
        {
            printf("LIMIT EXCEEDED\n");
            print_cstates();
	    printf("UNKNOWN\n");
            exit(-1);
        }
        push_new_estate(j); 	//map this estate with the astate in astates - j
        return;
    }
    //190905: new state + new estate + new astate
    num_states++;
    //printf("\nincremented num_states = %d, num_active_states\n ", num_states, num_active_states);
    if (num_states % 10000 == 0) print_cstates();
    if (num_states> MAX_STATES) 
    {
        printf("LIMIT EXCEEDED\n");
        print_cstates();
	printf("UNKNOWN\n");
        exit(-1);
    }
    //190905: repeat call. 'j' == -1 already computed - comes here only if astate is new.
    j = find_state(astates, num_astates, num_avars, avarvals);
    if (j < 0) // not found
    {
        push_new_astate();  //updates astates and cstates
        j = num_astates-1;
    }
    //190905: Commenting the following for witness generation
    //push_new_state(j); //updates states
    for(pushInpInd=0; pushInpInd < num_inpvals; pushInpInd++) {
	    push_new_state(j, pushInpInd); //updates states
	    if(pushInpInd+1 < num_inpvals)
		    num_states++; 	//not incremented in func push_new_state
    }
    return;
}

int astate_seen[MAX_STATES] ;
void transfer_estates()
{
    int i,j,k,ap;
    for (k=0; k< num_astates; k++) astate_seen[k]=0;
    i = num_estates;
    num_estates=0;
    for (j=0; j < i ; j++)
    {
        ap = estates[j]->astate_index;
        if (astate_seen[ap]==1)
        {
            estates[num_estates] = estates[j];
            num_estates++;
        }
        else
        {
            astate_seen[ap] = 1;
	    /*
            states[num_states] = states[num_active_states];
            states[num_active_states]= estates[j];
            num_active_states++;
            num_states++;
	    */
	    for(int pushInpInd=0; pushInpInd < num_inpvals; pushInpInd++) {
		    states[num_states] = states[num_active_states];
		    states[num_active_states]= estates[j];
		    states[num_active_states]->id = num_states;
		    states[num_active_states]->ival_index = pushInpInd;
		    num_active_states++;
		    num_states++;
	    }
        }
    }
}

int pop_state(int c_ptr)
{
    struct state *tmp;
    int new_inp, astate_place;
    int i,j,k;
    //printf("Index into states=%d\n", c_ptr);
    if (num_active_states == 0)
    {
        if (num_estates==0)
            return -1;
        if (num_states + num_estates > MAX_STATES)
        {
            printf("LIMIT EXCEEDED\n");
            print_cstates();
	    printf("UNKNOWN\n");
            exit(-1);
        }
        printf("Transferring %d from inactive estates to states\n", num_estates);
        printf("Existing states count %d %d \n", num_active_states, num_states);
        transfer_estates();
        c_ptr = 0;
        printf("Existing states count %d %d \n", num_active_states, num_states);
        printf("inactive estates after transfer %d\n", num_estates);
    }
    
    //190905 11-09: we are not using astates
    //if (num_active_astates == 0 && num_max_cstates >= num_cstates) 
        //return -1;
    new_inp = inpvals[states[c_ptr]->ival_index];
    //printf("Popping state from %d active_states, new_inp=%d, ival_index=%d\n", num_active_states, new_inp, states[c_ptr]->ival_index);
    //printf("Popped state id=%d, parentid=%d, inpval=%d, at num_states=%d ", states[c_ptr]->id, parentid, new_inp, num_states);
    populate_vars(states[c_ptr]->vvals);
    poppedst = states[c_ptr];
    parentid = states[c_ptr]->id; 	//190905: the popped node becomes the parent to the state to be generated after execution.

    //190905: the following should be commented
    //states[c_ptr]->ival_index++;
    
    //190905 11-09: we are not using astates
    //astate_place = states[c_ptr]->astate_index;
    //190905 10-09: the condition changes to ival_index+1 as we have commented the ival_index++ stmt right above.
    //if (astates[astate_place]->ival_index < states[c_ptr]->ival_index)
    //190905 11-09: we are not using astates
    /*
    if (astates[astate_place]->ival_index < states[c_ptr]->ival_index+1)
    {
        astates[astate_place]->ival_index++;
        if (astates[astate_place]->ival_index >= num_inpvals)
            num_active_astates--;
    }
    */
    //190905: the following if condition should be commented
    //if (states[c_ptr]->ival_index >= num_inpvals)
    {
        tmp = states[c_ptr];
        states[c_ptr] = states[num_active_states-1];
        states[num_active_states-1] = tmp;
        num_active_states--;
    }
    return new_inp;
}

/*void print_trace(int line_nondet, int line_err)
{
	int parid=-1, j=0, line=0, i;
	struct state * st;
	FILE * fp;
	printf("\nGenerating Trace.. \n");
	trace[j++] = poppedst;
	parid = poppedst->parentid;
	while(parid != -1)	//wont work if the initial state with input nondet reaches the error.
	{
		for(i=0; i<MAX_STATES; i++) {
			st = states[i];
			if(parid == st->id)
				break;
		}
		trace[j++] = st;
		poppedst = st;
		parid = poppedst->parentid;
	}

	fp = fopen("witness.graphml", "w");
	if(fp == NULL) {
		printf("\nError writing witness.\n");
		printf("UNKNOWN\n");
		exit(1);
	}
	fprintf(fp, "\n<node id=\"N0\">\n<data key=\"entry\">true</data></node>\n");
	for(i=j-1; i>=0; i--) {
		fprintf(fp, "<node id=\"N%d\"/>\n<edge source=\"N%d\" target=\"N%d\">\n<data key=\"sourcecode\">input = __VERIFIER_nondet_int();</data>\n<data key=\"startline\">%d</data>\n<data key=\"assumption\">input == (%d);</data>\n<data key=\"assumption.scope\">main</data>\n</edge>\n", j-i, j-i-1, j-i, line_nondet, inpvals[trace[i]->ival_index]); 
	}
	fprintf(fp, "<node id=\"NE\">\n<data key=\"violation\">true</data>\n</node>\n<edge source=\"N%d\" target=\"NE\">\n<data key=\"sourcecode\">error_17: __VERIFIER_error();</data>\n<data key=\"startline\">%d</data>\n</edge>\n</graph>\n</graphml>", j, line_err);
	fclose(fp);
}*/

void print_trace(char progName[], int line_nondet, int line_err, char hashKey[])
{
	int parid=-1, j=0, line=0, i;
	struct state * st;
	FILE * fp;
	printf("\nGenerating Trace.. \n");
	parid = poppedst->id;
	while(parid != -1)
	{
		for(i=0; i<MAX_STATES; i++) {
			st = states[i];
			if(parid == st->id)
				break;
		}
		poppedst = st;
		trace[j++] = st;
		parid = poppedst->parentid;
	}

	fp = fopen("witness1.graphml", "a");
	if(fp == NULL) {
		printf("\nError writing witness.\n");
		exit(1);
	}
	fprintf(fp, "\n<data key=\"programfile\">%s</data>\n<data key=\"programhash\">%s</data>\n<data key=\"memorymodel\">precise</data>\n<data key=\"architecture\">32bit</data>", progName, hashKey);
	fprintf(fp, "\n<node id=\"entry\">\n<data key=\"entry\">true</data></node>\n");
	for(i=j; i>1; i--) {
		fprintf(fp, " <node id=\"N%d\"/>\n<edge source=",i);
		if(i==j){
			fprintf(fp, "\"entry\"");
		}
		else{
			fprintf(fp,"\"N%d\"",i+1);	
		}
		fprintf(fp, " target=\"N%d\">\n<data key=\"startline\">%d</data>\n<data key=\"assumption\">result == %d</data>\n<data key=\"assumption.scope\">main</data>\n<data key=\"assumption.resultfunction\">__VERIFIER_nondet_int</data>\n</edge>\n", i, line_nondet, inpvals[trace[i-1]->ival_index]); 
	}
	fprintf(fp, " <node id=\"error\">\n<data key=\"violation\">true</data>\n</node>\n<edge source=\"N%d\" target=\"error\">\n<data key=\"startline\">%d</data>\n<data key=\"assumption\">result == %d</data>\n<data key=\"assumption.scope\">main</data>\n<data key=\"assumption.resultfunction\">__VERIFIER_nondet_int</data>\n</edge>\n", i+1, line_nondet, inpvals[trace[i-1]->ival_index]);
	fprintf(fp, "</graph>\n</graphml>");
	fclose(fp);
}

void init()
{
    parentid = -1;

    poppedst = calloc(sizeof(struct state),1);
    populate_vars(init_varvals);
	num_states = 0;
	num_estates = 0;
	num_cstates = 0;
	num_astates = 0;
	num_active_states = 0;
	num_active_astates = 0;
	num_max_cstates = 0;

	/*
	states[100000];	
	estates[100000];
	astates[100000];
	cstates[100000];
	*/
}

void loop(char progName[], int line_nondet, int line_err, int prg_type, int type, char hashKey[])
{

    int output = -1;
    int tries = 0;
    int cur_state_ptr = 0;
    int cur_last = 0;

    //init();

    copy_vars(varvals, avarvals);
    push_state();

    while ((!prg_type && num_active_states>0) || 
		    (prg_type && ((num_active_states>0 || num_estates > 0) && (num_active_astates > 0 || num_max_cstates < num_cstates))) )
    {
        int input;
        /*
        input = __VERIFIER_nondet_int();
        if ((input != 1) && (input != 2) && (input != 3) && (input != 4) && (input != 5) && (input != 6)) return -2;
        */
        //printf("cur_ptr = %d, cur_last = %d, num_acts = %d, num_total =%d \n", cur_state_ptr, cur_last, num_active_states, num_states);
        input = pop_state(cur_state_ptr);
	if(prg_type != 0) {
		cur_state_ptr++;
		if (cur_state_ptr > cur_last /* 190905 */ || num_active_states <= cur_last) 
		{
		    cur_state_ptr = 0;
		    cur_last = num_active_states -1 ;
		}

	}
        copy_vars(varvals, avarvals);
        tries++;
        //if (tries%100==0) printf("total tries %d\n",tries);
        //printf("Trying state with input = %d ", input);
        //print_state();

        // operate eca engine
        output = calculate_output(input);
        if (output <= -100)
        {  
	    print_trace(progName, line_nondet, line_err, hashKey);
            printf("\nError found\n");
            printf("Total tries %d\n",tries);
            exit(0);
        }
        copy_vars(varvals, avarvals);
        //printf("Output ", input); print_state();
        push_state();
        //printf("num_estates = %d, num_astates = %d, num_active_states = %d, num_active_astates =%d, num_cstates=%d \n", num_estates, num_astates, num_active_states, num_active_astates, num_cstates);


    }
}

int main(int ARGC, char* ARGV[])
{
    // default output
    int prg_type = -1, loop_cond=0;

    //FOR TRACE
    int line_nondet, line_err;

    if(ARGC>=4) {
	    line_nondet = atoi(ARGV[2]);
	    line_err = atoi(ARGV[3]);
	    prg_type = atoi(ARGV[4]);
	    if(prg_type > 1 || prg_type < 0) {
		    printf("\nERROR: LAST ARGUMENT SHOULD BE 0 OR 1-> 0: Resets only program, 1:algebraic expressions. \n");
		    exit(2);
	    }
    }
    //copy the init values of prg loop outputs.
    copy_vars(init_varvals, init_avarvals);
    loop(ARGV[1], line_nondet, line_err, prg_type, ALL, ARGV[5]);


    printf("\nNo Error\n");
    exit(0);
    //printf("Total tries %d\n",tries);
}
