#include "kval.h"
#define MAX_INT 2147483647
extern void *malloc (unsigned int __size) ;
int __li_0;

void __choose_indexes(int  iarr[],int nelems, int bound) {
	int i;
    bound = MAX_INT;
	iarr[0]=nondet_unsigned_int();
	__CPROVER_assume(0<=iarr[0]);
	for (i=1; i< nelems; i++) {
		 iarr[i]=nondet_unsigned_int();
		__CPROVER_assume(iarr[i-1] < iarr[i]);
	}
	__CPROVER_assume(iarr[nelems-1]<bound);
}

void __choose_vals(int  arr[], int nelems) 
{
    for (int i=0; i< nelems; i++)
    {
        arr[i] = nondet();
        __CPROVER_assume(arr[i]>-100000 && arr[i] < 100000);
    }
}

void *ptr_nondet();
void __choose_ptr_vals(void *arr[], int nelems) 
{
    for (int i=0; i< nelems; i++)
    {
        //arr[i] = ptr_nondet();
        arr[i] = malloc(20);
        //__CPROVER_assume(arr[i]>-100000 && arr[i] < 100000);
    }
}

// size is size of aggr in bytes
// nelems is number of elems (of aggr type) in array a
void __choose_aggr_vals(char *arr, int nelems, int size) 
{
    for (int i=0; i< nelems*size; i++)
    {
        //arr[i] = ptr_nondet();
        arr[i] = nondet();
        __CPROVER_assume(arr[i]>-100 && arr[i] < 100);
    }
}

int __get_index(int  iarr[], int nelems, int val) 
{
    int index = -1;
    for (int i=0; i< nelems; i++)
    {
        if (iarr[i] == val)
            return i;
    }
    // shoudl never come here
    __CPROVER_assume(0);
}
