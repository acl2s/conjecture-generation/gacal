#include "fixcode.h"
#define __KP1 (__K+1)
#define __KP2 (__K+2)
long long __skl_iters[__KP2];  // [i, 1..K+1]
int __prop_fails = 0 ;
// SKOLSZ will be __K+2
void __choose_iters(long long  iters[],int SKOLSZ, int bound) {
	int i;
    bound = MAX_INT;
	iters[0]=nondet_unsigned_int();
	__CPROVER_assume(-1<=iters[0]);
	for (i=1; i< SKOLSZ; i++) {
		 iters[i]=nondet_unsigned_int();
		__CPROVER_assume(iters[i-1] < iters[i]);
	}
	__CPROVER_assume(iters[SKOLSZ-1]<bound);
}

void __copy_saved_array(int  d_arr[], int s_arr[], int nelems) 
{
    for (int i=0; i< nelems; i++)
    {
        d_arr[i] = s_arr[i];
    }
}

void __copy_saved_ptr_array(void  *d_arr[], void *s_arr[], int nelems) 
{
    for (int i=0; i< nelems; i++)
    {
        d_arr[i] = s_arr[i];
    }
}

void __copy_saved_aggr_array(char  *d_arr, char *s_arr, int nelems, int size) 
{
    for (int i=0; i< nelems*size; i++)
    {
        d_arr[i] = s_arr[i];
    }
}
