#include "fixcode.h"
long long __skl_iters[__K];  // [0..K-1]
void __choose_iters(long long  iters[],int SKOLSZ, int bound) {
	int i;
    bound = MAX_INT;
	iters[0]=nondet_unsigned_int();
	__CPROVER_assume(0<=iters[0]);
	for (i=1; i< SKOLSZ; i++) {
		 iters[i]=nondet_unsigned_int();
		__CPROVER_assume(iters[i-1] < iters[i]);
	}
	__CPROVER_assume(iters[SKOLSZ-1]<bound);
}
