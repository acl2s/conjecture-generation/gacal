#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

#define MAX_STATES 100000

int varvals[1000];
int avarvals[1000] ;
struct state { 
	int *vvals;
	int id; 
	int parentid; 
	int* ival_indexes;
} ;

struct state *states[100000];
struct state *astates[100000];
int num_states = 0;
int num_astates = 0;
int num_active_states = 0;
int parentid = -1;
struct state *poppedst;
struct state *trace[1000];
#include "var_funcs.c"
void push_new_state(int* inpValInds);

int same_state(int s1[], int s2[], int nvars)
{
	int i;
	for (i=0; i< nvars; i++){
		if (s1[i]!=s2[i]) { 
			return 0;
		}	
	}
	return 1;
}

void print_state()
{
	int i;
	printf("Printing state");
	for (i=0; i< num_vars; i++)
	{
		printf("%2d ",varvals[i]);
	}
	printf("\n");
}

int find_state(struct state *sts[], int n_tot, int n_vars)
{
	int i;
	for (i=0; i<n_tot; i++) {
		int same = same_state(sts[i]->vvals, varvals, n_vars);
		if (same) {
			return i;
		}
	}
	return -1; // not found;
}


void push_new_state(int inpValInds[]) 
{
	int k;
	states[num_states]= (struct state *)
		calloc(sizeof(struct state),1);
	states[num_states]->vvals = (int *) 
		calloc(sizeof(int), num_vars);
	for (k=0; k< num_vars; k++) 
		states[num_states]->vvals[k]=varvals[k];
	states[num_states]->ival_indexes = (int *) 
		calloc(sizeof(int), num_inps);
	for (k=0; k< num_inps; k++) 
		states[num_states]->ival_indexes[k]=inpValInds[k];

	states[num_states]->id = num_states-1;
	states[num_states]->parentid = parentid;
	num_active_states++;
	num_states++;
	return ;
}
void push_new_astate() 
{
	int k;
	astates[num_astates]= (struct state *)
		calloc(sizeof(struct state),1);
	astates[num_astates]->vvals = (int *) 
		calloc(sizeof(int), num_vars);
	for (k=0; k< num_vars; k++) 
		astates[num_astates]->vvals[k]=varvals[k];
	num_astates++;
	return ;
}
void check_if_in_limits()
{
	if (num_states > MAX_STATES) 
	{
		printf("LIMIT EXCEEDED\n");
		printf("UNKNOWN\n");
		exit(-1);
	}
}

void push_state()
{	
	int i,j;
	i = find_state(astates, num_astates, num_vars);
	if (i>=0) return ; // state exists 
	push_new_astate();
	if (num_astates > MAX_STATES) 
	{
		printf("LIMIT EXCEEDED\n");
		//    print_cstates();
		printf("UNKNOWN\n");
		exit(-1);
	}
	push_possible_states();
}

void pop_state(int c_ptr)
{
	struct state *tmp;
	populate_vars(states[c_ptr]->vvals);
	poppedst = states[c_ptr];
	parentid = states[c_ptr]->id; 
	num_active_states--;    
}
