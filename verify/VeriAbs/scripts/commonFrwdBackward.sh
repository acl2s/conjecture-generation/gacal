#!/bin/bash
#$1: source file

JARDIR=$LABTECROOT/jars
BACKWARDOUTDIR=$LABMCPROJDIR/InfTraceBackward
FRWDOUTDIR=$LABMCPROJDIR/InfTrace
MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini
MYPRJFILE=$LABMCPROJDIR/Input/$1.prj
SCRIPTSDIR=$LABTECROOT/scripts
echo "Entered common script (forward and backward)"
#if [ ! -d "$BACKWARDOUTDIR" ]
#then
	mkdir $BACKWARDOUTDIR
	mkdir $FRWDOUTDIR
	
#fi

#jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"
jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"

# Append printf statements to identify conditions uniquely
java -cp $jars -Xms512m -Xmx$MAXHEAPSIZE -DENVFILE="$MYENVFILE" infLoop.Driver $BACKWARDOUTDIR $MYPRJFILE -print > $BACKWARDOUTDIR/trace_$1.txt 2>&1
rc=$?
if [[ $rc -gt 0 ]]
then
        echo "InfLoop Backward: error in 1st parsing: Exiting"
        exit 10;
fi
echo "InfLoop Common: 1st Unparsing done"

# Adding header file
sed -i '1i #include<stdio.h>' "$BACKWARDOUTDIR/Unparse_$1.c"
echo "InfLoop Common: header file added"

# Preprocess unparsed file before generating IRs
gcc -m32 -E "$BACKWARDOUTDIR/Unparse_$1.c" > "$BACKWARDOUTDIR/Preprocess_Unparse_$1.c"
echo "InfLoop Common: Preprocessing of unparsed file completed"

# Generate IRs for unparsed file
$LABTECROOT/bin/cppfe --edg--m --edg-w --edg--gcc --edg--c99 -O "$LABMCPROJDIR/IRs" "$BACKWARDOUTDIR/Preprocess_Unparse_$1.c"
echo "InfLoop Common: IRs for unparsed file generated"

#Copy unparsed and preprocessed file to InfTrace directory
cp "$BACKWARDOUTDIR/Unparse_$1.c" "$FRWDOUTDIR/Unparse_$1.c"

cp "$BACKWARDOUTDIR/Preprocess_Unparse_$1.c" "$FRWDOUTDIR/Preprocess_Unparse_$1.c"
