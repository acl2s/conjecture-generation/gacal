#!/usr/bin/perl

use strict;
use warnings;
use File::Copy;

my ($witnessFile, $cFile) = @ARGV;
my $maxheapSize="$ENV{'MAXHEAPSIZE'}";
if (not defined $witnessFile) {
  die "Need witness file\n";
}
if (not defined $cFile) {
  die "Need c file\n";
}
my $jarsPath = "$ENV{'LABTECROOT'}/jars";
my $outDir="$ENV{HOME}/veriabsvalidation";

#java command 
my $witnessSrcInvMapCmd="java -cp \"$jarsPath/Labtec.jar:$jarsPath/jdom.jar:$jarsPath\" -Xms512m -Xmx$maxheapSize -D\"witness.file=$witnessFile\" witness.InvSrcValidation ";
print "Validation: $witnessSrcInvMapCmd\n";
my $start = time();
my $returnCode = system( $witnessSrcInvMapCmd );
my $end = time();
my $diff=$end - $start;

print "\nWitness validation invariant check time, $diff"."s";

if($returnCode != 0)
{
	#print "\nValidation:Noinv|Voilation|Error\n";
	exit(0);
}else {
	#print "\nInvariant.";
	exit(1);
}
