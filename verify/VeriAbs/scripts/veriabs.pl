#!/usr/bin/perl

use Cwd;
use Getopt::Std;
use Getopt::Long;
use File::Copy;
use File::Basename;
use File::Path;
use Data::Dumper;

#use 5.010;

#Parameters:
#	1.Prj File name with complete path
#	2.Weapon for Annotation(ZERODIV/ARRAYINDEXOUTOFBOUND)(for '-a' option only)

my %options=();
my $prjFilePath;
my $prjFileForAnnotator;
my @prjFilesForLabtec;
my $annotationWeaponFile="";
my $VS90COMNTOOLS="";
my $VS100COMNTOOLS="";
our $LABTECROOT = $ENV{'LABTECROOT'};
our $witnessval = "$ENV{'WITVALFILE'}";
$LABTECROOT =~ s/\\/\//g;
#my $ESCATROOT = "$LABTECROOT/Labtec/ESCAT";
my $jarsPath = "$LABTECROOT/jars";
my $errorWitnessPath = "$LABTECROOT/errorWitness";	#avriti
my $correctnessWitnessPath = "$LABTECROOT/correctnessWitness";	#avriti
my $cpacPath="$ENV{'CPACROOT'}";
my $cpacTestPath="$ENV{'CPACROOT_TEST'}";
my $success_str="VERIABS_VERIFICATION_SUCCESSFUL";
my $failed_str="VERIABS_VERIFICATION_FAILED";
my $unknown_str="VERIABS_UNKNOWN";
my $valunknown_str="VeriAbs-val:$unknown_str";
my $witness;
my %assertFuncMapDS = ();
my $labtecOutputDir;
my $inputDir;
my $loopAbsInfoDir;
our $unparserOpDir;
my $atgInpDir;
my $irsDir;
my $logsDir;
my $outputDir;
my $LSHLAUDir;
my $origProjName;
my $projName;
my $projBasePath;
my $newPrjFilePath;
my $vdgraphLogFile;
my $vdgraphErrFile;
my $unparserLogFile;
my $unparserErrFile;
our $atgWithICELogFile;
our $atgWithICEErrFile;
our $completeLogFile;
our $completeErrFile;
our $timingsFile;
my $totalTimeForLAI;
my $totalTimeForLAU;
my $totalTimeForATGwICE;
my $prefixHeaderFile="";
our $includePathsFile="";
my $CBMC_64;
my $disable_xml=0;
my $dollar = "\$"; #avriti
my $osstring = "${dollar}^O";
my $os = eval $osstring;
#require "$LABTECROOT/Labtec/scripts/runAutoGenWithICE.pl"; 
#require "$LABTECROOT/Labtec/scripts/iceFuncs.pl"; #not required for svcomp
my $start_veriabs_time;
my $end_before_cpac_time;
my $diff_before_cpac_time;
my $end_lsh_time;
#Handling of options
my $toAnnotateWithAssert=0;
my $toExtractLoopInfo=0;
my $toUnparseForAbstraction=0;
my $toRunAutoGenWithICE=0;
my $toRunAll=0;
my $toCleanDirs=0;
my $SVCOMP16=0;
my $KIND=0;
my $KIND_MAX_K=150;
my $VeriAbstimeout=900;
my $bmc1timeout=120;
my $bmc2timeout=150;
my $bmc3timeout=120;
my $bmctimeoutVSRecurssion=60;
my $cbmcVFWitnessTimeOut = 300;
my $noSlice;
my $noStaticAnalysis;
my $noinpabs;
my $smt="";
my $z3opt="";
my $allAssertionFAILED=1;
my $isPathWise=0;
my $pathAnalTimeOut=180;
my $maxheapSize="$ENV{'MAXHEAPSIZE'}";
GetOptions(	"kind"=>\$KIND,
	"smt" => \$smt,
	"bmctimeout=i" => \$bmctimeout,
	"property-file=s" => \$propertyfile, #avriti added for accepting property file
	"svcomp17"=>\$SVCOMP16)or &printUsage();
"no-slice" => \$noSlice,
"no-static-analysis" => \$noStaticAnalysis,
"no-inp-abstraction" => \$noinpabs,

getopts("lucdCvVhxI:D:", \%options);
my $currentDir=getcwd;
#print "Working directory on running VeriAbs.pl : $currentDir\n";
#print "current dir before loop abstraction $currentDir";
$witness = "$currentDir/witness.graphml";
#this env is used by afl
$ENV{WITNESS_FILENAME} = $witness;
$cpacFalseWitnessTimeout = 200;
$timeAvailable = 900;
$witnessGeneratedByCBMC=1;

if($KIND) {
	$KIND=1;
}
if($smt) {
	$smt="--mathsat";
}

if($SVCOMP16) {
	$SVCOMP16=1;
	$KIND=1; #enabling kind by default
	$start_veriabs_time=time();
	if(@ARGV==2)
	{
		#print "SVCOMP flag set. veriabs.pl taking parameters $ARGV[0] $ARGV[1]";
		$labtecOutputDir="$ARGV[0]";
		$prjFilePath="$ARGV[1]";

	}
	else
	{
		print "\n***Invalid Arguments...\n"; #USAGE: runLabtec.pl --svcomp16 <labtecOutputDir> <prjFilePath>";
		&printUsage();
		exit(1);
	}
	#if ($witnessval eq "") {print "\n\n not set $witnessval\n\n"; };
	$toExtractLoopInfo=1;
}
elsif($options{l})
{
	if(@ARGV==2)
	{
		$labtecOutputDir="$ARGV[0]";
		$prjFilePath="$ARGV[1]";

	}
	else
	{
		print "\n***Invalid Arguments...\nUSAGE: runLabtec.pl -l <labtecOutputDir> <prjFilePath>";
		exit(1);
	}

	$toExtractLoopInfo=1;
}
#elsif($options{u})
#{
#if(@ARGV==2)
#{
#$labtecOutputDir="$ARGV[0]";	
#$prjFilePath="$ARGV[1]";
#}
#else
#{
#print "\n***Invalid Arguments...\nUSAGE: runLabtec.pl -u <labtecOutputDir> <prjFilePath>";
#exit(1);
#}
#
#$toUnparseForAbstraction=1;
#}
elsif($options{c})
{
	if(@ARGV==2)
	{
		$labtecOutputDir="$ARGV[0]";	
		$prjFilePath="$ARGV[1]";
	}
	else
	{
		print "\n***Invalid Arguments...\nUSAGE: runLabtec.pl -c <labtecOutputDir> <prjFilePath>";
		exit(1);
	}

	$toRunAutoGenWithICE=1;
	$CBMC_64=0;
}##New Changes
elsif($options{C})
{
	if(@ARGV==2)
	{
		$labtecOutputDir="$ARGV[0]";	
		$prjFilePath="$ARGV[1]";
	}
	else
	{
		print "\n***Invalid Arguments...\nUSAGE: runLabtec.pl -C <labtecOutputDir> <prjFilePath>";
		exit(1);
	}
	$toRunAutoGenWithICE=1;
	$CBMC_64=1;
}
elsif($options{v})
{
	if(@ARGV==2)
	{
		$labtecOutputDir="$ARGV[0]";	
		$prjFilePath="$ARGV[1]";
	}
	else
	{
		print "\n***Invalid Arguments...\nUSAGE: runLabtec.pl -v <labtecOutputDir> <prjFilePath>";
		exit(1);
	}

	$toRunAll=1;
	$CBMC_64=0;
}
elsif($options{V})
{
	if(@ARGV==2)
	{
		$labtecOutputDir="$ARGV[0]";	
		$prjFilePath="$ARGV[1]";
	}
	else
	{
		print "\n***Invalid Arguments...\nUSAGE: runLabtec.pl -v <labtecOutputDir> <prjFilePath>";
		exit(1);
	}

	$toRunAll=1;
	$CBMC_64=1;
}
elsif ($options{x})
{
	$toCleanDirs=1;
	$labtecOutputDir="$ARGV[0]";	
	$prjFilePath="$ARGV[1]";
}
elsif($options{h})
{
	&printUsage();
}
else
{
	&printUsage();
}
#disable xml changes
if($options{d})
{
	#print "The value is being changed";
	$disable_xml=1;
	#print "The value is changed";
}

if ($options{I})
{
	$includePathsFile=$options{I};

	if ($includePathsFile=~/\// || $includePathsFile=~/\\/ )
	{
		my $folderPath=$includePathsFile;
		my $pwd= getcwd;
		$folderPath=~s/(\w*\.txt$)//g;
		my $temp=$1;
		chdir "$folderPath";
		$folderPath= getcwd;
		$folderPath=~s/\/cygdrive\/(\D)/${1}:/g;
		$includePathsFile=$folderPath."/".$temp;
		chdir "$pwd";

	}
	$includePathsFile =~ s/\\/\//g;
}
if ($options{D}){$prefixHeaderFile=$options{D};}

# Getting the input base folder in absolute path
$labtecOutputDir =~ s/\\/\//g;
my $presentWorkDir=getcwd;
chdir "$labtecOutputDir";
$labtecOutputDir= getcwd;
$labtecOutputDir=~s/\/cygdrive\/(\D)/${1}:/g;
#$labtecOutputDir=&prependWithRightPath($labtecOutputDir); #not required for svcomp iceFuncs.pl
chdir "$presentWorkDir";

$prjFilePath=~s/\/cygdrive\/(\D)/${1}:/g;

$prjFilePath =~ s/\\/\//g;

if(!(-f $prjFilePath))
{
	print "Error : $prjFilePath does not exist";
	exit(1);

}

if(index($prjFilePath,".prj") == -1)
{
	print "Error : please provide a file with '.prj' extension";
	exit(1);
}

sub printUsage()
{
	my $usageFile = "$LABTECROOT/supportFiles/VeriAbsSVCOMP17Usage.txt";
	open(USAGEFILE,$usageFile);
	while(<USAGEFILE>)
	{
		print $_;
	}
	close(USAGEFILE);
	exit(1);
}

sub checkProjFiles()
{
	$prjFilePath=~/(.*)\/(.*)\.prj/;
	if($2=~/\./)
	{
		print "\n***Error : Prj file name should not contain '.'\n";
		exit(1);
	}

	open (PRJFILE, "$prjFilePath") || die "Error : Cannot open file $prjFilePath\n";
	my @cFilesArr = <PRJFILE>;
	close(PRJFILE);

	foreach my $cFile (@cFilesArr)
	{
		chomp($cFile);
		if(!($cFile=~/(.*).c/))
		{
			next;
		}
		$cFile =~ s/\\/\//g;
		if (!($cFile=~/\// || $cFile=~/\\/))
		{		
			$cFile=".\/".$cFile;
		}
		my $presentWorkDir=getcwd;
		$cFile=~/(.*)\/(.*)\.c/;
		my $onlyCFile="$2.c";
		if($2 =~ m/[^a-zA-Z0-9\_]/)
		{
			print "\nError : Invalid file name :: $onlyCFile\nFile name should not contain special characters(except '_')\n";
			exit(1);
		}

	}
}

if (!($prjFilePath=~/\// || $prjFilePath=~/\\/))
{
	$prjFilePath=".\/".$prjFilePath;
}

$prjFilePath=~/(.*)\/(.*)\.prj/;
my $onlyPrjFile="$2.prj";
my $onlyPrismLogFile="$2.txt";
chdir "$1";
$prjFilePath= getcwd;
$prjFilePath=~s/\/cygdrive\/(\D)/${1}:/g;
$prjFilePath=~s/\/$//g;
#$prjFilePath=&prependWithRightPath($prjFilePath); #not required for svcomp iceFuncs.pl
$projBasePath=$prjFilePath;
$prjFilePath="$prjFilePath"."/"."$onlyPrjFile";
chdir "$presentWorkDir";

#Check if given prj or any c file name contains '.', then exit
&checkProjFiles();

$onlyPrjFile=~ /(.*)\.prj$/;
$projName=$1;

our $projDir;
$projDir="$labtecOutputDir/$projName";
$ENV{'LABMCPROJDIR'} = $projDir;
my $invariantFile="$projDir/invariant.xml";
my $summInvariantFile="$projDir/summInvariant.xml";
$inputDir = "$projDir/Input"; 
$loopAbsInfoDir = "$projDir/LAI";
$unparserOpDir = "$projDir/LAU";
$atgInpDir = "$projDir/ATG_Inp";
$outputDir="$projDir/Output";
$irsDir="$projDir/IRs";
$logsDir="$projDir/Logs";
$LSHLAUDir="$projDir/LSH";
$newPrjFilePath= "$inputDir/$onlyPrjFile";
$prismLogPath= "$inputDir/$onlyPrismLogFile";
$timingsFile="$logsDir/Timings.csv";
$timingsLABMCFile="$logsDir/TimingsContextWise.csv"; #avriti
$detailedTimingsLABMCFile="$logsDir/DetailedTimingsLABMC.csv"; #avriti
$vdgraphLogFile="$logsDir/LAI.log";
$vdgraphErrFile="$logsDir/LAI.err";
$unparserLogFile="$logsDir/LAU.log";
$unparserErrFile="$logsDir/LAU.err";
$atgWithICELogFile="$logsDir/ATGWithICE.log";
$atgWithICEErrFile="$logsDir/ATGWithICE.err";
$completeLogFile="$logsDir/$projName\_completeLog.txt";
$completeErrFile="$logsDir/$projName\_completeError.txt";
$SEQTMPDIR =  "$projDir/LPM";
my $seqRunlogBMC="$SEQTMPDIR/seqBMClog.txt";

my $retStr = "";
my $cbmcExe;
my $cfileFromPrj;
my $no_induction=0;
my $output_abstraction=0;
my $bmcRunOnOrigFile=0;
if($toExtractLoopInfo == 1)
{
	$cbmcExe = "cbmc";
	#$z3opt="--z3";
	$z3opt="";
	if(("$os" eq "MSWin32") || ("$os" eq "cygwin"))
	{
		$cbmcExe = "cbmc.exe";
	}
	$cbmcExe="$LABTECROOT/bin/$cbmcExe";	#avriti added 26 oct 2016: Using cbmc from VeriAbs bin
	if (! -e $cbmcExe) { print "VeriAbs error: cbmc does not exist! $cbmcExe"; }
	#print "\n CBMC version : ".`$cbmcExe --version`;
	#print  "\nCPAC Root: $ENV{'CPACROOT'}\n";


	&cleanup("$projDir");
	unlink ($completeLogFile);
	unlink ($completeErrFile);
	&openfile($vdgraphLogFile,$vdgraphErrFile);
	&reopenfile($completeLogFile,$completeErrFile);

	&modifyPrjFile($prjFilePath);
	$cfileFromPrj = `sed "2q;d" $inputDir/$onlyPrjFile`;
	chomp($cfileFromPrj);

	#Array Abstarction start
	if ($witnessval eq "") { # not set
		$lsh_array_abs=1;
		&extractLoopAbstractionInfo($newPrjFilePath);
		$lsh_array_abs=0;
	}
	# Array Abstraction end
	#Abstraction without Induction
	$output_abstraction=1;
	$no_induction=1;
	&abstractWithoutInduction();
	$no_induction=0;

	my $isSlovedbyLA=10; #not solved by LA
	my $retStr="";
	($isSlovedbyLA,$retStr)=&loopAbsIndution();
	if ( $SVCOMP16 == 1 )
	{
		if ($isSlovedbyLA==10 ) # not solved by LA
		{
			##########################################################################################
			## try CBMC with loop unwinding assertions. we may get a verification failed or successful.
			if ($witnessval eq "") { # not set
				$retStr=&cbmcDefault();
			}
				if ($KIND != 1) {
					if( $retStr =~ "unwinding assertion loop" ) {
						print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
						print "\nCBMC:LOOP UNWINDING ASSERTION FAILURE\n";
					}
					elsif( $retStr =~ "recursion unwinding assertion" ) {
						print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
						print "\nCBMC:RECURSION UNWINDING ASSERTION FAILURE\n";
					}
					else {
						print LOGFILE "\n$unparserOpDir:CBMC:UNKNOWN, timeout/error?"; 
						print "\nCBMC:TOERR\n";
					}
					print "\n$unknown_str\n";
					#&runAFLunknown();
					#unlink "$currentDir/error-witness_$projName.graphml";
					unlink "$witness";
				}

				##########################################################################################
				## try LABMC with k induction. we may get a verification successful.


				elsif ($KIND == 1) {
					#avriti loop Bounds computed
					if ($witnessval eq "") { # not set
						if (-e "$unparserOpDir/loopbound.txt") {
							&cbmcWithKnownBound(150);
						}
					}
					&summarizer();
					#&identifyContextSwitchLoopPattern() ;
					#&runAFL(250) ;
					$pathAnalTimeOut=240;
					&pvAnalysis($pathAnalTimeOut);  #Added by Afzal
					if ($witnessval eq "") { # not set
						if( $retStr =~ "unwinding assertion loop" ) { #retStr : Bounded or unknown/infinite bound(20 tried) ie Bounds.txt not generated.
							print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
							#print "\nCBMC:LOOP UNWINDING ASSERTION FAILURE\n";
							&cbmcWithLAUF();
						}elsif( $retStr =~ "recursion unwinding assertion" ) { #retStr : Bounded or unknown/infinite bound(20 tried) ie Bounds.txt not generated.
							print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
							#print "\nCBMC:RECURSION UNWINDING ASSERTION FAILURE\n";
						}
						else {
							print LOGFILE "\n$unparserOpDir:CBMC:UNKNOWN, timeout/error?"; 
							#print "\nCBMC:TOERR\n";
							#OOM cases: binary search
							&cbmcOOMWithBinarySearch(150);
						}
						#end avriti loop Bounds computed
						#&hoistAssertAndAbstract();
						# continue with K-Induction if not VF/VS with above unwind counts
						print "\nKN Verification\n";
						#print "\nKN Verification disabled. Calling VeriFuzz.\n";
						#my $currtime=time() - $start_veriabs_time;
						#my $afltime=850 - $currtime;
						#&runAFL_InfLoop($afltime);
						#&runAFLunknown();
						#&runVeriFuzz(); # comment hositing, on calling verifuzz
						&KNVerification();
						#&runCPAC_PA_KI();
					}

				}
			#}
		}
#	}
		#}
		close LOGFILE;
		exit(0);
	}

	&closefile();

}
elsif($toUnparseForAbstraction == 1)
{
	$totalTimeForLAU=0;
	&cleanup("$projDir/LAU");
	&openfile($unparserLogFile,$unparserErrFile);
	&reopenfile($completeLogFile,$completeErrFile);
	print TIMINGSFILE "\n\n----------New LAU run----------\n";
	print CMPLOGFILE "************             Starting new run of Loop Abstraction Unparsing ::\n\n";
	print CMPERRFILE "************             Starting new run of Loop Abstraction Unparsing ::\n\n";

	my $currLAU_st_Time = time();
#	&modifyPrjFile($prjFilePath);
	&prepareUnparseFilesWithAbstraction($newPrjFilePath);
	#&errFunc($unparserLogFile,$unparserErrFile,"u");
	my $currLAU_end_Time = time();
	$totalTimeForLAU = $currLAU_end_Time - $currLAU_st_Time;
	print TIMINGSFILE "\nTotal time needed for Loop Abstraction Unparsing,$totalTimeForLAU\n";
	print TIMINGSFILE "\n-------------------------------\n";
	&closefile();
}
elsif($toRunAutoGenWithICE == 1)
{
	$totalTimeForATGwICE=0;
	&openfile($atgWithICELogFile,$atgWithICEErrFile);
	&reopenfile($completeLogFile,$completeErrFile);
	print TIMINGSFILE "\n\n----------New AutoGen with ICE run----------\n";
	print CMPLOGFILE "************             Starting new run of AutoGen Invocation with ICE ::\n\n";
	print CMPERRFILE "************             Starting new run of AutoGen Invocation with ICE ::\n\n";
	#avriti start
	open (DETIALEDTIMINGSLABMCFILE, ">$detailedTimingsLABMCFile") || die (" Error : Cannot open file $detailedTimingsLABMCFile\n"); #avriti
	print DETIALEDTIMINGSLABMCFILE "LAI(s),Inp Extraction(s),WCC(s),CBMC(s),Slicer(s),TD time(s),AutoGenWithICE(s),LABMC(s)\
	"; #avriti
	open (TIMINGSLABMCFILE, ">$timingsLABMCFile") || die (" Error : Cannot open file $timingsLABMCFile\n"); #avriti
	#**** added to get -l timings ********
	&closefile();
	&reopenfile($completeLogFile,$completeErrFile);
	open(my $data, '<', $timingsFile) or die "Can not open file $timingsFile";
	while (my $line = <$data>) 
	{
		chomp $line; 
		my @fields = split (/=|,/ , $line);
		if (defined $fields[0])
		{
			#print $fields[0]." ";
			if (index($fields[0], "Total time needed for Loop Abs Info Generation") != -1) 
			{
				if (defined $fields[1])
				{
					$LAI=$fields[1];
				}
			}
		}
	}
	#************
	$totalTimeForLAI=$LAI;
	print DETIALEDTIMINGSLABMCFILE "$totalTimeForLAI";

#avriti end

	my $currATGwICE_st_Time = time();
#	&modifyPrjFile($prjFilePath);
	&invokeATGWithICEInIterations($newPrjFilePath);
	#&errFunc($atgWithICELogFile,$atgWithICEErrFile,"c");
	my $currATGwICE_end_Time = time();
	$totalTimeForATGwICE = $currATGwICE_end_Time - $currATGwICE_st_Time;
	print TIMINGSFILE "\nTotal time needed for AutoGen with ICE invocation,$totalTimeForATGwICE\n";
	print TIMINGSFILE "\n-------------------------------\n";

	my $totalLABMCTime=$totalTimeForLAI + $totalTimeForATGwICE;	#avriti start 29 sept 2015
	print TIMINGSLABMCFILE "\nTotal LABMC(s),LAI(s),Total AutoGenWithICE(s),Inp Extraction(s), TD time(s)\
	";
	print TIMINGSLABMCFILE "$totalLABMCTime,$totalTimeForLAI,$totalTimeForATGwICE,$timeForInpExtraction,$timeForTDGen\
	";	#avriti end

	&closefile();
	&reopenfile($completeLogFile,$completeErrFile);
	print DETIALEDTIMINGSLABMCFILE ",$totalTimeForATGwICE,$totalLABMCTime";
	&closefile();
}
elsif($toRunAll == 1)
{
	&cleanup("$projDir");
	#Run all the options together

	$totalTimeForLAI=0;	
	&openfile($vdgraphLogFile,$vdgraphErrFile);
	&reopenfile($completeLogFile,$completeErrFile);
	print DETIALEDTIMINGSLABMCFILE "LAI(s),Inp Extraction(s),WCC(s),CBMC(s),Slicer(s),TD time(s),AutoGenWithICE(s),LABMC(s)\
	"; #avriti
	print TIMINGSFILE "\n----------New LAI run----------\n";
	print CMPLOGFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";
	print CMPERRFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";
	my $currLAI_st_Time = time();
	#copy prj file to Input folder & insert "FILE:" into it
	&modifyPrjFile($prjFilePath);
	&extractLoopAbstractionInfo($newPrjFilePath);
	#&errFunc($vdgraphLogFile,$vdgraphErrFile,"l");
	my $currLAI_end_Time = time();
	$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
	print TIMINGSFILE "\nTotal time needed for Loop Abs Info Generation,$totalTimeForLAI\n";
	print TIMINGSFILE "\n-------------------------------\n";
	print DETIALEDTIMINGSLABMCFILE "$totalTimeForLAI";
	&closefile();

	#changes for merging ranges
	#Create "ATG_Inp" directory for AutoGen input files, if it does not exist
	#if(!(-e $atgInpDir))
	#{
	#mkdir($atgInpDir) or die "Error : Couldn't create $atgInpDir directory";
	#print PRJLOGFILE "\nCreating $atgInpDir directory...\n";		
	#}

	#$totalTimeForLAU=0;
	#&openfile($unparserLogFile,$unparserErrFile);	
	#&reopenfile($completeLogFile,$completeErrFile);
	#print TIMINGSFILE "\n----------New LAU run----------\n";
	#print CMPLOGFILE "************             Starting new run of Loop Abstraction Unparsing ::\n\n";
	#print CMPERRFILE "************             Starting new run of Loop Abstraction Unparsing ::\n\n";
	#my $currLAU_st_Time = time();
	#&prepareUnparseFilesWithAbstraction($newPrjFilePath);
	##&errFunc($unparserLogFile,$unparserErrFile,"u");	#this line was always commented before changes for running AutoGen only once.
	#my $currLAU_end_Time = time();
	#$totalTimeForLAU = $currLAU_end_Time - $currLAU_st_Time;
	#print TIMINGSFILE "\nTotal time needed for Loop Abstraction Unparsing,$totalTimeForLAU\n";
	#print TIMINGSFILE "\n-------------------------------\n";
	#&closefile();

	$totalTimeForATGwICE=0;
	&openfile($atgWithICELogFile,$atgWithICEErrFile);
	&reopenfile($completeLogFile,$completeErrFile);
	print TIMINGSFILE "\n----------New AutoGen with ICE run----------\n";
	print CMPLOGFILE "************             Starting new run of AutoGen Invocation with ICE ::\n\n";
	print CMPERRFILE "************             Starting new run of AutoGen Invocation with ICE ::\n\n";
	my $currATGwICE_st_Time = time();
	&invokeATGWithICEInIterations($newPrjFilePath);
	#&errFunc($atgWithICELogFile,$atgWithICEErrFile,"c");
	my $currATGwICE_end_Time = time();
	$totalTimeForATGwICE = $currATGwICE_end_Time - $currATGwICE_st_Time;
	print TIMINGSFILE "\nTotal time needed for AutoGen with ICE invocation,$totalTimeForATGwICE\n";
	print TIMINGSFILE "\n---------------------------------------------\n";
	my $totalLABMCTime=$totalTimeForLAI + $totalTimeForATGwICE;	#avriti start 29 sept 2015
	print TIMINGSLABMCFILE "\nTotal LABMC(s),LAI(s),Total AutoGenWithICE(s),Inp Extraction(s), TD time(s)\
	";
	print TIMINGSLABMCFILE "$totalLABMCTime,$totalTimeForLAI,$totalTimeForATGwICE,$timeForInpExtraction,$timeForTDGen\
	";	#avriti end

	&closefile();
	&reopenfile($completeLogFile,$completeErrFile);
	print DETIALEDTIMINGSLABMCFILE ",$totalTimeForATGwICE,$totalLABMCTime"; #avriti
	&closefile();
}
elsif($toCleanDirs == 1)
{
	print "Deleting $projDir....";
	if(-d "$projDir")
	{
		&cleanup("$projDir");

	}
}

sub openfile()
{
	my ($logFile, $errFile)= @_;	
	if(!(-e $projDir))
	{
		mkdir($projDir) or die "Error : Couldn't create $projDir directory";		
		print PRJLOGFILE "\nCreating $projDir directory...\n";
	}

	if(!(-e $logsDir))
	{
		mkdir($logsDir) or die "Error : Couldn't create $logsDir directory";		
	}
# Opening respective log file for debugging purposes
	open (PRJLOGFILE, ">$logFile") || die (" Error : Cannot open file $logFile\n");
# Opening respective error file for writting error messages
	open (PRJERRFILE, ">$errFile") || die (" Error : Cannot open file $errFile\n");
}

sub reopenfile()
{
	my ($logFile, $errFile)= @_;	

# Opening complete error & log file to append log and errors
	open (CMPLOGFILE, ">>$logFile") || die (" Error : Cannot open file $logFile\n");
	open (CMPERRFILE, ">>$errFile") || die (" Error : Cannot open file $errFile\n");
	open (TIMINGSFILE, ">>$timingsFile") || die (" Error : Cannot open file $timingsFile\n");
	open (TIMINGSLABMCFILE, ">>$timingsLABMCFile") || die (" Error : Cannot open file $timingsLABMCFile\n"); #avriti
	open (DETIALEDTIMINGSLABMCFILE, ">>$detailedTimingsLABMCFile") || die (" Error : Cannot open file $detailedTimingsLABMCFile\n"); #avriti
}

sub closefile()
{
	close(PRJLOGFILE);
	close(PRJERRFILE);
	close(CMPLOGFILE);
	close(CMPERRFILE);
	close(TIMINGSFILE);
	close(TIMINGSLABMCFILE);#avriti
	close(DETIALEDTIMINGSLABMCFILE);
}

sub modifyPrjFile()
{
	my ($prjFilePath)= @_;

	open (PRJFILE, "$prjFilePath") || die "Error : Cannot open file $prjFilePath\n";
	my @cFilesArr = <PRJFILE>;
	close(PRJFILE);

	if(scalar @cFilesArr==0)
	{
		print "\n***Errors encountered...\n***Please refer to $vdgraphErrFile";
		print PRJERRFILE "\n***Input prj file is empty...!\n";
		exit(1);
	}		

	if(!(-e $inputDir))
	{
		mkdir($inputDir) or die "Error : Couldn't create $inputDir directory";		
		print PRJLOGFILE "\nCreating $inputDir directory...\n";
	}
	open (NEWPRJFILE, ">$newPrjFilePath") || die "Error : Cannot create file $newPrjFilePath\n";
	print NEWPRJFILE "FILE:";
	foreach my $cFile (@cFilesArr)
	{
		chomp($cFile);
		if(!($cFile=~/(.*).c/))
		{
			next;
		}
		$cFile =~ s/\\/\//g;
		if (!($cFile=~/\// || $cFile=~/\\/))
		{		
			$cFile=".\/".$cFile;
		}

		my $presentWorkDir=getcwd;
		$cFile=~/(.*)\/(.*)\.c/;
		my $onlyCFile="$2.c";
		chdir "$1";
		$cFile= getcwd;
		$cFile=~s/\/cygdrive\/(\D)/${1}:/g;
		$cFile=~s/\/$//g;
		$cFile="$cFile"."/"."$onlyCFile";
		#$cFile=&prependWithRightPath($cFile); #not required for svcomp iceFuncs.pl
		chdir "$presentWorkDir";
		print NEWPRJFILE "\n$cFile";
	}
	if ( $SVCOMP16 == 1 ) { # avriti SVCOMP added for adding main as entry point
		print NEWPRJFILE "\nENTRYPOINT:";
		print NEWPRJFILE "\nmain";
	}
	close(NEWPRJFILE);
	print PRJLOGFILE "\nCreated File : $newPrjFilePath\n";
}

sub extractValue()
{	
	my ($stringToMatch,$fileToRead,$fileToAppend) = @_;

	open (WFILE, ">>$fileToAppend") || die ("Error : Cannot open file $fileToAppend\n");
	open (RFILE, "$fileToRead") || die ("Error : Cannot open file $fileToRead\n");

	my @wholeSummary = <RFILE>;
	my $numOfElements = 0; 

	@tmpArr = grep ( /$stringToMatch/ , @wholeSummary );
	$numOfElements = scalar(@tmpArr);

	for (my $count=0; $count<$numOfElements;$count++)	
	{
		print WFILE "$tmpArr[$count]";
	}

	close(RFILE);
	close(WFILE);
}

sub grepNPrintErrsNWarns()
{
	my ($logFile, $errFile)= @_;

	my $flg=0;
	open (LOGFILE, ">>$logFile") || die ("Error : Cannot open file $logFile\n");
	open (ERRORFILE, "$errFile") || die ("Error : Cannot open file $errFile\n");
	while(<ERRORFILE>)
	{
		my $errLine=$_;
		$errLine =~ s/[\r\n]//g;
		if ($errLine =~ /^ELNK.*/)
		{
			if ($errLine =~/^ELNK0036.*/)
			{
				print LOGFILE "\n$errLine";
			}
			else
			{
				$flg++;
			}
		}
		if ($errLine =~ /^ECFE.*/)
		{
			$flg++;
		}
		if ($errLine =~ /Message: PrismException:.*/)
		{
			$flg++;
		}
		if ($errLine =~ /ATG_Error.*/)
		{
			$flg++;
		}	
		if ($errLine =~ /ATG_Warning.*/)
		{
			print LOGFILE "\n$errLine";
		}
		if ($errLine =~ /Warning.*/)
		{
			print LOGFILE "\n$errLine";
		}
		if ($errLine =~ /Error.*/)
		{
			$flg++;
		}
	}
	open (LOGFILE, "$logFile") || die ("Error : Cannot open file $logFile\n");
	open (ERRORFILE, ">>$errFile") || die ("Error : Cannot open file $errFile\n");
	while(<LOGFILE>)
	{
		my $logLine=$_;
		$logLine =~ s/[\r\n]//g;
		if ($logLine =~ /ATG_Error.*/)
		{
			$flg++;
			print ERRORFILE "\n$logLine";
		}
		if ($logLine =~ /Error.*/)
		{
			$flg++;
			print ERRORFILE "\n$logLine";
		}
		if ($logLine =~ /^ECFE.*/)
		{
			$flg++;
			print ERRORFILE "\n$logLine";
		}
		if ($logLine =~ /Message: PrismException:.*/)
		{
			$flg++;
			print ERRORFILE "\n$logLine";
		}	
		#if ($logLine =~ /Labtec Warning:.*/)
		#{
		#	$flg++;
		#	print "\n$logLine\n";
		#}
		# the input project did not have any asserts. The program is safe.
		if ($logLine =~ /The input program is safe.*/)
		{
			print "\n$logLine\n";
			print "\n$success_str\n";
			#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
			if ($propertyfile)
			{
				&callCPAC();
			}
			#avriti end
			$flg++;
			#exit(10);
			exit(0);
		}
	}
	return $flg;
}

sub errFunc()
{
	my ($logFile, $errFile, $flag)= @_;

	# get all Errors and Warnings and o/p in completeLogFile
	&extractValue ( "Labtec Error:.*\$", $errFile,$completeErrFile);
	&extractValue ( "LabtecException.*\$", $errFile,$completeErrFile);
	&extractValue ( "PrismException:.*\$", $errFile,$completeErrFile);
	&extractValue ("ATG_Warning *:", $errFile, $completeLogFile);
	&extractValue ("error LNK", $errFile,$completeErrFile);
	&extractValue (" : error ", $errFile,$completeErrFile);
	&extractValue ("ATG_Error.*:", $errFile,$completeErrFile);
	&extractValue ( ":I/O error.*\$", $errFile,$completeErrFile);
	&extractValue ( "error:.*\$", $errFile,$completeErrFile);
	&extractValue ( "Warning:.*\$", $errFile,$completeLogFile);

	&extractValue ( "Labtec Warning:.*\$", $logFile,$completeLogFile);
	&extractValue ( "warning .*\$", $logFile,$completeLogFile);
	&extractValue ("ATG_Warning *:", $logFile, $completeLogFile);
	&extractValue ("error LNK", $logFile,$completeErrFile);
	&extractValue (": error ", $logFile,$completeErrFile);
	&extractValue ("ATG_Error.*:", $logFile,$completeErrFile);
	&extractValue ( "error:.*\$", $logFile,$completeErrFile);
	if($flag eq "l")
	{
		print CMPLOGFILE "************             Loop Abstraction Information Generation Complete                **********\n\n\n" ;
		print CMPERRFILE "************             Loop Abstraction Information Generation Complete                **********\n\n\n" ;
	}
	elsif($flag eq "u")
	{
		print CMPLOGFILE "************             Loop Abstraction Unparsing Complete                **********\n\n\n" ;
		print CMPERRFILE "************             Loop Abstraction Unparsing Complete                **********\n\n\n" ;
	}
	elsif($flag eq "c")
	{
		print CMPLOGFILE "************             AutoGen Invocation with ICE Complete                **********\n\n\n" ;
		print CMPERRFILE "************             AutoGen Invocation with ICE Complete                **********\n\n\n" ;
	}

}

sub closeNexit()
{
	print "\n\nPrinting logs\n";
	system("cat $logsDir/*log") == 0
		or print "Couldn't launch [cat $logsDir/*log]: $! / $?";
	print "\n\nPrinting errors\n";
	system("cat  $logsDir/*err") == 0
		or print "Couldn't launch [cat $logsDir/*err]: $! / $?";
	my ($exitStatus) = @_;
	close(PRJERRFILE);
	close(PRJLOGFILE);
	close(CMPLOGFILE);
	close(CMPERRFILE);

	# call cbmc on original file after any error
=begin comment
	print "\nRunning verification check by cbmc er $z3opt $smt";
	if (! -e "$cfileFromPrj") { print "VeriAbs error: verification check by cbmc $cfileFromPrj does not exist!"; }

	my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe ".$z3opt." --unwinding-assertions --unwind 16 --32 ".$smt." $cfileFromPrj > $logsDir/cbmcOnlyOp.txt 2>$logsDir/cbmcOnly.err"; #avriti added graphML option
	print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
	`$cbmcCommand`;
	$retStr = `tail -n 1 $logsDir/cbmcOnlyOp.txt`;
	if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
		print LOGFILE "\n$logsDir:CBMC:VERIFICATION SUCCESSFUL";
		my $validVS=&checkValidCBMCVSUnparseEr(16,"$cfileFromPrj"); # Running cbmc for checking if no recursion unwinding assertion.
		if($validVS == 1)
		{
			print "\nBMC:$success_str\n";
			#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
			if ($propertyfile)
			{
				&callCPAC();
			}
		}else { print "\nruaf.\n";}
		#avriti end
	}
	else {
		&checkForCBMCVF("$logsDir/cbmcOnlyOp.txt");
		#print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

		$retStr = `tail -n 10 $logsDir/cbmcOnlyOp.txt`;
		my $retStr2 = `tail -n 1 $logsDir/cbmcOnlyOp.txt`;

		if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
		{
			print LOGFILE "\n$logsDir:CBMC:VERIFICATION FAILED";
			&generateCBMCVFWitness(16); # Running cbmc for witness generation
			print "\nVeriAbs:BMC:$failed_str\n";
			if( &isNonEmptyFile($witness)) {
				if ($witnessGeneratedByCBMC == 1) {
					&addToCBMCWitnessFile();
					print "\nWitness : $witness.\n";
				}
			}else {
				my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
				&generateDummyWitness($dummyfile);
			}
			exit(0); #done
			#&callCPACVoilation();
		}
	}
=end comment
=cut	
	exit($exitStatus);
	#exit(0);
}

sub cleanup
{
	my($path) = @_; 
	my($fileSep,$rmfile) ;
	if(("$os" eq "MSWin32") )
	#|| ("$os" eq "cygwin"))
	{
		$fileSep = "\\";
	}
	else
	{
		$fileSep = "/";
	}
	my @files;
	if(! (opendir(DIR,$path))) 
	{
		#print "Could not delete files $path\n"; 
		return;
	}

	@files = readdir(DIR);
	closedir(DIR) || die "could not close $path\n";
	my @dirArray = ();
	foreach my $file (@files) 
	{
		if ( ("$file" eq ".") || ("$file" eq "..") )
		{
			next;
		}

		my $rmfile = "$path".$fileSep."$file";

		if ( -d $rmfile )
		{
			push (  @dirArray,$rmfile );
		}
		else
		{
			unlink( $rmfile );
		}
	}

	foreach my $file (@dirArray) 
	{
		&cleanup( $file );
		my $x  = rmdir("$file");
	}
	rmdir("$path");
}

sub prepareIni()
{
	my ($prjBasePath,$projName,$ifArrAbs)= @_;
	my $envIniFile="$irsDir/labtec.ini";

	# prepare labtec.ini file
	open (ENVINIFILE, ">$envIniFile") || die ("Error : Cannot create file $envIniFile\n");
	print ENVINIFILE  "__PRISMROOT_=$LABTECROOT\n";
	print ENVINIFILE  "__MSGFILE_=$LABTECROOT/supportFiles/labtec.msg\n";
	print ENVINIFILE  "__REPOSDIR_=$irsDir\n";
	print ENVINIFILE  "__IRREPOSDIR_=$irsDir\n";
	print ENVINIFILE  "__MODE_=FLA\n";
	print ENVINIFILE  "__LOGFILE_=$projDir/$projName.log\n";

	#if ( $ifArrAbs == 0 ) {
	#print ENVINIFILE "__ARRAYABSTRACTION_=0\n";
	#}
	if ($lsh_array_abs==1) # off by default for all abstraction other than Array
	{
		print ENVINIFILE "__ARRAYABSTRACTION_=1\n";
	}else
	{
		if ( $KIND == 1 ) {
			print ENVINIFILE "__KINDUCT_=1\n";
		}
		if ( $SVCOMP16 == 1 ) {
			print ENVINIFILE "__SVCOMP16_=1\n";
		}
		if($bmcRunOnOrigFile == 1)
		{
			print ENVINIFILE "__ARRAYABSTRACTION_=1\n"; 
		}
		if ($no_induction == 1) {
			print ENVINIFILE "__ARRAYABSTRACTION_=1\n"; 
		} else {
			print ENVINIFILE "__ARRAYABSTRACTION_=0\n";
		}

	}
	close(ENVINIFILE);

	print PRJLOGFILE "\nCreated File : $envIniFile\n";
}

sub runCFE()
{
	my ($prjBasePath,$projName,$LabtecPrefixHeaderFile,@filesToBeProcessedArr)= @_;

	my $cppfeCommand;
	my $vsIncludePath;
	my $cppfeExe = "cppfe";
	if(("$os" eq "MSWin32") || ("$os" eq "cygwin"))
	{
		$cppfeExe = "cppfe.exe";
	}
	my $cppfeExePath = "$LABTECROOT/bin/$cppfeExe";


	my $cFileName = "";
	#print "\n***Preparing IRs for each C file...\n\n";
	print PRJLOGFILE "\n***Preparing IRs for each C file...\n\n";
	#Run cppfe for each C file to generate IRs
	foreach $cFile (@filesToBeProcessedArr)
	{
		print PRJLOGFILE "\n***Preparing IRs for $cFile...\n";
		$cFile=~ /(.*)\/(.*)\.c/;
		$cFileName = "$2.c";
		chdir("$1");

		$cppfeCommand=$cppfeExePath;

		#my $MSC_VER=1200;
		$VS90COMNTOOLS=$ENV{VS90COMNTOOLS};
		$VS100COMNTOOLS=$ENV{VS100COMNTOOLS};

		# If VC 9 is installed.
		if("$VS90COMNTOOLS" ne "")
		{
			$VS90COMNTOOLS=~s/\\Common7\\Tools\\//g;
			$vsIncludePath="$VS90COMNTOOLS\\VC\\include";

			#$MSC_VER=1500;	
		}	

		# If VC 10 is installed.
		elsif("$VS100COMNTOOLS" ne "")
		{
			$VS100COMNTOOLS=~s/\\Common7\\Tools\\//g;
			$vsIncludePath="$VS100COMNTOOLS\\VC\\include";

			#$MSC_VER=1600;
		}

		$vsIncludePath =~ s/\\/\//g;
		$cppfeCommand=$cppfeCommand." -I\"$vsIncludePath\"";

		if($includePathsFile ne "")
		{		
			if(-e $includePathsFile)
			{

				open(INCPATH,"$includePathsFile");
				while(<INCPATH>)
				{
					my $line = $_;
					$line =~ s/[\n|\r]//g;
					$line =~ s/\\/\//g;				
					if($line ne "")
					{
						$cppfeCommand = $cppfeCommand." -I\"$line\"";
					}
				}
				close(INCPATH);
			}
			else
			{
				print "Error: User provided include file $includePathsFile does not exist\n";
				print PRJERRFILE "Error: User provided include file $includePathsFile does not exist\n";
				&grepNPrintErrsNWarns($vdgraphLogFile,$vdgraphErrFile);
				&closeNexit(1);	
			}
		}

		$cppfeCommand = $cppfeCommand." --edg-m --edg-w --edg--gcc --edg--c99";
		if ($prefixHeaderFile ne "")
		{			
			if (-f $prefixHeaderFile)
			{
				$prefixHeaderFile =~ s/\\/\//g;
				if (!($prefixHeaderFile=~/\// || $prefixHeaderFile=~/\\/))
				{		
					$prefixHeaderFile=".\/".$prefixHeaderFile;
				}
				my $presentWorkDir=getcwd;
				$prefixHeaderFile =~ s/^\s+|\s+$//g;
				$prefixHeaderFile=~/(.*)\/(.*)\.h/;
				my $onlyHdrFile="$2.h";
				chdir "$1";
				$prefixHeaderFile= getcwd;
				$prefixHeaderFile=~s/\/cygdrive\/(\D)/${1}:/g;
				$prefixHeaderFile=~s/\/$//g;
				#$prefixHeaderFile=&prependWithRightPath($prefixHeaderFile); #not required for svcomp iceFuncs.pl
				$prefixHeaderFile="$prefixHeaderFile"."/"."$onlyHdrFile";	

				$cppfeCommand = $cppfeCommand." --edg--preinclude=$prefixHeaderFile";
			}	
			else
			{
				print PRJERRFILE "Error: User provided prefixHeader file $prefixHeaderFile does not exist\n";
				print  "Error: User provided prefixHeader file $prefixHeaderFile does not exist\n";
				&grepNPrintErrsNWarns($vdgraphLogFile,$vdgraphErrFile);
				&closeNexit(1);		
			}
		}

		$cppfeCommand = $cppfeCommand." --edg--preinclude=$LabtecPrefixHeaderFile -O \"$irsDir\"";

		$cppfeCommand = $cppfeCommand." \"$cFileName\" 2>> \"$vdgraphErrFile\" 1>> \"$vdgraphLogFile\"";

		print PRJLOGFILE "$cppfeCommand\n\n";
		system("$cppfeCommand");

=comment		
		if (-f $prefixHeaderFile)
		{
			print PRJLOGFILE "\n$cppfeExePath -I\"$vsIncludePath\" --edg-m --edg-w --edg--preinclude=$prefixHeaderFile --edg--preinclude=$LabtecPrefixHeaderFile -O \"$irsDir\" \"$cFileName\" 2>> \"$vdgraphErrFile\" 1>> \"$vdgraphLogFile\"\n\n";
			system("$cppfeExePath -I\"$vsIncludePath\" --edg-m --edg-w --edg--preinclude=$prefixHeaderFile --edg--preinclude=$LabtecPrefixHeaderFile -O \"$irsDir\" \"$cFileName\" 2>> \"$vdgraphErrFile\" 1>> \"$vdgraphLogFile\"");
		}
		else
		{
			print PRJLOGFILE "\n$cppfeExePath -I\"$vsIncludePath\" --edg-m --edg-w --edg--preinclude=$LabtecPrefixHeaderFile -O \"$irsDir\" \"$cFileName\" 2>> \"$vdgraphErrFile\" 1>> \"$vdgraphLogFile\"\n\n";
			system("$cppfeExePath -I\"$vsIncludePath\" --edg-m --edg-w --edg--preinclude=$LabtecPrefixHeaderFile -O \"$irsDir\" \"$cFileName\" 2>> \"$vdgraphErrFile\" 1>> \"$vdgraphLogFile\"");
		}
=cut		
		if ($? != 0) 
		{
			print "\n***IR Errors encountered...\n***Please refer to $completeErrFile\n";
			&grepNPrintErrsNWarns($vdgraphLogFile,$vdgraphErrFile);
			&errFunc($vdgraphLogFile,$vdgraphErrFile,"l");

			&closeNexit(1);
		}
	}

	print "\nIR generation completed";
	print PRJLOGFILE "\n***Parsing completed...\n";

}

#Extract Loop abstraction information required for unparsing
sub extractLoopAbstractionInfo()
{
	my ($prjFilePath)= @_;

	my @filesToBeProcessedArr;

	print CMPLOGFILE "************             Messages of Loop Abstraction Information Generation ::\n\n";
	print CMPERRFILE "************             Errors of Loop Abstraction Information Generation ::\n\n" ;

	#create IRs dir & LAI dir, if they do not exist

	if(!(-e $irsDir))
	{
		mkpath($irsDir) or die "Error : Couldn't create $irsDir directory:$!";
		print PRJLOGFILE "\nCreating $irsDir directory...\n";
	}
	if(!(-e "$irsDir/$projName"))
	{
		mkdir("$irsDir/$projName") or die "Error : Couldn't create $irsDir/$projName directory";
		print PRJLOGFILE "\nCreating $irsDir/$projName directory...\n";
	}
	if(!(-e $loopAbsInfoDir))
	{
		mkdir($loopAbsInfoDir) or die "Error : Couldn't create $loopAbsInfoDir directory";
		print PRJLOGFILE "\nCreating $loopAbsInfoDir directory...\n";
	}

	open (PRJFILE, "$prjFilePath") || die ("\nError : Cannot open file $prjFilePath\n");
	@cFilesArr=<PRJFILE>;
	foreach $cFile (@cFilesArr)
	{
		chomp($cFile);
		if ( $cFile=~/FILE:/)
		{
			next;
		}
		if ( $SVCOMP16 == 1 ) { # avriti SVCOMP skip adding entrypoint
			if ( $cFile=~/ENTRYPOINT:/)
			{
				last;
			}
		}		
		#$cFile=~ /(.*)\/(.*)\.c/;
		#print $cFile."\n";
		push(@filesToBeProcessedArr, $cFile);
	}
	close(PRJFILE);

	my $LabtecPrefixHeaderFile="$projDir/LabtecPrefixHeader.h";
	open (LABTECPREHEADERFILE, ">$LabtecPrefixHeaderFile") || die ("\nError : Cannot open file $LabtecPrefixHeaderFile\n");
	print LABTECPREHEADERFILE "//// CPPFE NEEDS THESE FOR Microsoft _In_ and _Out_\n";
	print LABTECPREHEADERFILE "#define _USE_ATTRIBUTES_FOR_SAL 0";
	close(LABTECPREHEADERFILE);

	print PRJLOGFILE "\nCreated File : $LabtecPrefixHeaderFile\n";

	#generate IRs
	&runCFE($projBasePath,$projName,$LabtecPrefixHeaderFile,@filesToBeProcessedArr);

	my $if_arr_abs = 1;
	my $out_abs_str = "";
	my $no_ind_str = "";
	my $loop_unwind_str = "";
	#prepare labtec.ini file
	if($output_abstraction == 1) {
		$KIND = 1;
		$if_arr_abs = 0;
		$out_abs_str = "--pure-output-abstraction";
		$out_abs_str = "--output-abstraction";
		$loop_unwind_str = "--unwind-loops";
	}
	if($no_induction == 1) {
		$no_ind_str = "--no-induction";
	}
	&prepareIni($projBasePath,$projName,$if_arr_abs);
	#if($output_abstraction == 1) {
	#$KIND = 1;
	#}

	#print "\n***Running Loop Abstraction Information extractor...\n\n";
	print PRJLOGFILE "\n***Running Loop Abstraction Information extractor...\n\n";

	my $currVdgraph_st_Time;
	my $retCode=0;

	if(("$os" eq "MSWin32") || ("$os" eq "cygwin")) # prjname=prjname.replace("/", "\\"); avriti commented line in Labtec4linux.but causing problem on windows 
	{
		print PRJLOGFILE "\njava -cp \"$jarsPath/Labtec.jar;$jarsPath/slicerWithUnparser.jar;$jarsPath/antlr.jar;$jarsPath/jcupjlex.jar;$jarsPath/joot.jar;$jarsPath/prism.jar;$jarsPath/stringtemplate-1.0.3.jar;$jarsPath/jdom.jar;$jarsPath/com.microsoft.z3.jar;\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$prjFilePath\"\n\n";

		$currVdgraph_st_Time = time();

		$retCode = system("java -cp \"$jarsPath/Labtec.jar;$jarsPath/slicerWithUnparser.jar;$jarsPath/antlr.jar;$jarsPath/jcupjlex.jar;$jarsPath/joot.jar;$jarsPath/prism.jar;$jarsPath/stringtemplate-1.0.3.jar;$jarsPath/Labtec.jar;$jarsPath/jdom.jar;$jarsPath/com.microsoft.z3.jar;\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$prjFilePath\"");
	}
	else
	{
		if ($lsh_array_abs==1)
		{
			#Array Abstraction
			$LSHArrayCmd="$LABTECROOT/scripts/vrf $projName";
			print "\nArray Abstraction\n";
			$LSH_result = "$projDir/LSH/arrstatus";
			$ASRT_result =  "$projDir/LSH/asrtstatus";
			#print "Before Executing $projDir";
			#print "Before Executing $temp";
			my $retCodeArr = system($LSHArrayCmd);
			#print "Labtec proj dir is: $temp\n\n";

			#if(-e "$projDir/concurrentProgramStructure.txt")
			#{
				# No Abstraction deployed.	
			#	$z3opt="";
			#	$bmcRunOnOrigFile = 1 ;
			#	$lsh_array_abs=0;
			#	&bmcCallOnOrigFile() ;

			#}
			if(-e "$projDir/concurrentSR.txt")
			{
				print "\nSR";
				$seqCmd	="sed -i 's/void.*node.*//g' $cfileFromPrj";
				print "\nprocessing done";
				system($seqCmd);
				#&runAFL(720);
				#&runVeriFuzz(); #Avriti: 26 Sep 2019
				#&runCPAC_PA_KI();
				&runParallelAFLCPAC();
				exit(0);
			}

			#Conputing Results from Array Abstraction.
			if(-f $LSH_result)
			{
				$retStr = `tail -n 1 $LSH_result`;
				if ($retStr =~ "NO")  # VS/VF
				{
					$end_lsh_time = time();
					
					$diff_time_lsh = $end_lsh_time - $start_veriabs_time;
					print "\nLSH time taken,$diff_time_lsh\n";
=begin comment
					my $LSHUnwind = 8;

					$cbmcVFWitnessTimeOut = 700 - $diff_time_lsh;
					if($cbmcVFWitnessTimeOut > 500){
						$LSHUnwind = 10;
					}
					elsif($cbmcVFWitnessTimeOut > 400){
						$LSHUnwind = 8;
					}
					elsif($cbmcVFWitnessTimeOut > 300){
						$LSHUnwind = 6;
					}

					$LSHUnwind = 5;
					print "\nLSH time taken-unwind,$cbmcVFWitnessTimeOut,$LSHUnwind\n";
=end comment
=cut
					print "VeriAbs:LSH:$failed_str\n";

					#my $oldsmt = $smt;
					#$smt = $smt." --partial-loops ";
					if(-e $witness) # remove existing witness
		    			{unlink($witness);}
					#&generateCBMCVFWitness($LSHUnwind); # Running cbmc for witness generation

					#$smt = $oldsmt;
					if( &isNonEmptyFile($witness)) {
						if ($witnessGeneratedByCBMC == 1) {
							&addToCBMCWitnessFile();
							print "\nWitness : $witness.\n";
						}
					}else {
						my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
						&generateDummyWitness($dummyfile);
					}
					exit(0); #done
				}elsif($retStr =~ "YES")
				{
					print "VeriAbs:LSH:$success_str\n";
					if ($propertyfile)
					{
						&callCPAC();
					}
					exit(0);
				}
			}

			if(-f $ASRT_result)
			{
				$retStrOfCFA = `tail -n 1 $ASRT_result`;
				if ($retStrOfCFA =~ "YES_AVP")
				{
					 $pathAnalTimeOut=15;
		   			 &pvAnalysis($pathAnalTimeOut);
				}

				
			}
			if ($retCodeArr>>8!=20)
			{
				&callVajra();
			}
			#exit(0);
		}
		elsif($no_induction == 1 || $output_abstraction == 1)
		{
			print PRJLOGFILE "\njava -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$prjFilePath\" $no_ind_str $out_abs_str $loop_unwind_str\n\n";
			print "\nAbstraction without induction + output abstraction";
			$currVdgraph_st_Time = time();
			$retCode = system("java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:$jarsPath/Labtec.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$prjFilePath\" $no_ind_str $out_abs_str $loop_unwind_str >$logsDir/plm.txt");
		}else
		{
			print  PRJLOGFILE "\njava -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$prjFilePath\" --plm > $logsDir/plm.txt\n\n";
			print "\nAbstraction with induction + plm";
			$currVdgraph_st_Time = time();
			$retCode = system("java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/Labtec.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$prjFilePath\" --plm > $logsDir/plm.txt");
			#Tanha: for plm
			my $plmDir = "$ENV{'LABMCPROJDIR'}/plm"; 
			if (-e $plmDir and -d $plmDir){
				print "\nPLM:";
				$PLCmd="$LABTECROOT/scripts/PLEsmc.sh $projName";
                	        my $plretCode=system($PLCmd);
				if($plretCode>>8 == 6){
					print "\nPLM: VERIABS_VERIFICATION_SUCCESSFUL";
					if ($propertyfile && $witnessval eq "")
				   	{
					    &callCPAC();
				    	}	
                    			exit(0);
				}	
                    		elsif($plretCode>>8 == 7){
                                	if ($witnessval eq "")
                                        {
					print "\nPLM: VERIABS_VERIFICATION_FAILED";
               			       	&generateCBMCVFWitness(30);
					if( &isNonEmptyFile($witness)) {
                                            if ($witnessGeneratedByCBMC == 1) {
                                                    &addToCBMCWitnessFile();
                                                    print "\nWitness : $witness.\n";
                                            }
                                    }else {
                                            my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
                                            &generateDummyWitness($dummyfile);
                                    }
			    	    }
					exit(0);
				}	
			}
			#Tanha: end
		}
	}
	&checkLabtecReturnCodes($retCode>>8);

	my $currVdgraph_end_Time = time();
	my $totalTimeForVdgraph=$currVdgraph_end_Time - $currVdgraph_st_Time;
	print TIMINGSFILE "\nTime to run vdgraph,$totalTimeForVdgraph\n";
	&grepNPrintErrsNWarns($vdgraphLogFile,$vdgraphErrFile);
	&errFunc($vdgraphLogFile,$vdgraphErrFile,"l");

	if (($retCode != 0)) 
	{
		print "\n***Errors encountered...\n***Please refer to $completeErrFile\n";
		&closeNexit(1);
	}
	#print "\nAbstraction completed";
	print PRJLOGFILE "\nAbstraction completed";

}

sub populateDSFromAssertFuncMap()
{
	my ($assertFuncFile,$wantMappedName)= @_;

	my %assertFuncMapDS = ();

	if (!(-f $assertFuncFile)) 
	{
		print "\n***Errors encountered...\n***Please refer to $completeErrFile\n";
		print PRJERRFILE "\n***File does not exist : $assertFuncFile!\n";
		print CMPERRFILE "\n***File does not exist : $assertFuncFile!\n";
		&errFunc($unparserLogFile,$unparserErrFile,"u");
		&closeNexit(1);
	}

	#read <prj>_assertFunc.csv file to get assert-function mapping
	open (ASSERTFUNCFILE, "$assertFuncFile")|| die ("\nError : Cannot open file $assertFuncFile\n");

	while(<ASSERTFUNCFILE>)
	{
		my $assertFuncLine = $_;
		chomp($assertFuncLine);
		if($assertFuncLine =~ /AssertId/)
		{
			next;
		}
		$assertFuncLine =~ s/[\r\n]//g;
		$assertFuncLine =~ /(.*),(.*),(.*)/;

		my $assertId = $1;
		my $fileColonFuncName = $2;
		my $mappedFuncName = $3;

		#If file_func name required
		if($wantMappedName == 1)
		{
			$assertFuncMapDS{$assertId} = $mappedFuncName;
		}
		else
		{
			#If file:func name required
			$assertFuncMapDS{$assertId} = $fileColonFuncName;	
		}
	}
	return %assertFuncMapDS; 
}

#sub prepareUnparseFilesWithAbstraction()
#{
#my ($prjFilePath)= @_;
#
#print CMPLOGFILE "************             Messages of Loop Abstraction Unparsing ::\n\n";
#print CMPERRFILE "************             Errors of Loop Abstraction Unparsing ::\n\n" ;
#
##Create "LA_Unp" dir for unparser output, if it does not exist
#if(!(-e $unparserOpDir))
#{
#mkdir($unparserOpDir) or die "Error : Couldn't create $unparserOpDir directory";
#print PRJLOGFILE "\nCreating $unparserOpDir directory...\n";		
#}

##First, read <prj>_assertFunc.csv and get assert-function mapping in hash table
#my $assertFuncFile = "$loopAbsInfoDir/$projName\_assertFunc.csv";
#my $wantMappedName = 1;
#my %assertFuncMapDS = ();
#%assertFuncMapDS = &populateDSFromAssertFuncMap($assertFuncFile,$wantMappedName);

#if (!keys %assertFuncMapDS) 
#{
#print "\n***Errors encountered...\n***Please refer to $completeErrFile\n";
#print PRJERRFILE "\n***No assert found in $assertFuncFile!\n***Please check source files...\n";
#print CMPERRFILE "\n***No assert found in $assertFuncFile!\n***Please check source files...\n";
#&errFunc($unparserLogFile,$unparserErrFile,"u");
#&closeNexit(1);
#}
#my $func;
#my $eachAssertFolder;
#foreach my $assertId (keys %assertFuncMapDS)
#{
#$func = $assertFuncMapDS{$assertId};
#$eachAssertFolder = "$unparserOpDir/A_$assertId";
##Create "A_<assertId>" folder for each assert
#if(!(-e $eachAssertFolder))
#{
#mkdir($eachAssertFolder) or die "Error : Couldn't create $eachAssertFolder directory";		
#}
#print "\n***Unparsing source for asssert Id : $assertId\n";
#print PRJLOGFILE "\n***Unparsing source for asssert Id : $assertId\n";
##call to unparser for each assert
#print PRJLOGFILE "\njava -cp \"$jarsPath/Labtec.jar;$jarsPath/antlr.jar;$jarsPath/jcupjlex.jar;$jarsPath/joot.jar;$jarsPath/prism.jar;$jarsPath/stringtemplate-1.0.3.jar;\" -DENVFILE=\"$irsDir/labtec.ini\" \"unparser.UnparserInterface\" \"-S -A $assertId -O \"$projDir\" -I \"$func\" \"$prjFilePath\"\n\n";
#
#my $currUnparser_st_Time = time();
#system("java -cp \"$jarsPath/Labtec.jar;$jarsPath/antlr.jar;$jarsPath/jcupjlex.jar;$jarsPath/joot.jar;$jarsPath/prism.jar;$jarsPath/stringtemplate-1.0.3.jar;\" -DENVFILE=\"$irsDir/labtec.ini\" \"unparser.UnparserInterface\" -S -A \"$assertId\" -O \"$projDir\" -I \"$func\" \"$prjFilePath\"");
#my $currUnparser_end_Time = time();
#my $totalTimeForUnparsing=$currUnparser_end_Time - $currUnparser_st_Time;
#print TIMINGSFILE "\nTime to run unparser for $func:$assertId,$totalTimeForUnparsing\n";
#
#&grepNPrintErrsNWarns($unparserLogFile,$unparserErrFile);
#&errFunc($unparserLogFile,$unparserErrFile,"u");
#
#my @fileCount=glob("$eachAssertFolder/*.c");
#if (scalar @fileCount==0) 
#{
#print "\n***Errors encountered...\n***Please refer to $completeErrFile\n";
#

#&closeNexit(1);
#}
#
#print PRJLOGFILE "\n***Abstracted source for Assert Id $assertId generated in : $eachAssertFolder\n";
#
#}
#
#print "\n***Finished Unparsing...\n";
#print PRJLOGFILE "\n***Finished Unparsing...\n";
#
#}

sub prepareAutoGenConfigFile()
{
	my ($atgInpDir)= @_;	

	my $configFilePath="$atgInpDir/AutoGenConfig.txt";
	my $bmctimeout=$ENV{'BMCTIMEOUT'};
	#copy("$LABTECROOT/Labtec/supportFiles/AutoGenConfig.txt","$configFilePath"); # added 24/4/2015
#=begin comment
	open (CONFIGFILE, ">$configFilePath") or die ("Error : Cannot create file $configFilePath\n");
	print CONFIGFILE "COV_CRITERION=pc\n";
	print CONFIGFILE "COVER_FUNCTIONS=SELECTED\n";
	print CONFIGFILE "GEN_TD_FOR_CALLED_FUNCTIONS=0\n";
	print CONFIGFILE "IGN_CHKR_ERR=1\n";
	print CONFIGFILE "TD_FOR_SELECTED_CONDS=1\n";
	#if ($bmctimeout)
	#{
	print CONFIGFILE "CBMC_TIMEOUT=$bmctimeout\n";
	#}else
	#{
	#print CONFIGFILE "CBMC_TIMEOUT=120\n";
	#}
	print CONFIGFILE "SLICER_TIMEOUT=450\n";
	close CONFIGFILE;
	#print PRJLOGFILE "\nCreated File : $configFilePath\n";
#=end comment
#=cut
	print PRJLOGFILE "\nCreated File : $configFilePath\n";

}

sub invokeATGWithICEInIterations()
{
	my ($prjFilePath)= @_;

	print CMPLOGFILE "************             Messages of AutoGen Invocation with ICE ::\n\n";
	print CMPERRFILE "************             Errors of AutoGen Invocation with ICE ::\n\n" ;

	#Create "ATG_Inp" directory for AutoGen input files, if it does not exist
	if(!(-e $atgInpDir))
	{
		mkdir($atgInpDir) or die "Error : Couldn't create $atgInpDir directory";
		print PRJLOGFILE "\nCreating $atgInpDir directory...\n";		
	}

	#prepare AutoGenConfig.txt in Labtec/ATG_Inp folder
	&prepareAutoGenConfigFile($atgInpDir);
	if(! -d $outputDir) {
		mkdir($outputDir) or die "Labtec_Error : Couldn't create $outputDir directory";
	}


	#read <prj>_assertFunc.csv and populate DS
	#my $assertFuncFile = "$loopAbsInfoDir/$projName\_assertFunc.csv";

	#%assertFuncMapDS = &populateDSFromAssertFuncMap($assertFuncFile,0);
	#if (!keys %assertFuncMapDS) 
	#{
	#	print "\n***Errors encountered...\n***Please refer to $completeErrFile";
	#	print PRJERRFILE "\n***No assert found in $assertFuncFile!\n***Please check source files...\n";
	#	print CMPERRFILE "\n***No assert found in $assertFuncFile!\n***Please check source files...\n";
	#	&errFunc($unparserLogFile,$unparserErrFile,"c");
	#	&closeNexit(1);
	#}

	print "\n\n***Invoking AutoGen with ICE...\n";
	print PRJLOGFILE "\n\n***Invoking AutoGen with ICE...\n";
	#my $funcWithAssert;
	#my $eachAssertFolder;
	#my $status;
	#my %assertStatusHTable = ();
	#foreach my $assertId (keys %assertFuncMapDS)
	#{
	#	$funcWithAssert = $assertFuncMapDS{$assertId};
	#	$eachAssertFolder = "$unparserOpDir/A_$assertId";

	#	print "\nProcessing assert : $assertId\n";
	#	print PRJLOGFILE "\nProcessing assert : $assertId\n";
	#	print CMPLOGFILE "******Processing assert : $assertId\n";
	#	print CMPERRFILE "******Processing assert : $assertId\n" ;

	my $currATGWithICE_st_Time = time();
	#call this function to run AutoGen with ICE & get cbmc status for each assert
	#print "The value passed to runATGWithICE for disable xml is $disable_xml";
	#$status = &runATGWithICE($assertId, $loopAbsInfoDir, $funcWithAssert, $eachAssertFolder,$atgInpDir,$CBMC_64,$disable_xml);	
	$status = &runATGWithICE($loopAbsInfoDir, $atgInpDir,$CBMC_64,$disable_xml);	
	my $currATGWithICE_end_Time = time();
	my $totalTimeForATGInvocation=$currATGWithICE_end_Time - $currATGWithICE_st_Time;
	print TIMINGSFILE "\nTime to invoke ATG with ICE for $funcWithAssert:$assertId,$totalTimeForATGInvocation\n";
#		$assertStatusHTable{$assertId} = $status;

#		print CMPLOGFILE "\n******Completed Processing assert : $assertId\n";
#		print CMPERRFILE "\n******Completed Processing assert : $assertId\n" ;
#	}

	#Create "Output" directory for AutoGen input files, if it does not exist
	#if(!(-e $outputDir))
	#{
	#print PRJLOGFILE "\nCreating $outputDir directory...\n";		
	#}

	#write each assert with its cbmc status to <prj>_assertSummary.txt
	#my $assertSummaryFile = "$outputDir/$projName\_assertSummary.txt";

	#open(SUMMARYFILE, ">$assertSummaryFile") || die ("\nError : Cannot open file $assertSummaryFile\n"); 
	#
	#print SUMMARYFILE "Assert ID : CBMC Status\n";
	#foreach $assert (keys %assertStatusHTable)
	#{
	#if(!($assertStatusHTable{$assert} eq ""))
	#{
	#print SUMMARYFILE "$assert : $assertStatusHTable{$assert}\n"; 
	#print PRJLOGFILE "\n$assert : $assertStatusHTable{$assert}\n"; 
	#}
	#}
	#close(SUMMARYFILE);
	open (PRJLOGFILE, ">>$atgWithICELogFile") || die (" Error : Cannot open file $atgWithICELogFile\n");
	print "\n***Finished AutoGen invocation with ICE\n";
	print PRJLOGFILE "\n***Finished AutoGen invocation with ICE\n";

	if(-f $assertSummaryFile)
	{
		print "\nCreated File : $assertSummaryFile\n";
		print PRJLOGFILE "\nCreated File : $assertSummaryFile\n";
		print "\nExited VeriAbs successfully..\n";
		print PRJLOGFILE "\nExited VeriAbs successfully..\n";
	}

	&errFunc($atgWithICELogFile,$atgWithICEErrFile,"c");

}

sub isNonEmptyFile() {
	my ($witnessfile) = @_;
	if (-f $witnessfile && -s $witnessfile)	{
		return 1;
	}
	return 0;
}

=begin old code
sub callCPAC()
{
	my $presentWorkDir=getcwd;
	chdir $cpacPath;
if (! -e "$cpacPath/scripts/cpa.sh") { print "VeriAbs error: cpa.sh does not exist! $cpacPath/scripts/cpa.sh"; }
if ( -e "$witness") { unlink($witness);}
	#Sample CPAC cmd
	#print "$presentWorkDir\nProperty file: $propertyfile\n$cpacPath";
	print "\nCPA dir $cpacPath";
	#scripts/cpa.sh -correctness-witnesses-k-induction -spec ALL.prp count1/LAU/test.c >op_count1_uparserchanges.txt 2>&1
	`chmod 777 scripts/cpa.sh`;
	my $cpacWitnessGenCmd;
	if (! -e "$unparserOpDir/test.c") {
	       $cpacWitnessGenCmd="scripts/cpa.sh -correctness-witnesses-k-induction -spec $propertyfile $cfileFromPrj >$logsDir/cpaLog.txt 2>&1";
	}
	else
	{
		$cpacWitnessGenCmd="scripts/cpa.sh -correctness-witnesses-k-induction -spec $propertyfile $unparserOpDir/test.c >$unparserOpDir/cpaLog.txt 2>&1";
	}
	open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
	print LOGFILE "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
	close(LOGFILE);

	#if($no_induction == 1)
	#{
	$cpacWitnessGenCmd="timeout -k 2s 300s ".$cpacWitnessGenCmd;
	#}
	$end2_before_cpac_time=time();
	my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
	print "\nTotal time taken before witness generation: $diff_before_cpac_time\n";
	print "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
	my $start = time();
	my $returnCode = system( $cpacWitnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nCPA time taken: $diff_cpac_time\n";
	if ( $returnCode != 0 ) 
	{ 
		print "Error in CPA-Checker Correctness Witness generation. [$cpacWitnessGenCmd]\n";
		&generateDummyWitness();
		exit(0);
	}
	#copy("output/correctness-witness.graphml","$correctnessWitnessPath/correctness-witness_$projName.graphml");
	#copy("output/correctness-witness.graphml","$currentDir/witness.graphml");
	copy("output/correctness-witness.graphml","$witness");
	if( &isNonEmptyFile($witness)) {
		#copy("$witness","$correctnessWitnessPath/correctness-witness_$projName.graphml");
		print "\nWitness : $witness.\n";
	}
	&generateDummyWitness();
	chdir $presentWorkDir;
	exit(0);
}

#this code was added to support insertion of invariants in cpac generated automaton
#this code is commented as cpac witness validation was failing to validate cpac automaton generated (30s timeout) without invariants
sub callCPAC()
{
	my $presentWorkDir=getcwd;
	chdir $cpacPath;

	my $witnessgendir = $LABTECROOT."/witnessgendir";
	if ( -e "$witnessgendir") {
	    rmtree($witnessgendir);
	}
	mkdir($witnessgendir) or die "Error : Couldn't create $witnessgendir directory";

	if (! -e "$cpacPath/scripts/cpa.sh") { print "VeriAbs error: cpa.sh does not exist! $cpacPath/scripts/cpa.sh"; }
	if ( -e "$witness") { unlink($witness);}

	`chmod 777 scripts/cpa.sh`;
	my $cpacWitnessGenCmd;
	$cpacWitnessGenCmd="scripts/cpa.sh -correctness-witnesses-k-induction -outputpath $witnessgendir -spec $propertyfile $cfileFromPrj >$logsDir/cpaLog.txt 2>&1";
	open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
	print LOGFILE "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
	close(LOGFILE);

	$end_before_cpac_time=time();
	my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
	print "\nTotal time taken before witness generation: $diff_before_cpac_time\n";

	$cpacWitnessGenCmd="timeout --preserve-status -k 2s 30s ".$cpacWitnessGenCmd;
	print "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;

	my $start = time();
	my $returnCode = system( $cpacWitnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nCPA time taken: $diff_cpac_time\n";

	my $cpacWitness = $witnessgendir."/correctness-witness.graphml";
	if ( ! -e $cpacWitness || ! -e $invariantFile) 
	{ 
		print "Error: Either $cpacWitness or $invariantFile doesn't exist. CPA Command: [$cpacWitnessGenCmd]\n";
		&generateDummyWitness();
		exit(0);
	}

	my $witnessGenCmd=" java -cp \"$jarsPath/generateWitness.jar:$jarsPath/jdom.jar:$jarsPath\" -Xms512m -Xmx$maxheapSize -D\"invariant.file=$invariantFile\" -D\"witness.file=$cpacWitness\" -D\"output.file=$witness\" generateWitness ";
	$witnessGenCmd = "timeout --preserve-status -k 2s 30s ".$witnessGenCmd;
	my $start = time();
	my $returnCode = system( $witnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nWitness generation time taken: $diff_cpac_time";

	if( &isNonEmptyFile($witness)) {
	    print "\nWitness : $witness.\n";
	} else {
	    &generateDummyWitness();
	}
	chdir $presentWorkDir;
	exit(0);
}
=end comment
=cut

sub callCPAC_true()
{
	my $presentWorkDir=getcwd;
	
	if($bmcRunOnOrigFile == 0)
	{
		#&selectCPACPath();
	}
	else
	{
		if (-f $seqRunlogBMC)
		{
			open LF, ">$seqRunlogBMC" or print "could not open $seqRunlogBMC";
			print LF "CBMC_SEQ:TRUE";
		}
	}
	chdir $cpacPath;
	
	if ( -e "$witness") { unlink($witness);}

	my $witnessgendir = $projDir."/witnessgendir";
	if ( -e "$witnessgendir") {
		rmtree($witnessgendir);
	}
	mkdir($witnessgendir) or die "Error : Couldn't create $witnessgendir directory";
	my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
	if (! -e $invariantFile) {
		print "witness_generation: Unable to find invariant file";
		&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;
		exit(0);
	}	    

	if (! -e "$cpacPath/scripts/cpa.sh") { print "VeriAbs error: cpa.sh does not exist! $cpacPath/scripts/cpa.sh"; }
	`chmod 777 scripts/cpa.sh`;
	my $cpacWitnessGenCmd;
	$cpacWitnessGenCmd="scripts/cpa.sh -correctness-witnesses-k-induction -outputpath $witnessgendir -spec $propertyfile $cfileFromPrj >$logsDir/cpaLog.txt 2>&1";
	open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
	print LOGFILE "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
	close(LOGFILE);

	$end_before_cpac_time=time();
	my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
	print "\nTotal time taken before witness generation: $diff_before_cpac_time\n";
	if ( $diff_before_cpac_time >300)
	{
		&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;
		exit(0);
	}
	$cpacWitnessGenCmd="timeout --preserve-status -k 20s 30s ".$cpacWitnessGenCmd;
	print "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;

	my $start = time();
	my $returnCode = system( $cpacWitnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nCPA time taken: $diff_cpac_time\n";

	my $cpacWitness = $witnessgendir."/correctness-witness.graphml";
	if ( ! -e $cpacWitness) 
	{ 
		print "Error: Either $cpacWitness or $invariantFile doesn't exist. CPA Command: [$cpacWitnessGenCmd]\n";
		&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;
		exit(0);
	}

	my $witnessGenCmd=" java -cp \"$jarsPath/generateWitness.jar:$jarsPath/jdom.jar:$jarsPath\" -Xms512m -Xmx$maxheapSize -D\"invariant.file=$invariantFile\" -D\"witness.file=$cpacWitness\" -D\"output.file=$witness\" -D\"reset.existing.inv=true\" generateWitness ";
	$witnessGenCmd = "timeout --preserve-status -k 2s 30s ".$witnessGenCmd;
	my $start = time();
	my $returnCode = system( $witnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nWitness generation time taken: $diff_cpac_time";

	#generateWitness can return non-zero return code when there are no invariants generated by us
	#in such cases return dummywitness instead of automaton created earlier
	if($returnCode != 0 && &isNonEmptyFile($cpacWitness)) {
		copy($cpacWitness, $witness);
		print "\nWitness without invariants: $witness.\n";
	}
	elsif($returnCode == 0 && &isNonEmptyFile($witness)) {
		print "\nWitness with invariants: $witness.\n";
	} else {
		if ( -e "$witness") { unlink($witness);}
		&generateDummyWitness($dummyfile);

		if($bmcRunOnOrigFile == 1)
		{
			if (-f $seqRunlogBMC)
			{
				open LF, ">$seqRunlogBMC" or print "could not open $seqRunlogBMC";
				print LF "CBMC_SEQ:TRUE";
			}
			if (!copy($cpacWitness, $witness)) {
				&generateDummyWitness($dummyfile);
			}
		}
	}
	chdir $presentWorkDir;
	exit(0);
}

sub callCPAC()
{
	my $presentWorkDir=getcwd;
	$end_before_cpac_time=time();
	my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
	print "\nTotal time taken before witness generation: $diff_before_cpac_time\n";
=begin comment
	if ( $diff_before_cpac_time >300)
	{
		&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;
		exit(0);
	}
=end comment
=cut
	if($bmcRunOnOrigFile == 0)
	{
		#&selectCPACPath();
	}
	else
	{
		if (-f $seqRunlogBMC)
		{
			open LF, ">$seqRunlogBMC" or print "could not open $seqRunlogBMC";
			print LF "CBMC_SEQ:TRUE";
		}
	}
	my $UApath=$LABTECROOT."/UAutomizer-linux";
	if ( -e "$witness") { unlink($witness);}

	my $witnessgendir = $projDir."/witnessgendir";
	if ( -e "$witnessgendir") {
		rmtree($witnessgendir);
	}
	mkdir($witnessgendir) or die "Error : Couldn't create $witnessgendir directory";
	my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
	if (! -e $invariantFile) {
		print "witness_generation: Unable to find invariant file";
		&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;
		exit(0);
	}
	chdir $UApath;
	`chmod -R 777 $UApath`;
	
	#timeout -k 2s 600s "+UApath+"/Ultimate.py --spec "+prpfile+" --architecture 64bit --file "+intask+" --witness-dir "+witnesspath+" > "+Logs+"/witness.out
	print "\nUAutomizer witness generation: PATH: $UApath\n";
	my $start = time();
	#my $uatimeout= 600 - ($start - $start_veriabs_time);
	my $UAwitGenCmd="timeout -k 2s 30s $UApath/Ultimate.py --spec $propertyfile --architecture 32bit --file $cfileFromPrj --witness-dir $witnessgendir >$logsDir/UALog.txt 2>&1"; # earlier cpac time 30s
	print "\n\n$UAwitGenCmd\n\n";
	my $returnCode = system( $UAwitGenCmd);
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nUA time taken: $diff_cpac_time,$returnCode,$uatimeout\n";

	
	my $cpacWitness = $witnessgendir."/witness.graphml";
	if ( ! -e $cpacWitness) 
	{ 
		print "Error: Either $cpacWitness or $invariantFile doesn't exist. UA Command: [$UAwitGenCmd]\n";
		&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;		 
		if($bmcRunOnOrigFile == 0){
			&callCPAC_true();
		}
		exit(0);
	}

	my $witnessGenCmd=" java -cp \"$jarsPath/generateWitness.jar:$jarsPath/jdom.jar:$jarsPath\" -Xms512m -Xmx$maxheapSize -D\"invariant.file=$invariantFile\" -D\"witness.file=$cpacWitness\" -D\"output.file=$witness\" -D\"reset.existing.inv=true\" generateWitness ";
	$witnessGenCmd = "timeout --preserve-status -k 2s 30s ".$witnessGenCmd;
	my $start = time();
	print "\n---\n $witnessGenCmd\n---\n";
	my $returnCode = system( $witnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nUA Witness generation time taken, return code: $diff_cpac_time, $returnCode";
	
	#generateWitness can return non-zero return code when there are no invariants generated by us
	#in such cases return dummywitness instead of automaton created earlier
	if($returnCode != 0 && &isNonEmptyFile($cpacWitness)) {
		copy($cpacWitness, $witness);
		print "\nUA Witness without invariants: $witness.\n";
	}
	elsif($returnCode == 0 && &isNonEmptyFile($witness)) {
		print "\nUA Witness with invariants: $witness.\n";
	} else {
		if ( -e "$witness") { unlink($witness);}
		&generateDummyWitness($dummyfile);

		if($bmcRunOnOrigFile == 1)
		{
			if (-f $seqRunlogBMC)
			{
				open LF, ">$seqRunlogBMC" or print "could not open $seqRunlogBMC";
				print LF "CBMC_SEQ:TRUE";
			}
			if (!copy($cpacWitness, $witness)) {
				&generateDummyWitness($dummyfile);
			}
		}
		if($bmcRunOnOrigFile == 0){
			&callCPAC_true();
		}
	}
	chdir $presentWorkDir;
	exit(0);
}

	    sub generateDummyWitness()
	    {
		    my ($dummyfile)= @_;
		    my $tag_spec_start = "data key=\"specification\"";
		    my $tag_programfile_start = "data key=\"programfile\"";
		    my $tag_programhash_start = "data key=\"programhash\"";
		    my $tag_spec_end = "data";

		    open (PROFILEH, "<$propertyfile") || die "$propertyfile File not found $!";; 
		    my $spec = <PROFILEH>;
		    close PROFILEH;
		    chomp ($spec);

		    open DUMMYFILEH, "<$dummyfile" or die $!;
		    my @lines = <DUMMYFILEH>;
		    close(DUMMYFILEH);

		    foreach(@lines) {
			    s/(<$tag_spec_start>)(.*?)(<\/$tag_spec_end>)/$1$spec$3/g;
			    s/(<$tag_programfile_start>)(.*?)(<\/$tag_spec_end>)/$1$cfileFromPrj$3/g;
			    my $programhash=`sha1sum $cfileFromPrj`;
			    chomp $programhash; 
			    my @fields = split (/\s/ , $programhash);
			    if (defined $fields[0])
			    {
				    s/(<$tag_programhash_start>)(.*?)(<\/$tag_spec_end>)/$1$fields[0]$3/g;
			    }
		    }

		    open(WITNESSFILE, ">$witness") || die "$witness File not found $!";
		    print WITNESSFILE @lines;
		    close(WITNESSFILE);

		    if( &isNonEmptyFile($witness)) {
			    print "\nwitness test_d : $witness.\n";
		    }
	    }
	    sub cbmcWithKnownBound()
	    {
		    my ($bmcTimeoutForKnownBound) = @_;
		    open my $boundfile, '<', "$unparserOpDir/loopbound.txt"; 
		    my $firstLine = <$boundfile>;
		    print "\n$firstLine KB $z3opt - $smt\n";	
		    close $boundfile;
		    chomp($firstLine);
		    if($firstLine <= 20) {
                $bmcTimeoutForKnownBound = 400;
            }
            else {
                $bmcTimeoutForKnownBound = 150;
            }
		    my $cbmcCommand = "timeout -k 2s ".$bmcTimeoutForKnownBound."s $cbmcExe ".$z3opt." --unwinding-assertions --unwind $firstLine --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
		    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		    `$cbmcCommand`;

		    $retStr = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
		    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION SUCCESSFUL";
			    my $validVS=&checkValidCBMCVS($firstLine,"$cfileFromPrj"); # Running cbmc for checking if no recursion unwinding assertion.
			    if($validVS == 1)
			    {
				    print "\nVeriAbs Bound:$success_str\n";
				    if ($propertyfile)
				    {
					    &callCPAC();
				    }
				    exit(0);#done
			    }else { print "\nruaf.\n";}
		    }
		    else {
			    &checkForCBMCVF("$unparserOpDir/cbmcOnlyOp.txt");
			    #print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

			    $retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
			    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;

			    if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
			    {
				    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
				    &generateCBMCVFWitness($firstLine); # Running cbmc for witness generation
				    print "\nVeriAbs Bound:$failed_str\n";
				    if( &isNonEmptyFile($witness)) {
					    if ($witnessGeneratedByCBMC == 1) {
						    &addToCBMCWitnessFile();
						    print "\nWitness : $witness.\n";
					    }
				    }else {
					    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
					    &generateDummyWitness($dummyfile);
				    }
				    exit(0); #done
				    #&callCPACVoilation();
			    }
=begin comment4
		if ($retStr !~ "unwinding assertion loop" && $retStr !~ "recursion unwinding assertion" && $retStr2 =~ "VERIFICATION FAILED")
		{
			print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
			print "\nCBMC:VeriAbs Known Bound:$failed_str\n";
			&callCPACVoilation();
			#if( &isNonEmptyFile($witness)) {
			#	&addToCBMCWitnessFile();
			#	print "\nWitness : $witness.\n";
			#}
			exit(0); #done
		}
=end comment4
=cut
		    }
	    }

	    sub cbmcWithLAUF()
	    {
		    my $unwindCount=16;
		    print "\nlauf $z3opt - $smt:";
		    for (my $i=1; $i <= 9; $i++) {

			    $unwindCount=(16 * (2**$i))-10;
			    print "$unwindCount.";

			    #my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe --unwinding-assertions --unwind $unwindCount --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
			    my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe ".$z3opt." --unwinding-assertions --unwind $unwindCount --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
			    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
			    #print "\nCBMC COMMAND:\n".$cbmcCommand;
			    `$cbmcCommand`;
			    #copy("$unparserOpDir/error-witness.graphml","$errorWitnessPath/error-witness_$projName.graphml"); #avriti added to copy error-witness to proper directory
			    #copy("$unparserOpDir/error-witness.graphml","$currentDir/error-witness_$projName.graphml");

			    $retStr = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
			    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
				    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION SUCCESSFUL";
				    my $validVS=&checkValidCBMCVS($unwindCount,"$cfileFromPrj"); # Running cbmc for checking if no recursion unwinding assertion.
				    if($validVS == 1)
				    {
					    print "\nVeriAbs LAUF Computed:$success_str\n";
					    if ($propertyfile)
					    {
						    &callCPAC();
					    }
					    exit(0);#done
				    }else { print "\nruaf.\n";}
			    }
			    else {
				    &checkForCBMCVF("$unparserOpDir/cbmcOnlyOp.txt");
				    #print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

				    $retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
				    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;

				    if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
				    {
					    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
					    &generateCBMCVFWitness($unwindCount); # Running cbmc for witness generation
					    print "\nVeriAbs Bound Computed:$failed_str\n";
					    if( &isNonEmptyFile($witness)) {
						    if ($witnessGeneratedByCBMC == 1) {
							    &addToCBMCWitnessFile();
							    print "\nWitness : $witness.\n";
						    }
					    }else {
						    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
						    &generateDummyWitness($dummyfile);
					    }
					    exit(0); #done
					    #&callCPACVoilation();
				    }
=begin comment5
			$retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
			my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
			if ($retStr !~ "unwinding assertion loop" && $retStr !~ "recursion unwinding assertion" && $retStr2 =~ "VERIFICATION FAILED")
			{
				print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
				print "\nCBMC:VeriAbs LAUF Bound Computed:$failed_str\n";
				&callCPACVoilation();
				#if( &isNonEmptyFile($witness)) {
				#	&addToCBMCWitnessFile();
				#	print "\nWitness : $witness.\n";
				#}
				exit(0); #done
			}
=end comment5
=cut

			    }


		    }
	    }

	    sub  cbmcOOMWithBinarySearch()
	    {

		    my ($bmcOOMtimeout) = @_;
		    my $low=0;
		    my $high=22;
		    $z3opt = '--z3';
                    my $isCbmcWithZ3 = 1;
		    if($bmcRunOnOrigFile == 1)
		    {
			    $high=18; 
		    }
		    print "\nOOMBS $z3opt - $smt:";
		    while( $low <= $high ) {
			    $mid = int(( $low + $high ) / 2);
			    if ($mid == 0){last;}
			    print "$mid.";
			    #my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe --unwinding-assertions --unwind $mid --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
			    my $cbmcCommand = "timeout -k 2s ".$bmcOOMtimeout."s $cbmcExe ".$z3opt." --unwinding-assertions --unwind $mid --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
			    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
			    #print "\nCBMC COMMAND:\n".$cbmcCommand;
			    if($isCbmcWithZ3 == 1){
				`$cbmcCommand`;
			    }
			    my $cbmcOnlyErrFile="$unparserOpDir/cbmcOnly.err";
			    if(-z $cbmcOnlyErrFile){
			    }
			    else{
				    $isCbmcWithZ3=0;
				    print "\nerror in cbmc only with z3...\n";
				    my $cbmcCommand = "timeout -k 2s ".$bmcOOMtimeout."s $cbmcExe --unwinding-assertions --unwind $mid --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
				    `$cbmcCommand`;
			    }
			    #copy("$unparserOpDir/error-witness.graphml","$errorWitnessPath/error-witness_$projName.graphml"); #avriti added to copy error-witness to proper directory
			    #copy("$unparserOpDir/error-witness.graphml","$currentDir/error-witness_$projName.graphml");

			    $retStr = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
			    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
				    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION SUCCESSFUL";
				    my $validVS=&checkValidCBMCVS($mid,"$cfileFromPrj"); # Running cbmc for checking if no recursion unwinding assertion.
				    if($validVS == 1)
				    {
					    print "\nVeriAbs BS BOUND:$success_str\n";
					    if ($propertyfile)
					    {
						    &callCPAC();
					    }
					    exit(0);#done
				    }else { print "\nruaf.\n"; $low = $mid + 1;}
			    }
			    else {
				    &checkForCBMCVF("$unparserOpDir/cbmcOnlyOp.txt");
				    #print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

				    $retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
				    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;

				    if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
				    {
					    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
					    &generateCBMCVFWitness($mid); # Running cbmc for witness generation
					    print "\nVeriAbs BS BOUND:$failed_str\n";
					    if( &isNonEmptyFile($witness)) {
						    if ($witnessGeneratedByCBMC == 1) {
							    &addToCBMCWitnessFile();
							    print "\nWitness : $witness.\n";
						    }
					    }else {
						    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
						    &generateDummyWitness($dummyfile);
					    }
					    exit(0); #done
					    #&callCPACVoilation();
				    }
=begin comment6
		$retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
		my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
		if ($retStr !~ "unwinding assertion loop" && $retStr !~ "recursion unwinding assertion" && $retStr2 =~ "VERIFICATION FAILED")
		{
			print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
			print "\nCBMC:VeriAbs OOM BS BOUND:$failed_str\n";
			&callCPACVoilation();
			#if( &isNonEmptyFile($witness)) {
			#	&addToCBMCWitnessFile();
			#	print "\nWitness : $witness.\n";
			#}
			exit(0); #done
		}
=end comment6
=cut

				    if( $retStr =~ "unwinding assertion loop" ) {
					    print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
					    print "\nCBMC:VeriAbs OOM BS BOUND:LOOP UNWINDING ASSERTION FAILURE\n";
					    $low = $mid + 1;
				    }
				    elsif( $retStr =~ "recursion unwinding assertion" ) {
					    print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
					    print "\nCBMC:VeriAbs OOM BS BOUND:RECURSION UNWINDING ASSERTION FAILURE\n";
					    $low = $mid + 1;
				    }
				    else {
					    print LOGFILE "\n$unparserOpDir:CBMC:UNKNOWN, timeout/error?"; 
					    print "\nCBMC:VeriAbs OOM BS BOUND:TOERR\n";
					    $high = $mid - 1;
				    }
			    }

		    }
	    }



	    sub gccPreprocessing_eca()
	    {
		    #avriti added 26 oct 2016 : Adding gcc preprocessing command
		    #my $gccPreprocessingCmd="gcc -E $unparserOpDir/*.c > $unparserOpDir/test.c";
		    #gcc take files from *.c in alphabetic order that can lead to missing of few file information.
		    my $gccPreprocessingCmd="gcc -m32 -D __LAB_BASE_ -E $unparserOpDir/assertFlags.c $unparserOpDir/kind.c $unparserOpDir/$projName.c > $unparserOpDir/test_base.c";
		    print LOGFILE "\nGcc Preprocessing Command:\n".$gccPreprocessingCmd;
		    print "\nGcc preprocessing started";
		    my $returnCode = system( $gccPreprocessingCmd );
		    if ( $returnCode != 0 ) 
		    { 
			    if (! -e "$unparserOpDir/assertFlags.c") { print "VeriAbs error: Unparsed file $unparserOpDir/assertFlags.c does not exist!"; }
			    if (! -e "$unparserOpDir/kind.c") { print "VeriAbs error: Unparsed file $unparserOpDir/kind.c does not exist!"; }
			    if (! -e "$unparserOpDir/$projName.c") { print "VeriAbs error: Unparsed file $unparserOpDir/$projName.c does not exist!"; }
			    die "Error in Gcc Preprocessing Command execution. [$gccPreprocessingCmd]\n"; 
		    }
		    $gccPreprocessingCmd="gcc -m32 -E $unparserOpDir/assertFlags.c $unparserOpDir/kind.c $unparserOpDir/$projName.c > $unparserOpDir/test_hyp.c";
		    $returnCode = system( $gccPreprocessingCmd );
		    if ( $returnCode != 0 ) 
		    { 
			    if (! -e "$unparserOpDir/assertFlags.c") { print "VeriAbs error: Unparsed file $unparserOpDir/assertFlags.c does not exist!"; }
			    if (! -e "$unparserOpDir/kind.c") { print "VeriAbs error: Unparsed file $unparserOpDir/kind.c does not exist!"; }
			    if (! -e "$unparserOpDir/$projName.c") { print "VeriAbs error: Unparsed file $unparserOpDir/$projName.c does not exist!"; }
			    die "Error in Gcc Preprocessing Command execution. [$gccPreprocessingCmd]\n"; 
		    }

		    #avriti end
	    }
	    sub gccPreprocessing()
	    {
		    #avriti added 26 oct 2016 : Adding gcc preprocessing command
		    #my $gccPreprocessingCmd="gcc -E $unparserOpDir/*.c > $unparserOpDir/test.c";
		    #gcc take files from *.c in alphabetic order that can lead to missing of few file information.
		    my $gccPreprocessingCmd="gcc -m32 -E $unparserOpDir/assertFlags.c $unparserOpDir/kind.c $unparserOpDir/$projName.c > $unparserOpDir/test.c";
		    print LOGFILE "\nGcc Preprocessing Command:\n".$gccPreprocessingCmd;
		    print "\nGcc preprocessing started"; 
		    my $returnCode = system( $gccPreprocessingCmd );
		    if ( $returnCode != 0 ) 
		    { 
			    if (! -e "$unparserOpDir/assertFlags.c") { print "VeriAbs error: Unparsed file $unparserOpDir/assertFlags.c does not exist!"; }
			    if (! -e "$unparserOpDir/kind.c") { print "VeriAbs error: Unparsed file $unparserOpDir/kind.c does not exist!"; }
			    if (! -e "$unparserOpDir/$projName.c") { print "VeriAbs error: Unparsed file $unparserOpDir/$projName.c does not exist!"; }
			    die "Error in Gcc Preprocessing Command execution. [$gccPreprocessingCmd]\n"; 
		    }
		    #avriti end
	    }

	    sub pvAnalysis()
	    {
		    	my ($pathAnalTimeOut)= @_;
			print "\nAVP\n";
			print PRJLOGFILE "\nAVP analysis started..\n";
		     	print PRJLOGFILE "\ntimeout -k 2s  ".$pathAnalTimeOut."s java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\"  --pathwise-ra\n\n";
		    	$retCode = system("timeout -k 2s  ".$pathAnalTimeOut."s java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/Labtec.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\"  --pathwise-ra");
			#system("cat  $prismLogPath");
		    	$isPathWise=1;
		    	&checkLabtecReturnCodes($retCode>>8,$isPathWise);

	    }

	    sub hoistAssertAndAbstract()
	    {
#check with -hoist-assert
		    print "\nPull-up..";
		    move($cfileFromPrj,"$cfileFromPrj.bak");
		    print PRJLOGFILE "\njava -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\"  --hoist-assert\n\n";
		    $retCode = system("java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/Labtec.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\" --hoist-assert");

=begin comment
	if ($retCode>>8 == 10) {
		print "\nThe input program is safe.\n";
		print "\n$success_str\n";
		#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
		if ($propertyfile)
		{
			&callCPAC();
		}
		#avriti end
		#exit (10);
		exit(0);
	}
	if (($? != 0)) #removed 17 dec, if errors in running system- hoistAssert.txt will not be created,but resume with other processing..kind called.
	{
		print "\n***Errors encountered in Abstraction(hoist-assert).\nPlease refer to $completeErrFile\n";
		&closeNexit(1);
	}
=end comment
=cut
		    #print "\n***Loop Abstraction Information extraction complete with pull-up.\n";
		    print PRJLOGFILE "\n***Loop Abstraction Information extraction complete with assert pull-up.\n";
		    #print "\n\nPrinting logs\n";
		    #system("cat  $logsDir/*log") == 0
		    # or print "Couldn't launch [cat $logsDir/*log]: $! / $?";
		    #print "\n\nPrinting errors\n";
		    #system("cat  $logsDir/*err") == 0
		    # or print "Couldn't launch [cat $logsDir/*err]: $! / $?";

		    if (-e "$labtecOutputDir/hoistAssert.txt") {	#if Asserts are hoisted then only call Abstraction followed by cbmc

			    &cleanup("$projDir");
			    unlink ($completeLogFile);
			    unlink ($completeErrFile);
			    &openfile($vdgraphLogFile,$vdgraphErrFile);
			    &reopenfile($completeLogFile,$completeErrFile);

			    print CMPLOGFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";
			    print CMPERRFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";

			    &modifyPrjFile($prjFilePath);
			    &extractLoopAbstractionInfo($newPrjFilePath);

			    &gccPreprocessing();
			    print "\nRunning PU...\n";
			    $cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe --unwind 20 --unwinding-assertions --32 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcop_hoistassert.txt 2>$unparserOpDir/cbmcop_hoistassert.err";	#avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
			    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
			    `$cbmcCommand`;
			    $retStr = `tail -n 1 $unparserOpDir/cbmcop_hoistassert.txt`;
			    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
				    print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION SUCCESSFUL";
				    #print "Property file: $propertyfile";
				    my $validVS=&checkValidCBMCVS(20,"$unparserOpDir/test.c"); # Running cbmc for checking if no recursion unwinding assertion.
				    if($validVS == 1)
				    {

					    print "\nVeriAbs:PU:$success_str\n";
					    if ($propertyfile)
					    {
						    &callCPAC();
					    }
					    exit(0);
				    }else { print "\nruaf.\n";}
			    }
		    }
		    #end hoist-assert
	    }

	    sub abstractWithoutInduction()
	    {
		    &cleanup("$projDir");
		    unlink ($completeLogFile);
		    unlink ($completeErrFile);
		    &openfile($vdgraphLogFile,$vdgraphErrFile);
		    &reopenfile($completeLogFile,$completeErrFile);
		    print TIMINGSFILE "\n\n----------New LAI run----------\n";
		    print CMPLOGFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";
		    print CMPERRFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";

		    my $currLAI_st_Time = time();
		    #copy prj file to Input folder & insert "FILE:" into it
		    &modifyPrjFile($prjFilePath);
		    $cfileFromPrj = `sed "2q;d" $inputDir/$onlyPrjFile`;
		    chomp($cfileFromPrj);
		    &extractLoopAbstractionInfo($newPrjFilePath);
		    #exit(0);

		    #`touch $unparserOpDir/opabs.txt`;
		    if(-e "$unparserOpDir/opabs.txt") {
			    #handle_inf_while();
			    &handle_inf_while_new();
		    }
		    $output_abstraction = 0;
		    &gccPreprocessing(); 
		    print "\nRunning on unparsed file without induction..";
		    my $timeOutVal=$bmc3timeout;
		    if( -f "$unparserOpDir/AllLoopsArrAbs.txt")
		    {
			    my $readLine = `tail -n 1 $unparserOpDir/AllLoopsArrAbs.txt`;
			    if($readLine =~ "1")
			    {
				    $timeOutVal=400;
			    }
		    }

		    my $cbmcCommand;
		   my $success = 0;
		   $success=( system("grep \"malloc\" $cfileFromPrj >/dev/null") ) ? 0 : 1;
		   if ($success==0)
		   {
			$cbmcCommand = "timeout -k 2s ".$timeOutVal."s $cbmcExe --unwinding-assertions --unwind 20 --32 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcOnlyOp_noi.txt 2>$unparserOpDir/cbmcOnly_noi.err"; #avriti added graphML option
		   }else
		   {
			$cbmcCommand = "timeout -k 2s ".$timeOutVal."s $cbmcExe --unwinding-assertions --unwind 20 --32 --z3 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcOnlyOp_noi.txt 2>$unparserOpDir/cbmcOnly_noi.err"; #avriti added graphML option
		   }
		    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		    my $ret_val = system($cbmcCommand);
		    if($ret_val>>8 ==124){
			    print "\nTIMEOUT:CBMC KILLED : $ret_val && timeout val $timeOutVal s.\n";
		    }
		    else{
			    print "\nNO_TIMEOUT for cbmc call: $ret_val && timeout val $timeOutVal s.\n";
		    }
		    my $retStr2 = `tail -n 10 $unparserOpDir/cbmcOnlyOp_noi.txt`;
		    $retStr = `tail -n 1 $unparserOpDir/cbmcOnlyOp_noi.txt`;
		    my $verificationFailValid = 0;
		    if( -f "$unparserOpDir/failVerificationValid.txt")
		    {
			    my $readLine = `tail -n 1 $unparserOpDir/failVerificationValid.txt`;
			    if($readLine =~ "1")
			    {
				    $verificationFailValid=1;
			    }
		    }
		    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			    print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION SUCCESSFUL";
			    my $validVS=&checkValidCBMCVS(20,"$unparserOpDir/test.c"); # Running cbmc for checking if no recursion unwinding assertion.
			    if($validVS == 1)
			    {
				    #print "Property file: $propertyfile";
				    print "\nVeriAbs no-induction:$success_str\n";
				    if ($propertyfile && $witnessval eq "")
				    {
					    &callCPAC();
				    }
				    exit(0);
			    }else { print "\nruaf.\n";}

		    }
		    &checkForCBMCVF("$unparserOpDir/cbmcOnlyOp_noi.txt");
		    #print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

		    $retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp_noi.txt`;
		    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp_noi.txt`;

		    if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED" && $verificationFailValid == 1)
		    {
			    if ($witnessval eq "")
                            {
			    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
			    &generateCBMCVFWitness(20); # Running cbmc for witness generation : commented since generating witness on abstract program
			    print "\nVeriabs no-induction:$failed_str\n";
			    if( &isNonEmptyFile($witness)) {
				    if ($witnessGeneratedByCBMC == 1) {
					    &addToCBMCWitnessFile();
					    print "\nWitness : $witness.\n";
				    }
			    }else {
				    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
				    &generateDummyWitness($dummyfile);
			    }
		    	    }else {print "\n$valunknown_str\n"}
			    exit(0); #done
			    #&callCPACVoilation();
		    }
=begin comment8
	elsif ($retStr2 !~ "unwinding assertion loop" && $retStr2 !~ "recursion unwinding assertion" && $retStr =~ "VERIFICATION FAILED" && $verificationFailValid == 1)
	#elsif ($retStr =~ "VERIFICATION FAILED" && $verificationFailValid == 1 )
	{
		print "\n Veriabs no-induction:$failed_str\n";
		if ($propertyfile)
		{
			&callCPACVoilation();
		}
		exit(0);
	}
=end comment8
=cut
	    }

	    sub addToCBMCWitnessFile()
	    {
		    my $cmd;
		    my $path;
		    my $hash_sum;
		    my $time;
		    $path="$cfileFromPrj";
		    $cmd="sha1sum $path| awk '{print \$1}'";
		    #print "\nhash sum cmd : $cmd";
		    $hash_sum=`$cmd`;
		    chomp($hash_sum);

		    open (PROFILEH, "<$propertyfile") || die "$propertyfile File not found $!";; 
		    my $spec = <PROFILEH>;
		    close PROFILEH;
		    chomp ($spec);

		    print "\nwitness for $path $hash_sum\n";

		    $path =~ s/\//\\\//g;
		    $time=gmtime();
		    $cmd = "sed -i 's/<graph edgedefault=\\\"directed\\\">/<graph edgedefault=\\\"directed\\\"> <data key=\\\"witness-type\\\">violation_witness<\\/data> <data key=\\\"producer\"> VeriAbs 1.3 <\\/data> <data key=\\\"specification\\\">$spec<\\/data>  <data key=\\\"programfile\\\">".$path."<\\/data>  <data key=\\\"programhash\\\">$hash_sum<\\/data> <data key=\\\"architecture\\\">32bit<\\/data><data key=\\\"creationtime\\\">$time<\\/data>/g' $witness";

		    #print "\nPrinting [$cmd]\n";
		    system($cmd) == 0
			    or die "Couldn't launch [$cmd]: $! / $?";
	    }

	    sub callCPACVoilation()
	    {
		    print "\nCPAC for violation witness generation\n";
		    my $presentWorkDir=getcwd;
		    chdir $cpacPath;
		    if (! -e "$cpacPath/scripts/cpa.sh") { print "VeriAbs error: cpa.sh does not exist! $cpacPath/scripts/cpa.sh"; }
		    if ( -e "$witness") { unlink($witness);}
		    #Sample CPAC cmd
		    #print "$presentWorkDir\nProperty file: $propertyfile\n$cpacPath";
		    print "\nCPA dir $cpacPath";
		    `chmod 777 scripts/cpa.sh`;
		    my $cpacWitnessGenCmd;

		    $cpacWitnessGenCmd="scripts/cpa.sh -noout -heap 10000M -predicateAnalysis -setprop cpa.composite.aggregateBasicBlocks=false -setprop cfa.simplifyCfa=false -setprop cfa.allowBranchSwapping=false -setprop cpa.predicate.ignoreIrrelevantVariables=false -setprop counterexample.export.assumptions.assumeLinearArithmetics=true -setprop counterexample.export.assumptions.includeConstantsForPointers=false -setprop counterexample.export.graphml=violation-witness.graphml -setprop counterexample.export.compressErrorWitness=false -spec $propertyfile $cfileFromPrj >$logsDir/cpaLog.txt 2>&1";

		    open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
		    print LOGFILE "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
		    close(LOGFILE);

		    my $cpacwitness="output/violation-witness.graphml";
		    my $cpacwitnessgz="output/violation-witness.graphml.gz"; ## commented code related to gunzip, since cpachecker1.6.1 trunk1 dumps witness as a gz file
		    if ( -e $cpacwitness) { 
			    print "\nRemoving already existing witness $cpacwitness";
			    unlink $cpacwitness;
		    }
		    if ( -e $cpacwitnessgz) { 
			    print "\nRemoving already existing witnessg $cpacwitnessgz";
			    unlink $cpacwitnessgz;
		    }
		    #if($no_induction == 1)
		    #{
		    $cpacWitnessGenCmd="timeout -k 2s 300s ".$cpacWitnessGenCmd;
		    #}
		    $end_before_cpac_time=time();
		    my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
		    print "\nTotal time taken before witness generation: $diff_before_cpac_time\n";
		    print "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
		    my $start = time();
		    my $returnCode = system( $cpacWitnessGenCmd );
		    my $end = time();
		    my $diff_cpac_time=$end - $start;
		    print "\nCPA time taken: $diff_cpac_time\n";
		    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
		    if ( $returnCode != 0 ) 
		    { 
			    print "Error in CPA-Checker Violation Witness generation. [$cpacWitnessGenCmd]\n"; 
			    &generateDummyWitness($dummyfile);	
			    exit(0);
		    }

		    if ( -e $cpacwitnessgz) { 
			    #unzip graphml	
			    my $gunzipCmd="gunzip $cpacwitnessgz";
			    print "\nunzip graphml ";#=> $gunzipCmd";
			    system($gunzipCmd) == 0
				    or die "\nCouldn't launch gunzip [$gunzipCmd]: $! / $?";
			    #copy("output/correctness-witness.graphml","$correctnessWitnessPath/correctness-witness_$projName.graphml");
			    #copy("output/correctness-witness.graphml","$currentDir/witness.graphml");

		    }
		    if ( -e $cpacwitness) { 
			    copy("$cpacwitness","$witness");
			    if( &isNonEmptyFile($witness)) {
				    #copy("$witness","$errorWitnessPath/error-witness_$projName.graphml");
				    print "\nWitness : $witness.\n";
			    }else {
				    &generateDummyWitness($dummyfile);
			    }
		    }
		    chdir $presentWorkDir;
		    exit(0);
	    }

	    sub checkForCBMCVF()
	    {

		    my ($cbmcOPfile)= @_;
		    my $cbmcResults= `sed -e '1,/Results/d' -e '/^\s*\$/,\$d'  $cbmcOPfile`;
		    #print "\n***$cbmcResults****\n";

		    chomp $cbmcResults; 
		    my @cbmcResultLines = split (/\n/,$cbmcResults); # get status of each assert in an array
		    #print "\n*$cbmcResultLines[0]\n*$cbmcResultLines[1]\n";
		    $allAssertionFAILED=0; 
		    # if any entry other than "assertion.* : FAILURE" is found => all assertions are not FAILURE.
		    # SKIP [main.unwind.0] unwinding assertion loop 0,FAILURE/SUCCESS results for VF.
		    foreach $line (@cbmcResultLines)
		    {
			    #$line =~ s/\s//g; # remove space spaces
			    # get property and result
			    my @cbmcPropStatus = split (/:/,$line);
			    #print "\n#$cbmcPropStatus[0]#$cbmcPropStatus[1]#";
			    if($cbmcPropStatus[0] =~ m/assertion\.[0-9]+/ && $cbmcPropStatus[1]=~ m/FAILURE/)
			    {
				    $allAssertionFAILED=1; # if any assertion fails, then program fails.
				    last;
			    }
		    }

	    }

	    sub handle_inf_while() 
	    {
		    print "\n\nInfinite while loop detected.\n";
		    $bmc1timeout=180;
		    $bmc2timeout=180;
		    if ( $SVCOMP16 == 1 ) {
			    my $currLAI_st_Time = time();
			    open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");

			    &gccPreprocessing_eca();
			    $cfileFromPrj = `sed "2q;d" $inputDir/$onlyPrjFile`;
			    chomp($cfileFromPrj);
			    $cFile=~/(.*)\/(.*)\.c/;
			    #my $onlyCFile="$2.c";
			    my $onlyCFile=basename($cfileFromPrj, ".c");
			    #print "\n\ninf_while onlyCFile=$onlyCFile\n";

			    my $retStr = "";
			    my $cbmcCommand = "";
			    my $unwindStr = "--unwind 20";
			    if($output_abstraction == 1) {
				    $unwindStr = "";
				    #$smt="--mathsat";
				    #$smt="--z3";
				    $smt="";
			    }

			    #PRIYANKA: FOR LOOP UNWINDING
			    #print "\nFiring mkloopbodies.pl $unparserOpDir\n";
			    #`perl $LABTECROOT/scripts/mkloopbodies.pl $unparserOpDir`;
			    #print "\nFiring run.pl $unparserOpDir $onlyCFile\n";
			    #`perl $LABTECROOT/scripts/run.pl $unparserOpDir $onlyCFile> $unparserOpDir/iter.txt 2>$unparserOpDir/iter.err`;
			    #$retStr = `tail -n 5 $unparserOpDir/iter.txt`;
			    #if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			    #print "\nVeriAbs:VeriAbs iterative:$success_str\n";
			    #exit(0);
			    #}
			    #if( $retStr =~ "VERIABS_VERIFICATION_FAILED")
			    #{
			    #print "\nVeriAbs:VeriAbs iterative:$failed_str\n";
			    #exit(0);
			    #}
			    #if( $retStr =~ "$unknown_str";
			    #print "\nVeriAbs:VeriAbs iterative:$success_str\n";
			    #exit(0);
			    #}

			    #start without --arrays-uf-always
			    print "\nRunning on unparsed file with induction for base case. $smt ";
			    $cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe $unwindStr --unwinding-assertions --32 ".$smt." $unparserOpDir/test_base.c -D __LAB_BASE_ --graphml-witness $witness > $unparserOpDir/cbmcop.txt 2>$unparserOpDir/cbmc.err";
			    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
			    `$cbmcCommand`;
			    $retStr = `tail -n 1 $unparserOpDir/cbmcop.txt`;

			    $currLAI_end_Time = time();
			    $totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
			    print TIMINGSFILE "\nTotal time needed for CBMC =$kindCount,$totalTimeForLAI\n";
			    print TIMINGSFILE "\n-------------------------------\n";

			    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {

				    my $check_false_once = 0;
				    if ($KIND == 1) {
					    # continue with K-Induction if not VF/VS with above unwind counts
					    print "\nVerification check KI. $smt\n";
					    my $kindCount = 1;
					    while ( $kindCount <= $KIND_MAX_K) { 

						    my $cbmcopfile = "$unparserOpDir/cbmcop_$kindCount.txt";
						    $bmc1timeout = $bmc1timeout + 150*$kindCount;

						    $cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe $unwindStr --unwinding-assertions --32 ".$smt." $unparserOpDir/test_hyp.c >$cbmcopfile  2>$unparserOpDir/cbmc_$kindCount.err";	#avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
						    `$cbmcCommand`;
						    my $ret_val = 0;
						    if($ret_val>>8 ==124){
							    print "\nTIMEOUT:CBMC KILLED : $ret_val && timeout val $bmc1timeout s.\n";
						    }
						    else{
							    print "\nNO_TIMEOUT for cbmc call: $ret_val && timeout val $bmc1timeout s.\n";
						    }
						    $retStr = `tail -n 1 $cbmcopfile`;

						    $currLAI_end_Time = time();
						    $totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
						    print TIMINGSFILE "\nTotal time needed for CBMC =$kindCount,$totalTimeForLAI\n";
						    print TIMINGSFILE "\n-------------------------------\n";

						    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
							    print "\nVeriAbs:kin:$success_str\n";
							    print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION SUCCESSFUL for k-induction step.";
							    if ($propertyfile)
							    {
								    my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
								    &generateDummyWitness($dummyfile);
							    }
							    last;
						    }
						    # k-induction with LABMC will give LUAF or RUAF when a loop is not abstracted
						    $retStr = `tail -n 10 $cbmcopfile`;
						    if( $retStr =~ "unwinding assertion loop" ) {
							    print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
							    print "\nVeriAbs:KN:LOOP UNWINDING ASSERTION FAILURE\n";
							    $kindCount = $KIND_MAX_K;	#break from the loop. LUAF due to loops not abstrated
						    }
						    elsif( $retStr =~ "recursion unwinding assertion" ) {
							    print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
							    print "\nVeriAbs:KN:RECURSION UNWINDING ASSERTION FAILURE\n"; #avriti 
							    $kindCount = $KIND_MAX_K;	#break from the loop. RUAF due to recursion
						    }
						    elsif( $retStr =~ "VERIFICATION FAILED" ) {
							    if($check_false_once != 1) {
								    $check_false_once = 1;
								    &runAFL(150) ;
								    #&runVeriFuzz(); # Avriti: 26 Sep 2019
								    &cbmcWithLAUF_small(6-1);
								    &cbmcWithLAUF_small(6*2-1);
								    &cbmcWithLAUF_small(6*3-1);
							    }
						    }
						    else {
							    my $retStr2 = `tail -n 1 $cbmcopfile`;
							    if( $retStr2 !~ "VERIFICATION FAILED" ) {	#timeout / error
								    $kindCount = $KIND_MAX_K;
								    #CHECK ORIG INPUT FILE WITH CBMC
								    print LOGFILE "\n$unparserOpDir:VeriAbs:KN:UNKNOWN, timeout/error?"; 
								    print "\n$unknown_str\n";
								    print "\nVeriAbs:KN:TOERR";
							    }
						    }
						    $kindCount+=2;
						    my $kindFile = "$unparserOpDir/kind.c";
						    open (KINDFILE, ">$kindFile") || die ("Cannout open $unparserOpDir/kind.c");
						    print KINDFILE "\n\nunsigned const int _LAB_KINDUCT=$kindCount;";
						    print LOGFILE "wrote unsigned const int _LAB_KINDUCT=$kindCount; to $unparserOpDir/kind.c\n\n";
						    close KINDFILE;
						    `sed -i "s/unsigned const int _LAB_KINDUCT.*/unsigned const int _LAB_KINDUCT=$kindCount;/g" $unparserOpDir/test_hyp.c`;
						    if ( $kindCount > $KIND_MAX_K) { 
							    print "\n$unknown_str\n";
							    unlink "$witness";
						    }

					    }
				    }


			    }
			    else { #if VERIFICATION FAILED FOR USER ASSERTION.
				    &checkForCBMCVF("$unparserOpDir/cbmcop.txt");
				    #print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

				    $retStr = `tail -n 10 $unparserOpDir/cbmcop.txt`;
				    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcop.txt`;
				    if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
				    {
					    if ($witnessval eq "")
                                            {
					    print LOGFILE "Fails in base case\n";
					    print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION FAILED";
					    #print "\nWitness generation using CPAC.\n";
					    &generateCBMCVFWitness($unwindStr); # Running cbmc for witness generation
					    print "\nVeriAbs:$failed_str\n";
					    if( &isNonEmptyFile($witness)) {
						    if ($witnessGeneratedByCBMC == 1) {
							    &addToCBMCWitnessFile();
							    print "\nWitness : $witness.\n";
						    }
					    }else {
						    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
						    &generateDummyWitness($dummyfile);
					    }
				    	    }else {print "\n$valunknown_str\n"}
					    exit(0); #done
					    #&callCPACVoilation();
				    }

			    }
		    } # end if(SVCOMP ==1)	
		    exit(0);
	    }

	    sub handle_inf_while_new()
	    {
		    #Tanha: InfLoop program
		    print("\nInfLoop Program");
		    system("$LABTECROOT/scripts/successCheck.sh Unknown");
		    print "InfLoop: Current process ID is: $$\n";
		    my $frwdPID;
		    my $backPID;
		    print "\nInfLoop ESM: Starting...";
		    $InfLoopPreprocessCmd="$LABTECROOT/scripts/esmc.sh $projName";
		    $retCode=system($InfLoopPreprocessCmd);
		    if($retCode>>8 == 10){
			    print "\nInfLoop: ESM: Error in 1st step...Exiting";
		    }
		    elsif($retCode>>8 == 2){
			    print "\nInfLoop: ESM: VERIABS_VERIFICATION_SUCCESSFUL";
			    if ($propertyfile && $witnessval eq "")
			    {
				    my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
				    &generateDummyWitness($dummyfile);
				    #&callCPAC_true();
			    }
			    exit(0);
		    }
		    elsif($retCode>>8 == 3){
			    if ($witnessval eq "")
			    {
			    print "\nInfLoop: ESM: VERIABS_VERIFICATION_FAILED";
		    	    }
			    exit(0);
		    }
		    elsif($retCode>>8 == 4){
			    print "\nInfLoop: ESM: Not solved\n";
		    }	  
		    if ($witnessval ne "") {print "Returning from FwBw."; exit(0);}
		    my $retCode=system("$LABTECROOT/scripts/commonFrwdBackward.sh $projName");
		    if($retCode>>8 != 10){
			    $frwdPID = fork;
			    if(not defined $frwdPID){
				    warn 'InfLoop: Forward approach: Could not fork';
			    }
			    if($frwdPID == 0){
				    print "InfLoop Frwd: Forked forward approach PID: $$\n";
				    print "\nInfLoop Frwd: Preprocessing before AFL\n";
				    $InfLoopPreprocessCmd="$LABTECROOT/scripts/frwdPreprocess.sh $projName";
				    system($InfLoopPreprocessCmd);
				    my $tempcfileFromPrj=$cfileFromPrj;
				    $cfileFromPrj="$ENV{'LABMCPROJDIR'}/InfTrace/Preprocess_Unparse_$projName.c";
				    print "InfLoop Frwd: Running AFL on $cfileFromPrj";
				    print "InfLoop Frwd: AFL started";
				    &runAFL_InfLoop(300);
				    #&runVeriFuzz_InfLoop(); #: Avriti 26 Sep 2019
				    print "InfLoop Frwd: AFL completed";
				    $cfileFromPrj=$tempcfileFromPrj;
				    print "\nInfLoop Frwd: Postprocessing after AFL\n";
				    $InfLoopPostProcessCmd="$LABTECROOT/scripts/postprocess.sh $projName $start_veriabs_time";
				    my $retCode=system($InfLoopPostProcessCmd);
				    if($retCode>>8 == 10)
				    {
					    print "\nInfLoop Frwd: Return code: 10 from VeriAbs";
					    system("$LABTECROOT/scripts/successCheck.sh Unknown");
				    }
				    else{
					    if ($propertyfile)
					    {
						    my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
						    &generateDummyWitness($dummyfile);
					    }
					    system("$LABTECROOT/scripts/successCheck.sh Success");
				    }
				    exit;
			    }
			    else{
				    $backPID = fork;
				    if(not defined $backPID){
					    warn 'InfLoop: Backward approach: Could not fork';
				    }
				    if($backPID == 0){
					    print "InfLoop Backward: Forked backtrack approach PID: $$\n";
					    print "InfLoop Backward: Starting $LABTECROOT/scripts/testScript.sh";
					    $InfLoopbacktrackCmd="$LABTECROOT/scripts/testScript.sh $projName $start_veriabs_time";
					    my $retCode = system($InfLoopbacktrackCmd);
					    if($retCode>>8 == 8)
					    {
						    print "\nInfLoop Backward: Return code: 8 from backward approach, inifinite iterations";
						    system("$LABTECROOT/scripts/successCheck.sh Unknown");
					    }

					    elsif($retCode>>8 == 10)
					    {
						    print "\nInfLoop Backward: Return code: 10 from backward approach, base case failed";
						    system("$LABTECROOT/scripts/successCheck.sh Unknown");
					    }
					    else{
						    if ($propertyfile)
						    {
							    my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
							    &generateDummyWitness($dummyfile);
						    }
						    system("$LABTECROOT/scripts/successCheck.sh Success");
					    }
					    exit;
				    }
			    }
		    }
		    else{
			    system("$LABTECROOT/scripts/successCheck.sh Unknown");
			    print "\nInfLoop: Common: VERIABS_UNKNOWN\n";
			    exit(0);
		    }
		    my $PID = wait();
		    print "\nInfLoop: $PID exiting\n";
		    my $retCode = system("$LABTECROOT/scripts/failureCheck.sh");
		    if($retCode>>8 == 0) #Kill other child
		    {
			    if($PID == $frwdPID){
				    system("kill -9 $backPID");
			    }
			    elsif($PID == $backPID){
				    system("kill -9 $frwdPID");
			    }
		    }
		    elsif($retCode>>8 == 10){
			    print "\nInfLoop: wait for other approach";
			    my $nextPID = wait();
			    print "\nInfLoop next: $nextPID exiting\n";	

		    }

		    exit(0);

		    #Tanha: end
	    }
	    sub cbmcWithLAUF_small()
	    {
		    my ($unwindCount) = @_;
		    print "\n$unwindCount lauf $smt\n";

		    if ( -e "$witness") { unlink($witness);}

		    #my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe --unwind $unwindCount --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp$unwindCount.txt 2>$unparserOpDir/cbmcOnly$unwindCount.err";
		    my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe --unwind $unwindCount --32 ".$smt." $cfileFromPrj --graphml-witness $witness > $unparserOpDir/cbmcOnlyOp$unwindCount.txt 2>$unparserOpDir/cbmcOnly$unwindCount.err";
		    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		    #print "\nCBMC COMMAND:\n".$cbmcCommand;
		    `$cbmcCommand`;
		    #copy("$unparserOpDir/error-witness.graphml","$errorWitnessPath/error-witness_$projName.graphml"); #avriti added to copy error-witness to proper directory
		    #copy("$unparserOpDir/error-witness.graphml","$currentDir/error-witness_$projName.graphml");

		    my $retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp$unwindCount.txt`;
		    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp$unwindCount.txt`;
		    if ($retStr !~ "unwinding assertion loop" && $retStr !~ "recursion unwinding assertion" && $retStr2 =~ "VERIFICATION FAILED")
		    {
			    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
			    print "\nVeriAbs:KB:$failed_str\n";
			    #&callCPACVoilation();
			    #copy("$unparserOpDir/$witness","$currentDir/$witness") #commented since graphml-witnes option dumps at $witness;
			    #if( &isNonEmptyFile($witness)) {
			    #&addToCBMCWitnessFile();
			    #	print "\nWitness : $witness.\n";
			    #}
			    if( &isNonEmptyFile($witness)) {
				    #copy("$witness","$errorWitnessPath/error-witness_$projName.graphml");
				    if ($witnessGeneratedByCBMC == 1) {
					    &addToCBMCWitnessFile();
					    print "\nWitness : $witness.\n";
				    }
			    }else {
				    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
				    &generateDummyWitness($dummyfile);
			    }
			    exit(0); #done
		    }
	    }

	    sub runAFL()
	    {
		    if ($witnessval ne "") {print "Return AFL."; return;}
		    #my $AFL_Timeout=150 ;
		    my ($AFL_Timeout) = @_;
		    my $AFLOutputDir=$labtecOutputDir;
		    my $AFLCommand = "";

		    if($bmcRunOnOrigFile == 1)
		    {
			    $AFLCommand = "time ( perl $LABTECROOT/scripts/exeFuzzer.pl $cfileFromPrj $AFL_Timeout $AFLOutputDir gcc crashon ) 2> $AFLOutputDir/AFL_timeTaken.out";
		    }
		    else
		    {
			    $AFLCommand = "time ( perl $LABTECROOT/scripts/exeFuzzer.pl $cfileFromPrj $AFL_Timeout $AFLOutputDir ) 2> $AFLOutputDir/AFL_timeTaken.out";
		    }
		    print "\nAFL Run COMMAND:\n".$AFLCommand;
		    #`$AFLCommand` ;
		    system("/bin/bash", "-c", $AFLCommand) ;
		    print LOGFILE "\nAFL Run COMMAND:\n".$AFLCommand;

		    my $AFLOutputLogFile="$AFLOutputDir/exp-log/run.log";
		    my $AFLLogFile="$AFLOutputDir/exp-log/run.log" ;
		    if (! -e "$AFLLogFile") {
			    print "\nAFL_RUN LOG FILE DOES NOT EXIST.";
			    print "\nAFL_UNKNOWN\n";
		    }

		    my $retStr2 = `tail -n 1 $AFLLogFile`;
		    if ($retStr2 =~ "FALSE TRUE")
		    {
			    print LOGFILE "\nAFL:VERIFICATION FAILED";
			    print "\nAFL:$failed_str\n";
			    exit(0); #done
		    }
		    elsif ($retStr2 =~ "FALSE UNKNOWN")
		    {
			    print "\nAFL_UNKNOWN\n";
		    }
	    }

	    sub runAFL_InfLoop()
	    {
		    #my $AFL_Timeout=150 ;
		    my ($AFL_Timeout) = @_;
		    my $AFLOutputDir=$labtecOutputDir;
		    my $AFLCommand = "";

		    if($bmcRunOnOrigFile == 1)
		    {
			    $AFLCommand = "time ( perl $LABTECROOT/scripts/exeFuzzer_verifuzz.pl $cfileFromPrj $AFL_Timeout $AFLOutputDir gcc crashon ) 2> $AFLOutputDir/AFL_timeTaken.out";
		    }
		    else
		    {
			    $AFLCommand = "time ( perl $LABTECROOT/scripts/exeFuzzer_verifuzz.pl $cfileFromPrj $AFL_Timeout $AFLOutputDir ) 2> $AFLOutputDir/AFL_timeTaken.out";
		    }
		    print "\nAFL Run COMMAND:\n".$AFLCommand;
		    #`$AFLCommand` ;
		    system("/bin/bash", "-c", $AFLCommand) ;
		    print LOGFILE "\nAFL Run COMMAND:\n".$AFLCommand;

		    my $AFLOutputLogFile="$AFLOutputDir/exp-log/run.log";
		    my $AFLLogFile="$AFLOutputDir/exp-log/run.log" ;
		    if (! -e "$AFLLogFile") {
			    print "\nAFL_RUN LOG FILE DOES NOT EXIST.";
			    print "\nAFL_UNKNOWN\n";
		    }

		    my $retStr2 = `tail -n 1 $AFLLogFile`;
		    if ($retStr2 =~ "FALSE TRUE")
		    {
			    print LOGFILE "\nAFL:VERIFICATION FAILED";
			    print "\nAFL:$failed_str\n";
			    system("$LABTECROOT/scripts/successCheck.sh Success");
			    exit(0); #done
		    }
		    elsif ($retStr2 =~ "FALSE UNKNOWN")
		    {
			    print "\nAFL_UNKNOWN\n";
		    }
	    }
	    sub generateCPACFalseWitness()
	    {
		    my $logprefix = "\ngenerateCPACFalseWitness: "; print "\nCPAC False witness:\n";# return"; return;

		    my $startTime = time();
		    my $timeElapsed = $startTime  - $start_veriabs_time;

		    if ($timeElapsed + $cpacFalseWitnessTimeout  + $cbmcVFWitnessTimeOut >= $timeAvailable) {
			    print $logprefix."not running as $timeElapsed + $cpacFalseWitnessTimeout + $cbmcVFWitnessTimeOut >= $timeAvailable";
			    return;
		    }

		    print $logprefix."started";

		    my $presentWorkDir=getcwd;
		    #&selectCPACPath(); #commenting, since invariants not needed for false witness 
		    chdir $cpacPath;
		    print "\nUsing $cpacPath\n";
		    if (! -e "$cpacPath/scripts/cpa.sh") { 
			    print $logprefix."cpa.sh does not exist at $cpacPath/scripts/cpa.sh"; 
			    goto cleanup;
		    }


#if $witness already exists and non-empty then return    
#    if ( -e $witness && &isNonEmptyFile($witness)) {
#	if (&isNonEmptyFile($witness)) {
#	    print $logprefix."nonempty $witness already exists hence returning";
#	    goto cleanup;
#	} else {
#	    unlink($witness);
#	}
		    #   }

		    if ( -e $witness) {
			    unlink($witness);
		    }

		    `chmod 777 scripts/cpa.sh`;

		    my $cpacWitnessGenCmd;

		    $cpacWitnessGenCmd="scripts/cpa.sh -noout -heap 10000M -predicateAnalysis -setprop cpa.composite.aggregateBasicBlocks=false -setprop cfa.simplifyCfa=false -setprop cfa.allowBranchSwapping=false -setprop cpa.predicate.ignoreIrrelevantVariables=false -setprop counterexample.export.assumptions.assumeLinearArithmetics=true -setprop counterexample.export.assumptions.includeConstantsForPointers=false -setprop counterexample.export.graphml=violation-witness.graphml -setprop counterexample.export.compressErrorWitness=false -spec $propertyfile $cfileFromPrj >$logsDir/cpaLog.txt 2>&1";

		    if (! open (LOGFILE, ">>$vdgraphLogFile")) {
			    print $logprefix."failed to open $vdgraphLogFile";
			    goto cleanup;
		    }

		    print LOGFILE $logprefix."CPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
		    close(LOGFILE);

		    my $cpacwitness="output/violation-witness.graphml";
		    my $cpacwitnessgz="output/violation-witness.graphml.gz"; 

		    if ( -e $cpacwitness) { 
			    print $logprefix."Removing already existing witness $cpacwitness";
			    unlink $cpacwitness;
		    }

		    if ( -e $cpacwitnessgz) { 
			    print $logprefix."\nRemoving already existing witnessg $cpacwitnessgz";
			    unlink $cpacwitnessgz;
		    }

		    $cpacWitnessGenCmd="timeout -k 10s $cpacFalseWitnessTimeout ".$cpacWitnessGenCmd;

		    print $logprefix."CPAC Witness Generation Command:\n".$cpacWitnessGenCmd;

		    my $returnCode = system( $cpacWitnessGenCmd );

		    my $cpacTime = time() - $startTime;

		    print $logprefix."witness generation time taken: $cpacTime \n";

		    if ( $returnCode != 0 ) 
		    { 
			    print $logprefix."Error in CPA-Checker Violation Witness generation. [$cpacWitnessGenCmd] $returnCode\n";
			    goto cleanup;
		    }

		    if ( -e $cpacwitnessgz) { 
			    #unzip graphml
			    my $gunzipCmd="gunzip ".$cpacwitnessgz;
			    print $logprefix."executing $gunzipCmd";
			    if (system($gunzipCmd) != 0) {
				    print $logprefix."Couldn't launch gunzip [$gunzipCmd]: $! / $?";
				    goto cleanup;
			    }
		    }

		    if ( -e $cpacwitness && &isNonEmptyFile($cpacwitness)) { 
			    copy("$cpacwitness","$witness");
			    print $logprefix."Witness : $witness.\n";
		    } else {
			    if (! -e $cpacwitness) {
				    print $logprefix."$cpacwitness doesn't exist.\n";
			    } else {
				    print $logprefix."$cpacwitness empty.\n";
			    }
		    }

		    cleanup:
		    chdir $presentWorkDir;
		    return;
	    }


	    sub generateCBMCVFWitness()
	    { 
		    #experimentation with cpac witness start
		    #For BMC call on orig File, cbmc witness generation.
		    if(-e $witness)
		    {	unlink($witness); }
		    if($bmcRunOnOrigFile == 0)
		    {
			    &generateCPACFalseWitness();
			    if (-e $witness  && &isNonEmptyFile($witness)) {
				    print "\ngenerateCBMCVFWitness: Returning from generateCBMCVFWitness as $witness already exists\n";
				    $witnessGeneratedByCBMC = 0;
				    return;
			    }
		    }
		    if (-f $seqRunlogBMC)
		    {
			    open LF, ">$seqRunlogBMC" or print "could not open $seqRunlogBMC";
			    print LF "CBMC_SEQ:FALSE";
		    }
		    #experimentation with cpac witness end

		    #without unwinding-assertions option
		    my ($unwindCount)= @_;
		    my $cbmcWitOp="$unparserOpDir/cbmcWitOp.txt";
		    my $cbmcWitErr="$unparserOpDir/cbmcWitOp.err";

		    if ($lsh_array_abs==1)
		    {
			    $cbmcWitOp="$LSHLAUDir/cbmcWitOp.txt";
			    $cbmcWitErr="$LSHLAUDir/cbmcWitOp.err";
		    }
		    my $cbmcCommand = "timeout -k 2s ".$cbmcVFWitnessTimeOut."s $cbmcExe --unwind $unwindCount --stop-on-fail --32 ".$smt." $cfileFromPrj --graphml-witness $witness > $cbmcWitOp 2>$cbmcWitErr";
		    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		    print "\nCBMC: Starting witness generation.\nStatus:";
		    my $CBMCRetCode=system($cbmcCommand);
		    print ($CBMCRetCode>>8);
		    my $retStr2 = `tail -n 1 $cbmcWitOp`;
		    if ($retStr2 =~ "VERIFICATION SUCCESSFUL") # if unwind count from abstract program, here CBMC might give VS and wrong witness.
		    {
			    unlink ($witness);
		    }
	    }


	    sub checkValidCBMCVS()
	    { 	#without unwinding-assertions option
		    my ($unwindCount,$cfile)= @_;
		    my $cbmcCommand="";
		    if($unwindCount !=0 )
		    {
			    $cbmcCommand = "timeout -k 2s ".$bmctimeoutVSRecurssion."s $cbmcExe --unwinding-assertions --unwind $unwindCount --stop-on-fail --32 ".$smt." $cfile > $unparserOpDir/cbmcWitOp.txt 2>$unparserOpDir/cbmcWitOp.err";
		    }
		    else # for KIND, no unwind count
		    {
			    $cbmcCommand = "timeout -k 2s ".$bmctimeoutVSRecurssion."s $cbmcExe --unwinding-assertions --stop-on-fail --32 ".$smt." $cfile > $unparserOpDir/cbmcWitOp.txt 2>$unparserOpDir/cbmcWitOp.err";
		    }

		    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		    #print "\n$cbmcCommand\n";
		    `$cbmcCommand`;

		    $retStr = `tail -n 10 $unparserOpDir/cbmcWitOp.txt`;
		    #my $retStr2 = `tail -n 1 $unparserOpDir/cbmcWitOp.txt`;
		    if ($retStr !~ "recursion unwinding assertion")
		    {
			    return 1;
		    }
		    return 0;
	    }

	    sub bmcCallOnOrigFile()
	    {
		    $pathAnalTimeOut=15;
		    &pvAnalysis($pathAnalTimeOut);
		    print "\n\n****** BMC CALL on ORIG FILE *****\n\n";
		    &identifyContextSwitchLoopPattern() ;
		    my $retCodeN ;
		    $retCodeN = system("java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/Labtec.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\"");

		    if (-e "$unparserOpDir/loopbound.txt") {
			    &cbmcWithKnownBound(400);							
		    }
		    #&cbmcOOMWithBinarySearch(300);
		    my $start = time();
		    &boundForAFLCall(12);
		    my $end = time();
		    my $diff_boundAndAFLCall_time=$end - $start;
		    print "\nTime taken: $$diff_boundAndAFLCall_time\n";
		    print LOGFILE "\n$unparserOpDir:CBMC_SEQ:UNKNOWN";
		    print "\nCBMC_SEQ:$unknown_str\n";
		    #&runAFLunknown();
		    exit(0);
	    }

	    sub identifyContextSwitchLoopPattern()
	    {	
		    $bmcSEQtimeout=310;

		    if (! -e "$SEQTMPDIR"){
			    `/bin/mkdir $SEQTMPDIR`;
			    print "\n\n\n*** Directory created to dump data for LOOP PATTERN ANALYSIS....\n\n\n";
		    }

		    # Array Abstraction has to be turned OFF.
		    &prepareIni($projBasePath,$projName,1);

		    print "\n\n\n***LOOP PATTERN ANALYSIS Started....\n\n\n";
		    print PRJLOGFILE "\njava -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\"  --loop_pattern_match\n\n";
		    $retCode = system("java -cp \"$jarsPath/Labtec.jar:$jarsPath/slicerWithUnparser.jar:$jarsPath/antlr.jar:$jarsPath/jcupjlex.jar:$jarsPath/joot.jar:$jarsPath/prism.jar:$jarsPath/stringtemplate-1.0.3.jar:$jarsPath/Labtec.jar:$jarsPath/jdom.jar:$jarsPath/com.microsoft.z3.jar:\" -Xms512m -Xmx$maxheapSize -DENVFILE=\"$irsDir/labtec.ini\" -DINVOUTPUTFILE=\"$invariantFile\" \"vdGraph.Vdgdriver\" \"$projDir\" \"$newPrjFilePath\" --loop_pattern_match");


		    print "\n\n\n***LOOP PATTERN ANALYSIS complete....\n\n\n";
		    print PRJLOGFILE "\n***LOOP PATTERN ANALYSIS complete\n";

		    if(-e "$SEQTMPDIR/lpm.txt") {

			    my $cBaseFileName = basename($cfileFromPrj,  ".c");
			    my $cFileName = "$SEQTMPDIR/$cBaseFileName.c" ;

			    my $cbmcCommand = "timeout -k 2s ".$bmcSEQtimeout."s $cbmcExe --unwinding-assertions --unwind 4 --32 ".$smt." $cFileName > $SEQTMPDIR/cbmcOnlyOp.txt 2>$SEQTMPDIR/cbmcOnly.err";

			    if (! -e "$cFileName"){
				    print "C file not Found.....\n";
			    }

			    print "\n\n$cbmcCommand\n\n" ;
			    print "\n$cFileName\n" ;	

			    `$cbmcCommand` ;
			    $retStr = `tail -n 1 $SEQTMPDIR/cbmcOnlyOp.txt`;
			    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
				    print LOGFILE "\n$unparserOpDir:CBMC_SEQ:VERIFICATION SUCCESSFUL";
				    print "\nBMC_SEQ:$success_str\n";
				    #avriti added 26 oct 2016: Correctness Witness Generation using CPAC
				    if ($propertyfile)
				    {
					    &callCPAC();
				    }
			    }
		    }
	    }

	    sub boundForAFLCall()
	    {
		    my $bmcTimeout = 300 ;
		    my ($unwindCount)= @_;
		    print "\nRunning CBMC with bound - $unwindCount.\n" ;
		    my $cbmcCommand = "timeout -k 2s ".$bmcTimeout."s $cbmcExe ".$z3opt." --unwinding-assertions --unwind ".$unwindCount." --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err";
		    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;

		    `$cbmcCommand`;

		    $retStr = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
		    if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION SUCCESSFUL";
			    my $validVS=&checkValidCBMCVS($unwindCount,"$cfileFromPrj"); # Running cbmc for checking if no recursion unwinding assertion.
			    if($validVS == 1)
			    {
				    print "\nVeriAbs BS BOUND:$success_str\n";
				    if ($propertyfile)
				    {
					    &callCPAC();
				    }
				    exit(0);#done
			    }else { print "\nruaf.\n"; $low = $mid + 1;}
		    }
		    else {
			    &checkForCBMCVF("$unparserOpDir/cbmcOnlyOp.txt");
			    #print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.

			    $retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
			    my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;

			    if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
			    {
				    print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
				    &generateCBMCVFWitness($unwindCount); # Running cbmc for witness generation
				    print "\nVeriAbs BS BOUND:$failed_str\n";
				    if( &isNonEmptyFile($witness)) {
					    if ($witnessGeneratedByCBMC == 1) {
						    &addToCBMCWitnessFile();
						    print "\nWitness : $witness.\n";
					    }
				    }else {
					    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
					    &generateDummyWitness($dummyfile);
				    }
				    exit(0); #done
			    }

			    if( $retStr =~ "unwinding assertion loop" ) {
				    print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
				    print "\nCBMC:VeriAbs OOM BS BOUND:LOOP UNWINDING ASSERTION FAILURE\n";
				    if($unwindCount == 12){
					    &boundForAFLCall(16);
				    }
				    else{
					    &runAFL(1000);
					    #&runVeriFuzz();# Avriti: 29 Sep 2019
				    }
			    }
			    elsif( $retStr =~ "recursion unwinding assertion" ) {
				    print LOGFILE "\n$unparserOpDir:CBMC:Recursion assertion failure";
				    print "\nCBMC:VeriAbs OOM BS BOUND:LOOP UNWINDING ASSERTION FAILURE\n";
				    if($unwindCount == 12){
					    &boundForAFLCall(16);
				    }
				    else{
					    &runAFL(1000); 
					    #&runVeriFuzz();# Avriti: 29 Sep 2019
				    }
			    }
			    else {
				    print LOGFILE "\n$unparserOpDir:CBMC:UNKNOWN, timeout/error?"; 
				    print "\nCBMC:VeriAbs OOM BS BOUND:TOERR\n";
				    if($unwindCount == 12){
					    &cbmcOOMWithBinarySearch(300) ;
				    }
				    else{
					    &runAFL(1000);
					    #&runVeriFuzz(); # Avriti: 29 Sep 2019
				    }
			    }	
		    }		
	    }

	    sub selectCPACPath()
	    {
# test new cpac trunk 2017 : start
		    if(-e $invariantFile)
		    {
			    open (RFILE, "$invariantFile") || print ("Error : Cannot open file $invariantFile\n");
			    my @invdata = <RFILE>;
			    my $numOfElements = 0; 
			    @tmpArr = grep ( /<inv/ ,@invdata );
			    $numOfElements = scalar(@tmpArr);
			    #print "\n@invdata | @tmpArr\n";
			    print "\ninvariant data: $numOfElements";
			    if($numOfElements == 0)
			    {
				    $cpacPath=$cpacTestPath;
			    }		
		    }	
		    #print "\nUsing $cpacPath\n";
		    # test new cpac trunk 2017 : end
	    }
sub checkLabtecReturnCodes()
{
		my ($retCode, $isPathWise)= @_;
	if ($retCode == 10) {
		print "\nThe input program is safe.\n";
		print "\n$success_str\n";
		#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
		if ($propertyfile)
		{
			&callCPAC();
		}
		#avriti end
		#exit (10);
		exit(0);
	}elsif ($retCode == 100)
	{ #sucess	
			if ($isPathWise == 1)
			{
				print "\nVeriAbs:AVP:$success_str\n";
			}
			else
			{
				print "\nVeriAbs:AV:$success_str\n";
			}
		
			if ($propertyfile && $witnessval eq "")
			{
				&callCPAC();
			}
			exit(0);
	}elsif ($retCode == 200)
	{
		if ($witnessval eq "")
                {
		print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
		&generateCBMCVFWitness(20); # Running cbmc for witness generation : commented since generating witness on abstract program
		if ($isPathWise == 1)
		{
			print "\nVeriAbs:AVP:$failed_str\n";
		}
		else
		{
			print "\nVeriabs:AV:$failed_str\n";
		}
		if( &isNonEmptyFile($witness)) {
			if ($witnessGeneratedByCBMC == 1) {
				&addToCBMCWitnessFile();
				print "\nWitness : $witness.\n";
			}
		}else {
			my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
			&generateDummyWitness($dummyfile);
		}
	        }else {print "\n$valunknown_str\n"}
		exit(0); #done
	}
}

=begin comment
sub callCPAC_true()
{
	my $presentWorkDir=getcwd;

	if($bmcRunOnOrigFile == 0)
	{
		#&selectCPACPath();
	}
	else
	{
		if (-f $seqRunlogBMC)
		{
			open LF, ">$seqRunlogBMC" or print "could not open $seqRunlogBMC";
			print LF "CBMC_SEQ:TRUE";
		}
	}
	chdir $cpacPath;

	if ( -e "$witness") { unlink($witness);}

	my $witnessgendir = $projDir."/witnessgendir";
	if ( -e "$witnessgendir") {
		rmtree($witnessgendir);
	}
	mkdir($witnessgendir) or die "Error : Couldn't create $witnessgendir directory";
	my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
	#if (! -e $invariantFile) {
	#	print "witness_generation: Unable to find invariant file";
		&generateDummyWitness($dummyfile);
		#chdir $presentWorkDir;
		#exit(0);
	#}	    

	if (! -e "$cpacPath/scripts/cpa.sh") { print "VeriAbs error: cpa.sh does not exist! $cpacPath/scripts/cpa.sh"; }
	`chmod 777 scripts/cpa.sh`;
	my $cpacWitnessGenCmd;
	$cpacWitnessGenCmd="scripts/cpa.sh -correctness-witnesses-k-induction -outputpath $witnessgendir -spec $propertyfile $cfileFromPrj >$logsDir/cpaLog.txt 2>&1";
	open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
	print LOGFILE "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;
	close(LOGFILE);

	$end_before_cpac_time=time();
	my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
	print "\nTotal time taken before witness generation: $diff_before_cpac_time\n";
	
	my $cpactimeout=600 - $diff_before_cpac_time;
	$cpacWitnessGenCmd="timeout --preserve-status -k 20s ".$cpactimeout."s ".$cpacWitnessGenCmd;
	print "\nCPAC Witness Generation Command:\n".$cpacWitnessGenCmd;

	my $start = time();
	my $returnCode = system( $cpacWitnessGenCmd );
	my $end = time();
	my $diff_cpac_time=$end - $start;
	print "\nCPA time taken: $diff_cpac_time\n";

	my $cpacWitness = $witnessgendir."/correctness-witness.graphml";
	if ( ! -e $cpacWitness) 
	{ 
		print "\ntest_d generated.\n";
		#&generateDummyWitness($dummyfile);
		chdir $presentWorkDir;
		exit(0);
	}
	else
	{
		copy($cpacWitness, $witness);
		print "\nWitness: $witness.\n";
	}

	#generateWitness can return non-zero return code when there are no invariants generated by us
	#in such cases return dummywitness instead of automaton created earlier
	chdir $presentWorkDir;
	exit(0);
}
=end comment
=cut

sub runAFLunknown()
{
	my $currtime=time() - $start_veriabs_time;
	my $afltime=850 - $currtime;
	&runAFL($afltime);
}

sub checkValidCBMCVSUnparseEr()
{ 	#without unwinding-assertions option
    my ($unwindCount,$cfile)= @_;
    my $cbmcCommand="";
    if($unwindCount !=0 )
    {
	    $cbmcCommand = "timeout -k 2s ".$bmctimeoutVSRecurssion."s $cbmcExe --unwinding-assertions --unwind $unwindCount --stop-on-fail --32 ".$smt." $cfile > $logsDir/cbmcWitOp.txt 2>$logsDir/cbmcWitOp.err";
    }
    else # for KIND, no unwind count
    {
	    $cbmcCommand = "timeout -k 2s ".$bmctimeoutVSRecurssion."s $cbmcExe --unwinding-assertions --stop-on-fail --32 ".$smt." $cfile > $logsDir/cbmcWitOp.txt 2>$logsDir/cbmcWitOp.err";
    }

    print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
    #print "\n$cbmcCommand\n";
    `$cbmcCommand`;

    $retStr = `tail -n 10 $logsDir/cbmcWitOp.txt`;
    #my $retStr2 = `tail -n 1 $logsDir/cbmcWitOp.txt`;
    if ($retStr !~ "recursion unwinding assertion")
    {
	    return 1;
    }
    return 0;
}
sub KNVerification()
{
	my $kindCount = 2;
	while ( $kindCount <= $KIND_MAX_K) { 

		#my $cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe --unwind 20 --unwinding-assertions --32 ".$smt." $unparserOpDir/*.c > $unparserOpDir/cbmcop.txt 2>$unparserOpDir/cbmcOnly.err"; #avriti
		my $cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe --arrays-uf-always --unwinding-assertions --32 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcop.txt 2>$unparserOpDir/cbmcOnly.err"; #avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
		print LOGFILE "CBMC COMMAND:\n".$cbmcCommand;
		`$cbmcCommand`;
		my $retStr = `tail -n 1 $unparserOpDir/cbmcop.txt`;
		$currLAI_end_Time = time();
		$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
		print TIMINGSFILE "\nTotal time needed for CBMC with KINDUCT=$kindCount,$totalTimeForLAI\n";
		print TIMINGSFILE "\n-------------------------------\n";
		if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			print LOGFILE "\n$unparserOpDir:VeriAbs:KN:VERIFICATION SUCCESSFUL"; 
			my $validVS=&checkValidCBMCVS(0,"$unparserOpDir/test.c"); # Running cbmc for checking if no recursion unwinding assertion.
			if($validVS == 1)
			{
				print "\nVeriAbs:KN:$success_str";
				#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
				if ($propertyfile)
				{
					&callCPAC();
				}
			}else { print "\nruaf.\n";}
			#avriti end
			last;
		}
		# k-induction with LABMC will give LUAF or RUAF when a loop is not abstracted
		$retStr = `tail -n 10 $unparserOpDir/cbmcop.txt`;
		if( $retStr =~ "unwinding assertion loop" ) {
			print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
			print "\nVeriAbs:KN:LOOP UNWINDING ASSERTION FAILURE\n";
			$kindCount = $KIND_MAX_K;	#break from the loop. LUAF due to loops not abstrated
		}
		elsif( $retStr =~ "recursion unwinding assertion" ) {
			print LOGFILE "\n$unparserOpDir:CBMC:Loop unwinding assertion failure";
			print "\nVeriAbs:KN:RECURSION UNWINDING ASSERTION FAILURE\n"; #avriti 
			$kindCount = $KIND_MAX_K;	#break from the loop. RUAF due to recursion
		}
		else {
			my $retStr2 = `tail -n 1 $unparserOpDir/cbmcop.txt`;
			#if( $retStr2 =~ "VERIFICATION FAILED" ) {
			#print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
			#print "\nCBMC:VERIFICATION FAILED\n";
			#}
			if( $retStr2 !~ "VERIFICATION FAILED" ) {	#timeout / error
				$kindCount = $KIND_MAX_K;
				print LOGFILE "\n$unparserOpDir:VeriAbs:KN:UNKNOWN, timeout/error?"; 
				print "\nVeriAbs:KN:TOERR"; 
			}
		}
		#elsif( $retStr !~ "VERIFICATION FAILED" ) {	#timeout / error
		#$kindCount = $KIND_MAX_K;
		#}
		$kindCount+=2;
		my $kindFile = "$unparserOpDir/kind.c";
		open (KINDFILE, ">$kindFile") || die ("Cannout open $unparserOpDir/kind.c");
		print KINDFILE "\n\nunsigned const int _LAB_KINDUCT=$kindCount;";
		print LOGFILE "wrote unsigned const int _LAB_KINDUCT=$kindCount; to $unparserOpDir/kind.c\n\n";
		close KINDFILE;
		#avriti commented 26 oct 2016: now change kind in preprocessed file test.c
		#print "kind count : $kindCount\n";
		`sed -i "s/unsigned const int _LAB_KINDUCT.*/unsigned const int _LAB_KINDUCT=$kindCount;/g" $unparserOpDir/test.c`;
		#avriti end
	}
	if ( $kindCount > $KIND_MAX_K) { 
		#print LOGFILE "\n$unparserOpDir:VeriAbs:KN:UNKNOWN, timeout/error?"; 
		print "\n$unknown_str\n";
		#&runAFLunknown();
		#unlink "$currentDir/error-witness_$projName.graphml";
		unlink "$witness";
	}
}

sub runVeriFuzz()
{
	my $currtime=time() - $start_veriabs_time;
	my $afltime=850 - $currtime;
	my $AFLCommand="timeout -k 2s $afltime "."$LABTECROOT/verifuzz/scripts/verifuzz.py --debug 1 --propertyFile $propertyfile $cfileFromPrj > $logsDir/verifuzzop.txt";
	print "\nAFL Run COMMAND:\n".$AFLCommand;
	#`$AFLCommand` ;
	system("/bin/bash", "-c", $AFLCommand) ;

	# $logsDir/verifuzzop.txt
	my $retStr2 = `tail -n 22 $logsDir/verifuzzop.txt`;
	if ($retStr2 =~ "VERIFUZZ_VF")
	{
	    print LOGFILE "\nVeriFuzz:VERIFICATION FAILED";
	    print "\nVeriFuzz:$failed_str\n";
	    exit(0); #done
	}
	else
	{
	    print "\nVeriFuzz_UNKNOWN\n";
	}
}

sub runVeriFuzz_InfLoop()#verifuzz not working Inf Loops
{
	#my $currtime=time() - $start_veriabs_time;
	my $afltime=300; # 300 was set for ECA
	my $AFLCommand="timeout -k 2s $afltime "."$LABTECROOT/verifuzz/scripts/verifuzz.py --debug 1 --propertyFile $propertyfile $cfileFromPrj > $logsDir/verifuzzop.txt";
	print "\nAFL Run COMMAND:\n".$AFLCommand;
	#`$AFLCommand` ;
	system("/bin/bash", "-c", $AFLCommand) ;

	# $logsDir/verifuzzop.txt
	my $retStr2 = `tail -n 22 $logsDir/verifuzzop.txt`;
	if ($retStr2 =~ "VERIFUZZ_VF")
	{
	    print LOGFILE "\nVeriFuzz:VERIFICATION FAILED";
	    print "\nVeriFuzz:$failed_str\n";
	    system("$LABTECROOT/scripts/successCheck.sh Success");
	    exit(0); #done
	}
	else
	{
	    print "\nVeriFuzz_UNKNOWN\n";
	}
}

sub runCPAC_PA_KI()
{
	#cpa.sh -config cpact/CPAchecker-1.7-unix/config/predicateAnalysis.properties -heap 10000M -timelimit 900s -spec ~/unreach-call.prp ~/svcomp20/git/sv-benchmarks/c/seq-mthreaded-reduced/pals_floodmax.4.2.ufo.UNBOUNDED.pals.c.v+lhb-reducer.c
	my $presentWorkDir=getcwd;
	chdir $cpacPath;
	print "\nCPAC root: $cpacPath\n";
	if (! -e "$cpacPath/scripts/cpa.sh") { print "VeriAbs error: cpa.sh does not exist! $cpacPath/scripts/cpa.sh"; }
        `chmod 777 scripts/cpa.sh`;
	my $cpacPACmd;
=begin comment_cpa_pa
        $cpacPACmd="scripts/cpa.sh  -config $cpacPath/config/predicateAnalysis.properties -heap 10000M -spec $propertyfile $cfileFromPrj >$logsDir/cpaPALog.txt 2>&1";
        open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
        print LOGFILE "\nCPAC PA Command:\n".$cpacPACmd;
        close(LOGFILE);

        $end_before_cpac_time=time();
        my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
        print "\nTotal time taken before CPAC PA: $diff_before_cpac_time\n";

        print "\nCPAC PA Command:\n".$cpacPACmd;

        my $start = time();
        my $returnCode = system( $cpacPACmd );
        my $end = time();
        my $diff_cpac_time=$end - $start;
        print "\nCPA PA time taken: $diff_cpac_time\n";
	
	$retStr = `tail -n 5 $logsDir/cpaPALog.txt`;
	if( $retStr =~ "Verification result: FALSE" ) {
		print "\nVeriAbs:CPAC-PA:$failed_str\n";
		#exit(0);
	}elsif ( $retStr =~ "Verification result: TRUE") {
		print "\nVeriAbs:CPAC-PA:$success_str\n";
		#exit(0);
	}

=end comment_cpa_pa
=cut
	#kInduction
	$cpacPACmd="scripts/cpa.sh  -config $cpacPath/config/kInduction.properties -heap 10000M -spec $propertyfile $cfileFromPrj >$logsDir/cpaPALog.txt 2>&1";
        open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");
        print LOGFILE "\nKI Command:\n".$cpacPACmd;
        close(LOGFILE);

        $end_before_cpac_time=time();
        my $diff_before_cpac_time=$end_before_cpac_time - $start_veriabs_time;
        print "\nTotal time taken before KI: $diff_before_cpac_time\n";

        print "\nKI Command:\n".$cpacPACmd;

        my $start = time();
        my $returnCode = system( $cpacPACmd );
        my $end = time();
        my $diff_cpac_time=$end - $start;
        print "\nKI time taken: $diff_cpac_time\n";

        $retStr = `tail -n 5 $logsDir/cpaPALog.txt`;
        if( $retStr =~ "Verification result: FALSE" ) {
		system("$LABTECROOT/scripts/successCheck.sh Success");
		if($witnessval eq "")
		{
                print "\nVeriAbs:KI:$failed_str\n";
		#system("$LABTECROOT/scripts/successCheck.sh Success");
		if ($propertyfile && $witnessval eq "")
                {
                       &callCPACVoilation();
		       #&generateCBMCVFWitness(30); commented since cpu time exceeds 900s
		       #if( &isNonEmptyFile($witness)) {
		       #    if ($witnessGeneratedByCBMC == 1) {
		       #	    &addToCBMCWitnessFile();
		       #	    print "\nWitness : $witness.\n";
		       #    }
		       # }else {
		       #    my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
		       #    &generateDummyWitness($dummyfile);
		       #}
                }
		}else {print "\n$valunknown_str\n"}
                exit(0);
        }elsif ( $retStr =~ "Verification result: TRUE") {
                print "\nVeriAbs:KI:$success_str\n";
		system("$LABTECROOT/scripts/successCheck.sh Success");
                if ($propertyfile && $witnessval eq "")
                {
                        &callCPAC();
                }
		exit(0);
        }
	chdir $presentWorkDir;
	return 10;
}

sub callVajra()
{
	my $vajraExe="$LABTECROOT/bin/vajra";
	if ( -e $vajraExe) {
		my $mts_file;
		my $onlyCFile=basename($cfileFromPrj, ".c");
		$mtsfile="$projDir/MTS/$onlyCFile.c";
		print "\nRunning $vajraExe ";
		if ( -f $mtsfile) {
			print "$mtsfile\n";
			system("timeout -k 2s 5s $vajraExe $mtsfile >$logsDir/vajra.log");
		}
		else {
			print "$cfileFromPrj";
			system("timeout -k 2s 5s $vajraExe $cfileFromPrj >$logsDir/vajra.log");
		}
		$retStr = `tail -n 4 $logsDir/vajra.log`;
		if( $retStr =~ "VAJRA_VERIFICATION_SUCCESSFUL" ) {
			print "\nVeriAbs:VAJRA:$success_str\n";
			exit(0);
		}elsif( $retStr =~ "VAJRA_VERIFICATION_FAILED") {
			print "\nVeriAbs:VAJRA:$failed_str\n";
			#my $oldsmt = $smt;
			# $smt = $smt." --partial-loops ";
			if(-e $witness) # remove existing witness
			{unlink($witness);}
			#&generateCBMCVFWitness(4); # Running cbmc for witness generation

			#$smt = $oldsmt;
			if( &isNonEmptyFile($witness)) {
				if ($witnessGeneratedByCBMC == 1) {
					&addToCBMCWitnessFile();
					print "\nWitness : $witness.\n";
				}
			}else {
				my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
				&generateDummyWitness($dummyfile);
			}
			exit(0);
		}

	    }else { print "\n$vajraExe not found.\n";}
}

sub runParallelAFLCPAC()
{

	my $frwdPID;
	my $backPID;
	print "\nAFL and CPA-Checker in parallel...";
	print "\nCurrent process ID is: $$\n";
	    $frwdPID = fork;
	    if(not defined $frwdPID){
		    warn 'Approach 1 Could not fork';
	    }
	    if($frwdPID == 0){
		    print "Approach 1: Forked approach PID: $$\n";
		    print "\nApproach 1: Running AFL";
		    $retCode=&runAFL_SRParallel(720);
		    #&runverifuzz_infloop(); #: avriti 26 sep 2019
		    print "\nApproach 1: AFL completed";
		    if($retCode == 10)
		    {
			    print "\nApproach 1: Return code: 10";
			    system("$LABTECROOT/scripts/successCheck.sh Unknown");
		    }
		    else{
			    if ($propertyfile)
			    {
				    my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
				    &generateDummyWitness($dummyfile);
			    }
			    system("$LABTECROOT/scripts/successCheck.sh Success");
		    }
		    exit;
	    }
	    else{
		    $backPID = fork;
		    if(not defined $backPID){
			    warn 'Approach 2 Could not fork';
		    }
		    if($backPID == 0){
			    print "\nApproach 2: Forked approach PID: $$\n";
			    print "\nApproach 2: Running KInduction\n";
			    my $retCode = &runCPAC_PA_KI();
			    if($retCode == 10)
			    {
				    print "\nApproach 2: Return code: 10";
				    system("$LABTECROOT/scripts/successCheck.sh Unknown");
			    }
			    else{
				    if ($propertyfile)
				    {
					    my $dummyfile="$LABTECROOT/supportFiles/correctness-witnesstp.graphml";
					    &generateDummyWitness($dummyfile);
				    }
				    system("$LABTECROOT/scripts/successCheck.sh Success");
			    }
			    exit;
		    }
	    }

	my $PID = wait();
        print "\nApproach: $PID exiting\n";
        my $retCode = system("$LABTECROOT/scripts/failureCheck.sh");
        #print "\nfailure check: $retCode\n";
        if($retCode>>8 == 0) #Kill other child
        {
            #print "\nfailure check: $retCode: $PID exiting: $frwdPID approach1: $backPID approach2\n";
            if($PID == $frwdPID){
                    #print "\nkill $backPID\n";
                    system("kill -9 $backPID");
            }
            elsif($PID == $backPID){
                    #print "\nkill $frwdPID\n";
                    system("kill -9 $frwdPID");
            }
        }
        elsif($retCode>>8 == 10){
            print "\nWait for other approach";
            my $nextPID = wait();
            print "\nNext: $nextPID exiting\n";

        }

	exit(0);
	#&runAFL(720);
	#&runVeriFuzz(); #Avriti: 26 Sep 2019
	#&runCPAC_PA_KI();
}

sub runAFL_SRParallel()
	    {
		    #my $AFL_Timeout=150 ;
		    my ($AFL_Timeout) = @_;
		    my $AFLOutputDir=$labtecOutputDir;
		    my $AFLCommand = "";

		    if($bmcRunOnOrigFile == 1)
		    {
			    $AFLCommand = "time ( perl $LABTECROOT/scripts/exeFuzzer.pl $cfileFromPrj $AFL_Timeout $AFLOutputDir gcc crashon ) 2> $AFLOutputDir/AFL_timeTaken.out";
		    }
		    else
		    {
			    $AFLCommand = "time ( perl $LABTECROOT/scripts/exeFuzzer.pl $cfileFromPrj $AFL_Timeout $AFLOutputDir ) 2> $AFLOutputDir/AFL_timeTaken.out";
		    }
		    print "\nAFL Run COMMAND:\n".$AFLCommand;
		    #`$AFLCommand` ;
		    system("/bin/bash", "-c", $AFLCommand) ;
		    print LOGFILE "\nAFL Run COMMAND:\n".$AFLCommand;

		    my $AFLOutputLogFile="$AFLOutputDir/exp-log/run.log";
		    my $AFLLogFile="$AFLOutputDir/exp-log/run.log" ;
		    if (! -e "$AFLLogFile") {
			    print "\nAFL_RUN LOG FILE DOES NOT EXIST.";
			    print "\nAFL_UNKNOWN\n";
		    }

		    my $retStr2 = `tail -n 1 $AFLLogFile`;
		    if ($retStr2 =~ "FALSE TRUE")
		    {
			    print LOGFILE "\nAFL:VERIFICATION FAILED";
			    print "\nAFL:$failed_str\n";
			    system("$LABTECROOT/scripts/successCheck.sh Success");
			    exit(0); #done
		    }
		    elsif ($retStr2 =~ "FALSE UNKNOWN")
		    {
			    print "\nAFL_UNKNOWN\n";
		    }
		    return 10;
	    }

sub loopAbsIndution()
{
	$totalTimeForLAI=0;
	#&cleanup("$projDir");
	#unlink ($completeLogFile);
	#unlink ($completeErrFile);
	#&openfile($vdgraphLogFile,$vdgraphErrFile);
	#&reopenfile($completeLogFile,$completeErrFile);
	print TIMINGSFILE "\n\n----------New LAI run----------\n";
	print CMPLOGFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";
	print CMPERRFILE "************             Starting new run of Loop Abstraction Information Generation ::\n\n";

	my $currLAI_st_Time = time();
	#copy prj file to Input folder & insert "FILE:" into it

	&extractLoopAbstractionInfo($newPrjFilePath);
	#&errFunc($vdgraphLogFile,$vdgraphErrFile,"l");
	my $currLAI_end_Time = time();
	$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
	print TIMINGSFILE "\nTotal time needed for Loop Abs Info Generation,$totalTimeForLAI\n";
	print TIMINGSFILE "\n-------------------------------\n";

	if ( $SVCOMP16 == 1 ) {
		my $currLAI_st_Time = time();
		open (LOGFILE, ">>$vdgraphLogFile") || die ("Cannot open $vdgraphLogFile for writing");

		&gccPreprocessing();

		my $retStr = "";
		my $cbmcCommand = "";
		#start without --arrays-uf-always
		print "\nRunning on unparsed file with induction..";
		my $success = 0;
		$success=( system("grep \"malloc\" $cfileFromPrj >/dev/null") ) ? 0 : 1;
		if ($success==0)
		{
		$cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe --unwind 20 --unwinding-assertions --32 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcop.txt 2>$unparserOpDir/cbmc.err";	#avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
		print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		}else
		{
		$cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe --unwind 20 --unwinding-assertions --32 --z3 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcop.txt 2>$unparserOpDir/cbmc.err";	#avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
		}
		`$cbmcCommand`;
		$retStr = `tail -n 1 $unparserOpDir/cbmcop.txt`;
		$currLAI_end_Time = time();
		$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
		print TIMINGSFILE "\nTotal time needed for CBMC =$kindCount,$totalTimeForLAI\n";
		print TIMINGSFILE "\n-------------------------------\n";
		if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION SUCCESSFUL";
			#print "Property file: $propertyfile";
			my $validVS=&checkValidCBMCVS(20,"$unparserOpDir/test.c"); # Running cbmc for checking if no recursion unwinding assertion.
			if($validVS == 1)
			{
				print "\nVeriAbs:$success_str\n";

				#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
				if ($propertyfile && $witnessval eq "")
				{
					&callCPAC();
				}
				return (0,$retStr);
			}else { print "\nruaf.\n";&cbmcWithLAUF();}
			#avriti end
		}#end start without --arrays-uf-always
=begin comment_with_array
		else
		#start with --arrays-uf-always
		{
			print "\nRunning cbmc with array opt on unparsed file : $unparserOpDir/test.c";
			$cbmcCommand = "timeout -k 2s 210s $cbmcExe --arrays-uf-always --unwind 20 --unwinding-assertions --32 ".$smt." $unparserOpDir/test.c > $unparserOpDir/cbmcop.txt 2>$unparserOpDir/cbmc.err";	#avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
			print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
			`$cbmcCommand`;
			$retStr = `tail -n 1 $unparserOpDir/cbmcop.txt`;
			$currLAI_end_Time = time();
			$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
			print TIMINGSFILE "\nTotal time needed for CBMC =$kindCount,$totalTimeForLAI\n";
			print TIMINGSFILE "\n-------------------------------\n";
			if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
				print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION SUCCESSFUL";
				#print "Property file: $propertyfile";
				print "\nVeriAbs:$success_str\n";

				#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
				if ($propertyfile)
				{
					&callCPAC();
				}
				#avriti end
			}
		}
=end comment_with_array
=cut
		else
		{
			return (10,$retStr); # Not verified by loop abstraction
		}
	}
}

sub summarizer()
{
# Summarizer
	my $retStr="";
	if ( -e "$unparserOpDir/notSummarizable.txt") { print "\nNot summarizable!";}
	if( ! -e "$unparserOpDir/Summ_$projName.c") {print "\nSummarized file does not exist. $unparserOpDir/Summ_$projName.c";}
	if (! -s "$unparserOpDir/Summ_$projName.c") { print "\nSummarized file file empty. yes\n";} 
	if (! -e "$unparserOpDir/notSummarizable.txt" && -e "$unparserOpDir/Summ_$projName.c" && -s "$unparserOpDir/Summ_$projName.c" )
	{
		print "\nRunning on summarizer generated file..";
		$cbmcCommand = "timeout -k 2s ".$bmc1timeout."s $cbmcExe --unwind 20 --unwinding-assertions --32 ".$smt." $unparserOpDir/Summ_$projName.c > $unparserOpDir/cbmcopSumm.txt 2>$unparserOpDir/cbmcSumm.err";	#avriti added 26 oct 2016 verify test.c created from all LAU/*.c files
		print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
		`$cbmcCommand`;
		$retStr = `tail -n 1 $unparserOpDir/cbmcopSumm.txt`;
		$currLAI_end_Time = time();
		$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
		print TIMINGSFILE "\nTotal time needed for CBMC =$kindCount,$totalTimeForLAI\n";
		print TIMINGSFILE "\n-------------------------------\n";
		if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
			print LOGFILE "\n$unparserOpDir:VeriAbs:VERIFICATION SUCCESSFUL";
			#print "Property file: $propertyfile";
			my $validVS=&checkValidCBMCVS(0,"$unparserOpDir/Summ_$projName.c");
			if($validVS == 1)
			{
				print "\nVeriAbs:Summarizer:$success_str\n";
				if ( -e $summInvariantFile) {
					if (-e $invariantFile) {
						move($invariantFile,"$invariantFile.bak");
					}
					copy($summInvariantFile,$invariantFile);
				}
				if ($propertyfile && $witnessval eq "")
				{
					&callCPAC();
				}
				exit(0);
			}else { print "\nruaf.\n";}
		}else
		{
			&checkForCBMCVF("$unparserOpDir/cbmcopSumm.txt");
			#print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

			$retStr = `tail -n 10 $unparserOpDir/cbmcopSumm.txt`;
			my $retStr2 = `tail -n 1 $unparserOpDir/cbmcopSumm.txt`;

			if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
			{
				print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
				&generateCBMCVFWitness(20); # Running cbmc for witness generation
				print "\nVeriAbs:Summarizer:$failed_str\n";
				if ($witnessval eq "")
				{
				if( &isNonEmptyFile($witness)) {
					if ($witnessGeneratedByCBMC == 1) {
						&addToCBMCWitnessFile();
						print "\nWitness : $witness.\n";
					}
				}else {
					my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
					&generateDummyWitness($dummyfile);
				}
			        }
				exit(0); #done
				#&callCPACVoilation();
			}
		}
	}else {print "\nNot running on summarizer generated file.\n"; }
	return $retStr;
	#Summarizer: End
}

sub cbmcDefault()
{
	#my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe --graphml-cex $unparserOpDir/error-witness.graphml --unwinding-assertions --unwind 20 --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err"; #avriti added graphML option
	print "\nRunning verification check by cbmc $z3opt $smt";
	if (! -e "$cfileFromPrj") { print "VeriAbs error: verification check by cbmc $cfileFromPrj does not exist!"; }

	#my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe --unwinding-assertions --unwind 16 --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err"; #avriti added graphML option
	my $cbmcCommand = "timeout -k 2s ".$bmc2timeout."s $cbmcExe ".$z3opt." --unwinding-assertions --unwind 16 --32 ".$smt." $cfileFromPrj > $unparserOpDir/cbmcOnlyOp.txt 2>$unparserOpDir/cbmcOnly.err"; #avriti added graphML option
	print LOGFILE "\nCBMC COMMAND:\n".$cbmcCommand;
	`$cbmcCommand`;
	#copy("$unparserOpDir/error-witness.graphml","$errorWitnessPath/error-witness_$projName.graphml"); #avriti added to copy error-witness to proper directory
	#copy("$unparserOpDir/error-witness.graphml","$currentDir/error-witness_$projName.graphml");
	$currLAI_end_Time = time();
	$totalTimeForLAI = $currLAI_end_Time - $currLAI_st_Time;
	print TIMINGSFILE "\nTotal time needed for CBMC =$kindCount,$totalTimeForLAI\n";
	print TIMINGSFILE "\n-------------------------------\n";
	my $retStr="";
	$retStr = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;
	if( $retStr =~ "VERIFICATION SUCCESSFUL" ) {
		print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION SUCCESSFUL";
		my $validVS=&checkValidCBMCVS(16,"$cfileFromPrj"); # Running cbmc for checking if no recursion unwinding assertion.
		if($validVS == 1)
		{
			print "\nBMC:$success_str\n";
			#avriti added 26 oct 2016: Correctness Witness Generation using CPAC
			if ($propertyfile)
			{
				&callCPAC();
			}
		}else { print "\nruaf.\n";}
		#avriti end
	}
	else {
		&checkForCBMCVF("$unparserOpDir/cbmcOnlyOp.txt");
		#print "\n--$allAssertionFAILED--\n"; # if all assertion.* are FAILURE and VERIFICATION FAILED, then VF.				

		$retStr = `tail -n 10 $unparserOpDir/cbmcOnlyOp.txt`;
		my $retStr2 = `tail -n 1 $unparserOpDir/cbmcOnlyOp.txt`;

		if($allAssertionFAILED==1 && $retStr2 =~ "VERIFICATION FAILED")
		{
			print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
			print "\nVeriAbs:BMC:$failed_str\n";
			&generateCBMCVFWitness(16); # Running cbmc for witness generation
			if( &isNonEmptyFile($witness)) {
				if ($witnessGeneratedByCBMC == 1) {
					&addToCBMCWitnessFile();
					print "\nWitness : $witness.\n";
				}
			}else {
				my $dummyfile="$LABTECROOT/supportFiles/dummywitnessfp.graphml";
				&generateDummyWitness($dummyfile);
			}
			exit(0); #done
			#&callCPACVoilation();
		}
	}
	return $retStr;

=begin comment2
	if ($retStr !~ "unwinding assertion loop" && $retStr !~ "recursion unwinding assertion" && $retStr2 =~ "VERIFICATION FAILED")
	{
		print LOGFILE "\n$unparserOpDir:CBMC:VERIFICATION FAILED";
		print "\nCBMC:$failed_str\n";
		&callCPACVoilation();
		#if( &isNonEmptyFile($witness)) {
		#	&addToCBMCWitnessFile();
		#	print "\nWitness : $witness.\n";
		#}
	}
=end comment2
=cut
}
