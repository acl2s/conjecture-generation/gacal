#!/bin/bash
#$1: source file
JARDIR=$LABTECROOT/jars
OUTDIR=$LABMCPROJDIR/InfTrace
MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini
MYPRJFILE=$LABMCPROJDIR/Input/$1.prj

#if [ ! -d "$OUTDIR" ]
#then
#	mkdir $OUTDIR
#fi

#jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"

# Append printf statements to identify conditions uniquely
#java -cp $jars -DENVFILE="$MYENVFILE" infLoop.Driver $OUTDIR $MYPRJFILE -print > $OUTDIR/trace_$1.txt 2>&1
#echo "InfLoop Forward: 1st Unparsing done"

# Adding header file
#sed -i '1i #include<stdio.h>' "$OUTDIR/Unparse_$1.c"
#echo "InfLoop Forward: header file added"

# Preprocess unparsed file before generating IRs
#gcc -m32 -E "$OUTDIR/Unparse_$1.c" > "$OUTDIR/Preprocess_Unparse_$1.c"
#echo "InfLoop Forward: Preprocessing of unparsed file completed"

# Generate IRs for unparsed file
#$LABTECROOT/bin/cppfe --edg--m --edg-w --edg--gcc --edg--c99 -O "$LABMCPROJDIR/IRs" "$OUTDIR/Preprocess_Unparse_$1.c"
#echo "InfLoop Forward: IRs for unparsed file generated" 

# Making folder in IRs directory
if [ ! -d "$LABMCPROJDIR/IRs/$1_newfrwd" ];
then
	mkdir $LABMCPROJDIR/IRs/$1_newfrwd
fi

# Make prj file for new Prism session
cp $LABMCPROJDIR/Input/$1.prj $LABMCPROJDIR/Input/$1_newfrwd.prj

# Path of preprocessed unparsed file
preprocessedUnparsedFilePath="$OUTDIR/Preprocess_Unparse_$1.c"

# Changing file path in new prj file
sed -i "/FILE/!b;n;c$preprocessedUnparsedFilePath" $LABMCPROJDIR/Input/$1_newfrwd.prj

echo "InfLoop Forward: Instrumenting file for AFL"
echo "InfLoop Forward: Java trace"
#jars="$JARDIR/verifuzz_withoutMemSafe.jar:$JARDIR/stringtemplate-1.0.3.jar:$JARDIR/slicerWithUnparser.jar:$JARDIR/prism.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/jdom.jar:$JARDIR/jcupjlex.jar:$JARDIR/antlr.jar"
jars="$JARDIR/verifuzz_withoutMemSafe.jar:$JARDIR/stringtemplate-1.0.3.jar:$JARDIR/slicerWithUnparser.jar:$JARDIR/prism.jar:$JARDIR/jdom.jar:$JARDIR/jcupjlex.jar:$JARDIR/antlr.jar"

java -cp $jars -Xms512m -Xmx$MAXHEAPSIZE -DENVFILE="$MYENVFILE" fuzzManager.RunFuzzingAnalysis -O $OUTDIR -L 2 $LABMCPROJDIR/Input/$1_newfrwd.prj > $OUTDIR/AFL_trace_$1.txt 2>&1

echo "InfLoop Forward: Java trace ends"
echo "AFL Instrumentation complete"
