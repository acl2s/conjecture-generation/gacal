#!/usr/bin/python
import time
total_time_start=time.time()
success_str="VERIABS_VERIFICATION_SUCCESSFUL"
failed_str="VERIABS_VERIFICATION_FAILED"
unknown_str="VERIABS_UNKNOWN"
import os
import sys

task=os.path.abspath(sys.argv[2])
propertyfile=os.path.abspath(sys.argv[1])


prjdirname=task.rsplit('/',1)[1]
prjdirname=prjdirname.replace(".","_")
prjdirname=prjdirname.replace("-","_")
cwd=os.path.abspath(sys.argv[0]).rsplit('/',2)[0]
outdir=os.getenv("HOME")
if os.path.isdir(outdir+"/ar_abs_temp"):
	os.system("rm -rf "+outdir+"/ar_abs_temp/*")
else:
	os.mkdir(outdir+"/ar_abs_temp")
outdir=outdir+"/ar_abs_temp"
r2pid=outdir+"/r2pid"

irpath=outdir+"/"+prjdirname
os.mkdir(irpath)

r1out=outdir+"/r1out.c"
enclfunc=outdir+"/enclfunc"

IRs=irpath+"/IRs"
os.mkdir(IRs)
Logs=irpath+"/Logs"
os.mkdir(Logs)
Repos=IRs
os.mkdir(Repos+"/"+prjdirname)

os.system("cp "+cwd+"/supportFiles/csizes_64.txt "+irpath+"/csizes.txt")
os.system("cp "+task+" "+outdir)
temp_task=outdir+"/"+task.rsplit('/',1)[1]
ini_path=irpath+"/env.ini"
trnsdorgi=outdir+"/trnsdorgi.c"
trnsdslice=outdir+"/trnsdslice.c"
cppfepath=cwd+"/bin/cppfe_SysVer"
cbmcpath=cwd+"/bin/cbmc"
witnesspath=cwd
if os.path.isfile(witnesspath+"/witness.graphml"):
        print("cleaned cwd")
        os.system("rm "+witnesspath+"/witness.graphml")
jars_path=cwd+"/jars"
slicerpath=cwd+"/frama-c-Chlorine-20180502/crude_slicer/frama-c-minimal"
UApath=cwd+"/UAutomizer-linux"
CPApath=cwd+"/cpact/CPAchecker-1.7-unix"
dummyWitnessPath=cwd+"/supportFiles/SoftwareSystems" 

###############################################################################################################################

def genDummyWitness(intask,witnesstype):
	import subprocess
	filehash=subprocess.check_output("sha1sum "+intask, shell=True)
	prgmhash=filehash.split("  ")[0]
	if witnesstype=="true":
		dummywitness=dummyWitnessPath+"/correctness-witnesstp.graphml"
		os.system("cp "+dummywitness+" "+cwd+"/witness.graphml")
		dummywitness=cwd+"/witness.graphml"

	else:
		 dummywitness=dummyWitnessPath+"/dummywitnessfp.graphml"
		 os.system("cp "+dummywitness+" "+cwd+"/witness.graphml")
		 dummywitness=cwd+"/witness.graphml"
	os.system("sed -i 's/<data key=\"programfile\">.*<\/data>/<data key=\"programfile\">"+intask.replace("/","\/")+"<\/data>/g' "+dummywitness)
	os.system("sed -i 's/<data key=\"programhash\">.*<\/data>/<data key=\"programhash\">"+prgmhash+"<\/data>/g' "+dummywitness)	
	return;	
################################################################################################################################
def property_read( propertyfile ):
	print("\nChecking property file..")
	property_contents=open(propertyfile).read()
	if "init(main())" in property_contents and "LTL" in property_contents:
       		if "__VERIFIER_error" in property_contents:
          		return "ReachSafety";
     		elif "overflow" in property_contents:
            		return "Overflow"; 
	        elif "valid-free" in property_contents and "valid-deref" in property_contents and "valid-deref" in property_contents:
			return "memsafety";
	return "unknown";

#################################################################################################################################


def slicer( intask ):
	print("\nSlicing the file ...")
        print("\n"+task)
	slicefile=intask.rsplit('/',1)[1]+"_slice.c"
	os.chdir(slicerpath)


	slicingCmd="./frama-c -no-autoload-plugins -no-findlib -load-module Crude_slicer.cmxs -machdep gcc_x86_64 -crude_slicer -timeout 400 -no-recognize_wrecked_container_of -widening_threshold 2000 -no-summaries -no-assert_stratification -print -ocode "+(outdir)+"/"+slicefile+" "+(intask)+" > "+Logs+"/slice.out 2> "+Logs+"/slice.err"

	#print("\n"+slicingCmd)

	os.system(slicingCmd)

	os.chdir(cwd)
        
        os.system("tail -n 1 "+Logs+"/slice.out >"+Logs+"/slice.tail")
        if "Slicer timed out" in open(Logs+"/slice.tail").read():
            print("\nSlicing timeout..\nUsing original file for analysis")
            return intask;
	if os.path.isfile(outdir+"/"+slicefile):
                os.system("gcc -c -o "+IRs+"/temp"+" "+intask+" >"+Logs+"/gcc.out 2>"+Logs+"/gcc.err")
                if os.path.isfile(Logs+"/gcc.err") and "error:" in open(Logs+"/gcc.err").read():
                    print("\nGCC error present in slice..\nUsing  original file for analysis")
                    return intask;
		os.system("sed -i 's/^#.*//g' "+outdir+"/"+slicefile)
		return outdir+"/"+slicefile;
	else:
		return intask;



##################################################################################################################################

def prepout(intask):
	print("\nPreparing for value analysis..")
	temp=outdir+"/temp.c"
	outfile=outdir+"/out.c"
	os.system("cp "+intask+" "+temp)
	os.system("sed -i 's/^#.*//g' "+temp)
	cppfeCmd=cppfepath+" --edg-w --edg--gcc "+temp+" -C "+cwd+"/supportFiles/csizes_64.txt -O "+IRs+" > "+Logs+"/cppfe.out 2>"+Logs+"/cppfe.err"
	os.system(cppfeCmd)
	if "ERROR In frontend!!" in open(Logs+"/cppfe.out").read():
		temp=temp_task
		cppfeCmd=cppfepath+" --edg-w --edg--gcc "+temp+" -C "+cwd+"/supportFiles/csizes_64.txt -O "+IRs+" > "+Logs+"/cppfe.out 2>"+Logs+"/cppfe.err"
		os.system(cppfeCmd)
	prj=open(outdir+"/"+prjdirname+".prj","a+")
	prjFile=outdir+"/"+prjdirname+".prj"
	prj.write("FILE:\n"+temp+"\nENTRYPOINTS:\nmain")
	prj.close()
	ini=open(irpath+"/env.ini","a+")
	ini.write("__PRISMROOT_="+cwd+"/prism\n__PRISMMODEL_="+cwd+"/prism/Models/UserModel/Lpum.cdf\n__REPOSDIR_="+Repos+"\n__IRREPOSDIR_="+IRs+"\n__MODE_=FLA")
	ini.close()
	javaprep="java -DENVFILE="+ini_path+" -classpath "+jars_path+"/Labtec.jar:"+jars_path+"/antlr.jar:"+jars_path+"/escat.jar:"+jars_path+"/jcupjlex.jar:"+jars_path+"/jdom.jar:"+jars_path+"/prism.jar:"+jars_path+"/slicerWithUnparser.jar:"+jars_path+"/stringtemplate.jar valueAnalysis.SystemsVerificationDriver "+prjFile+" "+Repos+" "+outfile+" > "+Logs+"/java4.out 2>"+Logs+"/java4.err"
	#print("\n"+javaprep)
	os.system(javaprep)
	if "Exception" in open(Logs+"/java4.err").read():
		print("\njava Exceptions found while prepering..")
		return "error";
	return outfile;
	
	
################################################
def clean():
	if os.path.isfile(outdir+"/"+prjdirname+".prj"):
        	os.system("rm "+outdir+"/"+prjdirname+".prj")
	os.system("rm "+Repos+"/"+prjdirname+"/*")
##################################################################################################################################
def RangeAnalysis1( intask ):
	print("\nIR generation...")
	cppfeCmd=cppfepath+" --edg-w --edg--gcc "+intask+" -C "+cwd+"/supportFiles/csizes_64.txt -O "+IRs+" > "+Logs+"/cppfe.out 2>"+Logs+"/cppfe.err"
	
	#print("\n"+cppfeCmd)
	os.system(cppfeCmd)

	if "ERROR In frontend!!" in open(Logs+"/cppfe.out").read():
                os.system(cppfepath+" --edg-w --edg--gcc "+temp_task+" -C "+cwd+"/supportFiles/csizes_64.txt -O "+IRs+" > "+Logs+"/cppfe.out 2>"+Logs+"/cppfe.err")
                if "ERROR In frontend!!" in open(Logs+"/cppfe.out").read():
		    print("\nError in IR generation of ..")
		    return "Unknown";
                else:
                    intask=temp_task


	ini=open(irpath+"/env.ini","a+")
	ini.write("__PRISMROOT_="+cwd+"/prism\n__PRISMMODEL_="+cwd+"/prism/Models/UserModel/Lpum.cdf\n__REPOSDIR_="+Repos+"\n__IRREPOSDIR_="+IRs+"\n__MODE_=FLA")
	ini.close()
	clean()

	prj=open(outdir+"/"+prjdirname+".prj","a+")
	prjFile=outdir+"/"+prjdirname+".prj"
	prj.write("FILE:\n"+intask+"\nENTRYPOINTS:\nmain")
	prj.close()	
	print("\nValue Analysis...")
        PathCnt=10 
        RACmd="timeout -k 2s 500s java -DENVFILE="+ini_path+" -classpath "+jars_path+"/Labtec.jar:"+jars_path+"/antlr.jar:"+jars_path+"/com.microsoft.z3.jar:"+jars_path+"/escat.jar:"+jars_path+"/jcupjlex.jar:"+jars_path+"/jdom.jar:"+jars_path+"/prism.jar:"+jars_path+"/slicerWithUnparser.jar:"+jars_path+"/stringtemplate-1.0.3.jar valueAnalysis.SystemsVerificationDriver "+prjFile+" "+Repos+" "+str(PathCnt)+" false "+enclfunc+" > "+Logs+"/java1.out 2>"+Logs+"/java1.err"
	
        #print("\n"+RACmd)
	os.system(RACmd)
        

	if "Exception" in open(Logs+"/java1.err").read():
		print("\njava Exceptions found..")
		return "Unknown";
        with open(Logs+"/java1.out") as f:
                    jout=f.readlines()
	range_result=jout[-1]	
	return range_result;


####################################################################################################################################

def genCorrectnessWitness(intask , prpfile):
	#current_time=time.time();
	#elapsed_time=round(total_time_start-current_time);
	#remain_time=900-elapsed_time-50	
	WitnessGenCmd="timeout -k 2s 600s "+UApath+"/Ultimate.py --spec "+prpfile+" --architecture 64bit --file "+intask+" --witness-dir "+witnesspath+" > "+Logs+"/witness.out"
	os.system("chmod -R 777 "+UApath)
	pathvar=os.environ["PATH"]
	os.system("export PATH="+pathvar+":"+UApath)
	
	#print("\n"+WitnessGenCmd)
	
        os.system(WitnessGenCmd)

	
        if os.path.isfile(witnesspath+"/witness.graphml"):
		#os.system("cp "+witnesspath+"/witness.graphml "+Logs+"/")
                with open(Logs+"/witness.out") as f:
                    wout=f.readlines()
		print("\nWitness status:"+str(wout[-2:]))
                if "TRUE" in str(wout[-2:]):
		    print("\nWitness generation successful...\n")
                    return "TRUE";
		else:
		    print("\nWitness generation failure...")
		    return "Unknown"
        else:
		print("\nWitness generation failure...")
                return "Unknown";


###################################################################################################################################

def cbmc_verification(intask):
	#import os
	
	with open(enclfunc) as encl:
		enclist=encl.readlines()
	if len(enclist) == 2 :
		return "unknown";
	enclfunc1=enclist[0]	
	#print("\nenclfunc:"+enclfunc1)
	print("\nVerification using cbmc...")
	#print("\npython "+cwd+"/scripts/SysVerFalsify.py "+intask+" "+enclfunc1+" "+Logs+" > "+Logs+"/falsify.out 2> "+Logs+"/falsify.err")
	os.system("python "+cwd+"/scripts/SysVerFalsify.py "+intask+" "+enclfunc1+" "+Logs+" > "+Logs+"/falsify.out 2> "+Logs+"/falsify.err")
	with open(Logs+"/falsify.out") as cbmc_out:
		outdata=cbmc_out.readlines()
	tail=outdata[-1]

        return tail;

###########################################################################################################################

def genFalseWitness(intask , prpfile):

    print("\nGenerating violation witness")

    #return "unknown";

    if not os.path.isfile(CPApath+"/scripts/cpa.sh"):
        print("\ncpa.sh not found..")
        return "error";
    os.system("chmod 777 "+CPApath+"/scripts/cpa.sh")
    WitnessGenCmd="timeout -k 2s 600s "+CPApath+"/scripts/cpa.sh -noout -heap 10000M -predicateAnalysis -64 -setprop cfa.useMultiEdges=false -setprop cpa.predicate.solver=MATHSAT5 -setprop cfa.simplifyCfa=false -setprop cfa.allowBranchSwapping=false -setprop cpa.predicate.ignoreIrrelevantVariables=false -setprop counterexample.export.assumptions.assumeLinearArithmetics=true -setprop coverage.enabled=false -setprop coverage.mode=TRANSFER -setprop coverage.export=true -setprop coverage.file=coverage.info -setprop parser.transformTokensToLines=false -setprop counterexample.export.assumptions.includeConstantsForPointers=false -setprop cpa.arg.errorPath.graphml="+witnesspath+"/witness.graphml -spec "+prpfile+" "+intask+" > "+Logs+"/witness.out 2>&1"
    #print("\n"+WitnessGenCmd)

    os.system(WitnessGenCmd)


    if os.path.isfile(Logs+"/witness.out"):
        os.system("tail -n 3 "+Logs+"/witness.out > "+Logs+"/witness.tail")
        if "Verification result: FALSE" in open(Logs+"/witness.tail").read():
	       if os.path.isfile(witnesspath+"/witness.graphml.gz"):
        	        os.system("gunzip "+witnesspath+"/witness.graphml.gz -f")
			#os.system("cp "+witnesspath+"/witness.graphml "+irpath)
            		return "failed";
    return "unknown";


###################################################################################################################################

def RangeAnalysis2(intask):
	cppfeCmd=cppfepath+" --edg-w --edg--gcc "+intask+" -C "+cwd+"/supportFiles/csizes_64.txt -O "+IRs+" > "+Logs+"/cppfe.out 2>"+Logs+"/cppfe.err"
	
	#print("\n"+cppfeCmd)
	os.system(cppfeCmd)
	clean()	
	prj=open(outdir+"/"+prjdirname+".prj","a+")
	prjFile=outdir+"/"+prjdirname+".prj"
	prj.write("FILE:\n"+intask+"\nENTRYPOINTS:\nmain")
	prj.close()	
	print("\nEnhanced Value Analysis...")
        PathCnt=100
        RACmd="timeout -k 2s 450s java -DENVFILE="+ini_path+" -classpath "+jars_path+"/Labtec.jar:"+jars_path+"/antlr.jar:"+jars_path+"/com.microsoft.z3.jar:"+jars_path+"/escat.jar:"+jars_path+"/jcupjlex.jar:"+jars_path+"/jdom.jar:"+jars_path+"/prism.jar:"+jars_path+"/slicerWithUnparser.jar:"+jars_path+"/stringtemplate.jar valueAnalysis.SystemsVerificationDriver "+prjFile+" "+Repos+" "+str(PathCnt)+" true > "+Logs+"/java2.out 2>"+Logs+"/java2.err"
        #print("\n"+RACmd)
	os.system(RACmd)
        

	if "Exception" in open(Logs+"/java2.err").read():
		print("\njava Exceptions found..")
		return "Unknown";
        with open(Logs+"/java2.out") as f:
                    jout=f.readlines()
	range_result=jout[-1]
	return range_result;
	
###################################################################################################################################
	
def transformBeforeSlicing(infile):
	cmd=" java -classpath "+jars_path+"/Labtec.jar valueAnalysis.SysVerTransformcode "+infile+" "+trnsdorgi+" 0 >"+Logs+"/java3.out 2>&1"
	#print("\n"+cmd)
	os.system(cmd)
	return;
##################################################################################################################################
def transformAfterSlicing(infile):
	cmd=" java -classpath "+jars_path+"/Labtec.jar valueAnalysis.SysVerTransformcode "+infile+" "+trnsdslice+" 1 >"+Logs+"/java4.out 2>&1"
	#print("\n"+cmd)
	os.system(cmd)
	return;


###################################################################################################################################
if property_read(propertyfile=propertyfile) == "ReachSafety":
	t1=time.time()
	cur_task=slicer(intask=temp_task)
	print("\n slice-out file:"+cur_task)
	slicedtask=cur_task
	t2=time.time()
	print("\n Slicing time:"+str(round(t2-t1)))
	t1=time.time()
	prptask=prepout(intask=cur_task)
	t2=time.time()
	print("\n Preprocessed File : "+prptask)
	if not prptask=="error":
		print("\n Preprocessing is done..\nThe time taken for preprocessing is "+str(round(t2-t1)))
		cur_task=prptask
	t1=time.time()
	t1=time.time()
	range_result=RangeAnalysis1(intask=cur_task)
	#print("java logs getting saved")
	t2=time.time()
	print("\n Value Analysis time:"+str(round(t2-t1)))
	if "RangeAnalysis-Result-TRUE" in range_result:
		print("\n"+success_str)
		t1=time.time()
                witness_res=genCorrectnessWitness(intask=task , prpfile=propertyfile)
		t2=time.time()
		if witness_res=="Unknown":
			print("1UAWitness generation failure")
			t1=time.time()
	                genDummyWitness(task,"true")
        	        t2=time.time()
	        	print("1testd generation time:"+str(round(t2-t1)))

		else:
			print("\n1correctness witness generation time:"+str(round(t2-t1)))
			
	else:
		t1=time.time()
		result=RangeAnalysis2(intask=cur_task)
		t2=time.time()
		print("\n Enhanced Value Analysis time:"+str(round(t2-t1)))
		if "RangeAnalysis-Result-TRUE" in result:
			print("\n"+success_str)
			t1=time.time()
			witness_res=genCorrectnessWitness(intask=task , prpfile=propertyfile)
			t2=time.time()
			print("2Witness generation time:"+str(round(t2-t1)))
			if witness_res=="Unknown":
				print("2UAWitness generation failure")
				t1=time.time()
				genDummyWitness(task,"true")
				t2=time.time()
				print("2testd generation time:"+str(round(t2-t1)))

			else:
				print("\n2witness saved at "+witnesspath+"/witness.graphml")
		else:
			t1=time.time()
			transformBeforeSlicing(infile=temp_task)
			slicedtask=slicer(intask=trnsdorgi)
			transformAfterSlicing(infile=slicedtask)
			cbmc_result=cbmc_verification(intask=trnsdslice)
			t2=time.time()
			print("CBMC Verification time:"+str(round(t2-t1)))
			if failed_str in cbmc_result:
				print("\n"+cbmc_result)
				t1=time.time()
				FalseWitnessStat=genFalseWitness(intask=task,prpfile=propertyfile)
				t2=time.time()
				if FalseWitnessStat=="failed":
					print("False Witness generation time:"+str(round(t2-t1)))
					if os.path.isfile(witnesspath+"/witness.graphml"):
						print("\nwitness saved at "+witnesspath+"/witness.graphml")
				else:
					print("CPAWitness generation failure")
					t1=time.time()
					genDummyWitness(task,"false")
					t2=time.time()
					print("testd generation time:"+str(round(t2-t1)))
			else:
				print("\n"+unknown_str)
else:	
	print("\n"+unknown_str)
