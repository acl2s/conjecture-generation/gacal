#!/usr/bin/perl -w
use strict;
use Scalar::Util qw(looks_like_number);
use File::Basename;
#use Digest::SHA1 qw(sha1_base64);
#=======================================================================
# This scipt takes four parameter, compile a c program, execute it and
# report a crash, if finds one.
# input parameters : 1. C program with full (absolute) path
#                    2. timeout in seconds
#                    3. Absolute path of output directory where the witness file can be found
#          (Optional)4. A parameter indicating whether to use GNU or clang
#          (Optional)5. Input seed file directory. No logic or check is added for this.
# Assumptions : 1. Environment variable $LABTECROOT is to tools root directory
#               2. $HOME/ar_abs_temp directory is available as working directory
# Caution : No extensive validation is done on the input parameters
#=======================================================================
my $bTime = time();
if ($#ARGV < 2){
    print "Usage: requires C program,timing parameter, output \n";
    exit 12;
}

if ($#ARGV > 4){
    print "Usage: requires C program, timing, output directory, rt no] to run \n";
    exit;
}    
#defualt timing .
if(! -e $ARGV[0]){
    print "Source file is missing $ARGV[0]";
    exit 8;
} 

my $LABTECROOT      = $ENV{'LABTECROOT'};
my $histDir         = $ENV{'HOME'};
my $witness         = $ENV{'WITNESS_FILENAME'};
my $WORKDIR         = "$histDir/ar_abs_temp";
my $inDir           = "$LABTECROOT/exp-in";
my $outDir          = $ARGV[2];
my $logDir          = "$WORKDIR/exp-log";
my $runlog          = "$logDir/run.log";
my $runStatuslog    = "$logDir/runStatus.log";
my $LIBDIR          = "$LABTECROOT/lib";
my $AFLFUZZ         = "$LABTECROOT/afl-2.35b/afl-fuzz";
my $AFLBUILD        = "$LABTECROOT/afl-2.35b/afl-gcc";
my $CPUCDCHECK      = "$LABTECROOT/lib/cpudumpc";

if($#ARGV > 2 && $ARGV[3] eq "clang" ){
    $AFLBUILD       = "$LABTECROOT/afl-2.35b/afl-clang-fast";
}
my $CFLAGS="-m32";
my $mod = "";
my $pgminp = "";
my $srcDir = "";
my $suff   = "";
my $binDir = "$WORKDIR/bin";
my $TMPDIR = "$WORKDIR/tmp";
#------- Timing vars -------------#
my $timeout = 180;
if($#ARGV > 1){
   if(looks_like_number($ARGV[1])){
       $timeout=$ARGV[1];
   }else{
       print "The timing is not numeric. 180s assumed";
   }
}
# Compile timing  
my $comptime = 0;
my $exectime  = 0;

#Does the input file exist ?
if(! -e "$ARGV[0]"){
    print "Source c file does not exist. Not much to do, exiting\n";
    exit 24;
}

($mod,$srcDir,$suff)=fileparse($ARGV[0]);
$mod=$mod."_AFL" ;
if(! -e "$binDir"){
   `/bin/mkdir $binDir`;
   print "Create $binDir \n";  
}

# Check directories and create if they don't exist.
if (! -e "$TMPDIR"){
   `/bin/mkdir $TMPDIR`;
   print "Temporary directory for storing the AFL build\n";
}elsif( -e "$TMPDIR/tmp.o"){
    # Clean temp directory 
    `/bin/rm $TMPDIR/*.*`;
}
if(! -e "$inDir"){
    print "Input directory AFL run does not exist !!!\n";
    exit 8;
}    

if (! -e "$outDir"){
    print "Output directory for storing the AFL run does not exist !!!\n";
    exit 8;
}    
    
if (! -e "$logDir") {
    `/bin/mkdir $logDir`;
    print "Create Log directory $logDir\n";
}

# Is witness file is available ? if not, set the default witness file.
if(length($witness) == 0){
    $witness = "$WORKDIR/Witness.graphml";
    $ENV{'WITNESS_FILENAME'} = $witness;
}
# define crash dir for storing the latest crash file. Idea is that, if a crash file exisit in this directory, then there is a true crash. Otherwise, it is false positive due to segmentation fault.
my$crashdir = "$WORKDIR/crash_dir";
if(! -e "$crashdir"){
    `/bin/mkdir  $crashdir`;
    print "Created crash directory $crashdir\n";
}
$ENV{'CRASH_DIR'} = "$crashdir";
#---------- Open Log file ------------------------------------
open LF, ">$runlog" or die "could not open $runlog";
#---------- Compile program. But record the time -------------

my $compile="";
if($CFLAGS eq "-m32"){
    $compile="AFL_QUIET=1 $AFLBUILD -m32 -o $TMPDIR/tmp.o -c $ARGV[0]";
}else{
    $compile="AFL_QUIET=1 $AFLBUILD -o $TMPDIR/tmp.o -c $ARGV[0]";
}
    
my $rc = system($compile);
if($rc ne 0){
    printf "Compilation failed \n";
    exit 12;
}
    
print "Compilation of module is done $ARGV[0] \n";
if(! -e "$TMPDIR/tmp.o"){
    print "unable to create load object $TMPDIR/tmp.o\n";
    exit 12;
}

my $lnkedit = "";
if($CFLAGS eq "-m32"){ 
    $lnkedit = "AFL_QUEIT=1 $AFLBUILD -m32 -o $binDir/$mod $TMPDIR/tmp.o $LIBDIR/svdrvr32.o";
}else{
    $lnkedit = "AFL_QUEIT=1 $AFLBUILD -o $binDir/$mod $TMPDIR/tmp.o $LIBDIR/svdrvr.o";
}
    
$rc = system($lnkedit);
if($rc ne 0){
    printf "Linking failed \n";
    exit 12;
}

print "Linkedit of module is done \n";
if(! -e "$binDir/$mod"){
    print "unable to create load object\n";
    exit 12;
}
$comptime = time() - $bTime;

if($comptime > $timeout){
    print "No time left for execution $comptime $timeout \n";
    exit 12;	
}
# Reserve 2 seconds of time for witness generation
$exectime = $timeout - $comptime - 2;
#----------- Running AFL -----------------------------
my $cmd = ""; 
print "Running with AFL $AFLFUZZ ....\n";
#my $crash_opt = "AFL_BENCH_UNTIL_CRASH=1";
my $crash_opt = "";
my $cpu_opt = "";
my $core_dump_opt = "";
#---------- Set flags correctly ---------------------
if( -e "$CPUCDCHECK"){
    #Need to enhance AFL_SKIP_CPUFREQ=1
    system("$CPUCDCHECK -c");
    if($? ne 0){
	$cpu_opt = "AFL_SKIP_CPUFREQ=1";
    }
    #Need to enhance AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES
    system("$CPUCDCHECK -D");
    if($? ne 0){
	$core_dump_opt = "AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1";
    }
}else{
    # Be safe to run AFL. The CPU and core_dump checks are not done; The below options gurantees
    # AFL run 
    $cpu_opt = "AFL_SKIP_CPUFREQ=1";
    $core_dump_opt = "AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1";
}

#Set crash_opt ON
if($#ARGV > 3 && $ARGV[4] eq "crashon" ){
    $crash_opt = "AFL_BENCH_UNTIL_CRASH=1";
}
$cmd = "$crash_opt $cpu_opt $core_dump_opt timeout $exectime $AFLFUZZ -i $inDir -o $outDir/$mod $binDir/$mod > $logDir/$mod.log 2> $logDir/$mod-err.log";

# Print fuzzer command
print "$cmd \n";
# Run the fuzzer
$rc = system($cmd);
#------------------Print LOG INFO OF AFL---------------------
#print "\nAFL LOG\n\n";
#my $usageFile = "$logDir/$mod.log";
#open(USAGEFILE,$usageFile);
#while(<USAGEFILE>)
#{
#	print $_;
#}
#close(USAGEFILE);
#------------------ Post processing ------------------------
my $fstat = "$crashdir/crash_001";
my $uniqcrash = "";
#If crash file exists, then there is a true crash.
if(-e $fstat){
    # Store the Source name in environment variable 
    $ENV{'WITNESS_SRC'} = $ARGV[0];
    # Generate SHA1 for the source program and store it in an evironment varaible
    my $shasum = `sha1sum $ARGV[0]`;
    my @words = split / /,$shasum;
    my $shvalue = "$words[0]";
    $ENV{'WITNESS_SHA'}= "$shvalue";
    #---------------- Witness creation ---------------------
    open WIT_DATA, "<", "$ARGV[0]"  or die " Can not open source code file for reading : $!";
    my $modifiedPgm = "$TMPDIR/$mod"."_modified.c";
    # Transform the  source program to include __LINE__ and __FUNCTION__
    open MOD_PGM, ">", "$modifiedPgm" or die " Can not open source code file for writing : $!";
    my $funcNm = "const char *";
    while(<WIT_DATA>){
	if(index($_,"__VERIFIER_nondet_") != -1){
	    if(index($_,"extern ") != -1){
		    s/__VERIFIER_nondet_(\w+)\(void\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
		    s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
	    }
	    else{
	        if(index($_," = ") != -1){
	           s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		elsif(index($_,"=") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
	        elsif(index($_,"return ") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		elsif(index($_,"while") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		elsif(index($_,"if") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		else{
			s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
		}
		s/__VERIFIER_nondet_(\w+)\(void\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
	    }
	}
	print MOD_PGM $_;	
    }    
    close WIT_DATA or warn $! ? "Error in closing file ": "Exit status $? from infile";
    close MOD_PGM  or warn $! ? "Error in closing file ":"Exit status $? from outfile";
    # Regenerate binary of the program with Witness generation module
    if($CFLAGS eq "-m32"){ 
	    #$lnkedit = "gcc -m32 -o $TMPDIR/$mod $TMPDIR/$modifiedPgm  $LIBDIR/witsvinp32.o";
	    $lnkedit = "gcc -m32 -o $TMPDIR/$mod $modifiedPgm  $LIBDIR/witsvinp32.o";
    }else{
	    #$lnkedit = "gcc -o $TMPDIR/$mod  $TMPDIR/$modifiedPgm $LIBDIR/witsvinp.o";
	    $lnkedit = "gcc -o $TMPDIR/$mod  $modifiedPgm $LIBDIR/witsvinp.o";
    }
	#print "\n $lnkedit";
    $rc = system($lnkedit);
    if($rc ne 0){
	printf "Linking failed while generating witness based test input\n";
	exit 12;
    }
    #Run the program
    if( -e "$TMPDIR/$mod"){
	`$TMPDIR/$mod < $fstat`;
	# If the witness is generated, then it is a false program. This accounts of any compilation errors.
	if( -e $witness){
	    open LF_STATUS, ">$runStatuslog" or die "could not open $runStatuslog";
	    print LF "$mod FALSE TRUE $uniqcrash \n";
	    print LF_STATUS "FALSE TRUE $uniqcrash \n";
	    print "Witness file $witness generated successfully";
	}else{
	    print LF "$mod FALSE UNKNOWN \n";
	}
    }else{
	print "Compilation errors.. program did not compile...";
	print LF "$mod FALSE UNKNOWN  \n";
    }
}else{
    #print "$fstat file does not exist ..jumping to next one \n";
    print LF "$mod FALSE UNKNOWN \n";
}
close LF  or warn $! ? "Error in closing file " : "Exist status $? from logfile";
exit 0;
