#!/bin/bash
#$1: Source file

OUTDIR=$LABMCPROJDIR/InfTraceBackward
declare -a successfulAsserts

assert="__CPROVER_assert"

#Get All the successful asserts from CBMC trace
#$1: Source file
function getAllSuccessfulAsserts()
{
	successfulAsserts=($(grep -w "SUCCESS" "$OUTDIR/cbmcTrace1_$1.txt" | awk '{print $2}' | awk -F':' '{print $1}'))
}
#Remove successful asserts for further analysis
#$1: Source file
function removeAsserts()
{	
	length=${#successfulAsserts[@]}
        if [ $length -eq 0 ]
        then
		echo "InfLoop Bw: Nothing to remove"
                exit 10;
        fi
	for goal in "${successfulAsserts[@]}"
        do
		if ( echo "$goal" | grep -q "Goal"  || echo "$goal" | grep -q "Error" )
		then
			echo "InfLoop Bw: Removing: $goal"
			sed -i /"$goal"/d "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
		fi
	done
}

getAllSuccessfulAsserts $1
removeAsserts $1
