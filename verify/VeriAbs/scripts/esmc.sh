#!/bin/bash
#$1: source file
JARDIR=$LABTECROOT/jars
RAWEXECOUTDIR=$LABMCPROJDIR/InfTraceRawExec
MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini
MYPRJFILE=$LABMCPROJDIR/Input/$1.prj
ESMCPRJFILE=$LABMCPROJDIR/Input/$1_esmc.prj

#if [ ! -d "$OUTDIR" ]
#then
	mkdir $RAWEXECOUTDIR
#fi

cp "$MYPRJFILE" "$ESMCPRJFILE"

# Making folder in IRs directory
if [ ! -d "$LABMCPROJDIR/IRs/$1_esmc" ];
then
	mkdir $LABMCPROJDIR/IRs/$1_esmc
fi

#jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"
jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"

# Append printf statements to identify conditions uniquely
java -cp $jars -Xms512m -Xmx$MAXHEAPSIZE -DENVFILE="$MYENVFILE" infLoop.Driver $RAWEXECOUTDIR $ESMCPRJFILE -esmc > $RAWEXECOUTDIR/trace_$1.txt 2>&1
rc=$?
if [[ $rc -gt 0 ]]
then
        echo "InfLoop ESMC: error in 1st unparsing: Exiting"
        exit 10;
fi
echo "InfLoop ESMC: 1st Unparsing done"

# Adding header file
sed -i '1i #include<stdio.h>' "$RAWEXECOUTDIR/Unparse_$1.c"
echo "InfLoop ESMC: header file added"
# Determining whether program has range variable
if ! grep -q -w "Range Variable" "$RAWEXECOUTDIR/Unparse_$1.c"
then
	rangeIndicator=0
else
	rangeIndicator=1
fi
cp $LABTECROOT/supportFiles/driver.c $RAWEXECOUTDIR/driver.c
gcc $RAWEXECOUTDIR/Unparse_$1.c -o $RAWEXECOUTDIR/Unparse_$1.o
rc=$?
if [[ $rc -gt 0 ]]
then 
	echo "InfLoop ESMC: error while compilation: Exiting"
	exit 10;
fi
echo "InfLoop ESMC: Compilation done"

nondetLineNo=$(grep "Nondet line number" $RAWEXECOUTDIR/trace_$1.txt | awk '{print $4}')
errorLineNo=$(grep "Error line number" $RAWEXECOUTDIR/trace_$1.txt | awk '{print $4}')

#Adding witness header
curr=$(pwd)
echo "Current: $curr"
cat $LABTECROOT/supportFiles/InfLoopViolationWitnessHeader.txt > witness1.graphml

shaKey=$(sha1sum $HOME/ar_abs_temp/$1.c | awk '{print $1}')
#echo "$shaKey"
timeout -k 2s 100s $RAWEXECOUTDIR/Unparse_$1.o "$HOME/ar_abs_temp/$1.c" $nondetLineNo $errorLineNo $rangeIndicator > $RAWEXECOUTDIR/exec_$1.txt $shaKey
#timeout -k 2s 100s $RAWEXECOUTDIR/Unparse_$1.o $nondetLineNo $errorLineNo 1> $RAWEXECOUTDIR/exec_$1.txt
#$RAWEXECOUTDIR/Unparse_$1.o > $RAWEXECOUTDIR/exec_$1.txt

if grep -q "No Error" $RAWEXECOUTDIR/exec_$1.txt
then
	exit 2;
elif grep -q "Error found" $RAWEXECOUTDIR/exec_$1.txt
then
	#cp "witness1.graphml" "$LABTECROOT/witnessgendir/new_$1-witness.graphml"
	mv "witness1.graphml" "$LABTECROOT/witness.graphml"
	exit 3;
else
	exit 4;
fi
