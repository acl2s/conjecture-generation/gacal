#!/bin/bash
SCRIPTSDIR=$LABTECROOT/scripts
OUTDIR=$LABMCPROJDIR/InfTrace
JARDIR=$LABTECROOT/jars
MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini

# Execute the program against AFL generated inputs
$SCRIPTSDIR/computeOutput.py --test ~/ar_abs_temp/Preprocess_Unparse_$1.c_AFL/queue/ --exe ~/ar_abs_temp/bin/Preprocess_Unparse_$1.c_AFL --dest $OUTDIR/traceFile_$1.txt
#echo "InfLoop Frwd: Python script completed"

# Get line numbers of unreached conditions
$SCRIPTSDIR/linesunrea.sh $1
rc=$?;
if [[ $rc == 10 ]]
then
	echo "InfLoop Frwd: Return code 10 from postprocess: Exiting"
	exit 10
fi
echo "InfLoop Frwd: Line numbers dumped"

# Extract variables
#awk '/Variables end/{f=0}; f; /Variables start/{f=1}' "$OUTDIR/trace_$1.txt" > "$OUTDIR/vars_$1.txt"

# Range of variables
$SCRIPTSDIR/assumeRange_v2.sh $1
echo "InfLoop Frwd: Range analysis completed"

# Preprocess unparsed file before generating IRs
#gcc -m32 -E "$OUTDIR/Unparse_$1.c" > "$OUTDIR/Preprocess_Unparse_$1.c"
#echo "InfLoop: Preprocessing of unparsed file completed"

# Generate IRs for unparsed file
#$LABTECROOT/bin/cppfe --edg--m --edg-w --edg--gcc --edg--c99 -O "$LABMCPROJDIR/IRs" "$OUTDIR/Preprocess_Unparse_$1.c"
#echo "InfLoop: IRs for unparsed file generated" 

# Making folder in IRs directory
#if [ ! -d "$LABMCPROJDIR/IRs/$1_newfrwd" ];
#then
#	mkdir $LABMCPROJDIR/IRs/$1_newfrwd
#fi

# Make prj file for new Prism session
#cp $LABMCPROJDIR/Input/$1.prj $LABMCPROJDIR/Input/$1_newfrwd.prj

# Path of preprocessed unparsed file
#preprocessedUnparsedFilePath="$OUTDIR/Preprocess_Unparse_$1.c"

# Changing file path in new prj file
#sed -i "/FILE/!b;n;c$preprocessedUnparsedFilePath" $LABMCPROJDIR/Input/$1_newfrwd.prj
rm $LABMCPROJDIR/IRs/$1_newfrwd/*
# Invocation of Prism session for inserting assumes and asserts
#jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"
jars="$JARDIR/Labtec.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:$JARDIR/jcupjlex.jar"

java -cp $jars -Xms512m -Xmx$MAXHEAPSIZE -DENVFILE="$MYENVFILE" infLoop.Driver $OUTDIR $LABMCPROJDIR/Input/$1_newfrwd.prj -assume "$OUTDIR/nonreachable_$1.txt" "$OUTDIR/lineNumbers_$1.txt" "$OUTDIR/rangeVariables_$1.txt" > $OUTDIR/trace_$1.txt 2>&1
#java -cp $jars -DENVFILE="$MYENVFILE" infLoop.Driver $OUTDIR $LABMCPROJDIR/Input/$1_new.prj -f "$OUTDIR/lineNumbers_$1.txt" "$OUTDIR/rangeVariables_$1.txt" > $OUTDIR/trace_$1.txt 2>&1
rc=$?
if [[ $rc -gt 0 ]]
then
	echo "InfLoop Frwd: Error in 2nd parsing: Exiting"
	exit;
fi
echo "InfLoop Frwd: 2nd Unparsing done"

#Run CBMC till all assertions are successful
currentTime=$(date +%s)
timeElapsed=$(($currentTime-$2))
remainingTime=$((800-$timeElapsed))
#echo "InfLoop Frwd: Remaining time from postprocess script: $remainingTime"
$SCRIPTSDIR/analyzeCbmcTrace_v1.sh $1 $remainingTime
rc=$?;
if [[ $rc == 8 ]]
then
        echo "InfLoop Frwd: CBMC not scalable"
        exit;
fi
