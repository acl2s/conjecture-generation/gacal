#!/bin/bash
#$1: File name
OUTDIR=$LABMCPROJDIR/InfTraceBackward

#getting non-reachable conditions
totalCond=$(grep "Cond" "$OUTDIR/Unparse_$1.c" | wc -l)
echo "InfLoop Bw: Total cond: $totalCond"
for i in $(seq 1 $totalCond);
do
#        if ! grep -q -w "$i" "$OUTDIR/reachable_$1.txt"; then
                echo "$i" >> "$OUTDIR/nonreachabletest_$1.txt"
		awk "/Cond: $i.\)/{print NR}" "$OUTDIR/Unparse_$1.c" >> "$OUTDIR/lineNumberstest_$1.txt"
 #       fi
done

