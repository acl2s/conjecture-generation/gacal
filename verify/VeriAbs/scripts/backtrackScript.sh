#!/bin/bash
#$1: Source file
#$2: Remaining time

OUTDIR=$LABMCPROJDIR/InfTraceBackward
CBMCPATH=$LABTECROOT/bin/cbmc
CBMCPATHDEFAULT=$LABTECROOT/bin/cbmc
#OUTDIR="/home/u1357043/ar_abs_temp_05_05/Problem05_label05_true_unreach_call_c/InfTrace"
SCRIPTSDIR=$LABTECROOT/scripts
#SCRIPTSDIR="/home/u1357043/gitrepos/Labtec/svcomp/VeriAbs/scripts"
declare -a successfulAsserts
i=0

function executeBaseCase()
{
	if grep -q "Range" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
	then
		#echo "---CBMC: timeout -k 2s $remainingTime $CBMCPATH -DbaseCase --z3 "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace"
		timeout -k 2s $remainingTime $CBMCPATHDEFAULT -DbaseCase --z3 "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace1_$1.txt"
	else
		timeout -k 2s $remainingTime $CBMCPATH -DbaseCase "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace1_$1.txt"
	fi	
	rc=$?
	return $rc
}

function executeInductionStep()
{
	if grep -q "Range" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
	then
		#echo "---CBMC: timeout -k 2s $remainingTime $CBMCPATH --z3 "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace"
		timeout -k 2s $remainingTime $CBMCPATHDEFAULT --z3 "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace1_$1.txt"
	else	
		timeout -k 2s $remainingTime $CBMCPATH "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace1_$1.txt"
	fi	
	rc=$?
	return $rc
}

remainingTime=$2
i=1
echo "Total remaining time: $remainingTime"

startTime=$(date +%s)
executeBaseCase $1
rc=$?
endTime=$(date +%s)
diffTime=$(($endTime-$startTime))
echo "Base case: Time taken by CBMC in iteration $i: $diffTime"
remainingTime=$(($remainingTime-$diffTime))

startTime=$(date +%s)
if [[ $rc -eq 0 ]]
then
	endTime=$(date +%s)
	diffTime=$(($endTime-$startTime))
	remainingTime=$(($remainingTime-$diffTime))
	echo "Remaining time for CBMC: $remainingTime"
	if [[ $remainingTime -le 0 ]]
	then
		echo "InfLoop Bw: Timeout"
		exit;
	fi

	startTime=$(date +%s)
	i=1;
	executeInductionStep $1
	rc=$?
	endTime=$(date +%s)
	diffTime=$(($endTime-$startTime))
	#echo "Induction step: Time taken by CBMC in iteration $i: $diffTime"

	remainingTime=$(($remainingTime-$diffTime))
	startTime=$(date +%s)
	if [[ $rc -eq 0 ]]
	then
		echo "InfLoop Bw: VERIABS_VERIFICATION_SUCCESSFUL"
		exit;	
	fi
	while [[ $rc -ne 0 ]]
	do
		totalAsserts=$(grep "assert" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" | wc -l)

		$SCRIPTSDIR/removeAsserts.sh $1
		rc_remove=$?
		$SCRIPTSDIR/insertAssumeAssert.sh $1
		rc_insert=$?
		
		if [[ $rc_remove -eq 10 && $rc_insert -eq 10 ]]
		then
			echo "InfLoop Bw: Infinite iterations of CBMC: Exiting"
			exit 8;	
		fi	
		totalAsserts=$(grep "assert" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" | wc -l)
		i=$(($i+1));
		echo "Total asserts before $i run: $totalAsserts"
		endTime=$(date +%s)
		diffTime=$(($endTime-$startTime))
		remainingTime=$(($remainingTime-$diffTime))

		if [[ $remainingTime -le 0 ]]
		then
			echo "InfLoop Bw: Timeout"
			exit;
		fi
		startTime=$(date +%s)
		executeBaseCase $1
		rc=$?
		endTime=$(date +%s)
		diffTime=$(($endTime-$startTime))
		#echo "Base case: Time taken by CBMC in iteration $i: $diffTime"

		remainingTime=$(($remainingTime-$diffTime))
		startTime=$(date +%s)
		if [[ $rc -eq 0 ]]
		then
			endTime=$(date +%s)
			diffTime=$(($endTime-$startTime))
			remainingTime=$(($remainingTime-$diffTime))
		#	echo "Remaining time for CBMC: $remainingTime"

			if [[ $remainingTime -le 0 ]]
			then
				echo "InfLoop: Timeout"
				exit;
			fi
			startTime=$(date +%s)
			executeInductionStep $1
			rc=$?
			endTime=$(date +%s)
			diffTime=$(($endTime-$startTime))
		#	echo "Induction step: Time taken by CBMC in iteration $i: $diffTime"

			remainingTime=$(($remainingTime-$diffTime))
			startTime=$(date +%s)
			if [[ $rc -eq 0 ]]
			then
				echo "InfLoop Bw: VERIABS_VERIFICATION_SUCCESSFUL"
				exit;
			fi
		else
			#			echo "InfLoop: VERIABS_UNKNOWN"
			exit 10;
		fi
	done
else
	#	echo "InfLoop: VERIABS_UNKNOWN"
	exit 10;
fi
