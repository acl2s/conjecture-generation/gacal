#!/bin/bash
#$1: source file
JARDIR=$LABTECROOT/jars
PLESMCOUTDIR=$LABMCPROJDIR/plm
#MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini
#MYPRJFILE=$LABMCPROJDIR/Input/$1.prj
#ESMCPRJFILE=$LABMCPROJDIR/Input/$1.prj

#if [ ! -d "$OUTDIR" ]
#then
#	mkdir $PLESMCOUTDIR
#fi

#cp "$MYPRJFILE" "$ESMCPRJFILE"

# Making folder in IRs directory

#Removing dump files
#rm $LABMCPROJDIR/IRs/$1/*

#jars="$JARDIR/productLinesTrial.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"
#jars="$JARDIR/productLinesTrial.jar:$JARDIR/jcupjlex.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"
# Append printf statements to identify conditions uniquely
#java -cp $jars -DENVFILE="$MYENVFILE" productLines.ProductLinesUnparserDriver $PLESMCOUTDIR $MYPRJFILE > $PLESMCOUTDIR/trace_$1.txt 2>&1
#rc=$?

#8 for unknown result, 10 for unparsing error
#if [[ $rc -eq 2 ]]
#then
#        echo "PL: VERIABS_UNKNOWN"
#        exit 8;
#elif [[ $rc -gt 0 ]]
#then
#	echo "PL: error in 1st unparsing (ESMC): Exiting"
#	exit 10;
#fi
#echo "PL: 1st Unparsing done"

cp $LABTECROOT/supportFiles/driver_PL.c $PLESMCOUTDIR/driver_PL.c
cp $LABTECROOT/supportFiles/initialize.c $PLESMCOUTDIR/initialize.c
cp $LABTECROOT/supportFiles/whileLogic.c $PLESMCOUTDIR/whileLogic.c
cp $LABTECROOT/supportFiles/endWhile.c $PLESMCOUTDIR/endWhile.c

gcc -m32 $PLESMCOUTDIR/Unparse_$1.c -o $PLESMCOUTDIR/Unparse_$1.o > $PLESMCOUTDIR/compilation_$1.txt 2>&1
rc=$?
if [[ $rc -gt 0 ]]
then 
	echo "PL: error while compilation (ESMC): Exiting"
	exit 11;
fi
echo "PL: Compilation done (ESMC)"

timeout -k 2s 100s $PLESMCOUTDIR/Unparse_$1.o > $PLESMCOUTDIR/exec_$1.txt

if grep -q -w "CORRECT PROGRAM" $PLESMCOUTDIR/exec_$1.txt
then
	exit 6;
elif grep -q -w "ERROR FOUND" $PLESMCOUTDIR/exec_$1.txt
then
	exit 7;
else
	exit 12;
fi
