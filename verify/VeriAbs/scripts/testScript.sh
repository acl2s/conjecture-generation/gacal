#!/bin/bash
#$1: source file
#$2: VeriAbs start time

JARDIR=$LABTECROOT/jars
OUTDIR=$LABMCPROJDIR/InfTraceBackward
MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini
MYPRJFILE=$LABMCPROJDIR/Input/$1.prj
SCRIPTSDIR=$LABTECROOT/scripts
#echo "Entered testScript.sh"
#if [ ! -d "$OUTDIR" ]
#then
#	mkdir $OUTDIR
#fi

#jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"

# Append printf statements to identify conditions uniquely
#java -cp $jars -DENVFILE="$MYENVFILE" infLoop.Driver $OUTDIR $MYPRJFILE -print > $OUTDIR/trace_$1.txt 2>&1
#rc=$?
#if [[ $rc -gt 0 ]]
#then
#        echo "InfLoop Backward: error in 1st parsing: Exiting"
#        exit;
#fi
#echo "InfLoop Backward: 1st Unparsing done"

# Adding header file
#sed -i '1i #include<stdio.h>' "$OUTDIR/Unparse_$1.c"
#echo "InfLoop Backward: header file added"

$SCRIPTSDIR/allPrints.sh $1
echo "InfLoop Bw: Line numbers dumped"

$SCRIPTSDIR/discreteValues.sh $1
echo "InfLoop Bw: Range analysis done"

# Preprocess unparsed file before generating IRs
#gcc -m32 -E "$OUTDIR/Unparse_$1.c" > "$OUTDIR/Preprocess_Unparse_$1.c"
#echo "InfLoop: Preprocessing of unparsed file completed"

# Generate IRs for unparsed file
#$LABTECROOT/bin/cppfe --edg--m --edg-w --edg--gcc --edg--c99 -O "$LABMCPROJDIR/IRs" "$OUTDIR/Preprocess_Unparse_$1.c"
#echo "InfLoop: IRs for unparsed file generated"

# Making folder in IRs directory
mkdir $LABMCPROJDIR/IRs/$1_new

# Make prj file for new Prism session
cp $LABMCPROJDIR/Input/$1.prj $LABMCPROJDIR/Input/$1_new.prj

# Path of preprocessed unparsed file
preprocessedUnparsedFilePath="$OUTDIR/Preprocess_Unparse_$1.c"

# Changing file path in new prj file
sed -i "/FILE/!b;n;c$preprocessedUnparsedFilePath" $LABMCPROJDIR/Input/$1_new.prj

# Invocation of Prism session for inserting assumes and asserts
#jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/log4j-1.2-api-2.0-beta8.jar:$JARDIR/log4j-api-2.0-beta8.jar:$JARDIR/log4j-core-2.0-beta8.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"
jars="$JARDIR/Labtec.jar:$JARDIR/jcupjlex.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:"

#java -cp $jars -DENVFILE="$MYENVFILE" infLoop.Driver $OUTDIR $LABMCPROJDIR/Input/$1_new.prj -f "$OUTDIR/nonreachable_$1.txt" "$OUTDIR/lineNumbers_$1.txt" "$OUTDIR/rangeVariables_$1.txt" > $OUTDIR/trace_$1.txt 2>&1
java -cp $jars -Xms512m -Xmx$MAXHEAPSIZE -DENVFILE="$MYENVFILE" infLoop.Driver $OUTDIR $LABMCPROJDIR/Input/$1_new.prj -backtrack "$OUTDIR/nonreachabletest_$1.txt" "$OUTDIR/lineNumberstest_$1.txt" "$OUTDIR/rangeVariables_$1.txt" > $OUTDIR/trace_$1.txt 2>&1
rc=$?
if [[ $rc -gt 0 ]]
then
	echo "InfLoop Bw: error in 2nd parsing: Exiting"
	exit;
fi
echo "InfLoop Bw: 2nd Unparsing done"

#Eliminating input variables
#sed -i 's/\(:[! (]*\)\(input *= *= *[0-9][0-9]*\)\([ )]*&&\)/\11\3/g' "$OUTDIR/unreachedFile.txt"
#sed -i 's/\(&&[! (]*\)\(input *= *= *[0-9][0-9]*\)\([ )]*&&\)/\11\3/g' "$OUTDIR/unreachedFile.txt"
#sed -i 's/\(&&[! (]*\)\(input *= *= *[0-9][0-9]*\)\([ )]*$\)/\11\3/g' "$OUTDIR/unreachedFile.txt"
#echo "InfLoop: Eliminated input variables"

echo "InfLoop Bw: Starting backtrack"
currentTime=$(date +%s)
timeElapsed=$(($currentTime-$2))
remainingTime=$((800-$timeElapsed))
$SCRIPTSDIR/backtrackScript.sh $1 $remainingTime
rc=$?
if [[ $rc -eq 10 ]]
then
        echo "InfLoop Bw: : Base case failed in backward approach"
        exit 10;
elif [[ $rc -eq 8 ]]
then 
	echo "InfLoop Bw: CBMC going into infinite iterations: Exiting"
	exit 8;
fi
