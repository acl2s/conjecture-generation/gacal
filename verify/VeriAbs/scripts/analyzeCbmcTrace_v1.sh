#!/bin/bash
#$1: Source file

OUTDIR=$LABMCPROJDIR/InfTrace
CBMCPATH=$LABTECROOT/bin/cbmc
#LABTECROOT="."
remainingTime=$2
defineMacro=""
choice=""
echo "InfLoop Frwd: Total remaining time is: $2"

function baseCase()
{
	choice="-DbaseCase"

	timeout -k 2s $remainingTime $CBMCPATH $choice $defineMacro "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace_$1.txt"

	endTime=$(date +%s)
	diffTime=$(($endTime-$startTime))
	#echo "Time taken by CBMC: $diffTime"

	remainingTime=$(($remainingTime-$diffTime))
	startTime=$(date +%s)
	checkSuccess $1
	rc=$?
	if [[ $rc -eq 2 ]]
	then
		echo "InfLoop Frwd: Base case checked"
		return
	else
		analyzeTrace $1
	#	echo "Base case completed"
	fi
	return

}

function inductionCase()
{
	choice=""

	#echo "Remaining time for CBMC: $remainingTime"
	timeout -k 2s $remainingTime $CBMCPATH $choice $defineMacro "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace_$1.txt"

	endTime=$(date +%s)
	diffTime=$(($endTime-$startTime))
	#echo "Time taken by CBMC: $diffTime"

	remainingTime=$(($remainingTime-$diffTime))
	startTime=$(date +%s)
	checkSuccess $1
	rc=$?
	if [[ $rc -ne 2 ]]
	then
		analyzeTrace $1
		#echo "Induction case completed"
	fi
	return


}

function analyzeTrace()
{
	failedAssertions=$(awk '/FAILURE/{print $2}' "$OUTDIR/cbmcTrace_$1.txt" | awk -F':' '{print $1}')
	if [ -z "$failedAssertions" ]
	then
		exit 8;
	fi
	while [ ! -z "$failedAssertions" ]
	do
		echo "InfLoop Frwd: Total failures: $(awk '/FAILURE/{print $2}'  "$OUTDIR/cbmcTrace_$1.txt"  | wc -l)"
		for i in $failedAssertions
		do
			if ! echo "$i" | grep -q "Error"
			then

				condition=$(grep -w "$i\"" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" | awk -F',' '{print $1}' | awk -F'assert' '{print $2}')
				if [ ! -z "$condition" ]
				then
					if echo "$i" | grep -q "Goal"
					then
						defineMacro="$defineMacro -D$i"
					fi
				#	echo "Found: $i"
					sed -i  /"__CPROVER_assume$condition);"/d "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
					sed -i  /"__CPROVER_assert$condition,"/d "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
				fi
			fi
		done
		#echo "Macros defined: $defineMacro"
		endTime=$(date +%s)
		diffTime=$(($endTime-$startTime))
		remainingTime=$(($remainingTime-$diffTime))
		#echo "Remaining time for CBMC: $remainingTime"

		startTime=$(date +%s)
		timeout -k 2s $remainingTime $CBMCPATH $defineMacro $choice "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" --trace > "$OUTDIR/cbmcTrace_$1.txt"

		endTime=$(date +%s)
		diffTime=$(($endTime-$startTime))
		remainingTime=$(($remainingTime-$diffTime))
		#echo "Time taken by CBMC: $diffTime"

		startTime=$(date +%s)
		checkSuccess $1
		rc=$?
		if [[ $rc -eq 2 ]]
		then
			return
		fi
		failedAssertions=$(awk '/FAILURE/{print $2}' "$OUTDIR/cbmcTrace_$1.txt" | awk -F':' '{print $1}')
		if [ -z "$failedAssertions" ]
		then
			exit 8;
		fi
	done
}

function checkSuccess()
{
	if  grep -q "VERIFICATION SUCCESSFUL" "$OUTDIR/cbmcTrace_$1.txt"
	then
		if [[ $choice == "" ]]
		then
			echo "InfLoop Frwd: VERIABS_VERIFICATION_SUCCESSFUL"
		fi
		return 2;
	fi
}
startTime=$(date +%s)
baseCase $1
endTime=$(date +%s)
diffTime=$(($endTime-$startTime))
remainingTime=$(($remainingTime-$diffTime))

startTime=$(date +%s)
inductionCase $1


