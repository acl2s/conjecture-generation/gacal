#!/usr/bin/perl -w
use strict;
use Scalar::Util qw(looks_like_number);
use File::Basename;
use File::Find;
use Cwd;
use File::Basename;
#use Digest::SHA1 qw(sha1_base64);
#=======================================================================
# This scipt takes four parameter, compile a c program, execute it and
# report a crash, if finds one.
# input parameters : 1. C program with full (absolute) path
#                    2. timeout in seconds
#                    3. Absolute path of output directory where the witness file can be found
#          (Optional)4. A parameter indicating whether to use GNU or clang
#          (Optional)5. Input seed file directory. No logic or check is added for this.
# Assumptions : 1. Environment variable $VERIFUZZ_ROOT is to tools root directory
#               2. $HOME/ar_abs_temp directory is available as working directory
# Caution : No extensive validation is done on the input parameters
#=======================================================================
my $bTime = time();
if ($#ARGV < 2){
    print "Usage: requires C program,timing parameter, output \n";
    exit 12;
}

if ($#ARGV > 4){
    print "Usage: requires C program, timing, output directory, rt no] to run \n";
    exit;
}    
#defualt timing .
if(! -e $ARGV[0]){
    print "Source file is missing $ARGV[0]";
    exit 8;
} 

my $VERIFUZZ_ROOT      = $ENV{'LABTECROOT'};
my $histDir         = $ENV{'HOME'};
my $witness         = $ENV{'WITNESS_FILENAME'};
my $WORKDIR         = "$histDir/ar_abs_temp";
my $inDir           = "$VERIFUZZ_ROOT/exp-in";
my $outDir          = $ARGV[2];
my $logDir          = "$WORKDIR/exp-log";
my $runlog          = "$logDir/run.log";
my $runStatuslog    = "$logDir/runStatus.log";
my $LIBDIR          = "$VERIFUZZ_ROOT/lib";
my $AFLFUZZ         = "$VERIFUZZ_ROOT/afl-2.35b_v1/afl-fuzz";
#my $AFLBUILD        = "$VERIFUZZ_ROOT/afl-2.35b/afl-gcc";
my $AFLBUILD        = "$VERIFUZZ_ROOT/afl-2.35b_v1/afl-prism-gcc";
my $CPUCDCHECK      = "$VERIFUZZ_ROOT/lib/cpudumpc";
my $DICT_PATH   = "$VERIFUZZ_ROOT/afl-2.35b_v1/dictionaries/eca.dict";
my($prismInstrumentedFile, $dirPath, $suffix) = fileparse($ARGV[0], qr/\.[^.]*/);
$prismInstrumentedFile = $dirPath.$prismInstrumentedFile."_instrumented.c" ;

if($#ARGV > 2 && $ARGV[3] eq "clang" ){
    $AFLBUILD       = "$VERIFUZZ_ROOT/afl-2.35b_v1/afl-clang-fast";
}
my $CFLAGS="-m32";
my $mod = "";
my $pgminp = "";
my $srcDir = "";
my $suff   = "";
my $binDir = "$WORKDIR/bin";
my $TMPDIR = "$WORKDIR/tmp";
#------- Timing vars -------------#
my $timeout = 180;
if($#ARGV > 1){
   if(looks_like_number($ARGV[1])){
       $timeout=$ARGV[1];
   }else{
       print "The timing is not numeric. 180s assumed";
   }
}
# Compile timing  
my $comptime = 0;
my $exectime  = 0;

#Does the input file exist ?
if(! -e "$ARGV[0]"){
    print "\nSource c file does not exist. Not much to do, exiting\n";
    exit 24;
}

($mod,$srcDir,$suff)=fileparse($ARGV[0]);
$mod=$mod."_AFL" ;
if(! -e "$binDir"){
   `/bin/mkdir $binDir`;
   print "\nCreate $binDir \n";
}

# Check directories and create if they don't exist.
if (! -e "$TMPDIR"){
   `/bin/mkdir $TMPDIR`;
   print "\nTemporary directory for storing the AFL build\n";
}elsif( -e "$TMPDIR/tmp.o"){
    # Clean temp directory 
    `/bin/rm $TMPDIR/*.*`;
}
if(! -e "$inDir"){
    print "\nInput directory AFL run does not exist !!!\n";
    exit 8;
}    

if (! -e "$outDir"){
    print "\nOutput directory for storing the AFL run does not exist !!!\n";
    exit 8;
}    
    
if (! -e "$logDir") {
    `/bin/mkdir $logDir`;
    print "\nCreate Log directory $logDir\n";
}

# Is witness file is available ? if not, set the default witness file.
if(length($witness) == 0){
    $witness = "$WORKDIR/Witness.graphml";
    $ENV{'WITNESS_FILENAME'} = $witness;
}
# define crash dir for storing the latest crash file. Idea is that, if a crash file exisit in this directory, then there is a true crash. Otherwise, it is false positive due to segmentation fault.
my$crashdir = "$WORKDIR/crash_dir";
if(! -e "$crashdir"){
    `/bin/mkdir  $crashdir`;
    print "\nCreated crash directory $crashdir\n";
}
$ENV{'CRASH_DIR'} = "$crashdir";
#---------- Open Log file ------------------------------------
open LF, ">$runlog" or die "could not open $runlog";
#---------- Compile program. But record the time -------------

my $compile="";

if (! -e "$prismInstrumentedFile") {
    print "\nPrism Instrumented file doesn't exist. Exiting.. $prismInstrumentedFile \n";
    exit(0) ;
}

if($CFLAGS eq "-m32"){
     #$compile="AFL_QUIET=1 $AFLBUILD -m32 -o $TMPDIR/tmp.o -c $ARGV[0]";
     $compile="AFL_QUIET=1 $AFLBUILD -m32 -o $TMPDIR/tmp.o -c $prismInstrumentedFile";
     #
     #No Optimization
     #$compile="AFL_QUIET=1 AFL_DONT_OPTIMIZE=1 $AFLBUILD -m32 -o $TMPDIR/tmp.o -c $prismInstrumentedFile";
}else{
    $compile="AFL_QUIET=1 $AFLBUILD -o $TMPDIR/tmp.o -c $prismInstrumentedFile";
}
    
my $rc = system($compile);
if($rc ne 0){
    print "\nCompilation command - $compile" ;
    print "\nCompilation failed \n";
    exit 12;
}
    
print "\nCompilation of module is done $ARGV[0] \n";
if(! -e "$TMPDIR/tmp.o"){
    print "\nUnable to create load object $TMPDIR/tmp.o\n";
    exit 12;
}

my $lnkedit = "";
if($CFLAGS eq "-m32"){ 
	#$lnkedit = "AFL_QUEIT=1 $AFLBUILD -m32 -o $binDir/$mod $TMPDIR/tmp.o $LIBDIR/svdrvr32.o";
	$lnkedit = "AFL_QUEIT=1 $AFLBUILD -m32 -o $binDir/$mod $TMPDIR/tmp.o $LIBDIR/svdrvr32.o";
	#
	#No Optimization
	#$lnkedit = "AFL_QUEIT=1 AFL_DONT_OPTIMIZE=1 $AFLBUILD -m32 -o $binDir/$mod $TMPDIR/tmp.o $LIBDIR/svdrvr32.o";
}else{
    $lnkedit = "AFL_QUEIT=1 $AFLBUILD -o $binDir/$mod $TMPDIR/tmp.o $LIBDIR/svdrvr.o";
}
    
$rc = system($lnkedit);
if($rc ne 0){
    print "\nLinking command - $lnkedit" ;
    printf "\nLinking failed \n";
    exit 12;
}

print "\nLinkedit of module is done \n";
if(! -e "$binDir/$mod"){
    print "\nUnable to create load object\n";
    exit 12;
}
$comptime = time() - $bTime;

if($comptime > $timeout){
    print "\nNo time left for execution $comptime $timeout \n";
    exit 12;	
}
# Reserve 2 seconds of time for witness generation
$exectime = $timeout - $comptime - 2;
#----------- Running AFL -----------------------------
my $cmd = ""; 
print "\nRunning with AFL $AFLFUZZ ....\n";
#my $crash_opt = "AFL_BENCH_UNTIL_CRASH=1";
my $crash_opt = "";
my $cpu_opt = "";
my $core_dump_opt = "";
#---------- Set flags correctly ---------------------
if( -e "$CPUCDCHECK"){
    #Need to enhance AFL_SKIP_CPUFREQ=1
    system("$CPUCDCHECK -c");
    if($? ne 0){
	$cpu_opt = "AFL_SKIP_CPUFREQ=1";
    }
    #Need to enhance AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES
    system("$CPUCDCHECK -D");
    if($? ne 0){
	$core_dump_opt = "AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1";
    }
}else{
    # Be safe to run AFL. The CPU and core_dump checks are not done; The below options gurantees
    # AFL run 
    $cpu_opt = "AFL_SKIP_CPUFREQ=1";
    $core_dump_opt = "AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1";
}

#Set crash_opt ON
if($#ARGV > 3 && $ARGV[4] eq "crashon" ){
    $crash_opt = "AFL_BENCH_UNTIL_CRASH=1";
}

=begin inputSeedGen
#Set crash_opt ON
if(! -e "$WORKDIR/inpgen"){
    `/bin/mkdir $WORKDIR/inpgen`;
    print "\nInput directory AFL  is created!!!\n";
}

if($#ARGV > 3 && $ARGV[4] eq "crashon" ){
    $crash_opt = "AFL_BENCH_UNTIL_CRASH=1";
    #Compile input file with random input generator
    if($CFLAGS eq "-m32"){ 
	$lnkedit = "gcc -m32 -o $WORKDIR/inpgen/s1_default $LIBDIR/svinpgen32.o $ARGV[0]";
    }else{
	$lnkedit = "gcc -o $WORKDIR/inpgen  $LIBDIR/svinpgen.o  $ARGV[0]";
    }
    $rc = system($lnkedit);
    if($rc ne 0){
	printf "Linking failed while generating seed generator\n";
	printf "Falling back to default exp-in diretory \n";
	printf "Linking Command - $lnkedit" ;
	$inDir           = "$VERIFUZZ_ROOT/exp-in";
    }else{
	#Run the program if it compiled properly
	my $seedfile = "$WORKDIR/inpgen/s1_default" ;
	if( -e "$WORKDIR/inpgen"){
	    `timeout 2 $WORKDIR/inpgen`;
	    # If the seed file is generated, then use this seed for fuzzing.
	    if( -e $seedfile){
		$inDir           = "$WORKDIR/inpgen";
		print "Seed inputfile $seedfile is generated successfully\n";
	    }else{
		print "Seed file is not generated properly\n";
		printf "Falling back to default exp-in diretory \n";
		$inDir       = "$VERIFUZZ_ROOT/exp-in";
	    }
	}else{
	    print "Compilation errors.. input generator did not compile...\n";
	    printf "Falling back to default exp-in diretory \n";
	    $inDir           = "$VERIFUZZ_ROOT/exp-in";
	}
    }
}
=end inputSeedGen
=cut

my $aflRunStartTime = time() ;
#Normal Run Command
#$cmd = "$crash_opt $cpu_opt $core_dump_opt timeout $exectime $AFLFUZZ -i $inDir -o $outDir/$mod $binDir/$mod > $logDir/$mod.log 2> $logDir/$mod-err.log";

#Cmd for using Dictionary.
$cmd = "$crash_opt $cpu_opt $core_dump_opt timeout $exectime $AFLFUZZ -i $inDir -x $DICT_PATH -o $outDir/$mod $binDir/$mod > $logDir/$mod.log 2> $logDir/$mod-err.log";

# Print fuzzer command
print "\n$cmd \n";
# Run the fuzzer
$rc = system($cmd);

my $firstAFLrunTimeTaken = time() - $aflRunStartTime ;
my $remainExecTime = $timeout - $comptime - $firstAFLrunTimeTaken - 10;

# If crashdir is empty and AFL exits on first run, then it is not a true crash (Arising from segmentation faults). Hence, run AFL for rest of the time, removing
# AFL_BENCH_UNTIL_CRASH=1. This option will look for more crash.
if(is_folder_empty($crashdir)) {
   if($remainExecTime < 0){
   	 print "\nNo time left for execution $timeout $comptime $remainExecTime $firstAFLrunTimeTaken \n";
   	 exit 14;	
   }
   
   #Cmd for using Dictionary. 
   $cmd = "$cpu_opt $core_dump_opt timeout $exectime $AFLFUZZ -i $inDir -x $DICT_PATH -o $outDir/$mod $binDir/$mod > $logDir/$mod.log 2> $logDir/$mod-err.log";
   #Normal Command
   #$cmd = "$cpu_opt $core_dump_opt timeout $remainExecTime $AFLFUZZ -i $inDir -o $outDir/$mod $binDir/$mod > $logDir/$mod.log 2> $logDir/$mod-err.log";
   # Print fuzzer command
   print "\n$cmd \n";
   # Run the fuzzer
   $rc = system($cmd);
}
#------------------Print LOG INFO OF AFL---------------------
#print "\nAFL LOG\n\n";
#my $usageFile = "$logDir/$mod.log";
#open(USAGEFILE,$usageFile);
#while(<USAGEFILE>)
#{
#	print $_;
#}
#close(USAGEFILE);
#------------------ Post processing ------------------------
my $fstat = "$crashdir/crash_001";
my $uniqcrash = "";
#If crash file exists, then there is a true crash.
if(-e $fstat){
    # Store the Source name in environment variable 
    $ENV{'WITNESS_SRC'} = $ARGV[0];
    # Generate SHA1 for the source program and store it in an evironment varaible
    my $shasum = `sha1sum $ARGV[0]`;
    my @words = split / /,$shasum;
    my $shvalue = "$words[0]";
    $ENV{'WITNESS_SHA'}= "$shvalue";
    #---------------- Witness creation ---------------------
    open WIT_DATA, "<", "$ARGV[0]"  or die " Can not open source code file for reading : $!";
    my $modifiedPgm = "$TMPDIR/$mod"."_modified.c";
    # Transform the  source program to include __LINE__ and __FUNCTION__
    open MOD_PGM, ">", "$modifiedPgm" or die " Can not open source code file for writing : $!";
    my $funcNm = "const char *";
    while(<WIT_DATA>){
	if(index($_,"__VERIFIER_nondet_") != -1){
	    if(index($_,"extern ") != -1){
		    s/__VERIFIER_nondet_(\w+)\(void\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
		    s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
	    }
	    else{
	        if(index($_," = ") != -1){
	           s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		elsif(index($_,"=") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
	        elsif(index($_,"return ") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		elsif(index($_,"while") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		elsif(index($_,"if") != -1){
	           		s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(__LINE__,__FUNCTION__\)/;
	        }
		else{
			s/__VERIFIER_nondet_(\w+)\(\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
		}
		s/__VERIFIER_nondet_(\w+)\(void\)/__VERIFIER_nondet_$1\(int,$funcNm\)/;
	    }
	}
	print MOD_PGM $_;
    }    
    close WIT_DATA or warn $! ? "Error in closing file ": "Exit status $? from infile";
    close MOD_PGM  or warn $! ? "Error in closing file ":"Exit status $? from outfile";
    # Regenerate binary of the program with Witness generation module
    if($CFLAGS eq "-m32"){ 
	    #$lnkedit = "gcc -m32 -o $TMPDIR/$mod $TMPDIR/$modifiedPgm  $LIBDIR/witsvinp32.o";
	    $lnkedit = "gcc -m32 -fgnu89-inline -o $TMPDIR/$mod $modifiedPgm  $LIBDIR/witsvinp32.o";
    }else{
	    #$lnkedit = "gcc -o $TMPDIR/$mod  $TMPDIR/$modifiedPgm $LIBDIR/witsvinp.o";
	    $lnkedit = "gcc -fgnu89-inline -o $TMPDIR/$mod  $modifiedPgm $LIBDIR/witsvinp.o";
    }
    print "\n$lnkedit \n";
    $rc = system($lnkedit);
    if($rc ne 0){
	printf "\nLinking failed while generating witness based test input\n";
	exit 12;
    }
    #Run the program
    if( -e "$TMPDIR/$mod"){
	`$TMPDIR/$mod < $fstat`;
	# If the witness is generated, then it is a false program. This accounts of any compilation errors.
	if( -e $witness){
	    open LF_STATUS, ">$runStatuslog" or die "could not open $runStatuslog";
	    print LF "$mod FALSE TRUE $uniqcrash \n";
	    print LF_STATUS "FALSE TRUE $uniqcrash \n";
	    print "\nWitness file $witness generated successfully";
	}else{
	    print LF "$mod FALSE UNKNOWN \n";
	}
    }else{
	print "\nCompilation errors.. program did not compile...";
	print LF "$mod FALSE UNKNOWN  \n";
    }
}else{
    #print "$fstat file does not exist ..jumping to next one \n";
    print LF "$mod FALSE UNKNOWN \n";
}
close LF  or warn $! ? "Error in closing file " : "Exist status $? from logfile";
exit 0;

sub is_folder_empty {
    my $dirname = shift;
    opendir(my $dh, $dirname) or die "Not a directory";
    return scalar(grep { $_ ne "." && $_ ne ".." } readdir($dh)) == 0;
}

sub truncateLargeSeedFiles {
    my ($tempSeedFile) = @_ ;
    my $size = -s $tempSeedFile;
    if($size > 1048576){
	print "Greater than 1MB" ;
	open DATA, "+>$tempSeedFile" or die "Couldn't open seedfile t1_default $!";
	truncate DATA,1000000 ;
	close DATA ;
    }
}
