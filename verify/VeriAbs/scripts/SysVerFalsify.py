#!/bin/usr/python
import sys
import os
task=sys.argv[1]
enclfunc=sys.argv[2]
Logs=sys.argv[3]
#################################################################################################################
def run_cbmc(intask,unwind,t):
	print("Unwinding --- "+str(unwind))
	cbmcCmd="timeout -k 2s "+t+" cbmc --unwind "+str(unwind)+" --unwinding-assertions --LP64 "+intask+" > "+Logs+"/cbmc.out 2>"+Logs+"/cbmc.err"
	print(cbmcCmd)

    	os.system(cbmcCmd)

    	if os.path.isfile(Logs+"/cbmc.out"):
        	with open(Logs+"/cbmc.out") as f:
            		cbmcout=f.readlines()
        	tail1=cbmcout[-1:]
        	if "VERIFICATION FAILED" in tail1[0]:
            		tail=cbmcout[-53:]
            		for i in range(len(tail)-1):
                		if enclfunc+".assertion" in tail[i]:
					if "FAILURE" in tail[i]:
						print "CBMC verification failed..."
                				return "failed";
            		return "unknown";
        	else:
            		return "timeout";
    	else:
        	return "error";	

################################################################################################################
success_str="VERIABS_VERIFICATION_SUCCESSFUL";
failed_str="VERIABS_VERIFICATION_FAILED";
unknown_str="VERIABS_UNKNOWN";
high=3
mx=7
low=1
cur=high
prv_res="u"
#res=run_cbmc(intask=task,unwind=1,t="600s")
res=run_cbmc(intask=task,unwind=cur,t="600s")

if not res=="error":
	while cur>=low:
    		if res=="failed":
        		break
    		elif res=="timeout":
        		cur=cur-1
        		if cur<low:
            			break
        		prv_res="t"
        		res=run_cbmc(intask=task,unwind=cur,t="800s")
    		elif res=="unknown":
        		cur=cur+2
        		if prv_res=="t" and cur>=high:
            			break
        		elif cur>mx:
            			break
        		prv_res="u"
        		res=run_cbmc(intask=task,unwind=cur,t="800s")
        		low=high+1
       			high=cur

if res=="failed":
	print(failed_str)
else:
	print(unknown_str)
