#!/bin/bash
OUTDIR=$LABMCPROJDIR/InfTraceBackward
declare -a discreteVariables

#Fetch all variables having discrete assignment
#$1: Source file
function fetchDiscreteVariables()
{
	discreteVariables=($(grep "Discrete" "$OUTDIR/Unparse_$1.c" | awk '{print $4}' | sort -u))
}

#Get all assignment operations
#$1: Source file
function getAllAssignOp()
{
	grep "[a-zA-Z_][a-zA-Z0-9_]*[ ]*=[ ]*[0-9][0-9]*" "$OUTDIR/Unparse_$1.c" > "$OUTDIR/assignOps_$1.txt"
}

#Get all possible values of the variable
#$1: Source file
#$2: Varible name
function getValues()
{
	grep -w "$2" "$OUTDIR/assignOps_$1.txt" | cut -d'=' -f 2 | cut -d';' -f 1 | sort -g -u | awk '{$1=$1;print}' > "$OUTDIR/values_$1.txt"
	echo "Discrete" "$2" $(cat "$OUTDIR/values_$1.txt") >> "$OUTDIR/rangeVariables_$1.txt"
}

fetchDiscreteVariables $1
getAllAssignOp $1
for var in "${discreteVariables[@]}"
do
	getValues $1 $var
done
