#!/bin/bash
#$1 -> source file
OUTDIR=$LABMCPROJDIR/InfTrace

discrete="Discrete"
range="Range"
declare -a discreteVariables
declare -a rangeVariables

# Fetch discrete/range variables
# $1: Source file
# $2: Discrete/Range
function fetchVariables()
{
	if [ "$2" == "$discrete" ]
       	then
		discreteVariables=($(awk '/'"$2"'/{print $3}' "$OUTDIR/traceFile_$1.txt" | sort -u))
	elif [ "$2" == "$range" ]
	then
		rangeVariables=($(awk '/'"$2"'/{print $3}' "$OUTDIR/traceFile_$1.txt" | sort -u))
	fi

}

# Fetch all values of the given variable
# $1: Source file
# $2: variable
function fetchValues()
{
	awk -F"$2 =" '{print $2}' "$OUTDIR/traceFile_$1.txt" | awk -F'Got' '{print $1}' | grep . | sort -g -u > "$OUTDIR/values_$1.txt"
}

# Fetch all variables having discrete assignments
fetchVariables $1 "$discrete"

# Extract all values of each discrete variable
for discreteVar  in "${discreteVariables[@]}"
do
	fetchValues $1 $discreteVar
	echo "$discrete" "$discreteVar" $(cat "$OUTDIR/values_$1.txt") >> "$OUTDIR/rangeVariables_$1.txt"
done

# Fetch all variables whose values fall in some range
fetchVariables $1 "$range"

#Extract min and max values of each range variable	
for rangeVar in "${rangeVariables[@]}"
do
	fetchValues $1 $rangeVar
	echo "$range" "$rangeVar" $(head -1 "$OUTDIR/values_$1.tx"t) $(tail -1 "$OUTDIR/values_$1.txt") >> "$OUTDIR/rangeVariables_$1.txt" 
done
