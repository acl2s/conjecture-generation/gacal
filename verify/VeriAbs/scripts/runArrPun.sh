#/bin/sh

#LABTECROOT=/home/u1411251/afzal/VeriAbs
#LABMCPROJDIR=/home/u1411251/ar_abs_temp/$1
#JARDIR=$LABTECROOT/jars
#OUTDIR=$LABMCPROJDIR/ARP

#ARRPRUN=$OUTDIR/ARRPRUN

\rm -rf $OUTDIR
mkdir $OUTDIR

jars="$JARDIR/Labtec.jar:$JARDIR/antlr.jar:$JARDIR/DMLayer_1.0.0.jar:$JARDIR/escat.jar:$JARDIR/jcupjlex.jar:$JARDIR/jdom.jar:$JARDIR/je-3.3.87.jar:$JARDIR/joot.jar:$JARDIR/jxl.jar:$JARDIR/prism.jar:$JARDIR/slicerWithUnparser.jar:$JARDIR/stringtemplate-1.0.3.jar"

#ENTRY = "Driver"


#$ini = "-DENVFILE=$LABMCPROJDIR/IRs/labtec.ini";
#MYENVFILE=$LABMCPROJDIR/IRs/labtec.ini



java -cp $jars -Xms512m -Xmx$MAXHEAPSIZE -DENVFILE=$MYENVFILE arrayPrune.ArrayPruneDriver $OUTDIR $LABMCPROJDIR/Input/$1.prj >$OUTDIR/javaout 2>&1

result=`grep "SUCCESSFUL:" $OUTDIR/javaout`
if [ "$result" = "" ]
then
	echo ARP:PROGRAM OUT OF SCOPE
	exit 0
else
	echo ARP:PROGRAM UNDER SCOPE
	timeout -k 2s 120s $LABTECROOT/bin/cbmc --32 $OUTDIR/*.c --z3 >$OUTDIR/cbmcout 2>&1
	cbmcresult=`grep "VERIFICATION SUCCESSFUL" $OUTDIR/cbmcout`
	cbmcfresult=`grep "VERIFICATION FAILED" $OUTDIR/cbmcout`
	if [ "$cbmcresult" = "" ]
	then
		if [ "$cbmcfresult" = "" ]
		then
			timeout -k 2s 400s $LABTECROOT/bin/cbmc --32 $OUTDIR/*.c >$OUTDIR/outerr 2>&1
   			onlyCBMCresult=`grep "VERIFICATION SUCCESSFUL" $OUTDIR/outerr`
   			onlyCBMCFresult=`grep "VERIFICATION FAILED" $OUTDIR/outerr`
			if [ "$onlyCBMCresult" = "" ]
			then
				if [ "$onlyCBMCFresult" = "" ]
				then
					 echo ARP:Program is UNKNOWN
                        		 exit 0
				else
					echo ARP:Program is UNSAF
                        		exit 2
				fi

			else
				echo ARP:Program is SAFE
                		exit 1
			fi
		else
			echo ARP:Program is UNSAF
			exit 2
		fi
	else
		echo ARP:Program is SAFE
		exit 1
	fi		

fi

