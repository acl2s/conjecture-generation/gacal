#!/usr/bin/python
import os
import argparse
EXECUTABLE_PATH = ""
TEST_INPUT_PATH = ""
DUMP_PATH = ""

def processCommandLineArguements():
    parser = argparse.ArgumentParser(description="Compute InfLoop Results")
    parser.add_argument('--test',required=True,help="TEST DIR PATH")
    parser.add_argument('--exe',required=True,help="EXE PATH")
    parser.add_argument('--dest',required=True,help="DUMPING PATH")
    return parser.parse_args()

def testList(TEST_DIR_PATH):
    testInputList = []
    filesList = os.listdir(TEST_DIR_PATH)
    for file in filesList:
        if file.startswith('id'):
            testInputList.append(file)
    #        print file
    return testInputList

def runAndDumpTrace():
    global EXECUTABLE_PATH,TEST_INPUT_PATH,DUMP_PATH
    testInputList = testList(TEST_INPUT_PATH)
    destinationTraceFile =  DUMP_PATH#+os.sep+"traceFile2.txt"
    for inputFile in testInputList:
        inputFilePath = TEST_INPUT_PATH+os.sep+inputFile
        runCmd = EXECUTABLE_PATH+" < "+inputFilePath+" >> "+destinationTraceFile
        os.system(runCmd)

def setAppropriatePaths(cmdArgs):
    global EXECUTABLE_PATH,TEST_INPUT_PATH,DUMP_PATH
    EXECUTABLE_PATH = cmdArgs.exe
    TEST_INPUT_PATH = cmdArgs.test
    DUMP_PATH = cmdArgs.dest

def main():
    cmdArgs = processCommandLineArguements()
    setAppropriatePaths(cmdArgs)
    runAndDumpTrace()

if __name__ == "__main__":
    main()
