#!/bin/bash
#$1: File name
OUTDIR=$LABMCPROJDIR/InfTrace

#check if trace file exists or not
if [ ! -f "$OUTDIR/traceFile_$1.txt" ]
then
	echo "Trace file does not exist: AFL might not have not run properly"
	exit 10
fi
#getting reachable conditions
grep "Cond" "$OUTDIR/traceFile_$1.txt" | cut -d'G' -f 1 | awk '{print $2}' | sort -g -u > "$OUTDIR/reachable_$1.txt"

#getting non-reachable conditions
totalCond=$(grep "Cond" "$OUTDIR/Unparse_$1.c" | wc -l)
#echo "$totalCond"
for i in $(seq 1 $totalCond);
do
        if ! grep -q -w "$i" "$OUTDIR/reachable_$1.txt"; then
                echo "$i" >> "$OUTDIR/nonreachable_$1.txt"
        fi
done

#getting line numbers of non reachable conditions
for i in $(cat < "$OUTDIR/nonreachable_$1.txt"); do
 awk "/Cond: $i.\)/{print NR}" "$OUTDIR/Unparse_$1.c" >> "$OUTDIR/lineNumbers_$1.txt"
done

