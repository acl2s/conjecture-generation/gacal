#!/bin/bash
#$1: Source file

OUTDIR=$LABMCPROJDIR/InfTraceBackward
declare -a reachableConds
assume="__CPROVER_assume"
assert="__CPROVER_assert"

#Get All the reachable conditions from CBMC trace
#$1: Source file
function getAllCond()
{
	reachableConds=($(grep "Cond" "$OUTDIR/cbmcTrace1_$1.txt" | awk '{print $2}'))
#	echo "$reachableConds"
}

#Extract appropriate condition and insert corresponding assume and assert
#$1: Source file
function insertAssumesAndAsserts()
{
	length=${#reachableConds[@]}
	if [ $length -eq 0 ]
	then
		echo "InfLoop Bw: Nothing to add"
		exit 10;
	fi	
	for cond in "${reachableConds[@]}"
	do
		if ! grep -q -w "Goal$cond" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
		then
			echo "InfLoop Bw: Adding: $cond"
			errorCondition=$(grep -w "Cond Goal$cond" "$OUTDIR/unreachedFile.txt" | cut -d':' -f 2)
			#	echo "$cond"
			#errorCondition=${errorCondition//\&/\\\&}
			echo "$assume(!($errorCondition));" > "$OUTDIR/assume.txt"
			echo "$assert(!($errorCondition), \"Goal$cond\");" > "$OUTDIR/assert.txt"
			#tac  "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" | sed  "0,/$assume/s//$assumeStat\n$assume/" | tac > "$OUTDIR/Unparse_Preprocess_Unparse_$1_test1.c"
			lastAssumeLineNo=$(awk '/__CPROVER_assume/{ln = FNR} END {print ln}' "$OUTDIR/Unparse_Preprocess_Unparse_$1.c")
			#assumeLineNo=$(($lastAssumeLineNo+1))
			sed -i "$lastAssumeLineNo r $OUTDIR/assume.txt" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
			lastAssertLineNo=$(awk '/__CPROVER_assert/{ln = FNR} END {print ln}' "$OUTDIR/Unparse_Preprocess_Unparse_$1.c")
                        #assertLineNo=$(($lastAssertLineNo+1))
                        sed -i "$lastAssertLineNo r $OUTDIR/assert.txt" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
			#mv "$OUTDIR/Unparse_Preprocess_Unparse_$1_test1.c" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
			#tac  "$OUTDIR/Unparse_Preprocess_Unparse_$1.c" | sed  "0,/$assert/s//$assertStat\n$assert/" | tac > "$OUTDIR/Unparse_Preprocess_Unparse_$1_test1.c"
			#mv "$OUTDIR/Unparse_Preprocess_Unparse_$1_test1.c" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
		fi
		#		sed -i "0,/$assert/s//$assertStat\n$assert/" "$OUTDIR/Unparse_Preprocess_Unparse_$1.c"
	done	
}

getAllCond $1
insertAssumesAndAsserts $1
