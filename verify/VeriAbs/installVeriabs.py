import argparse
import shutil
import sys
import tarfile

TOOL_NAME = "Veriabs Tool"
VERSION_NUM = "2017_01"
README_FILE = "README.txt"
LICENSE_FILE = "LICENSE.txt"
VERIABS_PACKAGE = "scripts.tgz"

def printVersionNum():
    print VERSION_NUM

def showReadMe():
    readmeFile = open(README_FILE)
    shutil.copyfileobj(readmeFile,sys.stdout)

def showLicenseFile():
    licenseFile = open(LICENSE_FILE)
    print "\n----------------------------------------"
    print "LICENSE"
    print "----------------------------------------\n"
    shutil.copyfileobj(licenseFile,sys.stdout)

def commandLineArgs(cmdArgs):
    if(cmdArgs.version == True):
        printVersionNum()
        return True
    if(cmdArgs.readme == True):
        showReadMe()
        return True
    return False

def checkLicenseComplaince():
    showLicenseFile()
    posString = raw_input("\n\nPlease type YES/Y to proceed..(The user agrees the terms and conditions for using Veriabs tool)\n")
    if(posString.upper() == "YES" or posString.upper() == "Y"):
        return True
    else:
        return False

def extractCompressedTool():
    tar = tarfile.open(VERIABS_PACKAGE)
    tar.extractall()
    tar.close()
    print "\n\n-------------------------------------"
    print "VeriAbs SUCCESSFULLY INSTALLED"
    print "-------------------------------------\n\n"

def processCommandLineArguements():
    parser = argparse.ArgumentParser(description=TOOL_NAME)
    parser.add_argument('--version',required=False,action='store_true',help="Display Version.")
    parser.add_argument('--readme',required=False,action='store_true',help= "Display README file.")
    return parser.parse_args()

def main():
    cmdArgs = processCommandLineArguements()
    isOptionalArgsPresent = commandLineArgs(cmdArgs)
    if(isOptionalArgsPresent == False):
        hasUserAgreed = checkLicenseComplaince()
        if(hasUserAgreed):
            extractCompressedTool()
        else :
            print "Please Go through LICENSE and enter YES/Y to use the tool."

if __name__ == "__main__":
    main()
