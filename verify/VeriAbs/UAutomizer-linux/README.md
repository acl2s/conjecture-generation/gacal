# UAutomizer-linux

Copyright (C) 2012-2017 University of Freiburg

* The UAutomizer-linux package has been downloaded from the below link
	https://github.com/ultimate-pa/ultimate/releases/download/v0.1.23/UltimateAutomizer-linux.zip
  The following changes are done in the downloaded package:
	- Removed the following binaries and corresponding licenses:
		- mathsat
		- cvc4
		- cvc4-License
	- Modified README, to remove MathSAT5 binary details.
