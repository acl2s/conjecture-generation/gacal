The repo for the witness descriptions:
https://github.com/sosy-lab/sv-witnesses/

We can submit things manually to the cloud here:
https://vcloud.sosy-lab.org/cpachecker/webclient/runs/witness_validation

This is Ultimate Automizer, which we can use to verify our generated witnesses
https://github.com/ultimate-pa/ultimate

./Ultimate.py \
--spec PropertyUnreachCall.prp \
--file ...file.c...
--architecture 32bit \
--validate ...witness.graphml...


This is CPAChecker, which we can also use to verify our generated witnesses
https://github.com/sosy-lab/cpachecker

scripts/cpa.sh \
-witnessValidation \
    -witness invariantGeneration.kInduction.invariantsAutomatonFile=...witness.graphml... \
    -spec PropertyUnreachCall.prp \
    ...file...
