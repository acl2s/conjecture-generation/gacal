#!/bin/bash

# the location of the java command
[ -z "$JAVA" ] && JAVA=java

#------------------------------------------------------------------------------
# From here on you should not need to change anything
#------------------------------------------------------------------------------

java_version="`$JAVA -XX:-UsePerfData -Xmx5m -version 2>&1`"
result=$?
if [ $result -eq 127 ]; then
  echo "Java not found, please install Java 1.8 or newer." 1>&2
  echo "For Ubuntu: sudo apt-get install openjdk-8-jre" 1>&2
  echo "If you have installed Java 8, but it is not in your PATH," 1>&2
  echo "let the environment variable JAVA point to the \"java\" binary." 1>&2
  exit 1
fi
if [ $result -ne 0 ]; then
  echo "Failed to execute Java VM, return code was $result and output was"
  echo "$java_version"
  echo "Please make sure you are able to execute Java processes by running \"$JAVA\"."
  exit 1
fi
java_version="`echo "$java_version" | grep -e "^\(java\|openjdk\) version" | cut -f2 -d\\\" | sed 's/\.//g' | cut -b1-2`"
if [ -z "$java_version" ] || [ "$java_version" -lt 18 -a "$java_version" -gt 13 ] ; then
  echo "Your Java version is too old, please install Java 1.8 or newer." 1>&2
  echo "For Ubuntu: sudo apt-get install openjdk-8-jre" 1>&2
  echo "If you have installed Java 8, but it is not in your PATH," 1>&2
  echo "let the environment variable JAVA point to the \"java\" binary." 1>&2
  exit 1
fi

platform="`uname -s`"

# where the project directory is, relative to the location of this script
case "$platform" in
  Linux|CYGWIN*)
    SCRIPT="$(readlink -f "$0")"
    [ -n "$PATH_TO_SEXP" ] || PATH_TO_SEXP="$(readlink -f "$(dirname "$SCRIPT")/..")"
    ;;
  # other platforms like Mac don't support readlink -f
  *)
    [ -n "$PATH_TO_SEXP" ] || PATH_TO_SEXP="$(dirname "$0")/.."
    ;;
esac

if [ ! -e "$PATH_TO_SEXP/bin/SexpMain.class" ] ; then
  if [ ! -e "$PATH_TO_SEXP/sexp.jar" ] ; then
    echo "Could not find Sexp binary, please check path to project directory" 1>&2
    exit 1
  fi
fi

export CLASSPATH="$CLASSPATH:$PATH_TO_SEXP/bin:$PATH_TO_SEXP/sexp.jar:$PATH_TO_SEXP/lib/*:$PATH_TO_SEXP/lib/java/runtime/*"

# loop over all input parameters and parse them
declare -a OPTIONS
JAVA_ASSERTIONS=-ea
while [ $# -gt 0 ]; do

  case $1 in
   "-benchmark")
       JAVA_ASSERTIONS=-da
       OPTIONS+=("$1")          # pass param to Sexp, too
       ;;
   "-debug")
       JAVA_VM_ARGUMENTS="$JAVA_VM_ARGUMENTS -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=5005,suspend=n"
       ;;
   "-disable-java-assertions")
       JAVA_ASSERTIONS=-da
       ;;
   "-generateReport")
       echo "Option -generateReport is not necessary anymore. Please open the HTML files produced by Sexp in the output directory."
       ;;
   -X*) # params starting with "-X" are used for JVM
       JAVA_VM_ARGUMENTS="$JAVA_VM_ARGUMENTS $1"
       ;;
   *) # other params are only for Sexp
       OPTIONS+=("$1")
       ;;
  esac

  shift
done

if [ -n "$TMPDIR" ]; then
  JAVA_VM_ARGUMENTS="$JAVA_VM_ARGUMENTS -Djava.io.tmpdir=$TMPDIR"
elif [ -n "$TEMP" ]; then
  JAVA_VM_ARGUMENTS="$JAVA_VM_ARGUMENTS -Djava.io.tmpdir=$TEMP"
elif [ -n "$TMP" ]; then
  JAVA_VM_ARGUMENTS="$JAVA_VM_ARGUMENTS -Djava.io.tmpdir=$TMP"
fi

if [ ! -z "$SEXP_ARGUMENTS" ]; then
  echo "Running Sexp with the following extra arguments: $SEXP_ARGUMENTS"
fi

case "$platform" in
  CYGWIN*)
    JAVA_VM_ARGUMENTS="$JAVA_VM_ARGUMENTS -classpath `cygpath -wp $CLASSPATH`"
    ;;
esac

# Run Sexp.
# Order of arguments for JVM:
# - options hard-coded in this script (to allow overriding them)
# - options specified in environment variable
# - options specified on command-line via "-X..." (except stack/head/tmpdir)
# - options specified on command-line to this script via direct token and tmpdir
# - Sexp class and options
# PerfDisableSharedMem avoids hsperfdata in /tmp (disable it to connect easily with VisualConsole and Co.).
exec "$JAVA" \
    -XX:+PerfDisableSharedMem \
    -Djava.awt.headless=true \
    $JAVA_VM_ARGUMENTS \
    $JAVA_ASSERTIONS \
    SexpMain \
    "${OPTIONS[@]}" \
    $SEXP_ARGUMENTS
