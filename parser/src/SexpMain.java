/*
 *  CPAchecker is a tool for configurable software verification.
 *  This file is part of CPAchecker.
 *
 *  Copyright (C) 2007-2018  Dirk Beyer
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 *  CPAchecker web page:
 *    http://cpachecker.sosy-lab.org
 */

import static java.util.stream.Collectors.toList;

import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.Scanner;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.model.ILanguage;
import org.eclipse.cdt.core.dom.ast.gnu.c.GCCLanguage;
import org.eclipse.cdt.core.parser.FileContent;

import org.eclipse.cdt.core.parser.IScannerInfo;
import org.eclipse.cdt.core.parser.IParserLogService;
import org.eclipse.cdt.internal.core.parser.IMacroDictionary;
import org.eclipse.cdt.internal.core.parser.InternalParserUtil;
import org.eclipse.cdt.internal.core.parser.scanner.InternalFileContent;
import org.eclipse.cdt.internal.core.parser.scanner.InternalFileContentProvider;
import org.eclipse.cdt.core.parser.ParserFactory;
import org.eclipse.cdt.core.index.IIndexFileLocation;

public class SexpMain {

    // -output -source
    public static void main(String[] args) {
	//for (int i=0; i<args.length; i++) {
	//    System.out.println(args[i]);
	//}
	String outputPath = "";
	String filePath = "";
	for (int i=0; i<args.length; i++) {
	    if (i < args.length - 1 && args[i].equals("-output")) {
		outputPath = args[i+1];
	    }
	    if (i < args.length - 1 && args[i].equals("-source")) {
		filePath = args[i+1];
	    }
	}
	assert(filePath != "");
	assert(outputPath != "");
	//System.out.println (outputPath);
	//System.out.println("========================================");
	//System.out.println("========================================");
	//System.out.println("========================================");
	//System.out.println("========================================");
	//System.out.println("========================================");
	//System.out.println("========================================");
	//compute and print sexp of parse
	ILanguage language = GCCLanguage.getDefault();
	IParserLogService parserLog = ParserFactory.createDefaultLogService();
	
	//System.out.println("Handling: " + filePath);
	try {
	    String theFile;
	    File file = new File(filePath);
	    Scanner scanner = new Scanner(file, Charset.defaultCharset().name());
	    theFile = scanner.nextLine();
	    while (scanner.hasNextLine()) {
		theFile += "\n" + scanner.nextLine();
	    }
	    char[] fileData = theFile.toCharArray();
	    //for (int i=0; i<fileData.length; i++) {
	    //	System.out.print(fileData[i]);
	    //}
	    //System.out.println();
	    
	    FileContent fileContent = FileContent.create(filePath, fileData);
	    IASTTranslationUnit i = language.getASTTranslationUnit(fileContent,
								   StubScannerInfo.instance,
								   FileContentProvider.instance,
								   null,
								   ILanguage.OPTION_NO_IMAGE_LOCATIONS,
								   parserLog);
	    String sexp = SexpConverter.toSexp(i);
	    
	    //System.out.println(sexp);
	    BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputPath), Charset.defaultCharset()); 
	    writer.write(sexp);
	    writer.close();
	    
	} catch (Exception e) {
	    System.out.println("Uh oh!");
	}
    }

  //
  //* Private class that tells the Eclipse CDT scanner that no macros and include
  //* paths have been defined externally.
  //
  protected static class StubScannerInfo implements IScannerInfo {

    private static final ImmutableMap<String, String> MACROS;

    static {
      ImmutableMap.Builder<String, String> macrosBuilder = ImmutableMap.builder();

      // _Static_assert(cond, msg) feature of C11
      macrosBuilder.put("_Static_assert(c, m)", "");
      // _Noreturn feature of C11
      macrosBuilder.put("_Noreturn", "");

      // These built-ins are defined as macros
      // in org.eclipse.cdt.core.dom.parser.GNUScannerExtensionConfiguration.
      // When the parser encounters their redefinition or
      // some non-trivial usage in the code, we get parsing errors.
      // So we redefine these macros to themselves in order to
      // parse them as functions.
      macrosBuilder.put("__builtin_constant_p", "__builtin_constant_p");
      macrosBuilder.put("__builtin_types_compatible_p(t1,t2)", "__builtin_types_compatible_p(({t1 arg1; arg1;}), ({t2 arg2; arg2;}))");
      macrosBuilder.put("__offsetof__", "__offsetof__");

      macrosBuilder.put("__func__", "\"__func__\"");
      macrosBuilder.put("__FUNCTION__", "\"__FUNCTION__\"");
      macrosBuilder.put("__PRETTY_FUNCTION__", "\"__PRETTY_FUNCTION__\"");

      // Eclipse CDT 8.1.1 has problems with more complex attributes
      macrosBuilder.put("__attribute__(a)", "");

      // There are some interesting macros available at
      // http://research.microsoft.com/en-us/um/redmond/projects/invisible/include/stdarg.h.htm
      macrosBuilder.put("_INTSIZEOF(n)", "((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))"); // at least size of smallest addressable unit
      //macrosBuilder.put("__builtin_va_start(ap,v)", "(ap = (va_list)&v + _INTSIZEOF(v))");
      macrosBuilder.put("__builtin_va_arg(ap,t)", "*(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t))");
      // macrosBuilder.put("__builtin_va_end(ap)", "(ap = (va_list)0)");

      MACROS = macrosBuilder.build();
    }

    protected final static IScannerInfo instance = new StubScannerInfo();

    @Override
    public Map<String, String> getDefinedSymbols() {
      // the externally defined pre-processor macros
      return MACROS;
    }

    @Override
    public String[] getIncludePaths() {
      return new String[0];
    }
  }

  private static class FileContentProvider extends InternalFileContentProvider {

    static final InternalFileContentProvider instance = new FileContentProvider();

    @Override
    public InternalFileContent getContentForInclusion(String pFilePath,
        IMacroDictionary pMacroDictionary) {
      return InternalParserUtil.createExternalFileContent(pFilePath,
          InternalParserUtil.SYSTEM_DEFAULT_ENCODING);
    }

    @Override
    public InternalFileContent getContentForInclusion(IIndexFileLocation pIfl,
        String pAstPath) {
      return InternalParserUtil.createFileContent(pIfl);
    }
  }

}
