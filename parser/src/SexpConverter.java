
import java.util.ArrayList;

import org.eclipse.cdt.internal.core.dom.parser.c.*;
import org.eclipse.cdt.core.dom.ast.*;

/*
Does not handle:
structs, type defs; contained in CASTTypeId?
CASTTypeIdExpression
CASTInitializerList

What are these?
CASTArrayDesignator
CASTArrayModifier
CASTArrayDesignator
CASTProblemExpression
CASTProblemStatement
CASTProblemDeclaration
CASTFieldReference?
CASTBaseDeclSpecifier?
CASTFieldDesignator?
 */


abstract class SexpConverter {

    static final boolean print = false;

    static final boolean addTypes = true;
    static final boolean addLocs = true;

    //static class Unimplemented extends Exception {
    //	public Unimplemented (String s) {
    //	    super (s);
    //	}
    //}
    //
    private static String FileLocationToSexp (IASTFileLocation l) {
    	return "(loc :start-line " + Integer.toString(l.getStartingLineNumber())
    	    + " :end-line " + Integer.toString(l.getEndingLineNumber())
    	    + " :offset " + Integer.toString(l.getNodeOffset())
	    + " :length " + Integer.toString(l.getNodeLength()) + ")";
    }
    //
    //private static String makeSexp (String node, ArrayList<String> subnodes, FileLocation fl) {
    //	String subs = "";
    //	for (int i=0; i<subnodes.size(); i++) {
    //	    subs += subnodes.get(i) + "\n";
    //	}
    //	return "(" + node + "\n" + subs + "\n" + FileLocationToString (fl) + ")";
    //}


    public static String toSexp (IASTTranslationUnit i) {
	String result = "(program :declarations (";
	IASTDeclaration[] decs = i.getDeclarations();
	for (int j=0; j<decs.length; j++) {
	    result += HandleDeclaration(decs[j], "") + "\n";
	}
	result += "))";
	return result;
    }

    private static String sep = "   ";

    private static String HandleDeclaration(IASTDeclaration d, String tab) {
	String sexp_loc = FileLocationToSexp(d.getFileLocation());
	String result;
	if (d instanceof CASTSimpleDeclaration) {
	    if (print) System.out.println(tab + "CASTSimpleDeclaration.");
	    if (print) System.out.println(tab + " - " + "decs");
	    String sexp_decs = "";
	    IASTDeclarator[] decs = ((CASTSimpleDeclaration) d).getDeclarators();
	    for (int i=0; i<decs.length; i++) {
		sexp_decs += HandleDeclarator(decs[i], tab + sep) + (i < decs.length - 1 ? "\n" : "");
	    }
	    if (print) System.out.println(tab + " - " + "decl-specifier");
	    String sexp_declsp = HandleDeclSpecifier(((CASTSimpleDeclaration) d).getDeclSpecifier(), tab + sep);
	    result = "(simple-declaration" 
		+ " :declarators (" + sexp_decs + ")" 
		+ " :decl-specifier " + sexp_declsp 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (d instanceof CASTFunctionDefinition) {
	    if (print) System.out.println(tab + "CASTFunctionDefinition.");
	    if (print) System.out.println(tab + " - " + "declarator");
	    String sexp_declarator = HandleDeclarator(((CASTFunctionDefinition) d).getDeclarator(), tab + sep);
	    if (print) System.out.println(tab + " - " + "decl-specifier");
	    String sexp_declsp = HandleDeclSpecifier(((CASTFunctionDefinition) d).getDeclSpecifier(), tab + sep);
	    if (print) System.out.println(tab + " - " + "body");
	    String sexp_body = HandleStatement(((CASTFunctionDefinition) d).getBody(), tab + sep);
	    result = "(function-definition" 
		+ " :declarator " + sexp_declarator 
		+ " :decl-specifier " + sexp_declsp 
		+ " :body " + sexp_body 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + "Declaration: " + d.toString());
	    result = "[Declaration " + d.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }

    private static String kind_to_sexp (IBasicType.Kind k) {
	if (k == IBasicType.Kind.eBoolean) {
	    return "bool";
	} else if (k == IBasicType.Kind.eChar) {
	    return "char";
	} else if (k == IBasicType.Kind.eChar16) {
	    return "char16";
	} else if (k == IBasicType.Kind.eChar32) {
	    return "char32";
	} else if (k == IBasicType.Kind.eDouble) {
	    return "double";
	} else if (k == IBasicType.Kind.eFloat) {
	    return "float";
	} else if (k == IBasicType.Kind.eInt) {
	    return "int";
	} else if (k == IBasicType.Kind.eVoid) {
	    return "void";
	}
	return "???";
    }

    private static String HandleType (IType t, String tab) {
	String result;
	if (t instanceof CFunctionType) {
	    IType[] its = ((CFunctionType) t).getParameterTypes();
	    IType rt = ((CFunctionType) t).getReturnType();
	    if (print) System.out.println (tab + "FunctionType.");
	    if (print) System.out.println(tab + " - " + "input types");
	    String sexp_its = "";
	    for (int i=0; i < its.length; i++) {
		sexp_its += HandleType(its[i], tab + sep) + (i < its.length - 1 ? "\n" : "");
	    }
	    if (print) System.out.println(tab + " - " + "return type");
	    String sexp_rt = HandleType(rt, tab + sep);
	    result = "(function-type" 
		+ " :input-types (" + sexp_its + ")" 
		+ " :return-type " + sexp_rt + ")";
	} else if (t instanceof CBasicType) {
	    if (print) System.out.println (tab + "BasicType.");
	    String sexp_modifier = int_to_type_modifier(((CBasicType) t).getModifiers());
	    if (print) System.out.println(tab + " - " + "modifier: " + sexp_modifier);
	    String sexp_kind = kind_to_sexp(((CBasicType) t).getKind());
	    if (print) System.out.println(tab + " - " + "kind: " + sexp_kind);
	    result = "(basic-type" 
		+ " :modifiers " + sexp_modifier 
		+ " :kind " + sexp_kind + ")";
	} else if (t instanceof CArrayType) {
	    if (print) System.out.println (tab + "ArrayType.");
	    IType t_over = ((CArrayType) t).getType();
	    if (print) System.out.println(tab + " - " + "type over");
	    String sexp_t_over = HandleType(t_over, tab + sep);
	    //IValue size = ((CArrayType) t).getSize();
	    result = "(array-type" 
		+ " :type " + sexp_t_over + ")";
	} else if (t instanceof CPointerType) {
	    if (print) System.out.println (tab + "PointerType.");
	    IType t_over = ((CPointerType) t).getType();
	    if (print) System.out.println(tab + " - " + "type over");
	    String sexp_t_over = HandleType(t_over, tab + sep);
	    //IValue size = ((CArrayType) t).getSize();
	    result = "(pointer-type" 
		+ " :type " + sexp_t_over + ")";
	} else {
	    if (print) System.out.println (tab + "Type: " + t.toString());
	    result = "[Type " + t.toString() + "]";
	}
	return result;
    }

    private static String HandleName (IASTName name, String tab) {
	if (print) System.out.println (tab + "Name: " + name.toString());
	if (name.toString() == "") {
	    return "(unnamed)";
	} else {
	    return "(name " + "|" + name.toString() + "|" + ")";
	}
    }

    private static String HandleExpression(IASTExpression e, String tab) {
	String sexp_loc = FileLocationToSexp(e.getFileLocation());
	String result;
	if (e instanceof CASTLiteralExpression) {
	    if (print) System.out.println(tab + "CASTLiteralExpression.");
	    IType t = ((CASTLiteralExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    String sexp_kind = int_to_literal_kind(((CASTLiteralExpression) e).getKind());
	    if (print) System.out.println(tab + " - " + "kind: " + sexp_kind);
	    String sexp_value = new String(((CASTLiteralExpression) e).getValue());
	    if (sexp_kind.equals("char-constant")) {
	    	sexp_value = "#\\" + sexp_value.substring(1, 2); // remove single quotes
	    }
	    if (sexp_value.length() >= 2 && sexp_value.substring(0,2).equals("0x")) {
	    	sexp_value = "#" + sexp_value.substring(1, sexp_value.length());
	    }
	    if (print) System.out.println(tab + " - " + "value: " + sexp_value);
	    result = "(literal-expression"
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :kind " + sexp_kind
		+ " :value " + sexp_value
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTIdExpression) {
	    if (print) System.out.println(tab + "CASTIdExpression.");
	    IASTName name = ((CASTIdExpression) e).getName();
	    IType t = ((CASTIdExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    if (print) System.out.println(tab + " - " + "name");
	    String sexp_name = HandleName(name, tab + sep);
	    result = "(id-expression" 
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :name " + sexp_name 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTUnaryExpression) {
	    if (print) System.out.println(tab + "CASTUnaryExpression.");
	    int operator = ((CASTUnaryExpression) e).getOperator();
	    IASTExpression operand = ((CASTUnaryExpression) e).getOperand();
	    IType t = ((CASTUnaryExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    if (print) System.out.println(tab + " - " + "operator: " + int_to_unary_operator(operator));
	    if (print) System.out.println(tab + " - " + "operand");
	    String sexp_operand = HandleExpression(operand, tab + sep);
	    String sexp_operator = int_to_unary_operator(operator);
	    result = "(unary-expression" 
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :operator " + sexp_operator 
		+ " :operand " + sexp_operand 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTBinaryExpression) {
	    if (print) System.out.println(tab + "CASTBinaryExpression.");
	    int operator = ((CASTBinaryExpression) e).getOperator();
	    IASTExpression operand1 = ((CASTBinaryExpression) e).getOperand1();
	    IASTExpression operand2 = ((CASTBinaryExpression) e).getOperand2();
	    IType t = ((CASTBinaryExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    if (print) System.out.println(tab + " - " + "operator: " + int_to_binary_operator(operator));
	    if (print) System.out.println(tab + " - " + "operand1");
	    String sexp_operand1 = HandleExpression(operand1, tab + sep);
	    if (print) System.out.println(tab + " - " + "operand2");
	    String sexp_operand2 = HandleExpression(operand2, tab + sep);
	    String sexp_operator = int_to_binary_operator(operator);
	    result = "(binary-expression" 
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :operator " + sexp_operator 
		+ " :operand1 " + sexp_operand1 
		+ " :operand2 " + sexp_operand2 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTFunctionCallExpression) {
	    if (print) System.out.println(tab + "CASTFunctionCallExpression.");
	    IASTExpression function = ((CASTFunctionCallExpression) e).getFunctionNameExpression();
	    IASTInitializerClause[] is = ((CASTFunctionCallExpression) e).getArguments();
	    IType t = ((CASTFunctionCallExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    if (print) System.out.println(tab + " - " + "function");
	    String sexp_function = HandleExpression(function, tab + sep);
	    if (print) System.out.println(tab + " - " + "arguments");
	    String sexp_args = "";
	    for (int i=0; i<is.length; i++) {
		sexp_args += HandleInitializerClause(is[i], tab + sep) + (i < is.length - 1 ? "\n" : "");
	    }
	    result = "(function-call-expression" 
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :function " + sexp_function 
		+ " :arguments (" + sexp_args + ")" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTCompoundStatementExpression) {
	    if (print) System.out.println(tab + "CASTCompoundStatementExpression.");
	    IType t = ((CASTCompoundStatementExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    IASTStatement[] statements = ((CASTCompoundStatementExpression) e).getCompoundStatement().getStatements();
	    String sexp_statements = "";
	    for (int i=0; i<statements.length; i++) {
		sexp_statements += HandleStatement(statements[i], tab + sep) + (i < statements.length - 1 ? "\n" : "");
	    }
	    result = "(compound-statement-expression"
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :statements (" + sexp_statements + ")" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTConditionalExpression) {
	    if (print) System.out.println(tab + "CASTConditionalExpression.");
	    IType t = ((CASTConditionalExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    IASTExpression test = ((CASTConditionalExpression) e).getLogicalConditionExpression();
	    IASTExpression neg = ((CASTConditionalExpression) e).getNegativeResultExpression();
	    IASTExpression pos = ((CASTConditionalExpression) e).getPositiveResultExpression();
	    if (print) System.out.println(tab + " - " + "test");
	    String sexp_test = HandleExpression(test, tab + sep);
	    if (print) System.out.println(tab + " - " + "pos");
	    String sexp_pos = HandleExpression(pos, tab + sep);
	    if (print) System.out.println(tab + " - " + "neg");
	    String sexp_neg = HandleExpression(neg, tab + sep);
	    result = "(conditional-expression" 
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :test " + sexp_test 
		+ " :then " + sexp_pos 
		+ " :else " + sexp_neg 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTArraySubscriptExpression) {
	    if (print) System.out.println(tab + ".");
	    IType t = ((CASTArraySubscriptExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    IASTExpression array = ((CASTArraySubscriptExpression) e).getArrayExpression();
	    IASTExpression subscript = ((CASTArraySubscriptExpression) e).getSubscriptExpression();
	    IASTInitializerClause arg = ((CASTArraySubscriptExpression) e).getArgument();

	    if (print) System.out.println(tab + " - " + "array");
	    String sexp_array = HandleExpression(array, tab + sep);
	    String sexp_subscript;
	    if (subscript == null) {
		if (print) System.out.println(tab + " - " + "subscript: _");
		sexp_subscript = "_";
	    } else {
		if (print) System.out.println(tab + " - " + "subscript");
		sexp_subscript = HandleExpression(subscript, tab + sep);
	    }
	    String sexp_arg;
	    if (arg == null) {
		if (print) System.out.println(tab + " - " + "arg: _");
		sexp_arg = "_";
	    } else {
		if (print) System.out.println(tab + " - " + "arg");
		sexp_arg = HandleInitializerClause(arg, tab + sep);
	    }
	    result = "(array-subscript-expression" 
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :array " + sexp_array 
		+ " :subscript " + sexp_subscript 
		+ " :arg " + sexp_arg 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (e instanceof CASTCastExpression) {
	    IASTTypeId operator = ((CASTCastExpression) e).getTypeId();
	    IASTExpression operand = ((CASTCastExpression) e).getOperand();
	    IType t = ((CASTCastExpression) e).getExpressionType();
	    if (print) System.out.println(tab + " - " + "type");
	    String sexp_t = HandleType(t, tab + sep);
	    if (print) System.out.println(tab + " - " + "operator");
	    String sexp_operator = HandleTypeId(operator, tab);
	    if (print) System.out.println(tab + " - " + "operand");
	    String sexp_operand = HandleExpression(operand, tab + sep);
	    // not using .getOperator()
	    result = "(cast-expression"
		+ (addTypes ? " :type " + sexp_t : "")
		+ " :operator " + sexp_operator 
		+ " :operand " + sexp_operand
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + "Expression: " + e.toString());
	    result = "[Expression " + e.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }
    
    private static String HandleTypeId (IASTTypeId t, String tab) {
	if (false) System.out.println(tab + "HandleTypeId");
	String sexp_decl = HandleDeclarator(t.getAbstractDeclarator(), tab + sep);
	String sexp_declsp = HandleDeclSpecifier(t.getDeclSpecifier(), tab + sep);
	return "(type-id"
	    + " :abstract-declarator " + sexp_decl
	    + " :decl-specifier " + sexp_declsp + ")";
    }

    private static String HandleStatement(IASTStatement s, String tab) {
	String sexp_loc = FileLocationToSexp(s.getFileLocation());
	String result;
	if (s instanceof CASTCompoundStatement) {
	    if (print) System.out.println(tab + "CASTCompoundStatement.");
	    IASTStatement[] ss = ((CASTCompoundStatement) s).getStatements();
	    String sexp_ss = "";
	    for (int i=0; i<ss.length; i++) {
		sexp_ss += HandleStatement(ss[i], tab + sep) + (i < ss.length - 1 ? "\n" : "");
	    }
	    result = "(compound-statement" 
		+ " :statements (" + sexp_ss + ")" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTReturnStatement) {
	    if (print) System.out.println(tab + "CASTReturnStatement.");
	    IASTExpression e = ((CASTReturnStatement) s).getReturnValue();
	    String sexp_e = "";
	    if (e == null) {
		if (print) System.out.println(tab + " - " + "no return value");
		sexp_e = "_";
	    } else {
		sexp_e += HandleExpression(e, tab + sep);
	    }
	    result = "(return-statement" 
		+ " :expression " + sexp_e 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTIfStatement) {
	    if (print) System.out.println(tab + "CASTIfStatement.");
	    IASTExpression test = ((CASTIfStatement) s).getConditionExpression();
	    IASTStatement then_b = ((CASTIfStatement) s).getThenClause();
	    IASTStatement else_b = ((CASTIfStatement) s).getElseClause();
	    if (print) System.out.println(tab + " - " + "test");
	    String sexp_test = HandleExpression(test, tab + sep);
	    if (print) System.out.println(tab + " - " + "then");
	    String sexp_then = HandleStatement(then_b, tab + sep);
	    String sexp_else = "";
	    if (else_b == null) {
		if (print) System.out.println(tab + " - " + "else: _");
		sexp_else = "_";
	    } else {
		if (print) System.out.println(tab + " - " + "else");
		sexp_else = HandleStatement(else_b, tab + sep);
	    }
	    result = "(if-statement" 
		+ " :test " + sexp_test 
		+ " :then " + sexp_then 
		+ " :else " + sexp_else 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTWhileStatement) {
	    if (print) System.out.println(tab + "CASTWhileStatement.");
	    IASTExpression test = ((CASTWhileStatement) s).getCondition();
	    IASTStatement body = ((CASTWhileStatement) s).getBody();
	    if (print) System.out.println(tab + " - " + "test");
	    String sexp_test = HandleExpression(test, tab + sep);
	    if (print) System.out.println(tab + " - " + "body");
	    String sexp_body = HandleStatement(body, tab + sep);
	    result = "(while-statement" 
		+ " :test " + sexp_test 
		+ " :body " + sexp_body 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTDoStatement) {
	    if (print) System.out.println(tab + "CASTDoStatement.");
	    IASTExpression test = ((CASTDoStatement) s).getCondition();
	    IASTStatement body = ((CASTDoStatement) s).getBody();
	    if (print) System.out.println(tab + " - " + "test");
	    String sexp_test = HandleExpression(test, tab + sep);
	    if (print) System.out.println(tab + " - " + "body");
	    String sexp_body = HandleStatement(body, tab + sep);
	    result = "(do-statement" 
		+ " :test " + sexp_test 
		+ " :body " + sexp_body 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTForStatement) {
	    if (print) System.out.println(tab + "CASTForStatement.");
	    IASTStatement init = ((CASTForStatement) s).getInitializerStatement();
	    IASTExpression test = ((CASTForStatement) s).getConditionExpression();
	    IASTExpression iter = ((CASTForStatement) s).getIterationExpression();
	    IASTStatement body = ((CASTForStatement) s).getBody();
	    if (print) System.out.println(tab + " - " + "init");
	    String sexp_init = HandleStatement(init, tab + sep);
	    if (print) System.out.println(tab + " - " + "test");
	    String sexp_test = HandleExpression(test, tab + sep);
	    if (print) System.out.println(tab + " - " + "iter");
	    String sexp_iter = HandleExpression(iter, tab + sep);
	    if (print) System.out.println(tab + " - " + "body");
	    String sexp_body = HandleStatement(body, tab + sep);
	    result = "(for-statement" 
		+ " :init " + sexp_init 
		+ " :test " + sexp_test 
		+ " :iter " + sexp_iter 
		+ " :body " + sexp_body 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTBreakStatement) {
	    if (print) System.out.println(tab + "CASTBreakStatement.");
	    result = "(break-statement" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTContinueStatement) {
	    if (print) System.out.println(tab + "CASTContinueStatement.");
	    result = "(continue-statement" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTDefaultStatement) {
	    if (print) System.out.println(tab + "CASTDefaultStatement.");
	    result = "(default-statement" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTNullStatement) {
	    if (print) System.out.println(tab + "CASTNullStatement.");
	    result = "(null-statement" 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTGotoStatement) {
	    if (print) System.out.println(tab + "CASTGotoStatement.");
	    IASTName name = ((CASTGotoStatement) s).getName();
	    if (print) System.out.println(tab + " - " + "name");
	    String sexp_name = HandleName(name, tab + sep);
	    result = "(goto-statement" 
		+ " :label " + sexp_name 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTDeclarationStatement) {
	    if (print) System.out.println(tab + "CASTDeclarationStatement.");
	    if (print) System.out.println(tab + " - " + "declaration");
	    IASTDeclaration decl = ((CASTDeclarationStatement) s).getDeclaration();
	    String sexp_decl = HandleDeclaration(decl, tab + sep);
	    result = "(declaration-statement" 
		+ " :declaration " + sexp_decl 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTExpressionStatement) {
	    if (print) System.out.println(tab + "CASTExpressionStatement.");
	    IASTExpression e = ((CASTExpressionStatement) s).getExpression();
	    if (print) System.out.println(tab + " - " + "expression");
	    String sexp_e = HandleExpression(e, tab + sep);
	    result = "(expression-statement" 
		+ " :expression " + sexp_e 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTCaseStatement) {
	    if (print) System.out.println(tab + "CASTCaseStatement.");
	    IASTExpression e = ((CASTCaseStatement) s).getExpression();
	    if (print) System.out.println(tab + " - " + "expression");
	    String sexp_e = HandleExpression(e, tab + sep);
	    result = "(case-statement" 
		+ " :expression " + sexp_e 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTSwitchStatement) {
	    if (print) System.out.println(tab + "CASTSwitchStatement.");
	    IASTExpression e = ((CASTSwitchStatement) s).getControllerExpression();
	    if (print) System.out.println(tab + " - " + "controller-expression");
	    String sexp_e = HandleExpression(e, tab + sep);
	    IASTStatement body = ((CASTSwitchStatement) s).getBody();
	    if (print) System.out.println(tab + " - " + "body");
	    String sexp_body = HandleStatement(body, tab + sep);
	    result = "(switch-statement" 
		+ " :controller-expression " + sexp_e 
		+ " :body " + sexp_body 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (s instanceof CASTLabelStatement) {
	    if (print) System.out.println(tab + "CASTLabelStatement.");
	    IASTName name = ((CASTLabelStatement) s).getName();
	    IASTStatement nested = ((CASTLabelStatement) s).getNestedStatement();
	    if (print) System.out.println(tab + " - " + "name");
	    String sexp_name = HandleName(name, tab + sep);
	    String sexp_nested = "";
	    if (nested == null) {
		if (print) System.out.println(tab + " - " + "nested: _");
		sexp_nested = "_";
	    } else {
		if (print) System.out.println(tab + " - " + "nested");
		sexp_nested = HandleStatement(nested, tab + sep);
	    }
	    result = "(label-statement" 
		+ " :label " + sexp_name 
		+ " :nested " + sexp_nested 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + "Statement: " + s.toString());
	    result = "[Statement " + s.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }

    private static String HandleInitializerClause(IASTInitializerClause i, String tab) {
	String sexp_loc = FileLocationToSexp(i.getFileLocation());
	String result;
	if (i instanceof IASTExpression) {
	    String sexp_ic = HandleExpression((IASTExpression) i, tab);
	    result = "(initializer-clause-expression" 
		+ " :expression " + sexp_ic 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + "InitializerClause: " + i.toString());
	    result = "[InitializerClause " + i.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }

    private static String HandleInitializer(IASTInitializer i, String tab) {
	String sexp_loc = FileLocationToSexp(i.getFileLocation());
	String result;
	if (i instanceof CASTEqualsInitializer) {
	    if (print) System.out.println(tab + "CASTEqualsInitializer.");
	    if (print) System.out.println(tab + " - " + "initializer-clause");
	    String sexp_ic = HandleInitializerClause(((CASTEqualsInitializer) i).getInitializerClause(), tab + sep);
	    result = "(initializer" 
		+ " :initializer-clause " + sexp_ic 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + "Initializer: " + i.toString());
	    result = "[Initializer " + i.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }
    
    private static String HandleDeclarator(IASTDeclarator d, String tab) {
	String sexp_loc = FileLocationToSexp(d.getFileLocation());
	String result;
	if (d instanceof CASTFunctionDeclarator) {
	    if (print) System.out.println(tab + "CASTFunctionDeclarator.");
	    IASTParameterDeclaration[] params = ((CASTFunctionDeclarator) d).getParameters();
	    if (print) System.out.println(tab + " - " + "name");
	    String sexp_name = HandleName(d.getName(), tab + sep);
	    if (print) System.out.println(tab + " - " + "params");
	    String sexp_params = "";
	    for (int i=0; i<params.length; i++) {
		sexp_params += HandleParameterDeclaration (params[i], tab + sep) + (i < params.length - 1 ? "\n" : "");
	    }
	    IASTInitializer i = d.getInitializer();
	    String sexp_init;
	    if (i == null) {
		if (print) System.out.println(tab + " - " + "initializer: _");
		sexp_init = "_";
	    } else {
		if (print) System.out.println(tab + " - " + "initializer");
		sexp_init = HandleInitializer(i, tab + sep);
	    }
	    result = "(function-declarator" 
		+ " :name " + sexp_name 
		+ " :parameters (" + sexp_params + ")" 
		+ " :initializer " + sexp_init 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else if (d instanceof CASTDeclarator) {
	    if (print) System.out.println(tab + "CASTDeclarator.");
	    if (print) System.out.println(tab + " - " + "name");
	    String sexp_name = HandleName(d.getName(), tab + sep);
	    IASTInitializer i = d.getInitializer();
	    String sexp_init;
	    if (i == null) {
		if (print) System.out.println(tab + " - " + "initializer: _");
		sexp_init = "_";
	    } else {
		if (print) System.out.println(tab + " - " + "initializer");
		HandleInitializer(i, tab + sep);
		sexp_init = HandleInitializer(i, tab + sep);
	    }
	    result = "(declarator" 
		+ " :name " + sexp_name 
		+ " :initializer " + sexp_init 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + "Declarator: " + d.toString());
	    if (print) System.out.println(tab + " - " + "name");
	    HandleName(d.getName(), tab + sep);
	    IASTInitializer i = d.getInitializer();
	    if (i == null) {
		if (print) System.out.println(tab + " - " + "initializer: _");
	    } else {
		if (print) System.out.println(tab + " - " + "initializer");
		HandleInitializer(i, tab + sep);
	    }
	    result = "[Declarator " + d.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }

    private static String HandleParameterDeclaration (IASTParameterDeclaration p, String tab) {
	String sexp_loc = FileLocationToSexp(p.getFileLocation());
	String result;
	if (p instanceof CASTParameterDeclaration) {
	    if (print) System.out.println (tab + "CASTParameterDeclaration.");
	    if (print) System.out.println (tab + " - " + "declarator");
	    String sexp_declarator = HandleDeclarator(p.getDeclarator(), tab + sep);
	    if (print) System.out.println (tab + " - " + "decl-specifier");
	    String sexp_declsp = HandleDeclSpecifier(p.getDeclSpecifier(), tab + sep);
	    result = "(parameter-declaration" 
		+ " :declarator " + sexp_declarator 
		+ " :decl-specifier " + sexp_declsp 
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println (tab + "ParameterDeclaration: " + p.toString());
	    if (print) System.out.println (tab + " - " + "declarator");
	    HandleDeclarator(p.getDeclarator(), tab + sep);
	    if (print) System.out.println (tab + " - " + "decl-specifier");
	    HandleDeclSpecifier(p.getDeclSpecifier(), tab + sep);
	    result = "[ParameterDeclaration " + p.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }

    private static String HandleDeclSpecifier (IASTDeclSpecifier ds, String tab) {
	String sexp_loc = FileLocationToSexp(ds.getFileLocation());
	String result;
	if (ds instanceof CASTSimpleDeclSpecifier) {
	    if (print) System.out.println(tab + "CASTSimpleDeclSpecifier.");
	    ArrayList<String> modifiers = new ArrayList<>();
	    if (((CASTSimpleDeclSpecifier) ds).isLong()) {
		modifiers.add("long");
	    } 
	    if (((CASTSimpleDeclSpecifier) ds).isLongLong()) {
		modifiers.add("long-long");
	    } 
	    if (((CASTSimpleDeclSpecifier) ds).isShort()) {
		modifiers.add("short");
	    } 
	    if (((CASTSimpleDeclSpecifier) ds).isComplex()) {
		modifiers.add("complex");
	    } 
	    if (((CASTSimpleDeclSpecifier) ds).isImaginary()) {
		modifiers.add("imaginary");
	    } 
	    if (((CASTSimpleDeclSpecifier) ds).isSigned()) {
		modifiers.add("signed");
	    } 
	    if (((CASTSimpleDeclSpecifier) ds).isUnsigned()) {
		modifiers.add("unsigned");
	    } 
	    String sexp_modifiers = "";
	    for (int i=0; i<modifiers.size(); i++) {
		sexp_modifiers = sexp_modifiers
		    + modifiers.get(i)
		    + ((i<modifiers.size() - 1) ? " " : "");
	    }
	    if (print) System.out.println(tab + " - " + "type: " + int_to_type(((CASTSimpleDeclSpecifier) ds).getType()));
	    String sexp_type = int_to_type(((CASTSimpleDeclSpecifier) ds).getType());
	    result = "(decl-specifier" 
		+ " :decl-type " + "(decl-type" + " :base " + sexp_type + " :modifiers " + "(" + sexp_modifiers + "))"
		+ (addLocs ? " :loc " + sexp_loc : "") + ")";
	} else {
	    if (print) System.out.println(tab + ds.toString());
	    result = "[DeclSpecifier " + ds.toString() + " :loc " + sexp_loc + "]";
	}
	return result;
    }

    private static String int_to_binary_operator (int i) {
	if (i == IASTBinaryExpression.op_assign) {
	    return "assign";
	} else if (i == IASTBinaryExpression.op_binaryAnd) {
	    return "bin-and";
	} else if (i == IASTBinaryExpression.op_binaryAndAssign) {
	    return "assign-bin-and";
	} else if (i == IASTBinaryExpression.op_binaryOr) {
	    return "bin-or";
	} else if (i == IASTBinaryExpression.op_binaryOrAssign) {
	    return "assign-bin-or";
	} else if (i == IASTBinaryExpression.op_binaryXor) {
	    return "binXor";
	} else if (i == IASTBinaryExpression.op_binaryXorAssign) {
	    return "assign-bin-xor";
	} else if (i == IASTBinaryExpression.op_divide) {
	    return "/";
	} else if (i == IASTBinaryExpression.op_divideAssign) {
	    return "assign-/";
	} else if (i == IASTBinaryExpression.op_equals) {
	    return "==";
	} else if (i == IASTBinaryExpression.op_greaterEqual) {
	    return ">=";
	} else if (i == IASTBinaryExpression.op_greaterThan) {
	    return ">";
	} else if (i == IASTBinaryExpression.op_lessEqual) {
	    return "<=";
	} else if (i == IASTBinaryExpression.op_lessThan) {
	    return "<";
	} else if (i == IASTBinaryExpression.op_logicalAnd) {
	    return "log-and";
	} else if (i == IASTBinaryExpression.op_logicalOr) {
	    return "log-or";
	} else if (i == IASTBinaryExpression.op_minus) {
	    return "-";
	} else if (i == IASTBinaryExpression.op_minusAssign) {
	    return "assign--";
	} else if (i == IASTBinaryExpression.op_modulo) {
	    return "mod";
	} else if (i == IASTBinaryExpression.op_moduloAssign) {
	    return "assign-mod";
	} else if (i == IASTBinaryExpression.op_multiply) {
	    return "*";
	} else if (i == IASTBinaryExpression.op_multiplyAssign) {
	    return "assign-*";
	} else if (i == IASTBinaryExpression.op_notequals) {
	    return "!=";
	} else if (i == IASTBinaryExpression.op_plus) {
	    return "+";
	} else if (i == IASTBinaryExpression.op_plusAssign) {
	    return "assign-+";
	} else if (i == IASTBinaryExpression.op_shiftLeft) {
	    return "<<";
	} else if (i == IASTBinaryExpression.op_shiftLeftAssign) {
	    return "<<=";
	} else if (i == IASTBinaryExpression.op_shiftRight) {
	    return ">>";
	} else if (i == IASTBinaryExpression.op_shiftRightAssign) {
	    return ">>=";
	} else {
	    return "???";
	}
    }

    // https://www.cct.lsu.edu/~rguidry/eclipse-doc36/org/eclipse/cdt/core/dom/ast/IASTUnaryExpression.html
    private static String int_to_unary_operator (int i) {
	if (i == IASTUnaryExpression.op_amper) {
	    return "reference";
	} else if (i == IASTUnaryExpression.op_minus) {
	    return "-";
	} else if (i == IASTUnaryExpression.op_plus) {
	    return "+";
	} else if (i == IASTUnaryExpression.op_star) {
	    return "dereference";
	} else if (i == IASTUnaryExpression.op_not) {
	    return "log-not";
	} else if (i == IASTUnaryExpression.op_tilde) {
	    return "bin-not";
	} else if (i == IASTUnaryExpression.op_postFixDecr) {
	    return "decr-post";
	} else if (i == IASTUnaryExpression.op_postFixIncr) {
	    return "incr-post";
	} else if (i == IASTUnaryExpression.op_prefixDecr) {
	    return "decr-pre";
	} else if (i == IASTUnaryExpression.op_prefixIncr) {
	    return "incr-pre";
	} else if (i == IASTUnaryExpression.op_bracketedPrimary) {
	    return "parens";
	} else {
	    return "???";
	}
    }

    // https://www.cct.lsu.edu/~rguidry/eclipse-doc36/org/eclipse/cdt/core/dom/ast/IASTSimpleDeclSpecifier.html
    private static String int_to_type (int i) {
	if (i == IASTSimpleDeclSpecifier.t_auto) {
	    return "auto";
	} else if (i == IASTSimpleDeclSpecifier.t_bool) {
	    return "bool";
	} else if (i == IASTSimpleDeclSpecifier.t_char) {
	    return "char";
	} else if (i == IASTSimpleDeclSpecifier.t_int) {
	    return "int";
	} else if (i == IASTSimpleDeclSpecifier.t_float) {
	    return "float";
	} else if (i == IASTSimpleDeclSpecifier.t_double) {
	    return "double";
	} else if (i == IASTSimpleDeclSpecifier.t_void) {
	    return "void";
	}
	return "???";
    }

    // https://www.cct.lsu.edu/~rguidry/eclipse-doc36/org/eclipse/cdt/core/dom/ast/IASTLiteralExpression.html#getKind()
    private static String int_to_literal_kind (int i) {
	if (i == IASTLiteralExpression.lk_char_constant) {
	    return "char-constant";
	} else if (i == IASTLiteralExpression.lk_false) {
	    return "false-constant";
	} else if (i == IASTLiteralExpression.lk_float_constant) {
	    return "float-constant";
	} else if (i == IASTLiteralExpression.lk_integer_constant) {
	    return "integer-constant";
	} else if (i == IASTLiteralExpression.lk_string_literal) {
	    return "string-constant";
	} else if (i == IASTLiteralExpression.lk_true) {
	    return "true-constant";
	}
	return "???";
    }

    // https://www.cct.lsu.edu/~rguidry/eclipse-doc36/org/eclipse/cdt/core/dom/ast/IBasicType.html
    private static String int_to_type_modifier (int i) {
	String result = "(";
	if (i == IBasicType.IS_LONG) {
	    result += "long ";
	} else if (i == IBasicType.IS_LONG_LONG) {
	    result += "long-long ";
	} else if (i == IBasicType.IS_SHORT) {
	    result += "short ";
	} else if (i == IBasicType.IS_SIGNED) {
	    result += "signed ";
	} else if (i == IBasicType.IS_UNSIGNED) {
	    result += "unsigned ";
	} 
	result += ")";
	return result;
    }





}
