
(define-condition unimplemented (error)
  ((text :initarg :text :reader text)))

; ====================================================

; 'a -> int
(defun hash-function (x)
  (sxhash x))

; ====================================================

; boolean is [U nil t]

(defparameter bool.false nil)
(defparameter bool.true t)

; ====================================================

; pair is (pair.make x y) = (list x y)

; 'a 'b -> ['a 'b pair]
(defun pair.make (x y)
  (list x y))

; ['a 'b pair] -> 'a
(defun pair.first (p)
  (car p))

; ['a 'b pair] -> 'b
(defun pair.second (p)
  (cadr p))

; ====================================================

; ['a option] = nil | ('some 'a)

(define-condition option.none-error (error)
  ((text :initarg :text :reader text)))

; ['a option]
(defparameter option.none 'none)

; ['a option] -> boolean
(defun option.exists (option)
  (not (equal option 'none)))

; 'a -> ['a option]
(defun option.some (x)
  (list 'some x))

; ['a option] -> 'a
(defun option.get (option-x)
  (if (option.exists option-x)
      (cadr option-x)
    (error 'option.none-error option-x)))

; ====================================================

; greater
(defparameter ord.gr 'ord.gr)
; less
(defparameter ord.ls 'ord.ls)
; equal
(defparameter ord.eq 'ord.eq)
; incomparable
(defparameter ord.inc 'ord.inc)

; order = [U ord.gr ord.ls ord.eq ord.inc]

; ====================================================

(define-condition list.length-error (error)
  ((text :initarg :text :reader text)))

(define-condition list.length-error/aux (error)
  ((text :initarg :text :reader text)))

; int int -> [int list]
; returns (list a1, a1+1, ..., a2-2, a2-1)
; and () if a1 >= a2
(defun create-range (a1 a2)
  (if (>= a1 a2)
      ()
    (cons a1 (create-range (+ a1 1) a2))))

; ['a list] -> int
(defun list.length (l)
  (len l))

; 'a ['a list] -> boolean
(defun list.in (e l)
  (if (endp l)
      bool.false
    (or (equal e (car l))
	(list.in e (cdr l)))))

; 'a ['a list] -> ['a list]
(defun list.remove (e l)
  (if (endp l)
      ()
    (if (equal e (car l))
	(cdr l)
	(cons (car l) (list.remove e (cdr l))))))

; ['a -> bool] ['a list] -> 'a option
(defun list.find (find l)
  (if (endp l)
      option.none
    (if (funcall find (car l))
	(option.some (car l))
      (list.find find (cdr l)))))

; ['a list] N -> 'a
(defun list.nth/aux (list n)
  (if (endp list)
      (error 'list.length-error/aux)
    (if (equal n 0)
	(car list)
      (list.nth/aux (cdr list) (- n 1)))))

; ['a 'b 'c -> 'c] 'c ['a list] ['b list] -> 'c
(defun list.nth (list n)
  (handler-case (list.nth/aux list n)
      (list.length-error/aux () (error 'list.length-error (list list n)))))


; ['a list] N -> ['a list]
(defun list.take (list n)
  (if (or (endp list) (= n 0))
      ()
    (cons (car list) (list.take (cdr list) (- n 1)))))

; ['a list] -> 'a
; returns the last element of a list
(defun list.last (list)
  (if (endp list)
      (error 'list.length-error)
    (if (endp (cdr list))
	(car list)
      (list.last (cdr list)))))

; ['a list] -> ['a list]
(defun list.reverse (l)
  (labels ((list.reverse-acc (l acc)
			     (if (endp l) acc (list.reverse-acc (cdr l) (cons (car l) acc)))))
	  (list.reverse-acc l ())))

; ['a -> 'b] ['a list] -> ['b list]
(defun list.map (f l)
  (if (endp l)
      l
    (cons (funcall f (car l)) (list.map f (cdr l)))))

; ['a -> boolean] ['a list] -> ['a list]
(defun list.filter (p l)
  (if (endp l)
      l
    (if (funcall p (car l))
	(cons (car l) (list.filter p (cdr l)))
      (list.filter p (cdr l)))))

; ['a 'b -> 'b] 'b ['a list] -> 'b
(defun list.foldr (f base list)
  (if (endp list)
      base
    (funcall f (car list) (list.foldr f base (cdr list)))))

; ['a 'b -> 'b] 'b ['a list] -> 'b
(defun list.foldl (f base list)
  (if (endp list)
      base
    (list.foldl f (funcall f (car list) base) (cdr list))))

; ['a 'b 'c -> 'c] 'c ['a list] ['b list] -> 'c
(defun list.foldr2/aux (f base list1 list2)
  (cond ((and (endp list1) (endp list2)) base)
	((or (endp list1) (endp list2)) (error 'list.length-error/aux))
	(t (funcall f (car list1) (car list2) (list.foldr2/aux f base (cdr list1) (cdr list2))))))

; ['a 'b 'c -> 'c] 'c ['a list] ['b list] -> 'c
(defun list.foldr2 (f base list1 list2)
  (handler-case (list.foldr2/aux f base list1 list2)
      (list.length-error/aux () (error 'list.length-error (list f base list1 list2)))))

; ['a 'b 'c -> 'c] 'c ['a list] ['b list] -> 'c
(defun list.foldl2/aux (f base list1 list2)
  (cond ((and (endp list1) (endp list2)) base)
	((or (endp list1) (endp list2)) (error 'list.length-error/aux))
	(t (list.foldl2/aux f (funcall f (car list1) (car list2) base) (cdr list1) (cdr list2)))))

; ['a 'b 'c -> 'c] 'c ['a list] ['b list] -> 'c
(defun list.foldl2 (f base list1 list2)
  (handler-case (list.foldl2/aux f base list1 list2)
      (list.length-error/aux () (error 'list.length-error (list f base list1 list2)))))

; ['a -> boolean] ['a list] -> boolean
(defun list.exists (f list)
  (if (endp list) bool.false (or (funcall f (car list)) (list.exists f (cdr list)))))

; ['a -> boolean] ['a list] -> boolean
(defun list.forall (f list)
  (if (endp list) bool.true (and (funcall f (car list)) (list.forall f (cdr list)))))

; 'a ['a list] ['a 'a -> boolean] -> ['a list]
(defun list.insert (x xs lt)
  (if (endp xs)
      (list x)
    (if (funcall lt x (car xs))
	(cons x xs)
      (cons (car xs) (list.insert x (cdr xs) lt)))))

; ['a list] ['a 'a -> boolean] -> ['a list]
(defun list.sort (xs lt)
  (if (endp xs) () (list.insert (car xs) (list.sort (cdr xs) lt) lt)))

; 'a [['a -> boolean] list] -> boolean
(defun list.satisfies-all (x fs)
  (list.foldr #'(lambda (f acc) (and acc (funcall f x))) bool.true fs))

; 'a [['a -> boolean] list] -> boolean
(defun list.satisfies-any (x fs)
  (list.foldr #'(lambda (f acc) (or acc (funcall f x))) bool.false fs))

; ['a -> 'b] ['a list] -> '_
(defun list.iter (fun list)
  (if (endp list)
      '_
    (progn (funcall fun (car list))
	   (list.iter fun (cdr list)))))

(defun list.zip (lst1 lst2)
  (list.foldr2 #'(lambda (a b acc) (cons (list a b) acc)) () lst1 lst2))

; [['a list] list] -> ['a list]
(defun list.concat (lst)
  (list.foldr #'append () lst))

(defun list.cartesian/acc (lst1 lst2 acc)
  (if (endp lst1)
      acc
    (list.cartesian/acc (cdr lst1) lst2
			(list.foldr #'(lambda (e2 acc)
					(cons (list (car lst1) e2) acc))
				    acc lst2))))

; [a list] [b list] -> [(list a b) list]
(defun list.cartesian (lst1 lst2)
  (if (or (endp lst1) (endp lst2))
      ()
    (list.cartesian/acc lst1 lst2 ())))

; ===================================================

#|
; ['a set] is ['a list]

(defun set.empty ())

; ['a list] -> ['a set]
(defun set.from-list (l)
  (if (endp l)
      set.empty
    (if set.contains )))
|#

; ===================================================

; map is [['a 'b pair] list]

; ['a 'b map]
(defparameter map.empty ())

; ['a 'b map] -> boolean
(defun map.is-empty (map)
  (equal map map.empty))

; 'a 'b -> ['a 'b map]
; creates a singleton map from the given pair
(defun map.singleton (x y)
  (list (pair.make x y)))

(define-condition map.existing-key-error (error)
  ((text :initarg :text :reader text)))

(define-condition map.no-key-error (error)
  ((text :initarg :text :reader text)))

(define-condition map.existing-key-error/aux (error)
  ((text :initarg :text :reader text)))

(define-condition map.no-key-error/aux (error)
  ((text :initarg :text :reader text)))

(defun map.add-overwrite (map x y)
  (if (map.is-empty map)
      (map.singleton x y)
    (if (equal (pair.first (car map)) x)
	(cons (pair.make x y) (cdr map))
      (cons (car map) (map.add-overwrite (cdr map) x y)))))
  
; ['b -> 'b] ['a 'b map] 'a 'b -> ['a 'b map]
(defun map.add-combine (f map x y)
  (if (map.is-empty map)
      (map.singleton x y)
    (if (equal (pair.first (car map)) x)
	(cons (pair.make x (funcall f (pair.second (car map))))
	      (cdr map))
      (cons (car map) (map.add-combine f (cdr map) x y)))))

; adds the binding for x in the map, erroring if there is one already
; ['a 'b map] 'a 'b -> ['a 'b map]
(defun map.add-exn/aux (map x y)
  (if (map.is-empty map)
      (map.singleton x y)
    (if (equal (pair.first (car map)) x)
	(error 'map.existing-key-error/aux)
      (cons (car map) (map.add-exn/aux (cdr map) x y)))))

(defun map.add-exn (map x y)
  (handler-case (map.add-exn/aux map x y)
    (map.existing-key-error/aux () (error 'map.existing-key-error (list map x y)))))

; ['a 'b map] 'a -> boolean
(defun map.contains (map x)
  (if (map.is-empty map)
      bool.false
    (or (equal (pair.first (car map)) x)
	(map.contains (cdr map) x))))

; ['a 'b map] 'a -> ['b option]
; finds the associated value for x in the map
(defun map.find (map x)
  (if (map.is-empty map)
      option.none
    (if (equal (pair.first (car map)) x)
	(option.some (pair.second (car map)))
      (map.find (cdr map) x))))

; ['a 'b map] 'a -> 'b
; finds the associated value for x in the map, erroring if there is not one
(defun map.find-exn/aux (map x)
  (if (map.is-empty map)
      (error 'map.no-key-error/aux)
    (if (equal (pair.first (car map)) x)
	(pair.second (car map))
      (map.find-exn/aux (cdr map) x))))

(defun map.find-exn (map x)
  (handler-case (map.find-exn/aux map x)
    (map.no-key-error/aux () (error 'map.no-key-error (list map x)))))

; ['a 'b map] 'a 'b -> 'b
; finds the associated value for x in the map, returning the default if there is not one
(defun map.find-default (map x default)
  (let* ((res (map.find map x)))
    (if (option.exists res)
	(option.get res)
      default)))

; ['a 'b -> 'c] ['a 'b map] -> ['a 'c map]
(defun map.map (f map)
  (if (map.is-empty map)
      map.empty
    (cons (pair.make (pair.first (car map))
		     (funcall f (pair.first (car map)) (pair.second (car map))))
	  (map.map f (cdr map)))))

; ['a 'b map] ('a 'b -> boolean) -> ['a 'b map]
(defun map.filter (f map)
  (if (map.is-empty map)
      map.empty
    (if (funcall f (pair.first (car map)) (pair.second (car map)))
	(cons (car map) (map.filter f (cdr map)))
      (map.filter f (cdr map)))))

(defun map.remove (key map)
  (map.filter #'(lambda (key* _) (not (equal key key*))) map))

; ('a 'b 'c -> 'c) 'c ['a 'b map] -> 'c
; note: f should be "commutative"
(defun map.fold (f default map)
  (if (map.is-empty map)
      default
    (funcall f (pair.first (car map)) (pair.second (car map)) (map.fold f default (cdr map)))))

; ('a 'b -> boolean) ['a 'b map] -> boolean
(defun map.forall (f map)
  (if (map.is-empty map)
      bool.true
    (and (funcall f (pair.first (car map)) (pair.second (car map)))
	 (map.forall f (cdr map)))))

; ('a 'b -> boolean) ['a 'b map] -> boolean
(defun map.exists (f map)
  (if (map.is-empty map)
      bool.false
    (or (funcall f (pair.first (car map)) (pair.second (car map)))
	(map.exists f (cdr map)))))

; [['a 'b pair] list] -> ['a 'b map]
(defun map.from-list (l)
  (list.foldr #'(lambda (p acc)
		  (map.add-exn acc (pair.first p) (pair.second p)))
	      map.empty l))

; ['a 'b map] -> [['a 'b pair] list]
(defun map.to-list (l)
  l)

; ['a 'b map] -> ['a list]
(defun map.keys (map)
  (map.fold #'cons () map))

; ('a 'b -> '_) ['a 'b map] -> '_
(defun map.iter (f map)
  (if (map.is-empty map)
      '_
    (progn (funcall f (pair.first (car map)) (pair.second (car map)))
	   (map.iter f (cdr map)))))

; ===================================================

; ['a 'b mmap] is (list ['a list] ['a 'b hash-table])

(defun mmap.empty ()
  (list () (make-hash-table :test 'equal)))

; ['a 'b mmap] -> ['a list]
(defun mmap.keys (mmap)
  (car mmap))

; ['a 'b mmap] -> boolean
(defun mmap.is-empty (mmap)
  (endp (mmap.keys mmap)))

(define-condition mmap.existing-key-error (error)
  ((text :initarg :text :reader text)))

(define-condition mmap.no-key-error (error)
  ((text :initarg :text :reader text)))

; ['a 'b mmap] 'a 'b -> '_
(defun mmap.add-overwrite (mmap x y)
  (if (list.in x (mmap.keys mmap))
      '_
    (setf (car mmap) (cons x (mmap.keys mmap))))
  (setf (gethash x (cadr mmap)) y)
  '_)

; ['a 'b mmap] 'a [['b option] -> 'b] -> '_
(defun mmap.add-combine (mmap x f)
  (let* ((y (funcall f (mmap.find mmap x))))
    (mmap.add-overwrite mmap x y)))

; ['a 'b mmap] 'a 'b -> '_
; adds the binding for x in the mmap, erroring if there is one already
(defun mmap.add-exn (mmap x y)
  (if (option.exists (mmap.find mmap x))
      (error 'mmap.existing-key-error)
    (mmap.add-overwrite mmap x y)))

; ['a 'b mmap] 'a -> boolean
(defun mmap.contains (mmap x)
  (option.exists (mmap.find mmap x)))

; ['a 'b mmap] 'a -> ['b option]
; finds the associated value for x in the map
(defun mmap.find (mmap x)
  (multiple-value-bind (result exists) (gethash x (cadr mmap))
		       (if exists
			   (option.some result)
			 option.none)))

; ['a 'b mmap] 'a -> 'b
; finds the associated value for x in the mmap, erroring if there is not one
(defun mmap.find-exn (mmap x)
  (let* ((res (mmap.find mmap x)))
    (if (option.exists res)
	(option.get res)
      (error 'mmap.no-key-error))))

; ['a 'b mmap] 'a 'b -> 'b
; finds the associated value for x in the mmap, returning the default if there is not one
(defun mmap.find-default (mmap x default)
  (let* ((res (mmap.find mmap x)))
    (if (option.exists res)
	(option.get res)
      default)))

; ['a 'b mmap] -> ['a 'b mmap]
(defun mmap.copy (mmap)
  (let* ((new (mmap.empty)))
    (list.iter #'(lambda (x)
		    (mmap.add-overwrite new x (mmap.find-exn mmap x)))
	       (mmap.keys mmap))))

; ('a 'b -> '_) ['a 'b mmap] -> '_
; note: can add keys to the map during this time, but they will not be affected by the map
(defun mmap.iter (f mmap)
  (list.iter #'(lambda (x)
		 (funcall f x (mmap.find-exn mmap x)))
	     (mmap.keys mmap)))

; ('a 'b 'c -> 'c) 'c ['a 'b mmap] -> 'c
; note: f should be commutative: (f a b (f c d e)) = (f c d (f a b e))
(defun mmap.fold (f default mmap)
  (list.foldl #'(lambda (x acc)
		  (funcall f x (mmap.find-exn mmap x) acc))
	      default (mmap.keys mmap)))

; [['a 'b pair] list] -> ['a 'b mmap]
; requires no duplicates for the keys in the list.
(defun mmap.from-list (l)
  (let* ((new (mmap.empty)))
    (list.iter #'(lambda (p) (mmap.add-exn new (pair.first p) (pair.second p))) l)))

; ['a 'b mmap] -> [['a 'b pair] list]
(defun mmap.to-list (mmap)
  (mmap.fold #'(lambda (a b c)
		 (cons (pair.make a b) c))
	     () mmap))

; ===================================================

; Note: could add a cache field for the top element to make queue-top always be O(1), but right now it is rarely O(n)

; an error for the queue operations when an empty queue occurs where it shouldn't
(define-condition queue.empty-error (error)
  ((text :initarg :text :reader text)))

; the empty queue
(defparameter queue.empty (cons () ()))

; pushes an element e onto the given queue
; ['a queue] 'a -> ['a queue]
(defun queue.push (queue e)
  (cons (cons e (car queue))
	(cdr queue)))

; gets the top (or front) element of the given queue
; ['a queue] -> 'a
(defun queue.top (queue)
  (if (endp (cdr queue))
      (if (endp (car queue))
	  (error 'queue.empty-error)
	(list.last (car queue)))
    (car (cdr queue))))

; returns a queue that has popped the top off of the given queue
; ['a queue] -> ['a queue]
(defun queue.pop (queue)
  (if (endp (cdr queue))
      (if (endp (car queue))
	  (error 'queue.empty-error)
	(cons '() (cdr (reverse (car queue)))))
    (cons (car queue) (cdr (cdr queue)))))

; returns whether the given queue is empty
; ['a queue] -> boolean
(defun queue.is-empty (queue)
  (and (endp (cdr queue))
       (endp (car queue))))

; returns a list of the queue's elements, where the top element is the first element
; : ['a queue] -> ['a list]
(defun queue.to-list (queue)
  (append (cdr queue) (reverse (car queue))))

; creates a queue from the given list, where the first element is the top element
; : ['a list] -> ['a queue]
(defun queue.from-list (list)
  (cons () list))


; '_ -> ['a 'b hash-table]
(defun hash-table.empty ()
  (make-hash-table :test 'equal))

(define-condition hash-table.no-key-error (error)
  ((text :initarg :text :reader text)))

; ['a 'b hash-table] 'a -> 'b option
(defun hash-table.find (hash-table key)
  (multiple-value-bind (result exists) (gethash key hash-table)
		       (if exists
			   (option.some result)
			 option.none)))

; ['a 'b hash-table] 'a -> 'b option
(defun hash-table.find-exn (hash-table key)
  (multiple-value-bind (result exists) (gethash key hash-table)
		       (if exists
			   result
			 (error 'hash-table.no-key-error))))

; ['a 'b hash-table] 'a 'b -> ()
(defun hash-table.add (hash-table key value)
  (setf (gethash key hash-table) value))



; ['a graph] = (list N [N 'a hash-table] [N [N list] hash-table])

; '_ -> ['a graph]
(defun graph.empty ()
  (list 0 (hash-table.empty) (hash-table.empty)))

; ['a graph] -> int
(defun graph.nv (graph)
  (car graph))

; ['a graph] -> [int 'a hashtable]
(defun graph.data (graph)
  (cadr graph))

; ['a graph] -> [int [int list] hash-table]
(defun graph.edges (graph)
  (caddr graph))

; ['a graph] -> 'a
; n must be in the graph
(defun graph.vertex-data (graph n)
  (hash-table.find-exn (graph.data graph) n))

; ['a graph] -> [int list]
; n must be in the graph
(defun graph.vertex-edges (graph n)
  (hash-table.find-exn (graph.edges graph) n))

; ['a graph] -> '_
(defun graph.add-vertex (graph a)
  (let* ((n (graph.nv graph)))
    (hash-table.add (graph.data graph) n a)
    (hash-table.add (graph.edges graph) n ())
    (setf (car graph) (+ n 1))
    '_))

; ['a graph] int int -> '_
(defun graph.add-edge (graph n1 n2)
  (let* ((n1-edges (graph.vertex-edges graph n1)))
    (if (and (not (equal n1 n2)) (not (list.in n2 n1-edges)))
	(progn (hash-table.add (graph.edges graph) n1 (cons n2 n1-edges))
	       '_)
      '_)))

; ['a graph] -> ['a graph]
(defun graph.transpose (graph)
  (let* ((n (graph.nv graph))
	 (new-graph (graph.empty)))
    (progn (list.iter #'(lambda (n*) (graph.add-vertex new-graph (graph.vertex-data graph n*)))
		      (create-range 0 n))
	   (list.iter #'(lambda (n*)
			  (list.iter #'(lambda (n**) 
					 (graph.add-edges new-graph n** n*))
				     (graph.vertex-edges graph n*)))
		      (create-range 0 n))
	   new-graph)))

(define-condition graph.cycle (error)
  ((text :initarg :text :reader text)))

(defun graph.top-sort (graph)
  (labels ((handle-list (list)
			(if (endp list)
			    ()
			  (let* ((res (list.find #'(lambda (e) 
						     (list.forall #'(lambda (e*) (not (list.in e* list)))
								  (graph.vertex-edges graph e)))
						 list)))
			    (if (option.exists res)
				(cons (option.get res) (handle-list (list.remove (option.get res) list)))
			      (error 'graph.cycle))))))
	  (handle-list (create-range 0 (graph.nv graph)))))


(defun graph.transitive-reduction (graph)
  (error 'unimplemented))
