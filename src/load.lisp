(load "src/acl2s-interaction.lisp")
(quiet-mode-on)

(acl2s-ld "src/itest-cgen.lisp")
(acl2s-ld "src/itest-ithm.lisp")

(load "src/base.lisp")
(load "src/conjecture-generation.lisp")

(setf *print-case* :downcase)

(load "src/c-reasoner-utils.lisp")
(load "src/c-parser.lisp")
(load "src/language-utils.lisp")
(load "src/pgraph.lisp")
(load "src/pgraph-conversion.lisp")
(load "src/c-source.lisp")
(load "src/traces.lisp")
(load "src/invariant-utils.lisp")
(load "src/special-invariants.lisp")
(load "src/invariants.lisp")
(load "src/generate-witness.lisp")
(load "src/generate-violation-witness.lisp")
(load "src/generate-and-prove-invariants.lisp")

;(declaim (sb-ext:muffle-conditions cl:warning))
;(declaim (sb-ext:muffle-conditions cl:style-warning))
