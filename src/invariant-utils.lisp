#|
;; term [var list] -> [var list]
(defun term-variables-acc (term acc)
  (cond ((term.is-const term) acc)
	((term.is-var term) (list-union (list (term.var-sym term)) acc))
	((term.is-fun term) (list.foldr #'(lambda (subterm acc)
					    (term-variables-acc subterm acc))
					acc (term.args term)))))
|#
;; term S -> [var list]
(defun term-variables (term S)
  (let* ((var-sizes (term.var-sizes term))
	 (variables (S.var-syms S))
	 (index 0)
	 (len (array-dimension var-sizes 0))
	 (res ()))
    (loop for i from 0 to (- len 1)
	  do (progn (if (equal (aref var-sizes index) 0) '_ (setf res (cons (car variables) res)))
		    (setf index (+ index 1))
		    (setf variables (cdr variables))))
    ;;(print (list (term.destruct term) res var-sizes (S.var-syms S)))
    res))

; [equality list] -> [equality list]
(defun inv-gen-sort-equalities (eqs)
  (list.sort eqs
	     #'(lambda (eq1 eq2)
		 (let* ((lhs1 (equality.lhs eq1))
			(rhs1 (equality.rhs eq1))
			(lhs2 (equality.lhs eq2))
			(rhs2 (equality.rhs eq2))
			(type1 (term.type lhs1))
			(type2 (term.type lhs2))
			(ls1 (term.size lhs1))
			(rs1 (term.size rhs1))
			(ls2 (term.size lhs2))
			(rs2 (term.size rhs2)))
		   (or (and (not (equal type1 type2))
			    (cond ((and (equal type1 'booleanp)
					(equal type1 'integerp))
				   t)
				  ((and (equal type1 'integerp)
					(equal type1 'booleanp))
				   nil)))
		       (and (equal type1 type2)
			    (or (and (= (min ls1 rs1) (min ls2 rs2))
				     (> (+ ls1 rs1) (+ ls2 rs2)))
				(> (min ls1 rs1) (min ls2 rs2)))))))))

; [hyp list] * [ACL2-expression list] -> [ACL2-expression list]
(defun inv-gen-find-minimal-destructed (req-hyps exps)
  (if (endp exps)
      ()
    (let* ((e (car exps))
	   (exps* (inv-gen-find-minimal-destructed req-hyps (cdr exps))))
      (if (acl2s-thm (list 'implies 
			   (cons 'and (append req-hyps exps*))
			   e))
	  exps*
	(cons e exps*)))))

(defun inv-gen-remove-true-destructed (req-hyps exps)
  (if (endp exps)
      ()
    (let* ((e (car exps))
	   (exps* (inv-gen-remove-true-destructed req-hyps (cdr exps))))
      (if (acl2s-thm (list 'implies 
			   (cons 'and req-hyps)
			   e))
	  exps*
	(cons e exps*)))))

; [hyp list] * [equality list] -> [equality list]
(defun inv-gen-find-minimal (req-hyps eqs)
  (if (endp eqs)
      ()
    (let* ((e (car eqs))
	   (eqs* (inv-gen-find-minimal req-hyps (cdr eqs))))
      (if (acl2s-thm (list 'implies 
			   (cons 'and (append req-hyps (list.map #'equality.destruct eqs*)))
			   (equality.destruct e)))
	  eqs*
	(cons e eqs*)))))

