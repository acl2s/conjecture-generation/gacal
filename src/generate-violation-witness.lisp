
(defun violation-witness-file-string (src-file violation-witness-graph-string)
  (string-append-all
   (list
    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" *new-line*
    "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" *new-line*
    ;" <key attr.name=\"originFileName\" attr.type=\"string\" for=\"edge\" id=\"originfile\">" *new-line* "  <default>" src-file "  </default>" *new-line* " </key>" *new-line*
    " <key attr.name=\"invariant\" attr.type=\"string\" for=\"node\" id=\"invariant\"/>" *new-line*
    " <key attr.name=\"invariant.scope\" attr.type=\"string\" for=\"node\" id=\"invariant.scope\"/>" *new-line*
    ;" <key attr.name=\"namedValue\" attr.type=\"string\" for=\"node\" id=\"named\"/>" *new-line*
    ; " <key attr.name=\"isFrontierNode\" attr.type=\"boolean\" for=\"node\" id=\"frontier\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    ; " <key attr.name=\"isViolationNode\" attr.type=\"boolean\" for=\"node\" id=\"violation\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    " <key attr.name=\"isEntryNode\" attr.type=\"boolean\" for=\"node\" id=\"entry\">" *new-line*
    "  <default>false</default>" *new-line*
    " </key>" *new-line*
    ; " <key attr.name=\"isSinkNode\" attr.type=\"boolean\" for=\"node\" id=\"sink\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    " <key attr.name=\"enterLoopHead\" attr.type=\"boolean\" for=\"edge\" id=\"enterLoopHead\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    ; " <key attr.name=\"violatedProperty\" attr.type=\"string\" for=\"node\" id=\"violatedProperty\"/>" *new-line*
    ; " <key attr.name=\"threadId\" attr.type=\"string\" for=\"edge\" id=\"threadId\"/>" *new-line*
    " <key attr.name=\"sourcecodeLanguage\" attr.type=\"string\" for=\"graph\" id=\"sourcecodelang\"/>" *new-line*
    " <key attr.name=\"programFile\" attr.type=\"string\" for=\"graph\" id=\"programfile\"/>" *new-line*
    (if *use-program-hash* " <key attr.name=\"programHash\" attr.type=\"string\" for=\"graph\" id=\"programhash\"/>" "")
    (if *use-program-hash* *new-line* "")
    " <key attr.name=\"specification\" attr.type=\"string\" for=\"graph\" id=\"specification\"/>" *new-line*
    " <key attr.name=\"architecture\" attr.type=\"string\" for=\"graph\" id=\"architecture\"/>" *new-line*
    " <key attr.name=\"producer\" attr.type=\"string\" for=\"graph\" id=\"producer\"/>" *new-line*
    (if *use-source-code* " <key attr.name=\"sourcecode\" attr.type=\"string\" for=\"edge\" id=\"sourcecode\"/>" "")
    (if *use-source-code* *new-line* "")
    " <key attr.name=\"startline\" attr.type=\"int\" for=\"edge\" id=\"startline\"/>" *new-line*
    " <key attr.name=\"endline\" attr.type=\"int\" for=\"edge\" id=\"endline\"/>" *new-line*
    " <key attr.name=\"startoffset\" attr.type=\"int\" for=\"edge\" id=\"startoffset\"/>" *new-line*
    " <key attr.name=\"endoffset\" attr.type=\"int\" for=\"edge\" id=\"endoffset\"/>" *new-line*
    " <key attr.name=\"control\" attr.type=\"string\" for=\"edge\" id=\"control\"/>" *new-line*
    ; " <key attr.name=\"assumption\" attr.type=\"string\" for=\"edge\" id=\"assumption\"/>" *new-line*
    ; " <key attr.name=\"assumption.resultfunction\" attr.type=\"string\" for=\"edge\" id=\"assumption.resultfunction\"/>" *new-line*
    ; " <key attr.name=\"assumption.scope\" attr.type=\"string\" for=\"edge\" id=\"assumption.scope\"/>" *new-line*
    " <key attr.name=\"enterFunction\" attr.type=\"string\" for=\"edge\" id=\"enterFunction\"/>" *new-line*
    " <key attr.name=\"returnFromFunction\" attr.type=\"string\" for=\"edge\" id=\"returnFrom\"/>" *new-line*
    ; " <key attr.name=\"predecessor\" attr.type=\"string\" for=\"edge\" id=\"predecessor\"/>" *new-line*
    ; " <key attr.name=\"successor\" attr.type=\"string\" for=\"edge\" id=\"successor\"/>" *new-line*
    " <key attr.name=\"witness-type\" attr.type=\"string\" for=\"graph\" id=\"witness-type\"/>" *new-line*
    violation-witness-graph-string
    "</graphml>" *new-line*)))

; string witness-node -> string
(defun violation-witness-node-to-string (prefix witness-node)
  (cond ((equal (car witness-node) 'witness-node)
	 (string-append-all (list prefix "<node id=\"" (l.find witness-node ':id) "\"/>")))
	((equal (car witness-node) 'violation-node)
	 (string-append-all (list prefix "<node id=\"" (l.find witness-node ':id) "\">" *new-line*
				  prefix " <data key=\"violation\">" (l.find witness-node ':violation) "</data>" *new-line*
				  prefix " <data key=\"violatedProperty\">" (l.find witness-node ':violatedProperty) "</data>" *new-line*
				  prefix "</node>")))
	((equal (car witness-node) 'entry-witness-node)
	 (string-append-all (list prefix "<node id=\"" (l.find witness-node ':id) "\">" *new-line*
				  prefix " <data key=\"entry\">true</data>" *new-line*
				  prefix "</node>")))))
  
; string witness-edge -> string
(defun violation-witness-edge-to-string (prefix witness-edge)
  (string-append-all
   (append (list prefix "<edge source=\"" (l.find witness-edge ':source)
		 "\" target=\"" (l.find witness-edge ':target) "\">" *new-line*)
	   (if (and (equal (l.find witness-edge ':startline) 0)
		    (equal (l.find witness-edge ':endline) 0)
		    (equal (l.find witness-edge ':startoffset) 0)
		    (equal (l.find witness-edge ':endoffset) 0))
	       ()
	     (append (list prefix "<data key=\"startline\">" (write-to-string (l.find witness-edge ':startline)) "</data>" *new-line*)
		     (list prefix "<data key=\"endline\">" (write-to-string (l.find witness-edge ':endline)) "</data>" *new-line*)
		     (list prefix "<data key=\"startoffset\">" (write-to-string (l.find witness-edge ':startoffset)) "</data>" *new-line*)
		     (list prefix "<data key=\"endoffset\">" (write-to-string (l.find witness-edge ':endoffset)) "</data>" *new-line*)))
	   (if *use-source-code*
	       (option-pull (l.find witness-edge ':source-code)
			#'(lambda (source-code) (list prefix "<data key=\"sourcecode\">" (xml-ize-source-code source-code) "</data>" *new-line*))
			())
	     ())
	   (option-pull (l.find witness-edge ':control)
			#'(lambda (control) (list prefix "<data key=\"control\">"
						  (if (equal control 'condition-true) "condition-true" "condition-false")
						  "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':enterFunction)
			#'(lambda (enterFunction) (list prefix "<data key=\"enterFunction\">" enterFunction "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':returnFrom)
			#'(lambda (returnFrom) (list prefix "<data key=\"returnFrom\">" returnFrom "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':enterLoopHead)
			#'(lambda (enterLoopHead) (list prefix "<data key=\"enterLoopHead\">"
							(if enterLoopHead "true" "false")
							"</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':assumption)
			#'(lambda (assumption) (list prefix "<data key=\"assumption\">"
						     assumption
						     "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':assumption.scope)
			#'(lambda (assumption-scope) (list prefix "<data key=\"assumption.scope\">"
						     assumption-scope
						     "</data>" *new-line*))
			())
	   (list prefix "</edge>"))))

(defun violation-witness-graph-string (src-file hash architecture witness-graph)
  (let* ((nodes (car witness-graph))
	 (edges (cadr witness-graph))
	 (node-string (list.foldl #'(lambda (node acc)
				      (string-append-all (list (violation-witness-node-to-string " " node)
								*new-line*
								acc)))
				  "" nodes))
	 (edge-string (list.foldl #'(lambda (edge acc)
				      (string-append-all (list (violation-witness-edge-to-string " " edge)
							       *new-line*
							       acc)))
				  "" edges)))
    (string-append-all
     (list " <graph edgedefault=\"directed\">" *new-line*
	   "  <data key=\"witness-type\">violation_witness</data>" *new-line*
	   "  <data key=\"sourcecodelang\">C</data>" *new-line*
	   "  <data key=\"producer\">" *tool-name* "</data>" *new-line*
	   "  <data key=\"specification\">CHECK( init(main()), LTL(G ! call(__VERIFIER_error())) )</data>" *new-line*
	   "  <data key=\"programfile\">" src-file "</data>" *new-line*
	   (if *use-program-hash* "  <data key=\"programhash\">" "")
	   (if *use-program-hash* hash "")
	   (if *use-program-hash* "</data>" "")
	   (if *use-program-hash* *new-line* "")
	   "  <data key=\"architecture\">" architecture "bit</data>" *new-line*
	   node-string
	   edge-string
	   " </graph>" *new-line*))))


; pgraph [node-name invariant map] node [ACl2-var program-var map] [node expression map] -> witness-graph
(defun create-violation-witness-graph (pgraph trace start-node name-map assertions)
  (let* ((count 1)
	 (prev (let* ((id (string-append "A" (write-to-string count)))
		      (_ (setf count (+ count 1))))
		 (list 'entry-witness-node ':id id)))
	 (witness-nodes (list prev))
	 (witness-edges ()))
    (list.iter #'(lambda (tuple)
		   (let* ((node (car tuple))
			  (state (cadr tuple))
			  (edge (caddr tuple))
			  (node* (cadddr tuple)))
		     (if (or (equal state '_) (equal node* '_))
			 (let* ((next (let* ((id (string-append "A" (write-to-string count)))
					     (_ (setf count (+ count 1))))
					(list 'witness-node ':id id)))
							    
				(final-node (let* ((id (string-append "A" (write-to-string count)))
						   (_ (setf count (+ count 1))))
					      (list 'violation-node ':id id
						    ':violation "true"
						    ':violatedProperty "__VERIFIER_error(); called in line 5")))
				(source-code (edge.source-code edge))
				(startline (l.find (edge.loc edge) ':start-line))
				(endline (l.find (edge.loc edge) ':end-line))
				(startoffset (l.find (edge.loc edge) ':offset))
				(endoffset (+ startoffset (l.find (edge.loc edge) ':length)))
				(final-edges (list (list 'witness-edge
							 ':enterLoopHead option.none
							 ':control option.none
							 ':enterFunction (option.some "__VERIFIER_assert")
							 ':returnFrom option.none
							 ':source (l.find prev ':id)
							 ':target (l.find next ':id)
							 ':source-code source-code
							 ':startline startline
							 ':endline endline
							 ':startoffset startoffset
							 ':endoffset endoffset
							 ':assumption option.none
							 ':assumption.scope option.none)
						   (list 'witness-edge
							 ':enterLoopHead option.none
							 ':control (option.some 'condition-true)
							 ':enterFunction option.none
							 ':returnFrom option.none
							 ':source (l.find next ':id)
							 ':target (l.find final-node ':id)
							 ':source-code (option.some "[!(cond != 0)]")
							 ':startline 4
							 ':endline 4
							 ':startoffset 156
							 ':endoffset 159
							 ':assumption (option.some "cond == 0;")
							 ':assumption.scope (option.some "__VERIFIER_assert")))))
			   (setf witness-nodes (append (list final-node next) witness-nodes))
			   (setf witness-edges (append final-edges witness-edges)))
		       (let* ((witness-node (let* ((id (string-append "A" (write-to-string count)))
						   (_ (setf count (+ count 1))))
					      (cond ((equal node* start-node)
						     (list 'entry-witness-node ':id id))
						    (t (list 'witness-node
							     ':id id)))))
			      (_ (setf witness-nodes (cons witness-node witness-nodes)))
			      (enterLoopHead (edge.enter-loop-head edge))
			      (control (edge.control edge))
			      (returnFrom (edge.return-from edge))
			      (enterFunction (edge.enter-function edge))
			      (source (l.find prev ':id))
			      (target (l.find witness-node ':id))
			      (source-code (edge.source-code edge))
			      (startline (l.find (edge.loc edge) ':start-line))
			      (endline (l.find (edge.loc edge) ':end-line))
			      (startoffset (l.find (edge.loc edge) ':offset))
			      (endoffset (+ startoffset (l.find (edge.loc edge) ':length)))
			      (new-witness-edges (if (edge.is-assertion edge)
						     (let* ((id1 (string-append "A" (write-to-string count)))
							    (_ (setf count (+ count 1)))
							    (id2 (string-append "A" (write-to-string count)))
							    (_ (setf count (+ count 1))))
						       (setf witness-nodes (append (list (list 'witness-node ':id id1) (list 'witness-node ':id id2)) witness-nodes))
						       (list (list 'witness-edge
								   ':enterLoopHead option.none
								   ':control option.none
								   ':enterFunction (option.some "__VERIFIER_assert")
								   ':returnFrom option.none
								   ':source source ':target id1 ':source-code source-code
								   ':startline startline ':endline endline ':startoffset startoffset ':endoffset endoffset
								   ':assumption option.none
								   ':assumption.scope option.none)
							     (list 'witness-edge
								   ':enterLoopHead option.none
								   ':control (option.some 'condition-false)
								   ':enterFunction option.none
								   ':returnFrom option.none
								   ':source id1 ':target id2 ':source-code (option.some "[!(cond != 0)]")
								   ':startline 4 ':endline 4 ':startoffset 156 ':endoffset 159
								   ':assumption (option.some "cond != 0")
								   ':assumption.scope (option.some "__VERIFIER_assert"))
							     (list 'witness-edge
								   ':enterLoopHead option.none
								   ':control option.none
								   ':enterFunction option.none
								   ':returnFrom (option.some "__VERIFIER_assert")
								   ':source id2 ':target target ':source-code (option.some "return;")
								   ':startline 7 ':endline 7 ':startoffset 202 ':endoffset 208
								   ':assumption option.none ':assumption.scope option.none)))
						   (list (list 'witness-edge
							       ':enterLoopHead enterLoopHead
							       ':control control
							       ':returnFrom returnFrom
							       ':enterFunction enterFunction
							       ':source source
							       ':target target
							       ':source-code (if (and (option.exists control) (option.exists source-code))
										 (option.some (string-append-all (list "[" (option.get source-code) "]")))
									       source-code)
							       ':startline startline
							       ':endline endline
							       ':startoffset startoffset
							       ':endoffset endoffset
							       ':assumption (option.some
									     (string-append-all
									      (list.foldr2 #'(lambda (var-pair value acc)
											       (if (and (not (equal (car var-pair) 'nondet))
													(not (equal (car var-pair) 'counter)))
												   (let* ((original (map.find-exn name-map (car var-pair))))
												     (if (integerp original)
													 acc
												       (cons original
													     (cons " == " (cons value (cons ";" acc))))))
												 acc))
											   () (node.live-vars node*) state)))
							       ':assumption.scope (option.some "main")))))
			      (_ (setf witness-edges (append new-witness-edges witness-edges)))
			      (_ (setf prev witness-node)))
			 '_))))
	       (reverse trace))
    (list witness-nodes witness-edges)))

; ...
(defun write-violation-witness-to-file (output-file-path hash src-file architecture violation-witness-graph)
  (let* ((vio-witness-graph-string (violation-witness-graph-string src-file hash architecture
								   violation-witness-graph)))
    (writeToFile output-file-path
		 (violation-witness-file-string src-file vio-witness-graph-string))))
				    
							  
