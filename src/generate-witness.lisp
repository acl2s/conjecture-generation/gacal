
(defparameter *use-source-code* nil)
(defparameter *use-program-hash* t)

(defun replace-all (string part replacement &key (test #'char=))
  "Returns a new string in which all the occurences of the part 
is replaced with replacement."
  (with-output-to-string (out)
    (loop with part-length = (length part)
          for old-pos = 0 then (+ pos part-length)
          for pos = (search part string
                            :start2 old-pos
                            :test test)
          do (write-string string out
                           :start old-pos
                           :end (or pos (length string)))
          when pos do (write-string replacement out)
          while pos)))

(defun xml-ize-source-code (source-code)
  (replace-all
   (replace-all
    (replace-all
     (replace-all
      (replace-all
       source-code
       "\"" "&quot;")
      "\'" "&apos;")
     "&" "&amp;")
    ">" "&gt;")
   "<" "&lt;"))

(defparameter *new-line* (format nil "~%"))

(defun witness-file-string (src-file witness-graph-string)
  (string-append-all
   (list
    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" *new-line*
    "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" *new-line*
    ;" <key attr.name=\"originFileName\" attr.type=\"string\" for=\"edge\" id=\"originfile\">" *new-line* "  <default>" src-file "  </default>" *new-line* " </key>" *new-line*
    " <key attr.name=\"invariant\" attr.type=\"string\" for=\"node\" id=\"invariant\"/>" *new-line*
    " <key attr.name=\"invariant.scope\" attr.type=\"string\" for=\"node\" id=\"invariant.scope\"/>" *new-line*
    ;" <key attr.name=\"namedValue\" attr.type=\"string\" for=\"node\" id=\"named\"/>" *new-line*
    ; " <key attr.name=\"isFrontierNode\" attr.type=\"boolean\" for=\"node\" id=\"frontier\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    ; " <key attr.name=\"isViolationNode\" attr.type=\"boolean\" for=\"node\" id=\"violation\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    " <key attr.name=\"isEntryNode\" attr.type=\"boolean\" for=\"node\" id=\"entry\">" *new-line*
    "  <default>false</default>" *new-line*
    " </key>" *new-line*
    ; " <key attr.name=\"isSinkNode\" attr.type=\"boolean\" for=\"node\" id=\"sink\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    " <key attr.name=\"enterLoopHead\" attr.type=\"boolean\" for=\"edge\" id=\"enterLoopHead\">" *new-line* "  <default>false</default>" *new-line* " </key>" *new-line*
    ; " <key attr.name=\"violatedProperty\" attr.type=\"string\" for=\"node\" id=\"violatedProperty\"/>" *new-line*
    ; " <key attr.name=\"threadId\" attr.type=\"string\" for=\"edge\" id=\"threadId\"/>" *new-line*
    " <key attr.name=\"sourcecodeLanguage\" attr.type=\"string\" for=\"graph\" id=\"sourcecodelang\"/>" *new-line*
    " <key attr.name=\"programFile\" attr.type=\"string\" for=\"graph\" id=\"programfile\"/>" *new-line*
    (if *use-program-hash* " <key attr.name=\"programHash\" attr.type=\"string\" for=\"graph\" id=\"programhash\"/>" "")
    (if *use-program-hash* *new-line* "")
    " <key attr.name=\"specification\" attr.type=\"string\" for=\"graph\" id=\"specification\"/>" *new-line*
    " <key attr.name=\"architecture\" attr.type=\"string\" for=\"graph\" id=\"architecture\"/>" *new-line*
    " <key attr.name=\"producer\" attr.type=\"string\" for=\"graph\" id=\"producer\"/>" *new-line*
    (if *use-source-code* " <key attr.name=\"sourcecode\" attr.type=\"string\" for=\"edge\" id=\"sourcecode\"/>" "")
    (if *use-source-code* *new-line* "")
    " <key attr.name=\"startline\" attr.type=\"int\" for=\"edge\" id=\"startline\"/>" *new-line*
    " <key attr.name=\"endline\" attr.type=\"int\" for=\"edge\" id=\"endline\"/>" *new-line*
    " <key attr.name=\"startoffset\" attr.type=\"int\" for=\"edge\" id=\"startoffset\"/>" *new-line*
    " <key attr.name=\"endoffset\" attr.type=\"int\" for=\"edge\" id=\"endoffset\"/>" *new-line*
    " <key attr.name=\"control\" attr.type=\"string\" for=\"edge\" id=\"control\"/>" *new-line*
    ; " <key attr.name=\"assumption\" attr.type=\"string\" for=\"edge\" id=\"assumption\"/>" *new-line*
    ; " <key attr.name=\"assumption.resultfunction\" attr.type=\"string\" for=\"edge\" id=\"assumption.resultfunction\"/>" *new-line*
    ; " <key attr.name=\"assumption.scope\" attr.type=\"string\" for=\"edge\" id=\"assumption.scope\"/>" *new-line*
    " <key attr.name=\"enterFunction\" attr.type=\"string\" for=\"edge\" id=\"enterFunction\"/>" *new-line*
    " <key attr.name=\"returnFromFunction\" attr.type=\"string\" for=\"edge\" id=\"returnFrom\"/>" *new-line*
    ; " <key attr.name=\"predecessor\" attr.type=\"string\" for=\"edge\" id=\"predecessor\"/>" *new-line*
    ; " <key attr.name=\"successor\" attr.type=\"string\" for=\"edge\" id=\"successor\"/>" *new-line*
    " <key attr.name=\"witness-type\" attr.type=\"string\" for=\"graph\" id=\"witness-type\"/>" *new-line*
    witness-graph-string
    "</graphml>" *new-line*)))

#|
id = string

witness-graph := (list [witness-node list]
		       [witness-edge list])

witness-node := (list 'witness-node
                      ':id id)
              | (list 'entry-witness-node
                      ':id id)
              | (list 'invariant-witness-node
                      ':id id
                      ':invariant source-code
                      ':invariant.scope "main")

witness-edge := (list 'witness-edge
                      ':enterLoopHead [boolean option]
                      ':control [['condition-false condition-true U] option]
                      ':returnFrom [... option]
                      ':enterFunction [... option]
                      ':source id
                      ':target id
                      ':source-code [source-code option]
                      ':startline int
                      ':startoffset int)

|#

(defparameter *tool-name* "GACAL")

; ['a option] ['a -> 'b] 'b -> 'b
(defun option-pull (opt f default)
  (if (option.exists opt)
      (funcall f (option.get opt))
    default))

; string witness-node -> string
(defun witness-node-to-string (prefix witness-node)
  (cond ((equal (car witness-node) 'witness-node)
	 (string-append-all (list prefix "<node id=\"" (l.find witness-node ':id) "\"/>")))
	((equal (car witness-node) 'entry-witness-node)
	 (string-append-all (list prefix "<node id=\"" (l.find witness-node ':id) "\">" *new-line*
				  prefix " <data key=\"entry\">true</data>" *new-line*
				  prefix "</node>")))
	((equal (car witness-node) 'invariant-witness-node)
	 (string-append-all (list prefix "<node id=\"" (l.find witness-node ':id) "\">" *new-line*
				  prefix " <data key=\"invariant\">" (xml-ize-source-code (l.find witness-node ':invariant)) "</data>" *new-line*
				  prefix " <data key=\"invariant.scope\">" (l.find witness-node ':invariant.scope) "</data>" *new-line*
				  prefix "</node>")))))
  
; string witness-edge -> string
(defun witness-edge-to-string (prefix witness-edge)
  (string-append-all
   (append (list prefix "<edge source=\"" (l.find witness-edge ':source)
		 "\" target=\"" (l.find witness-edge ':target) "\">" *new-line*)
	   (if (and (equal (l.find witness-edge ':startline) 0)
		    (equal (l.find witness-edge ':endline) 0)
		    (equal (l.find witness-edge ':startoffset) 0)
		    (equal (l.find witness-edge ':endoffset) 0))
	       ()
	     (append (list prefix "<data key=\"startline\">" (write-to-string (l.find witness-edge ':startline)) "</data>" *new-line*)
		     (list prefix "<data key=\"endline\">" (write-to-string (l.find witness-edge ':endline)) "</data>" *new-line*)
		     (list prefix "<data key=\"startoffset\">" (write-to-string (l.find witness-edge ':startoffset)) "</data>" *new-line*)
		     (list prefix "<data key=\"endoffset\">" (write-to-string (l.find witness-edge ':endoffset)) "</data>" *new-line*)))
	   (if *use-source-code*
	       (option-pull (l.find witness-edge ':source-code)
			#'(lambda (source-code) (list prefix "<data key=\"sourcecode\">" (xml-ize-source-code source-code) "</data>" *new-line*))
			())
	     ())
	   (option-pull (l.find witness-edge ':control)
			#'(lambda (control) (list prefix "<data key=\"control\">"
						  (if (equal control 'condition-true) "condition-true" "condition-false")
						  "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':enterFunction)
			#'(lambda (enterFunction) (list prefix "<data key=\"enterFunction\">" enterFunction "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':returnFrom)
			#'(lambda (returnFrom) (list prefix "<data key=\"returnFrom\">" returnFrom "</data>" *new-line*))
			())
	   (option-pull (l.find witness-edge ':enterLoopHead)
			#'(lambda (enterLoopHead) (list prefix "<data key=\"enterLoopHead\">"
							(if enterLoopHead "true" "false")
							"</data>" *new-line*))
			())
	   (list prefix "</edge>"))))

(defun witness-graph-string (src-file hash architecture witness-graph)
  (let* ((nodes (car witness-graph))
	 (edges (cadr witness-graph))
	 (node-string (list.foldr #'(lambda (node acc)
				      (string-append-all (list (witness-node-to-string " " node)
								*new-line*
								acc)))
				  "" nodes))
	 (edge-string (list.foldr #'(lambda (edge acc)
				      (string-append-all (list (witness-edge-to-string " " edge)
							       *new-line*
							       acc)))
				  "" edges)))
    (string-append-all
     (list " <graph edgedefault=\"directed\">" *new-line*
	   "  <data key=\"witness-type\">correctness_witness</data>" *new-line*
	   "  <data key=\"sourcecodelang\">C</data>" *new-line*
	   "  <data key=\"producer\">" *tool-name* "</data>" *new-line*
	   "  <data key=\"specification\">CHECK( init(main()), LTL(G ! call(__VERIFIER_error())) )</data>" *new-line*
	   "  <data key=\"programfile\">" src-file "</data>" *new-line*
	   (if *use-program-hash* "  <data key=\"programhash\">" "")
	   (if *use-program-hash* hash "")
	   (if *use-program-hash* "</data>" "")
	   (if *use-program-hash* *new-line* "")
	   "  <data key=\"architecture\">" architecture "bit</data>" *new-line*
	   node-string
	   edge-string
	   " </graph>" *new-line*))))


; pgraph [node-name invariant map] node [ACl2-var program-var map] [node expression map] -> witness-graph
(defun create-witness-graph (pgraph proven-invariants start-node name-map assertions)
  (let* ((mmap (mmap.empty))
	 (count 1)
	 (witness-nodes (list.foldr #'(lambda (node acc)
					(let* ((invs-opt (map.find proven-invariants (node.name node)))
					       (id (string-append "N" (write-to-string count)))
					       (_ (setf count (+ count 1))))
					  (mmap.add-exn mmap (node.name node) id)
					  (cond ((equal node start-node)
						 (cons (list 'entry-witness-node ':id id) acc))
						((and (option.exists invs-opt)
						      (not (endp (option.get invs-opt))))
						 (cons (list 'invariant-witness-node
							     ':id id
							     ':invariant (create-source-code-from-invariants (option.get invs-opt) name-map)
							     ':invariant.scope "main")
						       acc))
						(t (cons (list 'witness-node ':id id) acc)))))
				  () (pgraph.nodes pgraph)))
	 (assert-nodes ())
	 (witness-edges (list.foldr #'(lambda (edge acc)
					(let* ((enterLoopHead (edge.enter-loop-head edge))
					       (control (edge.control edge))
					       (returnFrom (edge.return-from edge))
					       (enterFunction (edge.enter-function edge))
					       (source (mmap.find-exn mmap (node.name (edge.from edge))))
					       (target (mmap.find-exn mmap (node.name (edge.to edge))))
					       (source-code (edge.source-code edge))
					       (startline (l.find (edge.loc edge) ':start-line))
					       (endline (l.find (edge.loc edge) ':end-line))
					       (startoffset (l.find (edge.loc edge) ':offset))
					       (endoffset (+ startoffset (l.find (edge.loc edge) ':length))))
					  (if (edge.is-assertion edge)
					      (let* ((id1 (string-append "N" (write-to-string count)))
						     (_ (setf count (+ count 1)))
						     (id2 (string-append "N" (write-to-string count)))
						     (_ (setf count (+ count 1))))
					      (progn (setf assert-nodes
							   (append (list (list 'witness-node ':id id1)
									 (list 'witness-node ':id id2))
								   assert-nodes))
						     (append (list (list 'witness-edge
									 ':enterLoopHead option.none
									 ':control option.none
									 ':enterFunction (option.some "__VERIFIER_assert")
									 ':returnFrom option.none
									 ':source source
									 ':target id1
									 ':source-code source-code
									 ':startline startline
									 ':endline endline
									 ':startoffset startoffset
									 ':endoffset endoffset)
								   (list 'witness-edge
									 ':enterLoopHead option.none
									 ':control option.none
									 ':enterFunction option.none
									 ':returnFrom option.none
									 ':source id1
									 ':target id2
f									 ':source-code (option.some "[!(cond == 0)]")
									 ':startline 4
									 ':endline 4
									 ':startoffset 156
									 ':endoffset 159)
								   (list 'witness-edge
									 ':enterLoopHead option.none
									 ':control option.none
									 ':enterFunction option.none
									 ':returnFrom option.none
									 ':source id1
									 ':target id2
									 ':source-code (option.some "[cond == 0]" )
									 ':startline 4
									 ':endline 4
									 ':startoffset 156
									 ':endoffset 159)
								   (list 'witness-edge
									 ':enterLoopHead option.none
									 ':control option.none
									 ':enterFunction option.none
									 ':returnFrom (option.some "__VERIFIER_assert")
									 ':source id2
									 ':target target
									 ':source-code (option.some "return;")
									 ':startline 7
									 ':endline 7
									 ':startoffset 202
									 ':endoffset 208))
							     acc)))
					      (cons (list 'witness-edge
							  ':enterLoopHead enterLoopHead
							  ':control control
							  ':returnFrom returnFrom
							  ':enterFunction enterFunction
							  ':source source
							  ':target target
							  ':source-code (if (and (option.exists control) (option.exists source-code))
									    (option.some (string-append-all (list "[" (option.get source-code) "]")))
									  source-code)
							  ':startline startline
							  ':endline endline
							  ':startoffset startoffset
							  ':endoffset endoffset)
						    acc))))
				    () (pgraph.edges pgraph)))
	 (witness-nodes* (append witness-nodes assert-nodes)))
    (list witness-nodes* witness-edges)))

; https://www.socher.org/index.php/Main/WriteToFileInLisp
; string string -> '_
(defun writeToFile (name content)
  (ensure-directories-exist (directory-namestring name))
  (with-open-file (stream name
			  :direction :output
			  :if-exists :rename-and-delete ; :ERROR :NEW-VERSION :RENAME :RENAME-AND-DELETE :OVERWRITE :APPEND :SUPERSEDE
			  :if-does-not-exist :create )
		  (format stream content))
  name) 

; ...
(defun write-witness-to-file (output-file-path hash src-file architecture witness-graph)
  (let* ((witness-graph-string (witness-graph-string src-file hash architecture
						     witness-graph)))
    (writeToFile output-file-path
		 (witness-file-string src-file witness-graph-string))))
				    
							  
