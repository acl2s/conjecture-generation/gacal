;; ==================
;; ----- Driver -----
;; ==================

(define-condition not-all-assertions-proved (error)
  ((text :initarg :text :reader text)))

(defparameter *use-ACL2-defs* nil)

; [ACL2-event list] bool bool -> '_
(defun admit-acl2-events (events verbose print-ACL2-admitted)
  (list.iter #'(lambda (event) 
		 (let* ((admitted (not (car (acl2s-event event)))))
		   (if (and verbose print-ACL2-admitted)
		       (if admitted
			   (print (list 'admitted event))
			 (print (list 'failed event)))
		     '_)
		   (if admitted
		       '_
		     (error 'ACL2-form-failed))))
	     events))

; [defun list] -> '_
(defun admit-defuns (defuns)
  (list.iter #'(lambda (def) (eval def)) defuns))

(defun create-graph/admit-functions/generate-trace/find-invariants
    (input-program-string src-prgm architecture
			  &key (verbose nil) (print-prgm t) (print-defs t) (print-ACL2-admitted t) (print-verif-cond nil))
  (let* ((start (get-internal-real-time))
	 (_ (disable-warnings))
	 ;; https://github.com/sbcl/sbcl/blob/master/src/code/print.lisp
;	 (*print-length* 400)
;	 (*print-level* 400)
         (*print-pretty* nil)
	 (*print-right-margin* 100)
	 ;;(*print-lines* 1)
	 (prgm (convert-program* src-prgm input-program-string))

	 (res (create-program-graph prgm))
	 (pgraph (car res))
	 (assertions (cadr res))
	 (main-node (caddr res))
	 (external-nodes (cadddr res))
	 (start-node (cadddr (cdr res)))
	 (end-node (cadddr (cddr res)))
	 (original-constraints (cadddr (cdddr res)))
	 (generalized-constraints (cadddr (cdddr (cdr res))))
	 (prgm (cadddr (cdddr (cddr res))))

	 (declarations (l.find prgm ':declarations))
	 (main-dec (option.get (list.find #'(lambda (declaration) (and (equal (car declaration) 'function-declaration)
									       (equal (l.find declaration ':name) '|main|)))
						  declarations)))
	 (_ (if (and verbose print-prgm) (print (create-printable-program prgm)) '_))
	 (name-map (program-name-map prgm map.empty))
	 (name-map (list.foldr #'(lambda (constraint name-map)
				   (if (and (consp constraint) (equal (length constraint) 3) (equal (car constraint) 'equal))
				       (let* ((var (cadr constraint))
					      (value (caddr constraint)))
					 (map.add-overwrite name-map var value))))
			       name-map original-constraints))

	 (nodes (pgraph.nodes pgraph))
	 (edges (pgraph.edges pgraph))
	 
	 (quotient-pgraph (quotient-pgraph assertions pgraph))
	 (quotient-nodes (pgraph.nodes quotient-pgraph))
	 (quotient-edges (pgraph.edges quotient-pgraph))

	 (_ (if verbose (print '==================) '_))
	 (_ (if verbose (print-pgraph pgraph) '_))
	 (_ (if verbose (print '==================) '_))
	 (_ (if verbose (print-pgraph quotient-pgraph) '_))
	 (_ (if verbose (print '==================) '_))
	 (_ (if verbose (print (list 'assertions assertions)) '_))
	 (_ (if verbose (print (list 'name 'map name-map)) '_))
	 (_ (if (and verbose (not (endp original-constraints)))
		(print (list 'generalized (cons 'and original-constraints) 'to (cons 'and generalized-constraints) 'for 'some 'trace 'generation))
	      '_))

	 (components (strongly-connected-components pgraph))
	 (quotient-components (strongly-connected-components quotient-pgraph))

	 (_ (if verbose (print (list 'topsort 'components (list.map #'(lambda (set) (list.map #'node.name set)) components)))))
	 (_ (if verbose (print (list 'topsort 'quotient 'components (list.map #'(lambda (set) (list.map #'node.name set)) quotient-components)))))


	 (pgraph-ACL2-data (pgraph-create-ACL2-data pgraph))
	 (node-defdata-map (pgraph-ACL2-data.defdatas pgraph-ACL2-data))
	 (edge-condition-map (pgraph-ACL2-data.conditions pgraph-ACL2-data))
	 (edge-statement-map (pgraph-ACL2-data.statements pgraph-ACL2-data))

	 (quotient-pgraph-ACL2-data (pgraph-create-ACL2-data quotient-pgraph))
	 (quotient-node-defdata-map (pgraph-ACL2-data.defdatas quotient-pgraph-ACL2-data))
	 (quotient-edge-condition-map (pgraph-ACL2-data.conditions quotient-pgraph-ACL2-data))
	 (quotient-edge-statement-map (pgraph-ACL2-data.statements quotient-pgraph-ACL2-data))

	 (prepped-pgraph (pgraph.make (list-diff (pgraph.nodes pgraph) external-nodes)
				      (let* ((start-to-main-edge (edge.make (new-edge-name) start-node main-node
									    t #'(lambda (exp) exp)
									    ()
									    (l.find main-dec ':dec-loc)
									    :enter-loop-head option.none :source-code option.none
									    :enter-function (option.some '|main|))))
					(cons start-to-main-edge (list.filter #'(lambda (edge) (not (or (list.in (edge.to edge) external-nodes)
													(list.in (edge.from edge) external-nodes)
													(equal (edge.from edge) start-node))))
									      (pgraph.edges pgraph))))))
	 (prepped-nodes (pgraph.nodes prepped-pgraph))
	 (prepped-edges (pgraph.edges prepped-pgraph))
	 (prepped-pgraph-ACL2-data (pgraph-create-ACL2-data prepped-pgraph))
	 (prepped-node-defdata-map (pgraph-ACL2-data.defdatas prepped-pgraph-ACL2-data))
	 (prepped-edge-condition-map (pgraph-ACL2-data.conditions prepped-pgraph-ACL2-data))
	 (prepped-edge-statement-map (pgraph-ACL2-data.statements prepped-pgraph-ACL2-data))

	 ;;(_ (print (acl2s-evaluate '(nondet-c-uint (list () () () ()) 0))))
	 (_ (if *use-ACL2-defs*
		(progn (admit-ACL2-events (list.map #'(lambda (node) (map.find-exn node-defdata-map (node.name node))) nodes) verbose print-ACL2-admitted)
		       (admit-ACL2-events (list.map #'(lambda (edge) (map.find-exn edge-condition-map (edge.name edge))) edges) verbose print-ACL2-admitted)
		       (admit-ACL2-events (list.map #'(lambda (edge) (map.find-exn edge-statement-map (edge.name edge))) edges) verbose print-ACL2-admitted))
	      (progn (admit-defuns (list.map #'(lambda (edge) (definec-to-defun (map.find-exn edge-condition-map (edge.name edge)))) edges))
		     (admit-defuns (list.map #'(lambda (edge) (definec-to-defun (map.find-exn edge-statement-map (edge.name edge)))) edges)))))
	 (_ (if *use-ACL2-defs*
		(progn (admit-ACL2-events (list.map #'(lambda (node) (map.find-exn quotient-node-defdata-map (node.name node))) quotient-nodes) verbose print-ACL2-admitted)
		       (admit-ACL2-events (list.map #'(lambda (edge) (map.find-exn quotient-edge-condition-map (edge.name edge))) quotient-edges) verbose print-ACL2-admitted)
		       (admit-ACL2-events (list.map #'(lambda (edge) (map.find-exn quotient-edge-statement-map (edge.name edge))) quotient-edges) verbose print-ACL2-admitted))
	      (progn (admit-defuns (list.map #'(lambda (edge) (definec-to-defun (map.find-exn quotient-edge-condition-map (edge.name edge)))) quotient-edges))
		     (admit-defuns (list.map #'(lambda (edge) (definec-to-defun (map.find-exn quotient-edge-statement-map (edge.name edge)))) quotient-edges)))))
	 (_ (if *use-ACL2-defs*
		(progn (admit-ACL2-events (list.map #'(lambda (node) (map.find-exn prepped-node-defdata-map (node.name node))) prepped-nodes) verbose print-ACL2-admitted)
		       (admit-ACL2-events (list.map #'(lambda (edge) (map.find-exn prepped-edge-condition-map (edge.name edge))) prepped-edges) verbose print-ACL2-admitted)
		       (admit-ACL2-events (list.map #'(lambda (edge) (map.find-exn prepped-edge-statement-map (edge.name edge))) prepped-edges) verbose print-ACL2-admitted))
	      (progn (admit-defuns (list.map #'(lambda (edge) (definec-to-defun (map.find-exn prepped-edge-condition-map (edge.name edge)))) prepped-edges))
		     (admit-defuns (list.map #'(lambda (edge) (definec-to-defun (map.find-exn prepped-edge-statement-map (edge.name edge)))) prepped-edges)))))
	 (_ (if (and verbose print-defs (not *use-ACL2-defs*))
		(progn (list.iter #'(lambda (edge) (print (definec-to-defun (map.find-exn prepped-edge-condition-map (edge.name edge))))) prepped-edges)
		       (list.iter #'(lambda (edge) (print (definec-to-defun (map.find-exn prepped-edge-statement-map (edge.name edge))))) prepped-edges))
	      '_))
	 (_ (if (and verbose print-defs *use-ACL2-defs*)
		(progn (print '<><><><><><><><><><><>)
		       (list.iter #'(lambda (node) (print (map.find-exn node-defdata-map (node.name node)))) nodes)
		       (list.iter #'(lambda (edge) (print (map.find-exn edge-condition-map (edge.name edge)))) edges)
		       (list.iter #'(lambda (edge) (print (map.find-exn edge-statement-map (edge.name edge)))) edges))

	      '_))
	 (end (get-internal-real-time))
	 (startup-time (- end start))
	 (res (handler-case
		  (generate-and-prove-invariants/assertions-iter prgm quotient-pgraph quotient-pgraph-ACL2-data quotient-components assertions start-node original-constraints generalized-constraints verbose)
		(invalidates-assertion ()
				       (generate-trace-for-violation *violation-start-state* start-node pgraph pgraph-ACL2-data assertions main-node prepped-pgraph start-node name-map))))
	 (_ (if verbose
		(progn (print (list 'total 'startup 'time
				    (float (/ startup-time internal-time-units-per-second))))
		       (print (list 'total 'trace 'time
				    (float (/ *total-trace-time* internal-time-units-per-second))))
		       (print (list 'total 'invariant 'iter 'time
				    (float (/ *total-invariant-iter-time* internal-time-units-per-second))))
		       (print (list 'total 'prove 'invariants 'iter 'time
				    (float (/ *total-prove-invariants-iter-time* internal-time-units-per-second))))
		       (print (list 'total 'iterate 'and 'prove 'time
				    (float (/ *total-iterate-and-prove-time* internal-time-units-per-second))))
		       (print (list 'total 'prove 'assertions 'time
				    (float (/ *total-prove-assertions-time* internal-time-units-per-second))))
		       (print (list 'total 'create 'new 'assignments 'time
				    (float (/ *total-create-new-assignments-time* internal-time-units-per-second))))
		       (print (list 'total 'proof 'cache 'calls *proof-cache-calls*))
		       (print (list 'total 'acl2s 'queries *acl2s-queries*))
		       (print (list 'total 'proof 'proven 'hits *proof-cache-proven-hits*))
		       (print (list 'total 'proof 'disproven 'hits *proof-cache-disproven-hits*))
		       (print (list 'total 'proof 'cgen 'hits *previous-cgen-hits*))
		       (print (list 'total 'prover 'time (float (/ *total-prover-time* internal-time-units-per-second))))
		       (print (list 'total 'cgen 'eval 'calls *total-cgen-eval-calls*))
		       (print (list 'total 'cgen 'eval 'time
				    (float (/ *total-cgen-eval-time* internal-time-units-per-second))))
		       (print (list 'total 'term 'eval 'time
				    (float (/ *total-term-eval-time* internal-time-units-per-second))))
		       (print (list 'total 'term 'eval 'time
				    (float (/ *total-invariantd-eval-time* internal-time-units-per-second))))
		       (print (list 'summary
				    (float (/ *total-trace-time* internal-time-units-per-second))
				    (float (/ *total-invariant-iter-time* internal-time-units-per-second))
				    (float (/ *total-prove-invariants-iter-time* internal-time-units-per-second))
				    (float (/ *total-iterate-and-prove-time* internal-time-units-per-second))
				    (float (/ *total-prove-assertions-time* internal-time-units-per-second))
				    (float (/ *total-create-new-assignments-time* internal-time-units-per-second))
				    (float (/ *total-prover-time* internal-time-units-per-second))
				    *proof-cache-calls*
				    *acl2s-queries* (list *acl2s-query-proven* *acl2s-query-unknown* *acl2s-query-disproven*)
				    *previous-cgen-hits*
				    *proof-cache-proven-hits* *proof-cache-disproven-hits*
				    *query-proven* *query-unknown* *query-disproven*
				    *proved-on-first-try* *proved-on-second-try*
				    ))
		       )
	      '_))
	 #|(proven-invariants (map.map #'(lambda (nn invariants)
					 (list.map #'(lambda (invariant)
						       (list.foldr #'(lambda (constraint invariant)
	 (if (and (consp constraint) (equal (length constraint) 3) (equal (car constraint) 'equal))
									   (let* ((var (cadr constraint))
										  (value (caddr constraint)))
									     (invariant-replace-var-with-exp invariant var value))
									 invariant))
								   invariant original-constraints))
						   invariants))
	 (car res)))|#
	 ;; just extend the name map...
	 (proven-invariants (car res))
	 (unproven-assertions (cadr res))
	 (_ (if verbose (print (list 'all 'proven 'invariants proven-invariants)) '_))
	 (_ (if verbose
		(if (map.is-empty unproven-assertions)
		    (print (list 'all 'assertions 'proven))
		  (progn (print (list 'unproven 'assertions))
			 (map.iter #'(lambda (node-name assertion) (print (list node-name assertion))) unproven-assertions)))
	      '_))
	 (_ (if (not (endp unproven-assertions)) (error 'not-all-assertions-proved) '_))
	 
	 (witness-graph (create-witness-graph prepped-pgraph proven-invariants start-node name-map assertions)))
    witness-graph))


