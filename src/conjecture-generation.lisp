
#|

TODO: removing the sorting from the hash-table comparison stuff
 - simply check if the term is less than the one already in the hash-table; if so, replace it.
TODO: add proper comparison stuff
 - compare between types that have a common given subtype. 
 - namely, every type compares with itself and if t is a subtype of t' then t and t' are compared.
 - provide user filters to remove comparisons or add them
TODO: Add user filtering stuff 
 - user provided theorems (so even if I don't have 3 variables I can still provide (+ x (+ y z)) = (+ (+ x y) z))
 - the nested pattern matching
 - can I index the filters and cache the result? this could be useful for the recursive ones?
   + I probably shouldn't, I don't think there would be much gotten out of it.
 - filtering not only at the term generation stage, but at the equality stage. Also having predicates which can stop terms from being compared.
TODO: Add the strictness stuff 
 - make sure it is still transitive
TODO: make most of the mapping data structures mutable.
TODO: subtypes
TODO: comparison-only constants (i.e. constants which are not used to build terms out of)
 - doesn't work for < and 0? but boolean stuff might be diff?

TODO: Create type-pairs; the user can add type tokens (ids) to guide term generation and attach a type meaning to these.
TODO: user should be able to guide the unconditional assignment generation (supply things like (>= (length n) 20))

TODO: add in the completion procedure
TODO: apply to sorting/other examples
TODO: add input validation stuff
TODO: handle conditional functions (division, min-element, etc)
 - add the option to scrape guards from ACL2(s) definitions

Add no comparison of booleans options/custom creation over boolean stuffs.

Custom generation procedure?
 - [[fun-sym nat pair] list]

|#

; requires "acl2s-interactions.lisp"
; requires "itest-cgen.lisp"
; requires "itest-ithm.lisp"
; requires "base.lisp"

; [byte array] [byte array] -> boolean
(defun <-arrays (a1 a2)
  (let* ((len (array-dimension a1 0))
	 (test t))
    (loop for i from 0 to (- len 1)
	  do (if (>= (aref a1 i) (aref a2 i))
		 (setf test nil)
	       ()))
    test))

; [byte array] [byte array] -> [byte array]
(defun add-arrays (a1 a2)
  (let* ((len (array-dimension a1 0))
	 (new-array (make-array len :element-type '(unsigned-byte 8))))
    (loop for i from 0 to (- len 1)
	  do (setf (aref new-array i) (+ (aref a1 i) (aref a2 i))))
    new-array))

; [int list] [int list] -> [int list]
; requires (equal (length l1) (length l2))
(defun add-lists (l1 l2)
  (if (and (endp l1) (endp l2))
      ()
    (if (or (endp l1) (endp l2))
	(error 'list.length-error)
      (cons (+ (car l1) (car l2)) (add-lists (cdr l1) (cdr l2))))))
#|
base-term := (fun-sym base-term ...) | var-sym | const-sym

S := (make-S :const-syms [const-sym list] :var-syms [var-sym list] :fun-syms [fun-sym list] 
             :const-lookup const-lookup :var-lookup var-lookup :fun-lookup fun-lookup 
             :const-values [const-sym value map] :types [type list] :req-hyps [hyp list]
             :fun-guards [fun-sym [[var list] [hyp list] pair] map] 
             :subtypes [type [type list] map] :supertypes [type [type list] map]
             :comparisons [type [type list] map])

evaluation := [[value option] list]
annotation := 
  (make-annotation :type type :size N 
                   :const-sizes [byte array] :var-sizes [byte array] :fun-sizes [byte array] 
                   :minimal-map [N [bool ref] mmap] :eval-map [evaluation ref] :destructed base-term)
term = [term-part annotation pair]
term-part := (list 'const const-sym value)
           | (list 'var var-sym) 
           | (list 'fun fun-sym [term list]) 

layer := [type [term list] map]
cache := [N layer map]

fun-type := (list [type list] type) 

fun-lookup := [fun-sym fun-type map]
var-lookup := [var-sym type map]
const-lookup := [const-sym (list value type) map]

assignment := [var-sym term map]

temp-part := (list 'template temp-sym) 
           | (list 'const const-sym value)
           | (list 'var var-sym) 
           | (list 'fun fun-sym [temp list]) 
temp := [temp-part temp-annotation pair]
temp-annotation = (make-temp-annotation :type type)

|#



(defstruct S
  const-syms var-syms fun-syms
  const-lookup var-lookup fun-lookup const-values
  types req-hyps fun-guards subtypes supertypes comparisons)

(defstruct annotation
  type size const-sizes var-sizes fun-sizes 
  minimal-map eval-map destructed)

(defstruct temp-annotation
  type) 

; S -> [const-sym list]
(defun S.const-syms (S)
  (S-const-syms S))
; S -> [var-sym list]
(defun S.var-syms (S)
  (S-var-syms S))
; S -> [fun-sym list]
(defun S.fun-syms (S)
  (S-fun-syms S))
; S -> [const-sym type map]
(defun S.const-lookup (S)
  (S-const-lookup S))
; S -> [var-sym type map]
(defun S.var-lookup (S)
  (S-var-lookup S))
; S -> [fun-sym fun-type map]
(defun S.fun-lookup (S)
  (S-fun-lookup S))
; S -> [const-sym value map]
(defun S.const-values (S)
  (S-const-values S))
; S -> [type list]
(defun S.types (S)
  (S-types S))
; S -> [hyp list]
(defun S.req-hyps (S)
  (S-req-hyps S))
; S -> [fun-sym [[value list] -> bool] map]
(defun S.fun-guards (S)
  (S-fun-guards S))
; S -> [type [type list] map]
(defun S.subtypes (S)
  (S-subtypes S))
; S -> [type [type list] map]
(defun S.supertypes (S)
  (S-supertypes S))
; S -> [type [type list] map]
(defun S.comparisons (S)
  (S-comparisons S))

; [const-sym list] [var-sym list] [fun-sym list] const-lookup var-lookup fun-lookup [const-sym value map] [type list] [hyp list] [fun-sym [[value list] -> bool] map] [type [type list] map] [type [type list] map] -> S
(defun S.create (const-syms var-syms fun-syms
			    const-lookup var-lookup fun-lookup const-values
			    types req-hyps fun-guards subtypes supertypes comparisons)
  (make-S :const-syms const-syms :var-syms var-syms :fun-syms fun-syms 
	  :const-lookup const-lookup :var-lookup var-lookup :fun-lookup fun-lookup 
	  :const-values const-values :types types
	  :req-hyps req-hyps :fun-guards fun-guards :subtypes subtypes :supertypes supertypes
	  :comparisons comparisons))

; type type -> ord
(defun type.ord (t1 t2)
  (if (equal t1 t2)
      ord.eq
    (if (acl2::lexorder t1 t2)
	ord.ls ord.gr)))

; fun-type -> [type list]
(defun get-input-types (fun-type)
  (car fun-type))
; fun-type -> type
(defun get-return-type (fun-type)
  (cadr fun-type))

(define-condition no-type-error (error)
  ((text :initarg :text :reader text)))
; term -> bool
(defun term.is-const (term)
  (equal (car (pair.first term)) 'const))
; term -> bool
(defun term.is-var (term)
  (equal (car (pair.first term)) 'var))
; term -> bool
(defun term.is-fun (term)
  (equal (car (pair.first term)) 'fun))

; term -> annotation
(defun term.annotation (term)
  (pair.second term))
; term -> type
(defun term.type (term)
  (annotation-type (term.annotation term)))
; term -> N
(defun term.size (term)
  (annotation-size (term.annotation term)))
; term -> [N array]
(defun term.const-sizes (term)
  (annotation-const-sizes (term.annotation term)))
; term -> [N array]
(defun term.var-sizes (term)
  (annotation-var-sizes (term.annotation term)))
; term -> [N array]
(defun term.fun-sizes (term)
  (annotation-fun-sizes (term.annotation term)))
; term N -> bool
(defun term.is-minimal (term n)
  (mmap.find-exn (annotation-minimal-map (term.annotation term)) n))
; term N -> bool
(defun term.has-minimal (term n)
  (option.exists (mmap.find (annotation-minimal-map (term.annotation term)) n)))
; term N bool -> '_
(defun term.set-is-minimal (term n v)
  (mmap.add-overwrite (annotation-minimal-map (term.annotation term)) n v))
(define-condition term.no-evals-error (error)
  ((text :initarg :text :reader text)))
; term -> [N [evaluation option] mmap]
(defun term.eval-map (term)
  (annotation-eval-map (term.annotation term)))
; term N -> evaluation
(defun term.get-evals (term n)
  (let* ((res (mmap.find (term.eval-map term) n)))
    (if (option.exists res)
	(option.get res)
      (error 'term.no-evaluations-error))))
; term N evaluation -> '_
(defun term.set-evals (term n evals)
  (mmap.add-overwrite (term.eval-map term) n evals))
; term N -> bool
(defun term.has-evals (term n)
  (let* ((res (mmap.find (term.eval-map term) n)))
    (option.exists res)))

; term -> base-term
(defun term.destruct (term)
  (annotation-destructed (term.annotation term)))
; term -> base-term
(defun term.compute-destruct (term)
  (cond ((term.is-fun term) (cons (term.fun-sym term)
				  (list.map #'term.compute-destruct (term.args term))))
	((term.is-var term) (term.var-sym term))
	((term.is-const term) (term.const-value term))))

; type N [N array] [N array] [N array] -> annotations
(defun annotation.create (type size const-sizes var-sizes fun-sizes destructed)
  (make-annotation :type type :size size 
		   :const-sizes const-sizes :var-sizes var-sizes :fun-sizes fun-sizes 
		   :minimal-map (mmap.empty) :eval-map (mmap.empty) :destructed destructed))

; const-sym S -> term
(defun term.make-const (const-sym S)
  (let* ((type (map.find-exn (S.const-lookup S) const-sym))
	 (value (map.find-exn (S.const-values S) const-sym))
	 (const-syms (S.const-syms S))
	 (var-syms (S.var-syms S))
	 (fun-syms (S.fun-syms S))
	 (new-const-array (make-array (len const-syms) :element-type '(unsigned-byte 8)))
	 (new-var-array (make-array (len var-syms) :element-type '(unsigned-byte 8)))
	 (new-fun-array (make-array (len fun-syms) :element-type '(unsigned-byte 8)))
	 (_ (list.foldl #'(lambda (var-sym* acc) 
			    (if (equal var-sym var-sym*) (setf (aref new-var-array acc) 1) '_)
			    (+ acc 1))
			0 fun-syms)))
  (pair.make (list 'const const-sym value)
	     (annotation.create type 1 new-const-array new-var-array new-fun-array value))))

; var-sym S -> term
(defun term.make-var (var-sym S)
  (let* ((type (map.find-exn (S.var-lookup S) var-sym))
	 (const-syms (S.const-syms S))
	 (var-syms (S.var-syms S))
	 (fun-syms (S.fun-syms S))
	 (new-const-array (make-array (len const-syms) :element-type '(unsigned-byte 8)))
	 (new-var-array (make-array (len var-syms) :element-type '(unsigned-byte 8)))
	 (new-fun-array (make-array (len fun-syms) :element-type '(unsigned-byte 8)))
	 (_ (list.foldl #'(lambda (var-sym* acc) 
			    (if (equal var-sym var-sym*) (setf (aref new-var-array acc) 1) '_)
			    (+ acc 1))
			0 var-syms)))
    (pair.make (list 'var var-sym)
	       (annotation.create type 1 new-const-array new-var-array new-fun-array var-sym))))

; fun-sym [term list] S -> term
(defun term.make-fun (fun-sym args S)
  (let* ((type (get-return-type (map.find-exn (S.fun-lookup S) fun-sym)))
	 (const-syms (S.const-syms S))
	 (var-syms (S.var-syms S))
	 (fun-syms (S.fun-syms S))
	 (new-const-array (make-array (len const-syms) :element-type '(unsigned-byte 8)))
	 (new-var-array (make-array (len var-syms) :element-type '(unsigned-byte 8)))
	 (new-fun-array (make-array (len fun-syms) :element-type '(unsigned-byte 8)))
	 (_ (list.foldl #'(lambda (fun-sym* acc) 
			    (if (equal fun-sym fun-sym*) (setf (aref new-fun-array acc) 1) '_)
			    (+ acc 1))
			0 fun-syms))
	 (size (list.foldr #'(lambda (arg acc) (+ (term.size arg) acc)) 1 args))
	 (const-sizes (list.foldr #'(lambda (arg acc) (add-arrays (term.const-sizes arg) acc))
				  new-const-array args))
	 (var-sizes (list.foldr #'(lambda (arg acc) (add-arrays (term.var-sizes arg) acc))
				new-var-array args))
	 (fun-sizes (list.foldr #'(lambda (arg acc) (add-arrays (term.fun-sizes arg) acc))
				new-fun-array args))
	 (destructed (cons fun-sym (list.map #'term.destruct args))))
  (pair.make (list 'fun fun-sym args)
	     (annotation.create type size const-sizes var-sizes fun-sizes destructed))))

(define-condition term.create-error (error)
  ((text :initarg :text :reader text)))
; base-term S -> term
(defun term.create (base S)
  (let* ((var-syms (S.var-syms S))
	 (const-values (S.const-values S))
	 (const-option (map.fold #'(lambda (x y acc) 
					 (if (option.exists acc) acc
					   (if (equal base y) (option.some x) option.none))) 
				     option.none const-values)))
  (cond ((option.exists const-option) (term.make-const (option.get const-option) S))
	((consp base) (term.make-fun (car base) (list.map #'(lambda (sub-base) (term.create sub-base S)) (cdr base)) S))
	((list.in base var-syms) (term.make-var base S))
	(t (error 'term.create-error)))))

; term -> fun-sym
; requires (term.is-fun term)
(defun term.fun-sym (term)
  (cadr (pair.first term)))
; term -> [term list]
; requires (term.is-fun term)
(defun term.args (term)
  (caddr (pair.first term)))
; term -> var-sym
; requires (term.is-var term)
(defun term.var-sym (term)
  (cadr (pair.first term)))
; term -> const-sym
; requires (term.is-const term)
(defun term.const-sym (term)
  (cadr (pair.first term)))
; term -> value
; requires (term.is-const term)
(defun term.const-value (term)
  (caddr (pair.first term)))

; term -> bool
; TODO:OPTIMIZE
(defun term.is-ground (term)
  (or (term.is-const term)
      (and (term.is-fun term)
	   (list.forall #'term.is-ground (term.args term)))))

; term term -> boolean
(defun term.equal (t1 t2)
  (or (eq t1 t2)
      (cond ((and (term.is-fun t1) (term.is-fun t2))
	     (and (equal (term.fun-sym t1) (term.fun-sym t2))
		  (list.foldr2 #'(lambda (st1 st2 acc) (and acc (term.equal st1 st2)))
			       t (term.args t1) (term.args t2))))
	    ((and (term.is-const t1) (term.is-const t2))
	     (equal (term.const-sym t1) (term.const-sym t2)))
	    ((and (term.is-var t1) (term.is-var t2))
	     (equal (term.var-sym t1) (term.var-sym t2)))
	    (t nil))))

(define-condition term.ord-error (error)
  ((text :initarg :text :reader text)))

; term term -> ord
; TODO:OPTIMIZE
(defun term.ord (t1 t2)
  (cond ((term.equal t1 t2) ord.eq)
	((and (term.is-fun t1) (term.is-fun t2))
	 (cond ((< (term.size t1) (term.size t2))
		ord.ls)
	       ((> (term.size t1) (term.size t2))
		ord.gr)
	       (t (if (equal (term.fun-sym t1) (term.fun-sym t2))
		      (list.foldl2 #'(lambda (st1 st2 acc)
				       (if (not (equal acc ord.eq))
					   acc
					 (term.ord st1 st2)))
				   ord.eq (term.args t1) (term.args t2))
		    (if (acl2::lexorder (term.fun-sym t1) (term.fun-sym t2))
			ord.ls
		      ord.gr)))))
	((term.is-fun t1) ord.gr)
	((term.is-fun t2) ord.ls)
	((and (term.is-var t1) (term.is-var t2))
	 (if (acl2::lexorder (term.var-sym t1) (term.var-sym t2))
	     ord.ls
	   ord.gr))
	((term.is-var t1) ord.gr)
	((term.is-var t2) ord.ls)
	((and (term.is-const t1) (term.is-const t2))
	 (if (acl2::lexorder (term.const-sym t1) (term.const-sym t2))
	     ord.ls
	   ord.gr))
	((term.is-const t1) (print (list 'term.ord-error t1 t2))
	 (error 'term.ord-error))
	((term.is-const t2) (print (list 'term.ord-error t1 t2))
	 (error 'term.ord-error))
	(t (print (list 'term.ord-error t1 t2))
	   (error 'term.ord-error))))

; term term -> ord
; TODO:OPTIMIZE
(defun term.partial-ord (t1 t2)
  (if (term.equal t1 t2) 
      ord.eq
    (if (or (term.is-const t1) (term.is-const t2))
	(term.ord t1 t2)
      (if (equal (term.size t1) (term.size t2))
	  ord.inc
	(cond ((and (< (term.size t1) (term.size t2))
		    (<-arrays (term.var-sizes t1) (term.var-sizes t2)))
	       ord.ls)
	      ((and (> (term.size t1) (term.size t2))
		    (<-arrays (term.var-sizes t2) (term.var-sizes t1)))
	       ord.gr)
	      (t ord.inc))))))

; [term list] -> [term list]
(defun sort-terms (terms)
  (list.sort terms #'(lambda (t1 t2) (equal ord.ls (term.ord t1 t2)))))

; ======================
; ----- Templates ------
; ======================

; temp -> bool
(defun temp.is-temp (temp)
  (equal (car (pair.first terp)) 'temp))

; temp -> bool
(defun temp.is-fun (temp)
  (equal (car (template.is-temp temp)) 'fun))

; temp -> bool
(defun temp.is-const (temp)
  (equal (car (template.is-temp temp)) 'const))

; temp -> bool
(defun temp.is-var (temp)
  (equal (car (template.is-temp temp)) 'var))

; temp -> temp-sym
; requires (template.is-temp temp)
(defun temp.temp-sym (temp)
  (cadr (pair.first temp)))

; temp [temp-sym term map] -> term
; map must be total on the temp-syms appearing in temp
(defun template.instantiate (temp map)
  (if (tempate.is-temp temp)
      (map.find-exn map (temp.temp-sym temp))
    (error 'unimplemented)))

; ======================
; ----- Equalities -----
; ======================

; term term -> equality
(defun equality.make (term1 term2)
  (if (equal (term.ord term1 term2) ord.ls)
      (list 'equal term2 term1)
    (list 'equal term1 term2)))
; equality -> term
(defun equality.lhs (equality)
  (cadr equality))
; equality -> term
(defun equality.rhs (equality)
  (caddr equality))

; equality -> (list 'equal base-term base-term)
(defun equality.destruct (equality)
  (list 'equal
	(term.destruct (equality.lhs equality))
	(term.destruct (equality.rhs equality))))

; hyp-config term term -> cond-equality
(defun conditional-equality.make (hyp-config term1 term2)
  (list 'implies hyp-config (equality.make term1 term2)))

; cond-equality -> (list 'implies hyps ('equal base-term base-term))
(defun destruct-conditional-equality (cond-equality)
  (let* ((equality (caddr cond-equality)))
    (list 'implies (cadr cond-equality)
	  (list 'equal
		(term.destruct (equality.lhs equality))
		(term.destruct (equality.rhs equality))))))

; [equality list] -> [equality list]
(defun directed-equalities (equalities)
  (list.filter #'(lambda (equality) 
		   (not (equal (term.partial-ord (equality.lhs equality)
						 (equality.rhs equality))
			       ord.inc)))
	       equalities))

; ===========================
; ----- Term Evaluation -----
; ===========================

; fun-sym [value list] S -> bool
(defun check-guards (fun-sym args S)
  (let* ((guard-opt (map.find (S.fun-guards S) fun-sym))) 
    (or (not (option.exists guard-opt))
	(apply (option.get guard-opt) args))))

; fun-sym [value list] -> [value option]
(defun eval-fun (fun-sym args S)
  (if (check-guards fun-sym args S)
      (handler-case (option.some (apply fun-sym args))
	(error (x) (print (list 'function 'guards 'satisfied 'but 'error 'was 'thrown (cons fun-sym args)))
	       option.none))
    option.none))

; term assignment -> value
; recursively evaluate the given term on the assignment.
(defun term.eval (term assignment S)
  (cond ((term.is-fun term) (option.get (eval-fun (term.fun-sym term)
						  (list.map #'(lambda (subterm)
								(term.eval subterm assignment S))
							    (term.args term))
						  S)))
	((term.is-var term) (map.find-exn assignment (term.var-sym term)))
	((term.is-const term) (term.const-value term))))

; term N -> evaluation
(defun term.eval-on-subterms (term target S)
  (assert (term.is-fun term))
  (labels (; term [[eval list] list] -> [eval list]
	   ; the list of evals must be non-empty (function must have arity at least one)
	   (fun-term-create-evals-aux
	    (fun-sym evals)
	    (if (endp (car evals))
		()
	      (let* ((arg-evals (list.foldr #'(lambda (eval acc) (if (and (option.exists acc)
									  (option.exists eval))
								     (option.some (cons (option.get eval)
											(option.get acc)))
								   option.none))
					    (option.some ()) (list.map #'car evals))))
		(cons (if (option.exists arg-evals)
			  (eval-fun fun-sym (option.get arg-evals) S)
			option.none)
		      (fun-term-create-evals-aux fun-sym (list.map #'cdr evals)))))))
	  (fun-term-create-evals-aux (term.fun-sym term) 
				     (list.map #'(lambda (subterm) 
						   (term.get-evals subterm target))
					       (term.args term)))))

; ===========================
; ----- Cache Functions -----
; ===========================

; layer type -> [term list]
(defun layer-get-terms (layer type)
  (map.find-default layer type ()))

; cache N type -> [term list]
(defun cache-get-terms (cache size type)
  (map.fold #'(lambda (n layer acc) (if (= n size) (layer-get-terms layer type) acc))
		() cache))

; cache N -> layer
(defun cache-get-layer (cache size)
  (map.find-default cache size map.empty))

; layer type [term list] -> layer
(defun layer-add-terms (layer type terms)
  (map.add-combine #'(lambda (terms*) (append terms terms*))
		       layer type terms))

; cache N type [term list] -> cache
(defun cache-add-terms (cache size type terms)
  (let* ((layer (map.find-default cache size map.empty)))
    (map.add-overwrite cache size
		 (layer-add-terms layer type terms))))

; S [assignment list] N -> cache
(defun create-initial-cache (S assignments target)
  (assert (not (endp assignments)))
  (let* ((const-syms (S.const-syms S))
	 (var-syms (S.var-syms S)))
    (list.foldl #'(lambda (var-sym cache)
		    (let* ((term (term.make-var var-sym S))
			   (evals (list.map #'(lambda (a) (option.some (term.eval term a S))) assignments)))
		      (term.set-evals term target evals)
		      (cache-add-terms cache 1 (map.find-exn (S.var-lookup S) var-sym) (list term))))
		(list.foldl #'(lambda (const-sym cache)
				(let* ((term (term.make-const const-sym S))
				       (evals (list.map #'(lambda (a) (option.some (term.eval term a S))) assignments)))
				  (term.set-evals term target evals)
				  (cache-add-terms cache 1 (map.find-exn (S.const-lookup S) const-sym) (list term))))
			    map.empty
			    const-syms)
		var-syms)))

; cache -> N
(defun cache-max-size (cache)
  (map.fold #'(lambda (n layer acc) (if (> n acc) n acc)) 0 cache))

; layer -> [type [base-term list] map]
(defun destruct-layer (layer)
  (map.map #'(lambda (type terms) (list.map #'(lambda (term) (term.destruct term)) terms)) layer))

; cache -> [N [type [base-term list] map] map]
(defun destruct-cache (cache)
  (map.map #'(lambda (n layer) (destruct-layer layer)) cache))

; cache -> N
(defun cache-num-terms (cache)
  (map.fold #'(lambda (_ layer acc)
		    (map.fold #'(lambda (_ terms acc)
					 (+ (length terms) acc))
				  acc layer))
		0 cache))
	
; cache [assignment list] N S -> cache
(defun evaluate-cache-on-assignments (cache assignments target S)
  ; iterate over the cache computing the results.
  (labels (; term -> '_
	   (evaluate-term-rec (term)
			      (if (term.has-evals term target)
				  '_
				(cond ((or (term.is-var term) (term.is-const term))
				       (let* ((evals (list.map #'(lambda (a) (option.some (term.eval term a S))) assignments)))
					 (term.set-evals term target evals)))
				      ((term.is-fun term)
				       (list.iter #'evaluate-term-rec (term.args term))
				       (let* ((evals (term.eval-on-subterms term target S)))
					 (term.set-evals term target evals)))))))
	  (map.iter #'(lambda (n layer)
			(map.iter #'(lambda (type terms)
				      (list.iter #'evaluate-term-rec terms))
				  layer))
		    cache)))

; =============================
; ----- Term Construction -----
; =============================

; ['a array] N 'a -> '_
(defun array.set (array i value)
  (setf (aref array i) value))

; ['a array] N -> 'a
(defun array.get (array i)
  (aref array i))

(defparameter nums-up-to-hash-table (hash-table.empty))
; N -> [N list]
(defun nums-up-to (n)
  (let* ((find (hash-table.find nums-up-to-hash-table n)))
    (if (option.exists find)
	(option.get find)
      (let* ((res (if (< n 0) () (cons n (nums-up-to (- n 1))))))
	(hash-table.add nums-up-to-hash-table n res)
	res))))

(defparameter positive-nums-up-to-hash-table (hash-table.empty))
; N -> [N list]
(defun positive-nums-up-to (n)
  (let* ((find (hash-table.find positive-nums-up-to-hash-table n)))
    (if (option.exists find)
	(option.get find)
      (let* ((res (if (< n 1) () (cons n (positive-nums-up-to (- n 1))))))
	(hash-table.add positive-nums-up-to-hash-table n res)
	res))))

(defparameter all-ordered-partitions-hash-table (hash-table.empty))
; N N -> [[N list] list]
(defun all-ordered-partitions (n k)
 (let* ((find (hash-table.find all-ordered-partitions-hash-table (list n k))))
    (if (option.exists find)
	(option.get find)
      (let* ((res (if (= k 0)
		      (if (= n 0) '(()) ())
		    (list.foldr #'(lambda (m acc)
				    (append (list.map #'(lambda (p) (cons m p))
						      (all-ordered-partitions (- n m) (- k 1)))
					    acc))
				() (nums-up-to n)))))
	(hash-table.add all-ordered-partitions-hash-table (list n k) res)
	res))))

; [N list] [type list] cache S -> [[term list] list]
(defun generate-arg-lists (p input-types cache S)
  (if (endp p)
      '(())
    (let* ((num (car p))
	   (type (car input-types))
	   (term-lists (list.map #'(lambda (type) 
				     (cache-get-terms cache num type))
				 (map.find-exn (S.subtypes S) type))))
      (if (endp terms)
	  ()
	(let* ((res (generate-arg-lists (cdr p) (cdr input-types) cache S)))
	  (list.foldr #'(lambda (terms acc)
			  (list.foldr #'(lambda (x acc)
					  (list.foldr #'(lambda (y acc)
							  (cons (cons x y) acc))
						      acc res))
				      acc terms))
		      () term-lists))))))

; N fun-sym [type list] cache S -> [term list]
; TODO:OPTIMIZE
(defun generate-terms-fun (n fun-sym input-types cache S)
  (list.foldr #'(lambda (p acc)
		  (let* ((args-list (generate-arg-lists p input-types cache S)))
		    (list.foldr #'(lambda (args acc) 
				     (let* ((term (term.make-fun fun-sym args S)))
				       (cons term acc)))
				 acc args-list)))
	      ()
	      (all-ordered-partitions (- n 1) (length input-types))))

; N cache S -> [type [term list] map] 
(defun construct-new-terms (n cache S)
  (let* ((fun-lookup (S.fun-lookup S)))
    (map.fold #'(lambda (fun-sym fun-type new-terms-cache)
		      (let* ((return-type (get-return-type fun-type))
			     (new-terms (generate-terms-fun n fun-sym (get-input-types fun-type) cache S)))
			(map.add-combine #'(lambda (base-terms)
						 (append new-terms base-terms))
					     new-terms-cache
					     return-type new-terms)))
		  map.empty fun-lookup)))

; N cache S -> cache
(defun construct-all-terms (n initial-cache S)
  (if (< n 2) 
      initial-cache
    (let* ((cache (construct-all-terms (- n 1) initial-cache S))
	   (new-layer (construct-new-terms n cache S)))
      (map.add-overwrite cache n new-layer))))

(define-condition term-instance-flag (error)
  ((text :initarg :text :reader text)))
; term term -> [substitution option]
; is t2 an instance of t1?
; TODO:OPTIMIZE
(defun term-instance (t1 t2 S)
  (labels (; term term substitution -> substitution
	   ; TODO:rework the options/control flow?
	   (term-instance-aux (t1 t2 acc)
			      (cond ((term.is-fun t1)
				     (if (equal (term.fun-sym t1)
						(term.fun-sym t2))
					 (list.foldr2 #'term-instance-aux acc (term.args t1) (term.args t2))
				       (error 'term-instance-flag)))
				    ((term.is-var t1)
				     (let* ((res (map.find acc (term.var-sym t1))))
				       (if (option.exists res)
					   (if (term.equal (option.get res) t2)
					       (map.add-overwrite acc (term.var-sym t1) t2)
					     (error 'term-instance-flag))
					 (if (list.in (term.type t1) 
						      (map.find-exn (S.supertypes S) (term.type t2)))
					     ; t2.type must be a subtype of t1.type
					     (map.add-overwrite acc (term.var-sym t1) t2)
					   (error 'term-instance-flag)))))
				    ((term.is-const t1) 
				     (if (and (term.is-const t2)
					      (equal (term.const-sym t1)
						     (term.const-sym t2)))
					 acc
				       (error 'term-instance-flag))))))
	  (handler-case (option.some (term-instance-aux t1 t2 map.empty))
	    (term-instance-flag (c) option.none))))

; term substitution S -> term
(defun term-instantiate (term subst S)
  (cond ((term.is-fun term)
	 (let* ((args* (list.map #'(lambda (subterm) (term-instantiate subterm subst S)) (term.args term))))
	   (term.make-fun (term.fun-sym term) args* S)))
	((term.is-var term)
	 (let ((res (map.find subst (term.var-sym term))))
	   (if (option.exists res) (option.get res) term)))
	((term.is-const term) term)))

; N N term term [equality list] S [term bool mmap] -> bool
; TODO:ADD subterms 
; TODO:OPTIMIZE (possibly with a hash-set?)
(defun find-less (d n term term* E S mmap)
  (or (and (not (term.equal term term*))
	   (equal (term.ord term* term)
		  ord.ls))
      (and (if (mmap.contains mmap term*)
	       nil
	     (progn (mmap.add-overwrite mmap term* t)
		    t))
	   (not (>= d n))
	   (list.exists
	    #'(lambda (equality)
		(let* ((rhs (equality.rhs equality))
		       (lhs (equality.lhs equality))
		       (matching-lhs (term-instance lhs term* S))
		       (matching-rhs (term-instance rhs term* S)))
		  (or (and (option.exists matching-lhs)
			   (find-less (+ d 1) n term 
				      (term-instantiate rhs (option.get matching-lhs) S)
				      E S mmap))
		      (and (option.exists matching-rhs)
			   (find-less (+ d 1) n term 
				      (term-instantiate lhs (option.get matching-rhs) S)
				      E S mmap)))))
	    E))))

(defparameter do-term-pruning t)
(defparameter term-pruning-depth 1)
; [type [term list] map] [type equality map] S -> [type [term list] map]
(defun prune-via-heuristics (new-terms-cache E_map S)
  (if do-term-pruning
      (map.map #'(lambda (type terms) 
		       (list.filter #'(lambda (term) 
					(not (find-less 0 term-pruning-depth term term
							(map.find-default E_map (term.type term) ())
							S
							(mmap.empty))))
				    terms))
		   new-terms-cache)
    new-terms-cache))

; equality equality S -> boolean
(defun equality-more-general (e e* S)
  (let* ((lhs (equality.lhs e))
	 (rhs (equality.rhs e))
	 (lhs* (equality.lhs e*))
	 (rhs* (equality.rhs e*))
	 (matches-lhs (term-instance lhs lhs* S))
	 (matches-rhs (term-instance rhs lhs* S)))
    (or (and (option.exists matches-lhs)
	     (term.equal rhs* (term-instantiate rhs (option.get matches-lhs) S)))
	(and (option.exists matches-rhs)
	     (term.equal rhs* (term-instantiate lhs (option.get matches-rhs) S))))))

(defparameter do-equality-pruning t)
(defparameter equality-pruning-depth 1)
; [equality list] [equality list] S -> [equality list]
; TODO:GENERALIZE
(defun prune-equalities-via-heuristics-aux (E_next E_prev S)
  (if do-equality-pruning
      (if (endp E_next)
	  E_next
	(let* ((E_next* (prune-equalities-via-heuristics-aux (cdr E_next) E_prev S))
	       (e (car E_next))
	       (lhs (equality.lhs e))
	       (rhs (equality.rhs e)))
	  (if (or (list.exists #'(lambda (e*) (equality-more-general e* e S)) E_prev)
		  (list.exists #'(lambda (e*) (equality-more-general e* e S)) E_next*))
	      E_next*
	    (cons e (list.filter #'(lambda (e*) (not (equality-more-general e e* S))) E_next*)))))
    E_next))

; [type equality map] [type equality map] S -> [type equality map]
(defun prune-equalities-via-heuristics (E_next_map E_prev_map S)
  (map.map #'(lambda (type E_next)
		   (let* ((E_prev (map.find-default E_prev_map type ())))
		     (prune-equalities-via-heuristics-aux E_next E_prev S)))
	       E_next_map))

; ============================
; ----------------------------
; ============================

(defparameter *strictness-flags* '(default extra-testing thm))

; N [type term map] cache [[type type pair] [evaluation term hash-table] map] S N -> [cache [[type type pair] equality map] pair]
; side-effect: modifies the hash-tables
; side-effect: evaluates the new terms on their subterms
; side-effect: kills the is-minimal on the terms
(defun find-new-equalities* (n new-terms-map cache hash-tables S target)
  ; fold over the new terms, collecting new equalities and killing the minimal fields
  ; add the new-terms-map to the cache, filtering out terms that have been found to be non-minimal
  (map.iter #'(lambda (type terms)
		(list.iter #'(lambda (term) (term.set-is-minimal term target t)) terms))
	    new-terms-map)
  (map.iter #'(lambda (type terms)
		(list.iter #'(lambda (term) 
			       (let* ((evals (term.eval-on-subterms term target S)))
				 (term.set-evals term target evals)))
			   terms))
	    new-terms-map)
  (let* ((new-equalities (mmap.empty))
	 (term-add #'(lambda (hash-table type-pair)
		       #'(lambda (term)
			   (let* ((evals (term.get-evals term target))
				  (res (hash-table.find hash-table evals)))
			     (if (option.exists res)
				 (let* ((term* (option.get res)))
				   ; (print (list 'equal (term.destruct term) (term.destruct term*))) TODO:NOW
				   (if (equal (term.ord term term*) ord.ls)
				       (progn (hash-table.add hash-table evals term)
					      (term.set-is-minimal term* target nil))
				     (term.set-is-minimal term target nil))
				   (mmap.add-overwrite new-equalities (pair.first type-pair)
						       (cons (equality.make term term*)
							     (mmap.find-default new-equalities type-pair ())))
				   (mmap.add-overwrite new-equalities (pair.second type-pair)
						       (cons (equality.make term term*)
							     (mmap.find-default new-equalities type-pair ()))))
			       (hash-table.add hash-table evals term))))))
	 (comparisons (list.foldr #'(lambda (type acc)
				      (append (list.map #'(lambda (type*) (pair.make type type*))
							(map.find-default (S.comparisons S) type ()))
					      acc))
				  () (S.types S)))
	 (_ (list.iter #'(lambda (type-pair)
			   ; (print (list 'comparing type-pair)) TODO:NOW
			    (let* ((type (pair.first type-pair))
				   (type* (pair.second type-pair))
				   (hash-table (map.find-exn hash-tables type-pair))
				   (new-terms-opt (map.find new-terms-map type))
				   (new-terms*-opt (map.find new-terms-map type*))
				   (new-terms (if (option.exists new-terms-opt) (option.get new-terms-opt) ()))
				   (new-terms* (if (option.exists new-terms*-opt) (option.get new-terms*-opt) ())))
			      (if (not (equal type type*))
				  (progn (list.iter (apply term-add (list hash-table type-pair)) new-terms)
					 (list.iter (apply term-add (list hash-table type-pair)) new-terms*))
				(list.iter (apply term-add (list hash-table type-pair)) new-terms))))
		       comparisons))
	 (new-cache (map.fold #'(lambda (type terms cache-acc)
				  (let* ((terms* (list.filter #'(lambda (term) (term.is-minimal term target)) terms)))
				    (cache-add-terms cache-acc n type terms*)))
			      cache new-terms-map)))
    (pair.make new-cache
	       (mmap.fold #'(lambda (type-pair eqs acc)
			      #|(if (map.contains acc type-pair)
				  (print (list type-pair (list.map #'equality.destruct eqs)
					       (map.map #'(lambda (_ eqs) (list.map #'equality.destruct eqs)) acc)))
				'_) TODO:NOW |#
			      (map.add-exn acc type-pair eqs))
			  map.empty new-equalities))))


; N type type [term list] [eval term hash-table] S N -> [[term list] [equality list] pair]
(defun prune-term-list (n type type* terms hash-table S target &key (strictness 'default))
  ;(print (list 'comparing n type type*))
  (let* ((terms* (list.sort terms ; reverse order by term.ord
			    #'(lambda (term-1 term-2)
				(let* ((ord (term.ord term-2 term-1)))
				  (or (equal ord.ls ord) (equal ord.eq ord)))))))
    (list.foldr #'(lambda (term acc)
		    (let* ((terms-acc (pair.first acc))
			   (equalities-acc (pair.second acc))
			   (evals (term.get-evals term target))
			   (res (hash-table.find hash-table evals)))
		      (if (option.exists res)
			  (let* ((term* (option.get res))
				 (req-hyps (S.req-hyps S))
				 (other-tests (and (not (equal strictness 'default))
						   (acl2s-test? (list 'implies 
								      (cons 'and req-hyps) 
								      (list 'equal (term.destruct term) (term.destruct term*))))
						   (not (equal strictness 'extra-testing))
						   (acl2s-thm (list 'implies 
								    (cons 'and req-hyps) 
								    (list 'equal (term.destruct term) (term.destruct term*)))))))
			    (pair.make terms-acc (cons (equality.make term term*)  equalities-acc)))
			(progn (hash-table.add hash-table evals term)
			       (pair.make (cons term terms-acc)
					  equalities-acc)))))
		(pair.make () ())
		terms*)))

; N [type term map] cache [[type type pair] [evaluation term hash-table] map] S N -> [cache [type equality map] pair]
; side-effect: modifies the hash-tables
; side-effect: evaluates the new terms on their subterms
(defun find-new-equalities (n new-terms-map cache hash-tables S target)
  (map.fold #'(lambda (type terms acc) 
		    (let* ((cache-acc (pair.first acc))
			   (equalities-acc (pair.second acc))
			   (_ (list.iter #'(lambda (term) 
					     (let* ((evals (term.eval-on-subterms term target S)))
					       (term.set-evals term target evals)))
					 terms))
			   (res (list.foldr #'(lambda (type* acc)
						(let* ((terms (pair.first acc))
						       (equalities (pair.second acc)))
						  (prune-term-list n type type* terms (map.find-exn hash-tables (pair.make type type*)) S target)))
					    (pair.make terms ()) (map.find-exn (S.comparisons S) type)))
			   (terms* (pair.first res))
			   (equalities (pair.second res)))
		      (pair.make (cache-add-terms cache-acc n type terms*)
				 (map.add-exn equalities-acc type equalities))))
		(pair.make cache map.empty)
		new-terms-map))

; N cache [type equality map] [[type type pair] [evaluation term hash-table] map] S N [[term -> boolean] list] -> [cache [type equality mmap] pair]
(defun term-construction (n cache E_map hash-tables S target filters)
  (let* ((initially-pruned (prune-via-heuristics (map.fold #'(lambda (x terms acc)
							       (map.add-exn acc x (list.filter #'(lambda (term) (list.satisfies-all term filters))
											   terms)))
							   map.empty
							   (construct-new-terms n cache S))
						 E_map S)))
    (find-new-equalities* n initially-pruned cache hash-tables S target)))

; S -> [[type type pair] [eval term hash-table] map]
(defun create-initial-hash-tables (S)
  (let* ((hash-tables (list.foldl #'(lambda (type acc)
				      (list.foldr #'(lambda (type* acc)
						      (map.add-exn acc (pair.make type type*)
								   (hash-table.empty)))
						  acc (map.find-exn (S.comparisons S) type)))
				  map.empty
				  (S.types S))))
    hash-tables))

; [[type type pair] [eval term hash-table] map] cache target S -> '_
; TODO: check for equalities
(defun populate-initial-hash-tables (hash-tables initial-cache target S)
  (map.iter #'(lambda (n layer)
		(map.iter #'(lambda (type terms)
			      (list.iter #'(lambda (term) 
					     (let* ((evals (term.get-evals term target)))
					       (list.iter #'(lambda (type*)
							      (let* ((hash-table (map.find-exn hash-tables (pair.make type type*))))
								(hash-table.add hash-table evals term)))
							  (map.find-exn (S.comparisons S) type))))
					 terms))
			  layer))
	    initial-cache))

; S -> [type [equality list] map]
(defun create-initial-equality-map (S)
  (list.foldr #'(lambda (type acc)
		  (map.add-exn acc type ()))
	      map.empty (S.types S)))

; N cache S N [[term -> bool] list] -> [cache [type equality map] pair]
(defun construct-all-terms-and-equalities (max initial-cache S target filters)
  (let* ((hash-tables (create-initial-hash-tables S))
	 (_ (populate-initial-hash-tables hash-tables initial-cache target S))
	 (initial-equality-map (create-initial-equality-map S)))
    (labels ((construct-layer 
	      (n cache equality-map)
	      (if (> n max)
		  (pair.make cache equality-map)
		(let* ((res (term-construction n cache equality-map hash-tables S target filters))
		       (pruned-equalities (prune-equalities-via-heuristics (pair.second res) equality-map S)))
		  (construct-layer (+ n 1)
				   (pair.first res)
				   (map.map #'(lambda (type old-eqs)
						     (append old-eqs (map.find-default pruned-equalities type ())))
						 equality-map))))))
	    (construct-layer 2 initial-cache initial-equality-map))))

; [hyp list] N -> [assignment list]
(defun create-assignments (hyps num-assignments)
  (acl2s-generate-counter-examples (list 'implies (cons 'and hyps) nil)
				   num-assignments))

; [hyp list] [hyp list] -> bool
(defun is-minimal (req-hyps hyps &key (trials 1000))
  (not (list.exists #'(lambda (hyp) 
			(let* ((query (list 'implies (cons 'and (append req-hyps (list.remove hyp hyps))) hyp)))
			  (acl2s-test? query :trials trials)))
		    hyps)))

; [hyp list] [hyp list] N -> [[[hyp list] [assignment list] pair] list]
(defun create-hypotheses-assignments (req-hyps opt-hyps-configs num-assignments)
  (if (endp opt-hyps-configs)
      ()
    (let* ((opt-hyps (car opt-hyps-configs))
	   (assignments (create-assignments (append req-hyps opt-hyps) num-assignments)))
      (if (not (is-minimal req-hyps opt-hyps))
	  (print `(given set of optional hypotheses is not minimal ,opt-hyps))
	'_)
      (if (endp assignments)
	  (progn (print `(no assignments found for ,opt-hyps))
		 (create-hypotheses-assignments req-hyps (cdr opt-hyps-configs) num-assignments))
	(cons (pair.make opt-hyps assignments) 
	      (create-hypotheses-assignments req-hyps (cdr opt-hyps-configs) num-assignments))))))

; [hyp list] [hyp list] N -> [[[hyp list] [assignment list] pair] list]
; TODO:OPTIMIZE the whole thing
; TODO:OPTIMIZE is it better to check minimality before creating assignments? perhaps first check if assignments exist, then check minimality?
(defun minimal-satisfiable-hypotheses (req-hyps hyps num-assignments)
  (if (endp hyps)
      (list (pair.make () (create-assignments req-hyps num-assignments)))
    (let* ((hyp (car hyps))
	   (res (minimal-satisfiable-hypotheses req-hyps (cdr hyps) num-assignments)))
      (list.foldr #'(lambda (hyps-pair acc)
		      (let* ((hyps* (cons hyp (pair.first hyps-pair)))
			     (assignments (create-assignments (append req-hyps hyps*) num-assignments)))
			;(print (pair.make hyps* assignments))
			;(print (list hyps* (is-minimal req-hyps hyps*)))
			(if (and (not (endp assignments))
				 (is-minimal req-hyps hyps*))
			    (cons (pair.make hyps* assignments) acc)
			  acc)))
		  res res))))

; [(list const-sym value type) list] -> const-lookup
(defun create-const-lookup (consts)
  (list.foldr #'(lambda (stuff acc)
		  (let* ((const-sym (car stuff))
			 (type (caddr stuff)))
		    (map.add-exn acc const-sym type)))
	      map.empty
	      consts))

; [(list var-sym type) list] -> var-lookup
(defun create-var-lookup (vars)
  (map.from-list vars))

; [(list fun-sym fun-type) list] -> fun-lookup
(defun create-fun-lookup (funs)
  (map.from-list funs))

; [(list var-sym type) list] -> [hyp list]
(defun create-req-hyps (vars)
  (list.foldr #'(lambda (stuff acc)
		  (let* ((var-sym (car stuff))
			 (type (cadr stuff)))
		    (cons (list type var-sym) acc)))
	      () vars))

; ============================
; ============================
; ============================

; [hyp list] hyp hyp -> boolean
; under the required hypotheses, does the first hypothesis config imply the second?
(defun hyps-implies (req-hyps hyps-config1 hyps-config2)
  (acl2s-test? (list 'implies (cons 'and req-hyps) 
		     (list 'implies hyps-config1
			   hyps-config2))
	       :trials 100))

; [hyp list] [hyp list] -> hyp-graph
; requires: no configurations are logically (testably?) equivalent
(defun create-implication-graph (req-hyps hyps-configs)
  (if (endp hyps-configs)
      (graph.empty)
    (let* ((hyps-config (car hyps-configs))
	   (hyp-graph (create-implication-graph req-hyps (cdr hyps-configs)))
	   (n (graph.nv hyp-graph)))
      (progn (graph.add-vertex hyp-graph hyps-config)
	     (list.iter #'(lambda (n*) 
			    (let* ((hyps-config* (graph.vertex-data hyp-graph n*)))
			      (if (hyps-implies req-hyps hyps-config hyps-config*)
				  (graph.add-edge hyp-graph n n*)
				'_)
			      (if (hyps-implies req-hyps hyps-config* hyps-config)
				  (graph.add-edge hyp-graph n* n)
				'_)))
			(create-range 0 n))
	     hyp-graph))))



; [term list] [term list] -> [term list]
(defun term-list-intersect (tl1 tl2)
  (if (endp tl1)
      ()
    (let ((res (term-list-intersect (cdr tl1) tl2)))
      (if (list.in (car tl1) tl2)
	  (cons (car tl1) res)
	res))))

; [[term list] list] -> [term list]
; TODO:OPTIMIZE
(defun intersect-all (ts)
  (if (endp ts)
      ()
    (if (endp (cdr ts))
	(car ts)
      (intersect-all (cons (term-list-intersect (car ts) (cadr ts)) (cddr ts))))))

; [cache list] -> cache
; TODO:OPTIMIZE
(defun intersect-caches (caches)
  (if (endp caches)
      map.empty
    (let* ((cache (car caches))
	   (caches* (cdr caches)))
      (map.fold #'(lambda (n layer acc)
			(map.fold #'(lambda (type terms acc)
					  (let* ((terms* (intersect-all (cons terms (list.map #'(lambda (cache*) (cache-get-terms cache* n type)) caches*)))))
					    (if (endp terms*) acc (cache-add-terms acc n type terms*))))
				      acc layer))
		    map.empty cache))))

; graph -> '_
(defun graph.print (graph)
  (let* ((n (graph.nv graph)))
    (print n)
    (list.iter #'(lambda (n*)
		   (print (list n* 
				(graph.vertex-data graph n*))))
	       (create-range 0 n))
    (list.iter #'(lambda (n*)
		   (print (list n* (graph.vertex-edges graph n*))))
	       (create-range 0 n))
  '_))

; [term list] [equality list] [evaluation term hash-table] S N -> [[term list] [equality list] pair]
(defun find-eqs-aux (terms E eval-table S target &key (strictness 'default))
  (labels ((rec-aux (terms)
		    (if (endp terms)
			()
		      (let* ((term (car terms)))
			(if (and (or (not (term.is-fun term))
				     (list.forall #'(lambda (subterm) (term.is-minimal subterm target)) (term.args term)))
				 t);(not (find-less 0 term-pruning-depth term term E S)))
			    (let* ((eval (term.get-evals term target))
				   (res (hash-table.find eval-table eval)))
			      (if (option.exists res) ; term is equal to another term
				  (progn (term.set-is-minimal term target nil)
					 (setf E (cons (list 'equal term (option.get res)) E))
					 (rec-aux (cdr terms)))
				(progn (hash-table.add eval-table eval term)
				       (cons term (rec-aux (cdr terms))))))
			  (progn (term.set-is-minimal term target nil)
				 (rec-aux (cdr terms))))))))
	  (pair.make (rec-aux terms) E)))

; cache N -> '_
(defun reset-minimal (cache target)
  (labels ((reset-minimal-term (term)
			       (term.set-is-minimal term target t)
			       (if (term.is-fun term)
				   (list.iter #'reset-minimal-term (term.args term))
				 '_)))
	  (map.iter #'(lambda (_ layer) (map.iter #'(lambda (_ terms) (list.iter #'reset-minimal-term terms)) layer)) cache)))

; cache [assignment list] S N -> [cache [equality list] pair]
; TODO:OPTIMIZE (sorting stuff)
(defun find-eqs (cache assignments S target &key (ignore-bool nil))
  (let* ((hash-tables (create-initial-hash-tables S))
	 (equality-mapping map.empty);(create-initial-equality-map S)
	 (max (cache-max-size cache)))
    (evaluate-cache-on-assignments cache assignments target S)
    (reset-minimal cache target)
    ;(print (list S hash-tables (S.types S)))
    (labels ((rec-aux (n cache)
		      (if (> n max)
			  cache
			(rec-aux (+ n 1)
				 (map.map 
				  #'(lambda (n* layer)
				      (if (not (equal n n*))
					  layer
					(map.map #'(lambda (type terms)
						     (if (and ignore-bool (equal type 'booleanp))
							 terms
						       (let* ((E (map.find-default equality-mapping type ()))
							      (eval-table (map.find-exn hash-tables (pair.make type type)))
							      (res (find-eqs-aux (sort-terms terms) E eval-table S target))
							      (terms* (pair.first res))
							      (E* (pair.second res)))
							 (setf equality-mapping (map.add-overwrite equality-mapping type E*))
							 terms*)))
						     layer)))
				  cache)))))
	    (let* ((cache* (rec-aux 1 cache)))
	      (pair.make cache*
			 (map.fold #'(lambda (type eqs acc) (append eqs acc))
				   () equality-mapping))))))

; 'a ['a list] -> ['a list]
(defun list-in-eq (e l)
  (if (endp l)
      nil
    (or (eq e (car l))
	(list-in-eq e (cdr l)))))


; [N list] [hyp graph] [N cache map] [N [assignment list] map] S -> [cond-equality list]
(defun find-cond-conj-aux (top-sort implication-graph config->cache config->assignments S)
  (if (endp top-sort)
      ()
    (let* ((cur (car top-sort))
	   (cache (intersect-caches (list.map #'(lambda (n*) 
						  (map.find-exn config->cache n*))
					      (graph.vertex-edges implication-graph cur))))
	   (assignments (map.find-exn config->assignments cur))
	   (res (find-eqs cache assignments S (+ cur 1)))
	   (hyps (graph.vertex-data implication-graph cur))
	   (cond-eqs (list.map #'(lambda (eq) (list 'implies hyps eq)) (pair.second res)))
	   (config->cache* (map.add-overwrite config->cache cur (pair.first res))))
      (append cond-eqs
	      (find-cond-conj-aux (cdr top-sort) implication-graph
			       config->cache* config->assignments S)))))

; cache [hyp list] [hyp list] N S -> [cond-equality list]
(defun find-cond-conj (cache hyps-configs req-hyps num-assignments S)
  (let* ((implication-graph (create-implication-graph req-hyps hyps-configs))
	 (n (car implication-graph)) 
	 (config->assignments 
	  (list.foldr #'(lambda (n* acc)
			  (let* ((hyps-config (graph.vertex-data implication-graph n*)))
			    (map.add-overwrite acc n* (create-assignments (cons hyps-config req-hyps) num-assignments))))
		      map.empty
		      (create-range 0 n)))
	 (top-sort (graph.top-sort implication-graph))
	 (_ (graph.add-vertex implication-graph t))
	 (_ (list.iter #'(lambda (n*) (graph.add-edge implication-graph n* n)) (create-range 0 n)))
	 (_ (print '|----------------------------|))
	 (_ (graph.print implication-graph))
	 (_ (print (list 'top-sort top-sort)))
	 (_ (print (list 'assignments (map.map #'(lambda (a b) (length b)) config->assignments))))
	 (_ (print '|----------------------------|))
	 (cache* (map.map #'(lambda (n layer) 
				  (map.map #'(lambda (type terms) 
						   (sort-terms terms)) 
					       layer)) 
			      cache))
	 (red-implication-graph implication-graph)); (graph.transitive-reduction implication-graph)))
    (find-cond-conj-aux top-sort red-implication-graph 
		     (map.singleton n cache*) config->assignments S)))

; const-lookup var-lookup fun-lookup -> [type list]
; returns the set of all types appearing in the lookups, containing no duplicates
(defun all-types (const-lookup var-lookup fun-lookup)
  (map.fold #'(lambda (_ type acc)
		    (if (list.in type acc)
			acc (cons type acc)))
		(map.fold #'(lambda (_ type acc)
				  (if (list.in type acc)
				      acc (cons type acc)))
			      (map.fold #'(lambda (_ fun-type acc)
						(list.foldr #'(lambda (type acc)
								(if (list.in type acc)
								    acc (cons type acc)))
							    acc
							    (cons (get-return-type fun-type)
								  (get-input-types fun-type))))
					    ()
					    fun-lookup)
			      var-lookup)
		const-lookup))

; [equality list] -> [equality list]
(defun sort-equalities (eqs)
  (list.sort eqs
	     #'(lambda (eq1 eq2)
		 (let* ((lhs1 (equality.lhs eq1))
			(rhs1 (equality.rhs eq1))
			(lhs2 (equality.lhs eq2))
			(rhs2 (equality.rhs eq2))
			(ls1 (term.size lhs1))
			(rs1 (term.size rhs1))
			(ls2 (term.size lhs2))
			(rs2 (term.size rhs2)))
		   (or (and (= (min ls1 rs1) (min ls2 rs2))
			    (> (+ ls1 rs1) (+ ls2 rs2)))
		       (> (min ls1 rs1) (min ls2 rs2)))))))

; [type list] [(list type type) list] -> [type [type list] map]
(defun create-subtypes (all-types subtypes)
  (labels ((add-rec (next wl seen)
		    (if (endp next)
			(list seen wl)
		      (if (list.in (car next) seen)
			  (add-rec (cdr next) wl seen)
			(add-rec (cdr next) (cons (car next) wl) (cons (car next) seen)))))
	   (find-all-supertypes 
	    (seen wl)
	    (if (endp wl)
		seen
	      (let* ((type (car wl))
		     (next (list.foldr #'(lambda (pairish acc)
					   (if (equal (cadr pairish) type)
					       (cons (car pairish) acc)
					     acc))
				       () subtypes))
		     (pairish (add-rec next (cdr wl) seen)))
		(find-all-supertypes (car pairish) (cadr pairish))))))
	  (let* ((initial-map (list.foldr #'(lambda (type acc) 
					      (map.add-exn acc type ())) 
					  map.empty all-types)))
	    (map.map #'(lambda (type _)
			 (find-all-supertypes (list type) (list type)))
		     initial-map))))

; [type list] [(list type type) list]
(defun create-supertypes (all-types subtypes)
  (labels ((add-rec (next wl seen)
		    (if (endp next)
			(list seen wl)
		      (if (list.in (car next) seen)
			  (add-rec (cdr next) wl seen)
			(add-rec (cdr next) (cons (car next) wl) (cons (car next) seen)))))
	   (find-all-supertypes 
	    (seen wl)
	    (if (endp wl)
		seen
	      (let* ((type (car wl))
		     (next (list.foldr #'(lambda (pairish acc)
					   (if (equal (car pairish) type)
					       (cons (cadr pairish) acc)
					     acc))
				       () subtypes))
		     (pairish (add-rec next (cdr wl) seen)))
		(find-all-supertypes (car pairish) (cadr pairish))))))
	  (let* ((initial-map (list.foldr #'(lambda (type acc) 
					      (map.add-exn acc type ())) 
					  map.empty all-types)))
	    (map.map #'(lambda (type _)
			 (find-all-supertypes (list type) (list type)))
		     initial-map))))

; [type list] [type list] -> [type list]
(defun type-list-intersect (tl1 tl2)
  (if (endp tl1)
      ()
    (let ((res (term-list-intersect (cdr tl1) tl2)))
      (if (list.in (car tl1) res)
	  (cons (car tl1) res)
	res))))

; [type list] [type [type list] map] [type [type list] map] -> [type [type list] map]
(defun create-comparisons (all-types subtypes supertypes)
  (list.foldr #'(lambda (type acc)
		  (map.add-exn acc type (list.filter #'(lambda (type*)
							 (not (endp (type-list-intersect (map.find-exn subtypes type)
											 (map.find-exn subtypes type*)))))
						     all-types)))
	      map.empty all-types))

; N [(list value type) list] [(list var-sym type) list] [(list fun-sym fun-type) list] -> [[equality conditional-equality union] list]
(defun find-all-conjectures (n consts vars funs 
			       &key (num-assignments 100) (num-hyp-assignments 100)
			       (guards ()) (hyps-configs ())
			       (filters ())
			       (equality-filters ())
			       (subtypes ()) (given-assignments ())
			       (debug nil) (print-terms nil) (print-cache nil) (print-cache-summary nil))
  (let* ((consts* (list.map #'(lambda (stuff) (cons (multiple-value-bind (x y) (gentemp "c") x) stuff)) consts))
	 (const-lookup (create-const-lookup consts*))
	 (var-lookup (create-var-lookup vars))
	 (fun-lookup (create-fun-lookup funs))
	 (const-syms (list.map #'car consts*))
	 (var-syms (list.map #'car vars))
	 (fun-syms (list.map #'car funs)) 
	 (req-hyps (create-req-hyps vars))
	 (all-types (all-types const-lookup var-lookup fun-lookup))
	 (gen-subtypes (create-subtypes all-types subtypes))
	 (gen-supertypes (create-supertypes all-types subtypes))
	 (comparisons (create-comparisons all-types gen-subtypes gen-supertypes))
	 (_ (print (list 'subtypes gen-subtypes)))
	 (_ (print (list 'comparisons comparisons)))
	 (S (S.create const-syms var-syms fun-syms
		      const-lookup var-lookup fun-lookup
		      (map.from-list (list.map #'(lambda (stuff) (pair.make (car stuff) (cadr stuff))) consts*))
		      all-types req-hyps (map.from-list guards) gen-subtypes gen-supertypes comparisons))
	 (assignments (append (create-assignments req-hyps num-assignments) given-assignments))
	 (initial-cache (create-initial-cache S assignments 0))
	 (res-1 (construct-all-terms-and-equalities n initial-cache S 0 filters))
	 (term-cache (pair.first res-1))
	 (uncond-eqs (map.fold #'(lambda (_ eqs acc) 
				       (append (list.map #'equality.destruct (sort-equalities eqs)) acc)) 
				   () (pair.second res-1)))
	 (terms (map.fold #'(lambda (n layer acc)
				  (map.fold #'(lambda (type terms acc)
						    (append terms acc))
						acc layer))
			      () term-cache))
	 (res-2 (find-cond-conj term-cache hyps-configs req-hyps num-assignments S))
	 (cond-eqs (list.map #'destruct-conditional-equality res-2))
	 )
    (if debug (print term-cache))
    (if print-cache (print (destruct-cache initial-cache)) '_)
    (if print-cache (print (destruct-cache term-cache)) '_)
    (if print-cache-summary (print (map.map #'(lambda (_ layer) (map.map #'(lambda (_ terms) (length terms)) layer)) term-cache)))
    (if print-terms (print (list.map #'term.destruct terms)) '_)
    (append uncond-eqs cond-eqs)))

; =========================
; =========================
; =========================

(defun test-find-all-conjectures-1 (n)
  (find-all-conjectures n '((nil booleanp) (t booleanp))
			'((x integer-listp) (y integer-listp) (z integer-listp))
			'((append ((integer-listp integer-listp) integer-listp))
			  (reverse ((integer-listp) integer-listp)))
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-2 (n)
  (find-all-conjectures n '((0 integerp) (1 integerp) (nil booleanp) (t booleanp))
			'((x integerp) (y integerp) (z integerp))
			'((+ ((integerp integerp) integerp))
			  (* ((integerp integerp) integerp))
			  (- ((integerp integerp) integerp))
			  (= ((integerp integerp) booleanp)))
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-3 (n)
  (find-all-conjectures n '((0 integerp) (1 integerp) (nil booleanp) (t booleanp))
			'((x integerp) (y integerp) (z integerp))
			'((+ ((integerp integerp) integerp))
			  (* ((integerp integerp) integerp))
			  (- ((integerp integerp) integerp))
			  (/ ((integerp integerp) integerp)))
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-4 (n)
  (find-all-conjectures n '((nil integer-listp) (nil booleanp) (t booleanp))
			'((x integer-listp) (a integerp) (b integerp))
			'((cons ((integerp integer-listp) integer-listp))
			  (append ((integer-listp integer-listp) integer-listp))
			  (orderedp ((integer-listp) booleanp))
			  (insert ((integerp integer-listp) integer-listp))
			  (isort ((integer-listp) integer-listp))
			  (qsort ((integer-listp) integer-listp))
			  (less ((integerp integer-listp) integer-listp))
			  (notless ((integerp integer-listp) integer-listp)))
			:num-assignments 100
			:num-hyp-assignments 100
			:hyps-configs '((orderedp x)
					(and (orderedp (cons b x)) (<= a b))
					(and (orderedp x) (<= a b))
					(and (orderedp x) (< a b)))))

(defun test-find-all-conjectures-5 (n)
  (find-all-conjectures n '()
			'((x integer-listp) (a integerp) (b integerp))
			'((isort ((integer-listp) integer-listp))
			  (less ((integerp integer-listp) integer-listp)))
			:num-assignments 100
			:num-hyp-assignments 100
			:hyps-configs '((orderedp x)
					(< a b)
					(and (orderedp x) (< a b)))))

(defun mult-leq (n)
  #'(lambda (term)
      (labels ((get-mult-skeleton (term)
				  (if (or (term.is-const term) (term.is-var term)
					  (and (term.is-fun term) (not (equal (term.fun-sym term) '*))))
				      (list term)
				    (cons (car (term.args term))
					  (get-mult-skeleton (cadr (term.args term))))))
	       (get-plus-skeleton (term)
				  (if (or (term.is-const term) (term.is-var term)
					  (and (term.is-fun term) (not (equal (term.fun-sym term) '+))))
				      (list term)
				    (cons (car (term.args term))
					  (get-plus-skeleton (cadr (term.args term)))))))
	      ;(list.forall #'(lambda (subterm) (<= (length (get-mult-skeleton subterm)) n)) (get-plus-skeleton term))
	      (or (not (equal (term.type term) 'integerp))
		  (<= (length (get-mult-skeleton term)) n)))))
      

(defun is-valid-arith-dnf (term)
  ; create 'mult' skeleton, which is a list of 'plus' skeletons
  ; a 'plus' skeleton is a list of terms which are either a variable or a ground term, and only the first term may be a ground term. 
  ; make sure the list of plus skeletons (as terms) are sorted
  ; make sure the variables part of each plus skeleton is sorted
  (or (term.is-const term)
      (term.is-var term)
      (not (equal (term.type term) 'integerp))
      (not (equal (term.type term) 'natp))
      (labels ((get-mult-skeleton (term)
				  (if (or (term.is-const term) (term.is-var term)
					  (and (term.is-fun term) (not (equal (term.fun-sym term) '*))))
				      (list term)
				    (cons (car (term.args term))
					  (get-mult-skeleton (cadr (term.args term))))))
	       (get-plus-skeleton (term)
				  (if (or (term.is-const term) (term.is-var term)
					  (and (term.is-fun term) (not (equal (term.fun-sym term) '+))))
				      (list term)
				    (cons (car (term.args term))
					  (get-plus-skeleton (cadr (term.args term))))))
	       (is-valid-mult-skeleton (mult-sk)
				       (or (and (term.is-ground (car mult-sk))
						(not (endp (cdr mult-sk)))
						(list.forall #'term.is-var (cdr mult-sk))
						(equal (cdr mult-sk) (sort-terms (cdr mult-sk))))
					   (and (list.forall #'term.is-var mult-sk)
						(equal mult-sk (sort-terms mult-sk)))))
	       (is-valid-plus-skeleton (plus-sk)
				       (or (and (term.is-ground (car plus-sk))
						(list.forall #'(lambda (other) (is-valid-mult-skeleton (get-mult-skeleton other)))
							     (cdr plus-sk))
						(equal (cdr plus-sk) (sort-terms (cdr plus-sk))))
					   (and (list.forall #'(lambda (other) (is-valid-mult-skeleton (get-mult-skeleton other)))
							     plus-sk)
						(equal plus-sk (sort-terms plus-sk))))))
	      (is-valid-plus-skeleton (get-plus-skeleton term)))))

(defun test-find-all-conjectures-6 (n)
  (find-all-conjectures n '((0 integerp) (1 integerp))
			'((x integerp) (y integerp) (z integerp))
			'((+ ((integerp integerp) integerp))
			  (* ((integerp integerp) integerp)))
			:print-cache-summary t
			:filters (list #'is-valid-arith-dnf (mult-leq 3))
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-7 (n)
  (find-all-conjectures n '((0 integerp) (1 integerp) (2 integerp) (4 integerp) (8 integerp))
			'((x integerp) (y integerp) (z integerp) (r integerp) (n integerp))
			'((+ ((integerp integerp) integerp))
			  (* ((integerp integerp) integerp)))
			:print-cache-summary t
			:filters (list #'is-valid-arith-dnf (mult-leq 3))
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-8 (n)
  (find-all-conjectures n '((0 integerp))
			'((x integerp))
			'((+ ((integerp integerp) integerp))
			  (/ ((integerp integerp) integerp)))
			:guards (list (list '/ #'(lambda (x y) (not (equal y 0)))))
			:print-cache-summary t
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-9 (n)
  (find-all-conjectures n '((0 natp) (1 natp))
			'((x integerp) (y integerp) (z integerp) (m natp) (n natp))
			'((+ ((integerp integerp) integerp))
			  (* ((integerp integerp) integerp))
			  ;(< ((integerp integerp) booleanp))
			  )
			:print-cache t
			:print-cache-summary t
			:filters (list #'is-valid-arith-dnf (mult-leq 3))
			:subtypes '((natp integerp))
			;:filters (list #'is-valid-arith-dnf (mult-leq 3))
			:num-assignments 100
			:num-hyp-assignments 100))

(defun test-find-all-conjectures-10 (n)
  (find-all-conjectures n '((0 integerp) (1 integerp))
			'((x integerp) (y integerp) (z integerp))
			'((+ ((integerp integerp) integerp))
			  (* ((integerp integerp) integerp))
			  ;(< ((integerp integerp) booleanp))
			  )
			:print-cache t
			:print-cache-summary t
			:filters (list #'is-valid-arith-dnf (mult-leq 3))
			;:filters (list #'is-valid-arith-dnf (mult-leq 3))
			:num-assignments 100
			:num-hyp-assignments 100))

(defparameter *test/sorting/constants*
  '((nil integer-listp) (nil booleanp) (t booleanp)))

(defparameter *test/sorting/vars*
  '((x integer-listp) (a integerp) (b integerp)))

(defparameter *test/sorting/hyp-configs*
  '((orderedp x)
    (and (orderedp (cons b x)) (<= a b))
    (and (orderedp x) (<= a b))
    (and (orderedp x) (< a b))))

(defun create-sorting-assignments (vars num-assignments max-list-size)
  (if (equal num-assignments 0)
      ()
    (cons (list.foldr #'(lambda (var-pair acc)
			  (if (equal (cadr var-pair) 'integerp)
			      (cons (list (car var-pair) (random max-list-size))
				    acc)
			    (cons (list (car var-pair) (make-random-list max-list-size max-list-size))
			    acc)))
		      () vars)
	  (create-array-assignments vars (- num-assignments 1) max-list-size))))

(defun test/sorting/isort-qsort (n)
  (find-all-conjectures n *test/sorting/constants*
			*test/sorting/vars*
			'((cons ((integerp integer-listp) integer-listp))
			  (append ((integer-listp integer-listp) integer-listp))
			  (orderedp ((integer-listp) booleanp))
			  (insert ((integerp integer-listp) integer-listp))
			  (isort ((integer-listp) integer-listp))
			  (qsort ((integer-listp) integer-listp))
			  (less ((integerp integer-listp) integer-listp))
			  (notless ((integerp integer-listp) integer-listp)))
			:given-assignments (create-sorting-assignments *test/sorting/vars* 300 30)
			:num-assignments 100
			:num-hyp-assignments 100
			:hyps-configs *test/sorting/hyp-configs*))

(defun test/sorting/isort-ssort (n)
  (find-all-conjectures n *test/sorting/constants*
			*test/sorting/vars*
			'((cons ((integerp integer-listp) integer-listp))
			  (append ((integer-listp integer-listp) integer-listp))
			  (orderedp ((integer-listp) booleanp))
			  (insert ((integerp integer-listp) integer-listp))
			  (isort ((integer-listp) integer-listp))
			  (ssort ((integer-listp) integer-listp))
			  (del ((integerp integer-listp) integer-listp))
			  (min-l ((integer-listp) integerp)))
			:guards (list (list 'min-l #'(lambda (x) (not (endp x)))))
			:given-assignments (create-sorting-assignments *test/sorting/vars* 300 30)
			:num-assignments 1000
			:num-hyp-assignments 100
			:hyps-configs *test/sorting/hyp-configs*))




; =================================
; ----- Tests and Interaction -----
; =================================

(defun get-object-size/octets (object)
  (sb-sys:without-gcing
   (nth-value 2 (sb-vm::reconstitute-object
		 (ash (logandc1 sb-vm:lowtag-mask
				(sb-kernel:get-lisp-obj-address object))
			     (- sb-vm:n-fixnum-tag-bits))))))

(defun object-size (object)
  (cond ((integerp object) 4)
	((symbolp object) (get-object-size/octets object))
	((consp object) (+ (get-object-size/octets object)
			   (object-size (car object))
			   (object-size (cdr object))))
	(t (get-object-size/octets object))))

(defmacro check-equal (e1 e2)
  `(let ((v1 ,e1)
	 (v2 ,e2))
     (if (== v1 v2)
	 (list 'TEST-PASSES)
       (list 'TEST-FAILS ',e1 'CHECK v1 'EXPECT v2))))

(defun error-print (msg e)
  (print e)
  (error msg))


;(test-find-all-conjectures-1 2)
;(test-find-all-conjectures-2 1)
;(test-find-all-conjectures-3 1)
;(test-find-all-conjectures-4 1)
;(test-find-all-conjectures-5 1)
