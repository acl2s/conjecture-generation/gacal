
; arguments
(defparameter *cmd-line-args* sb-ext:*posix-argv*)

; (print (list 'command-line-args *cmd-line-args*))

(define-condition no-input-file (error)
  ((text :initarg :text :reader text)))

(defun find-input-file (args)
  (if (endp args)
      (error 'no-input-file)
    (if (equal (car args) "-input-file")
	(if (endp (cdr args))
	    (error 'no-input-file)
	  (cadr args))
      (find-input-file (cdr args)))))

(define-condition no-sexp-input-file (error)
  ((text :initarg :text :reader text)))

(defun find-sexp-input-file (args)
  (if (endp args)
      (error 'no-sexp-input-file)
    (if (equal (car args) "-sexp-input-file")
	(if (endp (cdr args))
	    (error 'no-sexp-input-file)
	  (cadr args))
      (find-sexp-input-file (cdr args)))))

(define-condition no-output-file (error)
  ((text :initarg :text :reader text)))

(defun find-output-file (args)
  (if (endp args)
      (error 'no-output-file)
    (if (equal (car args) "-output-file")
	(if (endp (cdr args))
	    (error 'no-output-file)
	  (cadr args))
      (find-output-file (cdr args)))))


(define-condition no-input-hash (error)
  ((text :initarg :text :reader text)))

(defun find-input-hash (args)
  (if (endp args)
      (error 'no-input-hash)
    (if (equal (car args) "-input-hash")
	(if (endp (cdr args))
	    (error 'no-input-hash)
	  (cadr args))
      (find-input-hash (cdr args)))))

(defun find-verbose (args)
  (and (not (endp args))
       (or (equal (car args) "--verbose")
	   (find-verbose (cdr args)))))

; ===========================

;(load "src/load.lisp")


(defun file-get-contents (filename)
  (with-open-file (stream filename)
    (let ((contents (make-string (file-length stream))))
      (read-sequence contents stream)
      contents)))
(defun file-get-lines (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect line)))

(defun run-conj-gen (args)
  (handler-case
      (let* ((input-file (find-input-file args))
	     (sexp-input-file (find-sexp-input-file args))
	     (output-file (find-output-file args))
	     (architecture 32)
	     (hash (find-input-hash args))
	     (verbose (find-verbose args)))
	(handler-case
	    (with-open-file (s sexp-input-file)
			    (let* ((input-program-string (file-get-contents input-file))
				   (src-prgm (read s))
				   (witness-graph (time (create-graph/admit-functions/generate-trace/find-invariants input-program-string src-prgm architecture :verbose verbose :print-defs nil)))
					;(witness-graph (time (create-graph/admit-functions/generate-trace/find-invariants input-program-string src-prgm architecture :verbose verbose :print-acl2s-defs nil)))
					;(witness-graph-string (witness-graph-string input-file hash architecture witness-graph))
				   (_ (write-witness-to-file output-file hash input-file architecture witness-graph)))
			      (format t "~%VERIFICATION_SUCCESSFUL~%")
			      '_))
	  (violation-witness-found ()
				   (progn (write-violation-witness-to-file output-file hash input-file architecture *violation-witness-graph*)
					  (format t "~%VERIFICATION_FAILED~%")))
	  ))
    (no-conversion (e) (progn (format t "~%NO CONVERSION~%") (print e) (format t "UNKNOWN~%")))
    (not-all-assertions-proved () (progn (format t "~%COULD NOT PROVE ALL ASSERTIONS~%") (format t "UNKNOWN~%")))
    ;;(sb-ext:timeout (e) (progn (format t "~%TIMEOUT~%") (format t "~%UNKNOWN~%")))
    (error () (progn (format t "~%BAD ERROR~%") (format t "~%UNKNOWN~%")))
    ))

(run-conj-gen *cmd-line-args*)



