
#|
node = (make-node node-name [(list ACL2-var ACL2-type) list] [(list ACL2-var ACL2-type) list] ACL2-defdata)
edge = (make-edge edge-name node node ACL2-definec ACL2-definec loc
                  [bool option] [['condition-true 'condition-false U] option] [source-code option] boolean)
|#

(defstruct node name live-vars)

(defun node.make (name live-vars)
  (make-node :name name :live-vars live-vars))
(defun node.name (node)
  (node-name node))
(defun node.live-vars (node)
  (node-live-vars node))
  
(defstruct edge
  name from to condition-expression statement-expression
  vars loc enter-loop-head control source-code is-assertion return-from enter-function)

; edge-name node node ACL2-expression [ACL2-expression -> ACL2-expression] loc -> edge
(defun edge.make (name node-from node-to cond-exp stm-hole vars loc
		       &key (enter-loop-head option.none) (control option.none)
		       (source-code option.none) (is-assertion nil)
		       (return-from option.none) (enter-function option.none))
  (make-edge :name name :from node-from :to node-to
	     :condition-expression cond-exp :statement-expression stm-hole
	     :vars vars
	     :loc loc
	     :enter-loop-head enter-loop-head :control control
	     :source-code source-code :is-assertion is-assertion
	     :return-from return-from :enter-function enter-function))
(defun edge.name (edge)
  (edge-name edge))
(defun edge.from (edge)
  (edge-from edge))
(defun edge.to (edge)
  (edge-to edge))
(defun edge.condition-expression (edge)
  (edge-condition-expression edge))
(defun edge.statement-expression (edge)
  (edge-statement-expression edge))
(defun edge.vars (edge)
  (edge-vars edge))
(defun edge.loc (edge)
  (edge-loc edge))
(defun edge.enter-loop-head (edge)
  (edge-enter-loop-head edge))
(defun edge.control (edge)
  (edge-control edge))
(defun edge.source-code (edge)
  (edge-source-code edge))
(defun edge.is-assertion (edge)
  (edge-is-assertion edge))
(defun edge.return-from (edge)
  (edge-return-from edge))
(defun edge.enter-function (edge)
  (edge-enter-function edge))


; pgraph is program graph
; praph = (list [node list] [edge list])
; pgraph-ACL2-data = (list [node-name defdata map] [edge-name ACL2-definec map] [edge-name ACL2-definec map])


(defun pgraph.make (nodes edges)
  (list nodes edges))

(defun pgraph.nodes (pgraph)
  (car pgraph))

(defun pgraph.edges (pgraph)
  (cadr pgraph))
  
(defun pgraph.succs (node pgraph)
  (list.foldr #'(lambda (edge acc)
		  (if (equal (edge.from edge) node)
		      (cons (edge.to edge) acc)
		    acc))
	      () (pgraph.edges pgraph)))

(defun pgraph.preds (node pgraph)
  (list.foldr #'(lambda (edge acc)
		  (if (equal (edge.to edge) node)
		      (cons (edge.from edge) acc)
		    acc))
	      () (pgraph.edges pgraph)))

; pgraph node-name -> node
(defun pgraph.find-node (graph node-name)
  (option.get (list.find #'(lambda (node) (equal (node.name node) node-name)) (car graph))))


(defun pgraph-ACL2-data.defdatas (pgraph-ACL2-data)
  (car pgraph-ACL2-data))
(defun pgraph-ACL2-data.conditions (pgraph-ACL2-data)
  (cadr pgraph-ACL2-data))
(defun pgraph-ACL2-data.statements (pgraph-ACL2-data)
  (caddr pgraph-ACL2-data))

; returns the strongly connected components in a topsort
(defun strongly-connected-components (graph)
  (let* ((nodes (pgraph.nodes graph))
	 (edges (pgraph.edges graph))
	 (reachable (mmap.empty))
	 (_ (list.iter #'(lambda (node) (mmap.add-exn reachable (node.name node) (list node))) nodes))
	 ;(reachableT (list.foldr #'(lambda (node mmap) (mmap.add-exn mmap (node.name node) (list node))) (mmap.empty) nodes))
	 )
    (labels ((compute-reachable (node wl)
				(if (endp wl)
				    '_
				  (let* ((node* (car wl))
					 (wl* (cdr wl))
					 (next (pgraph.succs node* graph))
					 (next* (list-diff next (mmap.find-exn reachable (node.name node)))))
				    (mmap.add-overwrite reachable (node.name node) (append next* (mmap.find-exn reachable (node.name node))))
				    (compute-reachable node (append next* wl*)))))
	     #|(compute-reachableT (node wl)
				(if (endp wl)
				    '_
				  (let* ((node* (car wl))
					 (wl* (cdr wl))
					 (next (pgraph.preds node* graph))
					 (next* (list-diff next (mmap.find-exn reachable (node.name node)))))
				    (mmap.add-overwrite reachable (node.name node) (append next* (mmap.find-exn reachable (node.name node))))
	     (compute-reachableT node (append next* wl*)))))|#
	     (topsort-groups (groups)
			     (list.foldr #'(lambda (group groups*)
					     (list.insert group groups*
							  #'(lambda (group1 group2)
							      ; group1 < group2 if there is any node1 in group1, node2 in group2, s.t. node1 ~~> node2
							      (list.exists #'(lambda (node1)
									       (list.exists #'(lambda (node2)
												(list.in node2 (mmap.find-exn reachable (node.name node1))))
											    group2))
									   group1))))
					 () groups))
	     )
	    (let* ((_ (list.iter #'(lambda (node)
				     (compute-reachable node (list node))
					;(compute-reachableT node (list node))
				     )
				 nodes))
		   (groups (list.foldr #'(lambda (node acc)
					   (let* ((flag nil)
						  (next (list.map #'(lambda (set)
								       (if (and (list.in (car set) (mmap.find-exn reachable (node.name node)))
										(list.in node (mmap.find-exn reachable (node.name (car set)))))
									   (if flag
									       (error 'unimplemented)
									     (progn (setf flag t)
										    (cons node set)))
									set))
								   acc))
						  (next* (if flag next (cons (list node) acc))))
					     next*))
				       () nodes)))
	      (topsort-groups groups)))))


(defun concat-source-codes (sc-opt1 sc-opt2)
  (if (option.exists sc-opt1)
      (if (option.exists sc-opt2)
	  (option.some (string-append-all (list (option.get sc-opt1) "    " (option.get sc-opt2))))
	sc-opt1)
    sc-opt2))

(defun concat-or-source-codes (sc-opts)
  (if (endp sc-opts)
      option.none
    (if (endp (cdr sc-opts))
	(car sc-opts)
      (let* ((sc-opt1 (car sc-opts))
	     (sc-opt2 (concat-or-source-codes (cdr sc-opts))))
	(if (option.exists sc-opt1)
	    (if (option.exists sc-opt2)
		(option.some (string-append-all (list (option.get sc-opt1) "  |  " (option.get sc-opt2))))
	      sc-opt1)
	  sc-opt2)))))
	

;; [edge list] -> [[edge list] option]
(defun find-repeat-edges-succ (edges)
  (if (endp edges) option.none
    (let* ((edge (car edges))
	   (edges* (list.filter #'(lambda (edge*) (equal (edge.to edge) (edge.to edge*))) (cdr edges))))
      (if (endp edges*)
	  (find-repeat-edges-succ (cdr edges))
	(option.some (cons edge edges*))))))
;; [edge list] -> [[edge list] option]
(defun find-repeat-edges-pred (edges)
  (if (endp edges) option.none
    (let* ((edge (car edges))
	   (edges* (list.filter #'(lambda (edge*) (equal (edge.from edge) (edge.from edge*))) (cdr edges))))
      (if (endp edges*)
	  (find-repeat-edges-pred (cdr edges))
	(option.some (cons edge edges*))))))
	 
(defparameter *merge-disjuncts* nil)

; [node list] [node list] [edge list] -> pgraph
(defun quotient-graph/wl (assertions wl nodes edges)
  (if (endp wl)
      (pgraph.make nodes edges)
    (let* ((node (car wl))
	   (pred-edges (list.filter #'(lambda (edge) (equal (edge.to edge) node)) edges))
	   (succ-edges (list.filter #'(lambda (edge) (equal (edge.from edge) node)) edges))
	   (repeat-pred (find-repeat-edges-pred pred-edges))
	   (repeat-succ (find-repeat-edges-succ succ-edges)))
      (cond ((and (equal (length pred-edges) 1)
		  (equal (length succ-edges) 1)
		  (not (option.exists (map.find assertions (node.name node)))))
	     (let* ((edge-pred (car pred-edges))
		    (edge-succ (car succ-edges))
		    (node-pred (edge.from edge-pred))
		    (node-succ (edge.to edge-succ))
		    (stm-hole-pred (edge.statement-expression edge-pred))
		    (stm-hole-succ (edge.statement-expression edge-succ))
		    (stm-hole* #'(lambda (exp) (funcall stm-hole-pred (funcall stm-hole-succ exp))))
		    (cond-exp-pred (edge.condition-expression edge-pred))
		    (cond-exp-succ (edge.condition-expression edge-succ))
		    (cond-exp* (cond ((and (equal cond-exp-pred t) (equal cond-exp-succ t)) t)
				     ((equal cond-exp-pred t) (funcall stm-hole-pred cond-exp-succ))
				     ((equal cond-exp-succ t) cond-exp-pred)
				     (t (list 'and cond-exp-pred (funcall stm-hole-pred cond-exp-succ)))))
		    (new-edge (edge.make (new-edge-name) node-pred node-succ cond-exp* stm-hole* (list-union (edge.vars edge-pred) (edge.vars edge-succ)) ;; isn't quite true
					 '_ :source-code (concat-source-codes (edge.source-code edge-pred) (edge.source-code edge-succ))))
		    (nodes* (list.remove node nodes))
		    (edges* (cons new-edge (list.remove edge-pred (list.remove edge-succ edges)))))
	       (quotient-graph/wl assertions (list-union (list node-pred node-succ) wl) nodes* edges*)))
	    ((and *merge-disjuncts* (option.exists repeat-succ))
	     (let* ((merge-edges (option.get repeat-succ))
		    (cond-exp* (cons 'or (list.map #'(lambda (edge) (edge.condition-expression edge)) merge-edges)))
		    (stm-hole* #'(lambda (exp) (cons 'cond (list.foldr #'(lambda (edge acc) (cons (list (edge.condition-expression edge) (funcall (edge.statement-expression edge) exp)) acc)) () merge-edges))))
		    (new-edge (edge.make (new-edge-name) node (edge.to (car merge-edges)) cond-exp* stm-hole* (list.foldr #'(lambda (edge acc) (list-union (edge.vars edge) acc)) () merge-edges)
					 '_ :source-code (concat-or-source-codes (list.map #'edge.source-code merge-edges))))
		    (edges* (cons new-edge (list-diff edges merge-edges))))
	       (quotient-graph/wl assertions wl nodes edges*)))
	    ((and *merge-disjuncts* (option.exists repeat-pred))
	     (let* ((merge-edges (option.get repeat-pred))
		    (cond-exp* (cons 'or (list.map #'(lambda (edge) (edge.condition-expression edge)) merge-edges)))
		    (stm-hole* #'(lambda (exp) (cons 'cond (list.foldr #'(lambda (edge acc) (list (edge.condition-expression edge) (funcall (edge.condition-statement edge) exp))) () merge-edges))))
		    (new-edge (edge.make (new-edge-name) (edge.from (car merge-edges)) node cond-exp* stm-hole* (list.foldr #'(lambda (edge acc) (list-union (edge.vars edge) acc)) () merge-edges)
					 '_ :source-code (concat-or-source-codes (list.map #'edge.source-code merge-edges))))
		    (edges* (cons new-edge (list-diff edges merge-edges))))
	       (quotient-graph/wl assertions wl nodes edges*)))
	    (t (quotient-graph/wl assertions (cdr wl) nodes edges))))))

#|
; [node list] [node list] [edge list] -> pgraph
(defun quotient-graph/wl (wl nodes edges)
  (print (list.map #'node.name wl))
  #|(print (list (list.map #'node.name wl)
	       (list.map #'node.name nodes)
	       (list.map #'(lambda (edge) (list (node.name (edge.from edge)) (edge.name edge) (node.name (edge.to edge)))) edges)))|#
  (if (endp wl)
      (pgraph.make nodes edges)
    (let* ((node (car wl))
	   (succ-edges (list.filter #'(lambda (edge) (equal (edge.from edge) node)) edges)))
      (if (and (equal (length succ-edges) 1)
	       ;(equal (edge.condition-expression (car succ-edges)) t) ; upon second thought, I don't think this is necessary, as long as cond-exp** keeps track of both!
	       (not (edge.is-assertion (car succ-edges))))
	  (let* ((edge (car succ-edges))
		 (node* (edge.to edge))
		 (pred-edges (list.filter #'(lambda (edge) (equal (edge.to edge) node*)) edges)))
	    (if (equal (length pred-edges) 1)
		 (let* ((succ-edges* (list.filter #'(lambda (edge*) (equal (edge.from edge*) node*)) edges))
			(succ-edges** (list.map #'(lambda (edge*)
						    ;(print (edge.name edge*))
						    (let* ((node** (edge.to edge*))
							   (loc (edge.loc edge*))
							   (stm-exp (edge.statement-expression edge))
							   (stm-exp* (edge.statement-expression edge*))
							   (stm-hole** #'(lambda (exp) (funcall stm-exp* (funcall stm-exp exp))))
							   (cond-exp (edge.condition-expression edge))
							   (cond-exp* (edge.condition-expression edge*))
							   (cond-exp** (list 'and cond-exp (funcall stm-exp cond-exp*))))
						      (edge.make (new-edge-name) node node** cond-exp** stm-hole** loc ...)))
						succ-edges*))
			(nodes* (list.remove node* nodes))
			(edges* (append succ-edges** (list.remove edge (list.filter #'(lambda (edge*) (not (equal (edge.from edge*) node*))) edges)))))
		   (quotient-graph/wl (list.remove node* wl) nodes* edges*))
	      (quotient-graph/wl (cdr wl) nodes edges)))
	(quotient-graph/wl (cdr wl) nodes edges)))))

|#

; pgraph -> pgraph
; returns a quotiented graph where if n1 c1,s1-> n2 with c1 = t and there are no other outgoing edges from n1, then kill n2, and fold its outgoing edges with n1's
(defun quotient-pgraph (assertions pgraph)
  (let* ((nodes (pgraph.nodes pgraph))
	 (edges (pgraph.edges pgraph)))
    (quotient-graph/wl assertions nodes nodes edges)))




; pgraph -> pgraph-ACL2-data
(defun pgraph-create-ACL2-data (pgraph)
  (let* ((nodes (pgraph.nodes pgraph))
	 (edges (pgraph.edges pgraph))
	 (defdata-map (list.foldr #'(lambda (node acc)
				      (map.add-exn acc (node.name node) (create-defdata (node.live-vars node))))
				  map.empty nodes))
	 (conditions-map (list.foldr #'(lambda (edge acc)
					 (let* ((cond-exp (edge.condition-expression edge))
						(node (edge.from edge))
						(defdata (map.find-exn defdata-map (node.name node)))
						(live-vars (node.live-vars node)))
					   (map.add-exn acc (edge.name edge)
							`(definec ,(new-condition-name) (input ,(name-to-type (defdata.name defdata))) :boolean
							   ,(use-fill-in-context 'input (list.map #'car live-vars) cond-exp)))))
				     () edges))
	 (statements-map (list.foldr #'(lambda (edge acc)
					 (let* ((stm-hole (edge.statement-expression edge))
						(node (edge.from edge))
						(defdata (map.find-exn defdata-map (node.name node)))
						(live-vars (node.live-vars node))
						(node* (edge.to edge))
						(defdata* (map.find-exn defdata-map (node.name node*)))
						(live-vars* (node.live-vars node*)))
					   (map.add-exn acc (edge.name edge)
							`(definec ,(new-statement-name) (input ,(name-to-type (defdata.name defdata)))
							   ,(name-to-type (defdata.name defdata*))
							   ,(use-fill-in-context 'input (list.map #'car live-vars)
										 (funcall stm-hole
											  (cons 'list (list.map #'car live-vars*))))))))
				     () edges)))
    (list defdata-map conditions-map statements-map)))
			
	   



;; pgraph -> pgraph
(defun recompute-live-vars (pgraph)
  (let* ((nodes (pgraph.nodes pgraph))
	 (edges (pgraph.edges pgraph)))
    (labels ((wl-algo (wl nodes edges)
		      (if (endp wl)
			  (pgraph.make nodes edges)
			(let* ((node (car nodes))
			       (succ-edges (list.filter #'(lambda (edge) (equal node (edge.from edge))) edges))
			       (pred-edges (list.filter #'(lambda (edge) (equal node (edge.to edge))) edges))
			       (new-live-vars (list.foldr #'(lambda (edge acc) (list-union (node.live-vars (edge.to edge)) acc)) () succ-edges)))
			  (assert (list-subset (node.live-vars node) new-live-vars))
			  (if (list-subset new-live-vars (node.live-vars node))
			       (wl-algo (cdr wl) nodes edges)
			    (let* ((node* (node.make (node.name node) new-live-vars))
				   (nodes* (list.map #'(lambda (node**) (if (equal node node**) node* node**)) nodes))
				   (edges* (list.map #'(lambda (edge)
							 (let* ((from* (if (equal (edge.from edge) node) node (edge.from edge)))
								(to* (if (equal (edge.to edge) node) node (edge.to edge))))
							   (edge.make (edge.name edge) from* to*
								      (edge.condition-expression edge) (edge.statement-expression edge)
								      (edge.vars edge)
								      (edge.loc edge)
								      :enter-loop-head (edge.enter-loop-head edge)
								      :control (edge.control edge)
								      :source-code (edge.source-code edge)
								      :is-assertion (edge.is-assertion edge))))
						     edges))
				   (wl* (list.remove node* (list-union (list.foldr #'(lambda (edge acc)
										       (if (equal node* (edge.to edge))
											   (cons (edge.from edge) acc)
											 acc))
										   () edges)
								       (cdr wl)))))
			      (wl-algo wl* nodes* edges*)))))))
	    (wl-algo nodes nodes edges))))

;; pgraph -> '_
(defun print-pgraph (pgraph)
  (let* ((nodes (pgraph.nodes pgraph))
	 (edges (pgraph.edges pgraph)))
    (list.iter #'(lambda (edge) (print (append (list (node.name (edge.from edge)))
					       (if (option.exists (edge.source-code edge))
						   (list '-- (option.get (edge.source-code edge)) '->)
						 (list '--->))
					       ;;(list (edge.condition-expression edge) (funcall (edge.statement-expression edge) '_))
					       (list (node.name (edge.to edge))))))
	       edges)))

