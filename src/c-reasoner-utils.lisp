
; 'a bool -> '_
(defun print-verbose (q verbose)
  (if verbose (print q) '_))

; ['a list] ['a list] -> ['a list]
(defun list-intersect (l1 l2)
  (if (endp l1)
      ()
    (let ((res (list-intersect (cdr l1) l2)))
      (if (list.in (car l1) l2)
	  (cons (car l1) res)
	res))))

; ['a list] ['a list] -> ['a list]
(defun list-union (l1 l2)
  (if (endp l1)
      l2
    (let ((res (list-union (cdr l1) l2)))
      (if (list.in (car l1) l2)
	  res
	(cons (car l1) res)))))

(defun list-diff (l1 l2)
  (list.filter #'(lambda (e1) (not (list.in e1 l2))) l1))

(defun list-subset (l1 l2)
  (list.forall #'(lambda (e1) (list.in e1 l2)) l1))

; [string list] -> string
(defun string-append-all (list-of-strings)
  (format nil "~{~A~}" list-of-strings))

(defun new-node-name ()
  (gentemp "node-"))
(defun new-loop-node-name ()
  (gentemp "loop-node-"))
(defun new-assert-node-name ()
  (gentemp "assert-node-"))
(defun new-edge-name ()
  (gentemp "edge-"))
(defun new-condition-name ()
  (gentemp "cond-"))
(defun new-statement-name ()
  (gentemp "stm"))
(defun new-defdata-name ()
  (gentemp "data"))
(defun new-var-name ()
  (gentemp "var"))


(define-condition find-not-found (error)
  ((text :initarg :text :reader text)))

(define-condition find-length-error (error)
  ((text :initarg :text :reader text)))

(define-condition find-not-found-local (error)
  ((text :initarg :text :reader text)))

(define-condition find-length-error-local (error)
  ((text :initarg :text :reader text)))

(defun l.find-acc (sexp* token*)
  (if (endp sexp*)
      (error 'find-not-found-local)
    (if (equal (car sexp*) token*)
	(if (endp (cdr sexp*))
	    (error 'find-length-error-local)
	  (cadr sexp*))
      (l.find-acc (cdr sexp*) token*))))

; 'a ['a list] -> 'a
(defun l.find (sexp token)
  (handler-case (l.find-acc sexp token)
    (find-not-found-local (arg) (progn (print (list 'looking 'for token 'in sexp))
				       (error 'find-not-found)))
    (find-length-error-local (arg) (progn (print (list 'length 'error 'for token 'in sexp))
					  (error 'find-length-error)))))


(define-condition ACL2-form-failed (error)
  ((text :initarg :text :reader text)))


(defun invariant-replace-var-with-exp (invariant var exp)
  (if (consp invariant)
      (list (car invariant) (list.map #'(lambda (invariant*) (invariant-replace-var-with-exp invariant* var exp)) (cdr invariant)))
    (if (equal invariant var)
	exp
      invariant)))
