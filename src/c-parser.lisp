
;; name-map := [symbol (list ACL2-var symbol)]

(define-condition no-conversion (error)
  ((text :initarg :text :reader text)))

;; decl-type -> type
(defun convert-decl-type (decl-type)
  (let* ((modifiers (l.find decl-type ':modifiers)))
    (if (or (list.in 'long modifiers)
	    (list.in 'long-long modifiers)
	    (list.in 'short modifiers)
	    (list.in 'signed modifiers)
	    (list.in 'complex modifiers)
	    (list.in 'imaginary modifiers))
	(error 'no-conversion)
      (cond ((equal (l.find decl-type ':base) 'bool)
	     (if (list.in 'unsigned modifiers)
		 (error 'no-conversion)
	       (list 'basic-type ':kind 'bool)))
	    ((equal (l.find decl-type ':base) 'char)
	     (if (list.in 'unsigned modifiers)
		 (error 'no-conversion)
	       (list 'basic-type ':kind 'char)))
	    ((equal (l.find decl-type ':base) 'int)
	     (if (list.in 'unsigned modifiers)
		 (list 'basic-type ':kind 'uint)
	       (list 'basic-type ':kind 'int)))
	    ((equal (l.find decl-type ':base) 'double)
	     (error 'no-conversion))
	    ((equal (l.find decl-type ':base) 'float)
	     (error 'no-conversion))
	    ((equal (l.find decl-type ':base) 'void)
	     (error 'no-conversion))
	    ((equal (l.find decl-type ':base) 'auto)
	     (error 'no-conversion))))))

;; c-type -> type
(defun convert-type (type)
  (cond ((equal (car type) 'basic-type)
	 (let* ((modifiers (l.find type ':modifiers)))
	   (if (or (list.in 'long modifiers)
		   (list.in 'long-long modifiers)
		   (list.in 'short modifiers)
		   (list.in 'signed modifiers)
		   (list.in 'complex modifiers)
		   (list.in 'imaginary modifiers))
	       (error 'no-conversion)
	     (cond ((equal (l.find type ':kind) 'bool)
		    (if (list.in 'unsigned modifiers)
			(error 'no-conversion)
		      (list 'basic-type ':kind 'bool)))
		   ((equal (l.find type ':kind) 'char)
		    (if (list.in 'unsigned modifiers)
			(error 'no-conversion)
		      (list 'basic-type ':kind 'char)))
		   ((equal (l.find type ':kind) 'int)
		    (if (list.in 'unsigned modifiers)  
			(list 'basic-type ':kind 'uint)
		      (list 'basic-type ':kind 'int)))
		   ((equal (l.find type ':kind) 'double)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'float)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'char16)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'char32)
		    (error 'no-conversion))
		   ((equal (l.find type ':kind) 'void)
		    (error 'no-conversion))))))
	((equal (car type) 'function-type)
	 (error 'no-conversion))
	((equal (car type) 'array-type)
	 (error 'no-conversion))
	((equal (car type) 'pointer-type)
	 (error 'no-conversion))))

;; [expression (list 'not-an-expression statement) U] -> expression
(defun coerce-expression-to-expression (exp)
  (if (equal (car exp) 'not-an-expression)
      (error 'no-conversion)
    exp))

;; [expression (list 'not-an-expression [statement list]) U] -> [statement list]
(defun coerce-expression-to-statements (exp)
  (if (equal (car exp) 'not-an-expression)
      (cadr exp)
    (error 'no-conversion)))

;; [[statement list] (list 'not-a-statements expression) U] -> statement
(defun coerce-statements-to-statements (stm)
  (if (equal (car stm) 'not-a-statements)
      (error 'no-conversion)
    stm))

;; [[statement list] (list 'not-a-statements expression) U] -> expression
(defun coerce-statements-to-expression (stm)
  (if (equal (car stm) 'not-a-statements)
      (cadr stm)
    (error 'no-conversion)))

;; [expression list] [type list] -> [[expression list] type pair]
;; iterates through the types, trying to convert every expression to the current type
;; if successful, returns the coerced expressions along with the type, otherwise errors with no-conversion
(defun try-coerce-expressions-to-type (expressions types)
  (let* ((res-opt (list.foldl #'(lambda (type res-opt)
				  (if (option.exists res-opt)
				      res-opt
				    (handler-case (option.some (pair.make (list.map #'(lambda (exp) (coerce-expression-to-type exp type)) expressions) type))
				      (no-conversion () option.none))))
			      option.none types)))
    (if (option.exists res-opt)
	(option.get res-opt)
      (error 'no-conversion))))

(defun bool-fix-exp (exp)
  (let* ((uint-type '(basic-type :kind uint))
	 (int-type '(basic-type :kind int))
	 (res (try-coerce-expressions-to-type (list exp) (list uint-type int-type)))
	 (type (pair.second res))
	 (exp* (car (pair.first res))))
    (list 'binary-expression 
	  ':type type ':loc (l.find exp ':loc) ':source-code (l.find exp ':source-code)
	  ':operator '!=
	  ':operand1 exp*
	  ':operand2 (list 'literal-expression 
			   ':loc (l.find exp ':loc) ':type type ':source-code (l.find exp ':source-code)
			   ':value 0))))

;; expression type -> expression
(defun coerce-expression-to-type (expression type)
  (let* ((type* (l.find expression ':type))
	 (uint-type '(basic-type :kind uint))
	 (int-type '(basic-type :kind int))
	 (char-type '(basic-type :kind char))
	 (bool-type '(basic-type :kind bool))
	 (loc (l.find expression ':loc))
	 (source-code (l.find expression ':source-code)))
    (cond ((equal (car expression) 'literal-expression)
	   (let* ((value (l.find expression ':value)))
	     (cond ((equal type type*)
		    expression)
		   ((and (equal type uint-type)
			 (equal type* int-type))
		    (list 'literal-expression
			  ':value (mod value *c-uint-max*)
			  ':type type
			  ':source-code source-code
			  ':loc loc))
		   ((and (equal type int-type)
			 (equal type* uint-type))
		    (list 'literal-expression
			  ':value (l.find expression ':value) 
			  ':type type
			  ':source-code source-code
			  ':loc loc))
		   ((and (equal type bool-type)
			 (or (equal type* int-type)
			     (equal type* uint-type)))
		    (list 'binary-expression
			  ':operator '!=
			  ':operand1 expression
			  ':operand2 (list 'literal-expression
					   ':value 0
					   ':type type*
					   ':source-code source-code
					   ':loc loc)
			  ':loc loc ':type bool-type ':source-code source-code))
		   (t (error 'no-conversion)))))

	  ((equal (car expression) 'id-expression)
	   (cond ((equal type type*)
		  expression)
		 ((and (equal type bool-type)
		       (or (equal type* int-type)
			   (equal type* uint-type)))
		  (list 'binary-expression
			':operator '!=
			':operand1 expression
			':operand2 (list 'literal-expression
					 ':value 0
					 ':type type*
					 ':source-code source-code
					 ':loc loc)
			':loc loc ':type bool-type ':source-code source-code))
		 ((and (equal type* uint-type) ;; TODO: I don't think I want to modify the original type as I may use that elsewhere... so I'll just add 0 to it. Works since uint is a subtype of int? (in our framework)
		       (equal type int-type))
		  (list 'binary-expression
			':operator '+
			':operand1 expression
			':operand2 (list 'literal-expression
					 ':value 0
					 ':type type*
					 ':source-code source-code
					 ':loc loc)
			':loc loc ':type int-type ':source-code source-code))
		 (t (error 'no-conversion))))

	  ((equal (car expression) 'unary-expression)
	   (let* ((operand (l.find expression ':operand))
		  (operator (l.find expression ':operator)))
	   (cond #|((equal operator '-)
		  (cond ((equal type uint-type)
			 (error 'no-conversion))
			((equal type int-type)
			 (list 'unary-expression
			       ':type type
			       ':operator '-
			       ':operand (coerce-expression-to-type operand type)
			       ':source-code source-code
			       ':loc loc))
			((equal type char-type)
			 (error 'no-conversion))
			((equal type bool-type)
			 (bool-fix-exp expression))))|#
		 ((equal operator 'log-not)
		  (cond ((equal type uint-type)
			 (error 'no-conversion))
			((equal type int-type)
			 (error 'no-conversion))
			((equal type char-type)
			 (error 'no-conversion))
			((equal type bool-type)
			 (list 'unary-expression
			       ':type type
			       ':operator 'log-not
			       ':operand (coerce-expression-to-type operand type)
			       ':source-code source-code
			       ':loc loc))))
		 ((equal operator 'bin-not)
		  (cond ((equal type uint-type)
			 (list 'unary-expression
			       ':type type
			       ':operator 'bin-not
			       ':operand (coerce-expression-to-type operand type)
			       ':source-code source-code
			       ':loc loc))
			((equal type int-type)
			 (error 'no-conversion))
			((equal type char-type)
			 (error 'no-conversion))
			((equal type bool-type)
			 (bool-fix-exp expression)))))))

	  ((equal (car expression) 'binary-expression)
	   (let* ((operator (l.find expression ':operator))
		  (operand1 (l.find expression ':operand1))
		  (operand2 (l.find expression ':operand2)))
	     (cond ((or (equal operator '==)
			(equal operator '!=))
		    (cond ((equal type uint-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (let* ((res (try-coerce-expressions-to-type (list operand1 operand2) (list uint-type int-type char-type)))
				  (operand1* (car (pair.first res)))
				  (operand2* (cadr (pair.first res))))
			     (list 'binary-expression
				   ':type type
				   ':operator operator
				   ':operand1 operand1*
				   ':operand2 operand2*
				   ':source-code source-code
				   ':loc loc)))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '>=) (equal operator '>) (equal operator '<=) (equal operator '<))
		    (cond ((equal type uint-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (let* ((res (try-coerce-expressions-to-type (list operand1 operand2) (list uint-type int-type)))
				  (operand1* (car (pair.first res)))
				  (operand2* (cadr (pair.first res))))
			     (list 'binary-expression
				   ':type type
				   ':operator operator
				   ':operand1 operand1*
				   ':operand2 operand2*
				   ':source-code source-code
				   ':loc loc)))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator 'log-and) (equal operator 'log-or))
		    (cond ((equal type uint-type)
			   (error 'no-conversion))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (list 'binary-expression
				 ':type type
				 ':operator operator
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator 'bin-and) (equal operator 'bin-or) (equal operator 'bin-xor))
		    (cond ((equal type uint-type)
			   (list 'binary-expression
				 ':type type
				 ':operator operator
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type int-type)
			   (error 'no-conversion))
			  ((equal type bool-type)
			   (bool-fix-exp expression))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '+) (equal operator 'unsigned-+))
		    (cond ((equal type uint-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned-+
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '+
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type bool-type)
			   (bool-fix-exp expression))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '-) (equal operator 'unsigned--))
		    (cond ((equal type uint-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned--
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '-
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type bool-type)
			   (bool-fix-exp expression))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '*) (equal operator 'unsigned-*))
		    (cond ((equal type uint-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned-*
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '*
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type bool-type)
			   (bool-fix-exp expression))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((or (equal operator '/) (equal operator 'unsigned-/))
		    (cond ((equal type uint-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'unsigned-/
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator '/
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type bool-type)
			   (bool-fix-exp expression))
			  ((equal type char-type)
			   (error 'no-conversion))))
		   ((equal operator 'mod)
		    (cond ((equal type uint-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'mod
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type int-type)
			   (list 'binary-expression
				 ':type type
				 ':operator 'mod
				 ':operand1 (coerce-expression-to-type operand1 type)
				 ':operand2 (coerce-expression-to-type operand2 type)
				 ':source-code source-code
				 ':loc loc))
			  ((equal type bool-type)
			   (bool-fix-exp expression))
			  ((equal type char-type)
			   (error 'no-conversion)))))))
	  
	  ((equal (car expression) 'nondeterministic-expression)
	   (list 'nondeterministic-expression
		 ':type type
		 ':source-code source-code
		 ':loc loc))
	   #|(cond ((equal type type*)
		  expression)
		 (t (error 'no-conversion))))|#
	  ((equal (car expression) 'conditional-expression)
	   (list 'conditional-expression
		 ':type type
		 ':test (coerce-expression-to-type (l.find expression ':test) bool-type)
		 ':then (coerce-expression-to-type (l.find expression ':then) type)
		 ':else (coerce-expression-to-type (l.find expression ':else) type)
		 ':source-code source-code
		 ':loc loc))
	  )))


(defun create-statement-from-list (statements loc)
  (if (equal (length statements) 1)
      (car statements)
    (list 'compound-statement
	  ':statements statements
	  ':loc loc)))

(defun convert-top-level-declarations (declarations name-map input-file-string)
  (if (endp declarations)
      ()
    (let* ((declaration (car declarations))
	   (res (cond ((equal (car declaration) 'simple-declaration)
		       (let* ((res (create-declaration-statements declaration (l.find declaration ':loc) name-map input-file-string :forget-functions t))
			      (stms (pair.first res))
			      (name-map* (pair.second res)))
			 (list (list.map #'(lambda (stm)
					     (let* ((_ (if (not (equal (car stm) 'declaration-statement))
							   (error 'no-conversion)
							 '_))
						    (declaring (l.find stm ':declaring))
						    (init (l.find stm ':initializer)))
					       (list 'simple-declaration ':declaring declaring ':initializer init ':loc (l.find declaration ':loc)
						     ':source-code (find-source-code input-file-string (l.find declaration ':loc)))))
					 stms)
			       name-map*)))
		      ((equal (car declaration) 'function-definition)
		       (let* ((declarator (l.find declaration ':declarator))
			      (_ (if (not (equal (car declarator) 'function-declarator))
				     (error 'no-conversion :text (list 'bad 'declarator 'expected 'function-declarator 'not (car declarator) declarator))
				   '_))
			      (res (list.foldr #'(lambda (parameter acc)
						   (let* ((parameters (car acc))
							  (name-map (cadr acc))
							  (declarator* (l.find parameter ':declarator))
							  (_ (if (not (equal (car declarator*) 'declarator))
								 (error 'no-conversion :text (list 'bad 'declarator 'expected 'declarator 'not (car declarator*) declarator*))
							       '_))
							  (old-name (l.find declarator* ':name)))
						     (if (not (equal (car old-name) 'name))
							 acc
						       (let* ((old-name (cadr old-name))
							      (type (convert-decl-type (l.find (l.find parameter ':decl-specifier) ':decl-type)))
							      (ACL2-var (new-var-name))
							      (var (list ACL2-var old-name))
							      (name-map* (map.add-exn name-map old-name (list ACL2-var old-name))))
							 (list (cons (list 'declaring ':var var ':type type) parameters)
							       name-map*)))))
					       (list () name-map) (l.find declarator ':parameters)))
			      (parameters (car res))
			      (name-map* (cadr res))
			      (name (l.find declarator ':name))
			      (decl-spec (l.find declaration ':decl-specifier))
			      (return-type 'todo-void) ;; TODO (convert-decl-type (l.find decl-spec ':decl-type)))
			      (stm (l.find declaration ':body))
			      (stm* (create-statement-from-list (pair.first (convert-statement stm name-map* input-file-string))
							       (l.find declaration ':loc))))
			 (list (list (list 'function-declaration ':name (cadr name) ':parameters parameters ':return-type return-type ':body stm*
					   ':loc (l.find declaration ':loc)
					   ':dec-loc (l.find declarator ':loc) 
					   ':source-code (find-source-code input-file-string (l.find declaration ':loc))))
			       name-map*)))
		      (t (error 'no-conversion :text (list 'bad 'declaration declaration)))))
	   (declarations* (car res))
	   (name-map* (cadr res))
	   (res* (convert-top-level-declarations (cdr declarations) name-map* input-file-string)))
      (append declarations* res*))))

;; c-program -> program
(defun convert-program* (src-prgm input-file-string)
  (let* ((declarations (l.find src-prgm ':declarations))
	 (declarations* (convert-top-level-declarations declarations map.empty input-file-string)))
    (list 'program ':declarations declarations*)))

;; c-program -> statement 
(defun convert-program (src-prgm input-file-string)
  (let* ((declarations (l.find src-prgm ':declarations))
	 (main-opt (list.find #'(lambda (declaration)
				  (and (equal (car declaration) 'function-definition)
				       (equal (car (l.find declaration ':declarator)) 'function-declarator)
				       (equal (car (l.find (l.find declaration ':declarator) ':name)) 'name)
				       (equal (cadr (l.find (l.find declaration ':declarator) ':name)) '|main|)))
			      declarations))
	 (no-outside-defs (list.forall #'(lambda (declaration)
					   (and (equal (car declaration) 'function-definition)
						(equal (car (l.find declaration ':declarator)) 'function-declarator)
						(equal (car (l.find (l.find declaration ':declarator) ':name)) 'name)))
				       declarations)))

    (if (option.exists main-opt) 
	(create-statement-from-list (pair.first (convert-statement (l.find (option.get main-opt) ':body) map.empty input-file-string))
				    (l.find (l.find (option.get main-opt) ':body) ':loc))
      (error 'no-conversion :text (list 'no 'main 'function)))))

;; c-declaration loc name-map -> [[statement list] name-map pair]
(defun create-declaration-statements (declaration loc names input-file-string &key (forget-functions nil))
  (cond ((equal (car declaration) 'simple-declaration)
	 (let* ((decl-spec (l.find declaration ':decl-specifier))
		(decs (list.map
		       #'(lambda (declarator)
			   (cond ((equal (car declarator) 'declarator)
				  (let* ((name (let* ((name-opt (l.find declarator ':name)))
						 (cond ((equal (car name-opt) 'name)
							(cadr name-opt))
						       ((equal (car name-opt) 'unnamed)
							(error 'no-conversion :text (list 'vars 'must 'have 'names))))))
					 (initializer (l.find declarator ':initializer))
					 (type (convert-decl-type (l.find decl-spec ':decl-type)))
					 (init-exp (if (equal initializer '_)
						       (list 'nondeterministic-expression
							     ':type type
							     ':source-code (find-source-code input-file-string (l.find declaration ':loc)) ;; ""
							     ':loc loc)
						     (coerce-expression-to-expression (convert-expression (l.find (l.find initializer ':initializer-clause) ':expression) names input-file-string))))
					 (ACL2-var (new-var-name))
					 (declaring (list 'declaring ':var (list ACL2-var name) ':type type))
					 (dec-stm (list 'declaration-statement
							':declaring declaring
							':initializer (coerce-expression-to-type init-exp type)
							':source-code (find-source-code input-file-string (l.find declaration ':loc)) 
							':loc loc)))
				    dec-stm))
				 ((equal (car declarator) 'function-declarator)
				  (error 'no-conversion :text (list 'expected 'declarator 'got 'function 'declarator declarator)))))
		       (list.filter #'(lambda (declarator)
					(or (not (equal (car declarator) 'function-declarator))
					    (if forget-functions
						nil
					      (error 'no-conversion :text (list 'expected 'declarator 'got 'function 'declarator declarator)))))
				    (l.find declaration ':declarators)))))
	   (pair.make decs (list.foldr #'(lambda (dec names)
					   (let* ((name-pair (l.find (l.find dec ':declaring) ':var))
						  (name (cadr name-pair)))
					     (map.add-overwrite names name name-pair)))
				       names decs))))
	((equal (car declaration) 'function-declaration)
	 (error 'no-conversion :text (list 'expected 'simpledeclaration 'got 'function 'declarator declarator)))))

;; c-binary-op c-expression c-expression name-map -> [expression (list 'not-an-expression statement) U]
(defun convert-binary-expression (op exp1 exp2 type loc source-code names input-file-string)
  (cond ((equal op 'assign)
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type (l.find lvalue ':type))
		      (expression (coerce-expression-to-type (coerce-expression-to-expression (convert-expression exp2 names input-file-string)) type)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'bin-and) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'bin-and
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-bin-and) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type)
					 ':operator 'bin-and
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'bin-or) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'bin-and
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-bin-or) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type)
					 ':operator 'bin-or
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'bin-xor) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'bin-and
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-bin-xor) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type)
					 ':operator 'bin-xor
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op '==) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '==
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '!=) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '!=
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '>=) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '>=
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '>) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '>
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '<=) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '<=
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '<) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '<
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'log-and) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'log-and
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'log-or) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'log-or
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '+) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '+
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-+) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type)
					 ':operator '+
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op '-) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '-
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign--) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type)
					 ':operator '-
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op '*) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '*
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-*) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type)
					 ':operator '*
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op '/) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '/
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-/) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) 
					 ':operator '/
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'mod) 
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator 'mod
	       ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
	       ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'assign-mod) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp1 names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) 
					 ':operator 'mod
					 ':operand1 (coerce-expression-to-expression (convert-expression exp1 names input-file-string))
					 ':operand2 (coerce-expression-to-expression (convert-expression exp2 names input-file-string))
					 ':source-code "..."
					 ':loc loc)
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op '<<) 
	 (error 'no-conversion :text (list 'bitshift unsupported)))
	((equal op '<<=)
	 (error 'no-conversion :text (list 'bitshift unsupported)))
	((equal op '>>) 
	 (error 'no-conversion :text (list 'bitshift unsupported)))
	((equal op '>>=)
	 (error 'no-conversion :text (list 'bitshift unsupported)))
	(t (error 'no-conversion :text (list 'unknown 'binary 'op op)))))

;; c-unary-op c-exp c-type source-code loc -> [expression (list 'not-an-expression statement) U]
(defun convert-unary-expression (op exp type loc source-code names input-file-string)
  (cond ((equal op 'reference) 
	 (error 'no-conversion :text (list 'reference unsupported)))
	((equal op 'dereference)
	 (error 'no-conversion :text (list 'dereference unsupported)))
	((equal op '-) 
	 #|(list 'unary-expression
	       ':type (convert-type type)
	       ':operator '-
	       ':operand (coerce-expression-to-expression (convert-expression exp names input-file-string))
	       ':source-code source-code
	       ':loc loc))|#
	 (list 'binary-expression
	       ':type (convert-type type)
	       ':operator '-
	       ':operand1 (list 'literal-expression 
				 ':type (convert-type type) 
				 ':loc loc
				 ':source-code ""
				 ':value 0)
	       ':operand2 (coerce-expression-to-expression (convert-expression exp names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op '+) (convert-expression exp names input-file-string))
	((equal op 'log-not)
	 (list 'unary-expression
	       ':type (convert-type type)
	       ':operator 'log-not
	       ':operand (coerce-expression-to-expression (convert-expression exp names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'bin-not) 
	 (list 'unary-expression
	       ':type (convert-type type)
	       ':operator 'bin-not
	       ':operand (coerce-expression-to-expression (convert-expression exp names input-file-string))
	       ':source-code source-code
	       ':loc loc))
	((equal op 'decr-post)
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '-
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names input-file-string))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type) 
							  ':loc loc
							  ':source-code "1"
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'incr-post) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '+
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names input-file-string))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type) 
							  ':loc loc
							  ':source-code "1"
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'decr-pre) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '-
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names input-file-string))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type)
							  ':loc loc
							  ':source-code "1"
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'incr-pre) 
	 (list 'not-an-expression
	       (let* ((lvalue (convert-lvalue exp names input-file-string))
		      (type* (l.find lvalue ':type))
		      (expression (coerce-expression-to-type
				   (list 'binary-expression
					 ':type (convert-type type) ':loc loc
					 ':operator '+
					 ':operand1 (coerce-expression-to-expression (convert-expression exp names input-file-string))
					 ':operand2 (list 'literal-expression 
							  ':type (convert-type type)
							  ':loc loc
							  ':source-code "1"
							  ':value 1)
					 ':source-code "...")
				   type*)))
		 (list (list 'assignment-statement
			     ':lvalue lvalue
			     ':expression expression
			     ':source-code source-code
			     ':loc loc)))))
	((equal op 'parens) 
	 (convert-expression exp names input-file-string))))


;; c-expression -> lvalue
(defun convert-lvalue (exp names input-file-string)
  (cond ((equal (car exp) 'literal-expression)
	 (error 'no-conversion :text (list 'literal 'is 'not 'an 'lvalue)))
	((equal (car exp) 'id-expression)
	 (if (equal (car (l.find exp ':name)) 'name)
	     (list 'var-lvalue
		   ':type (convert-type (l.find exp ':type))
		   ':var (map.find-exn names (cadr (l.find exp ':name)))
		   ':source-code (find-source-code input-file-string (l.find exp ':loc))) 
	   (error 'no-conversion :text (list 'bad 'id 'in 'lvalue exp))))
	((equal (car exp) 'unary-expression)
	 (error 'no-conversion :text (list 'unary 'expression 'not 'lvalue)))
	((equal (car exp) 'binary-expression)
	 (error 'no-conversion :text (list 'binary 'expression 'not 'lvalue)))
	((equal (car exp) 'function-call-expression)
	 (error 'no-conversion :text (list 'function 'call 'not 'lvalue)))
	((equal (car exp) 'compound-statement-expression)
	 (error 'no-conversion :text (list 'compound 'statement 'not 'lvalue)))
	((equal (car exp) 'conditional-expression)
	 (error 'no-conversion :text (list 'conditional 'exp 'not 'lvalue)))
	((equal (car exp) 'array-subscript-expression)
	 (error 'no-conversion :text (list 'array 'subscript 'unsupported 'lvalue)))
	((equal (car exp) 'cast-expression)
	 (error 'no-conversion :text (list 'case 'expression 'not 'lvalue)))))


;; c-expression ? -> [expression (list 'not-an-expression statement) U]
(defun convert-expression (exp names input-file-string)
  (let* ((source-code (find-source-code input-file-string (l.find exp ':loc)))) 
    (cond ((equal (car exp) 'literal-expression)
	   (let* ((value (l.find exp ':value)))
	     (if (not (realp value)) (error 'no-conversion :text (list 'literal 'not 'a 'real value)) ;; TODO
	       (list 'literal-expression
		     ':type (convert-type (l.find exp ':type)) ':loc (l.find exp ':loc)
		     ':value (l.find exp ':value)
		     ':source-code source-code))))

	((equal (car exp) 'id-expression)
	 (let* ((name (l.find exp ':name)))
	   (if (equal (car name) 'name)
	       (list 'id-expression
		     ':type (convert-type (l.find exp ':type)) ':loc (l.find exp ':loc)
		     ':var (map.find-exn names (cadr name))
		     ':source-code source-code)

	     (error 'no-conversion :text (list 'variable 'must 'be 'named 'in 'id 'expression)))))
	((equal (car exp) 'unary-expression)
	 (convert-unary-expression (l.find exp ':operator)
				   (l.find exp ':operand)
				   (l.find exp ':type)
				   (l.find exp ':loc)
				   source-code
				   names input-file-string))
	((equal (car exp) 'binary-expression)
	 (convert-binary-expression (l.find exp ':operator)
				    (l.find exp ':operand1)
				    (l.find exp ':operand2)
				    (l.find exp ':type)
				    (l.find exp ':loc)
				    source-code
				    names input-file-string))
	((equal (car exp) 'function-call-expression)
	 (let* ((func (l.find exp ':function)))
	   (cond ((and (equal (car func) 'id-expression)
		       (equal (car (l.find func ':name)) 'name))
		  (let* ((name (cadr (l.find func ':name)))
			 (loc (l.find exp ':loc)))
		    (cond ((equal name '|__VERIFIER_nondet_uint|)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'uint) ':loc loc
				 ':source-code source-code))
			  ((equal name '|__VERIFIER_nondet_int|)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'int) ':loc loc
				 ':source-code source-code))
			  ((equal name '|__VERIFIER_nondet_bool|)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'bool) ':loc loc
				 ':source-code source-code))
			  ((equal name '|__VERIFIER_nondet_char|)
			   (list 'nondeterministic-expression ':type (list 'basic-type ':kind 'char) ':loc loc
				 ':source-code source-code))

			  ((equal name '|__VERIFIER_assert|)
			   (let* ((assert-exp (coerce-expression-to-type (coerce-expression-to-expression
									  (convert-expression (l.find (car (l.find exp ':arguments)) ':expression)
											      names input-file-string))
									 '(basic-type :kind bool))))
			     (list 'not-an-expression
				   (list (list 'assert-statement ':expression assert-exp ':loc loc
					       ':source-code source-code)))))
			  ((equal name '|__VERIFIER_assume|)
			   (let* ((assume-exp (coerce-expression-to-type (coerce-expression-to-expression
									  (convert-expression (l.find (car (l.find exp ':arguments)) ':expression)
											      names input-file-string))
									 '(basic-type :kind bool))))
			     (list 'not-an-expression
				   (list (list 'assume-statement ':expression assume-exp ':loc loc
					       ':source-code source-code)))
			     (error 'no-conversion)))


			  (t (error 'no-conversion :text (list 'bad 'function 'call exp))))))
		  (t (error 'no-conversion :text (list 'bad 'function 'call exp))))))
	((equal (car exp) 'compound-statement-expression)
	 (list 'not-an-expression
	       (list (list 'compound-statement
			   ':statements 
			   (reverse 
			    (pair.first
			     (list.foldl #'(lambda (stm* acc)
					     (let* ((stms (pair.first acc))
						    (names* (pair.second acc))
						    (res (convert-statement stm* names* input-file-string)))
					       (stm** (coerce-statements-to-statements (pair.first res))
						      (names** (pair.second res)))
					       (pair.make (append stm** stms)
							  names**)))
					 (pair.make () names) (l.find exp ':statements))))
			   ':loc (l.find exp ':loc)
			   ':source-code source-code))))
	((equal (car exp) 'conditional-expression)
	 (list 'conditional-expression
	       ':type (convert-type (l.find exp ':type)) ':loc (l.find exp ':loc)
	       ':test (coerce-expression-to-expression (convert-expression (l.find exp ':test) names input-file-string))
	       ':then (coerce-expression-to-expression (convert-expression (l.find exp ':then) names input-file-string))
	       ':else (coerce-expression-to-expression (convert-expression (l.find exp ':else) names input-file-string))
	       ':source-code source-code))
	((equal (car exp) 'array-subscript-expression)
	 (error 'no-conversion :text (list 'array 'subscript 'expression 'unsupported)))
	((equal (car exp) 'cast-expression)
	 (error 'no-conversion :text (list 'cast 'expression 'unsupported))))))

;; c-statement name-map -> [[[statement list] (list 'not-a-statements expression) U] name-map pair]
(defun convert-statement (stm names input-file-string)
  (let* ((source-code (find-source-code input-file-string (l.find stm ':loc)))) 
  (cond ((equal (car stm) 'compound-statement)
	 (let* ((res (list.foldl #'(lambda (stm* acc)
				     (let* ((stms (pair.first acc))
					    (names* (pair.second acc))
					    (res (convert-statement stm* names* input-file-string))
					    (stm** (coerce-statements-to-statements (pair.first res)))
					    (names** (pair.second res)))
				       (pair.make (append stm** stms)
		 				  names**)))
				 (pair.make () names) (l.find stm ':statements)))
		(statements (reverse (pair.first res)))
		(compound-stm (list 'compound-statement
				    ':statements statements
				    ':loc (l.find stm ':loc)
				    ':source-code source-code)))
	   (pair.make (list compound-stm)
		      names)))
	((equal (car stm) 'return-statement)
	 (let* ((return-exp (if (equal (l.find stm ':expression) '_)
				(list 'literal-expression ':type '(basic-type :kind int) ':value 0 ':loc (l.find stm ':loc)
				      ':source-code source-code)
			      (coerce-expression-to-expression (convert-expression (l.find stm ':expression) names input-file-string))))
		(return-stm (list 'return-statement
				  ':expression return-exp
				  ':loc (l.find stm ':loc)
				  ':source-code source-code)))
	   (pair.make (list return-stm)
		      names)))
	((equal (car stm) 'if-statement)
	 (if (equal (l.find stm ':else) '_)
	     (let* ((test-exp (coerce-expression-to-type
			       (coerce-expression-to-expression
				(convert-expression (l.find stm ':test) names input-file-string))
			       '(basic-type :kind bool)))
		    (then-stm (coerce-statements-to-statements (pair.first (convert-statement (l.find stm ':then) names input-file-string))))
		    (if-then-stm (list 'if-then-statement
				     ':test test-exp
				     ':then (create-statement-from-list then-stm (l.find (l.find stm ':then) ':loc))
				     ':loc (l.find stm ':loc)
				     ':source-code source-code)))
	       (pair.make (list if-then-stm) names))
	   (let* ((test-exp (coerce-expression-to-type
			     (coerce-expression-to-expression
			      (convert-expression (l.find stm ':test) names input-file-string))
			     '(basic-type :kind bool)))
		  (then-stm (coerce-statements-to-statements (pair.first (convert-statement (l.find stm ':then) names input-file-string))))
		  (else-stm (coerce-statements-to-statements (pair.first (convert-statement (l.find stm ':else) names input-file-string))))
		  (if-then-else-stm (list 'if-then-else-statement
				     ':test test-exp
				     ':then (create-statement-from-list then-stm (l.find (l.find stm ':then) ':loc))
				     ':else (create-statement-from-list else-stm (l.find (l.find stm ':else) ':loc))
				     ':loc (l.find stm ':loc)
				     ':source-code source-code)))
	     (pair.make (list if-then-else-stm) names))))
	((equal (car stm) 'while-statement)
	 (let* ((test-exp (coerce-expression-to-type
			   (coerce-expression-to-expression
			    (convert-expression (l.find stm ':test) names input-file-string))
			   '(basic-type :kind bool)))
		(body-stm (coerce-statements-to-statements (pair.first (convert-statement (l.find stm ':body) names input-file-string))))
		(while-stm (list 'while-statement
				 ':test test-exp
				 ':body (create-statement-from-list body-stm (l.find (l.find stm ':body) ':loc))
				 ':loc (l.find stm ':loc)
				 ':source-code source-code)))
	   (pair.make (list while-stm) names)))
	((equal (car stm) 'do-statement)
	 (let* ((test-exp (coerce-expression-to-type
			   (coerce-expression-to-expression
			    (convert-expression (l.find stm ':test) names input-file-string))
			   '(basic-type :kind bool)))
		(body-stm (coerce-statements-to-statements (pair.first (convert-statement (l.find stm ':body) names input-file-string))))
		(do-stm (list 'do-statement
			      ':test test-exp
			      ':body (create-statement-from-list body-stm (l.find (l.find stm ':body) ':loc))
			      ':loc (l.find stm ':loc)
			      ':source-code source-code)))
	   (pair.make (list do-stm) names)))
	((equal (car stm) 'for-statement)
	 (let* ((init-res (convert-statement (l.find stm ':init) names input-file-string))
		(init-stm (coerce-statements-to-statements (pair.first init-res)))
		(names* (pair.second init-res))
		(test-exp (coerce-expression-to-type
			   (coerce-expression-to-expression
			    (convert-expression (l.find stm ':test) names* input-file-string))
			   '(basic-type :kind bool)))
		(iter-stm (coerce-expression-to-statements (convert-expression (l.find stm ':iter) names* input-file-string)))
		(body-stm (coerce-statements-to-statements (pair.first (convert-statement (l.find stm ':body) names* input-file-string))))
		(for-stm (list 'for-statement
			       ':init (create-statement-from-list init-stm (l.find (l.find stm ':init) ':loc))
			       ':test test-exp
			       ':iter (create-statement-from-list iter-stm (l.find (l.find stm ':iter) ':loc))
			       ':body (create-statement-from-list body-stm (l.find (l.find stm ':body) ':loc))
			       ':loc (l.find stm ':loc)
			       ':source-code source-code)))
	   (pair.make (list for-stm) names)))
	((equal (car stm) 'break-statement)
	 (pair.make (list (list 'break-statement
				':loc (l.find stm ':loc)
				':source-code source-code))
		    names))
	((equal (car stm) 'continue-statement)
	 (pair.make (list (list 'continue-statement
				':loc (l.find stm ':loc)
				':source-code source-code))
		    names))
	((equal (car stm) 'default-statement)
	 (error 'no-conversion :text (list 'default 'statement 'unsupported)))
	((equal (car stm) 'null-statement)
	 (pair.make (list (list 'empty-statement ':loc (l.find stm ':loc) ':source-code source-code))
		    names))
	((equal (car stm) 'declaration-statement)
	 (create-declaration-statements (l.find stm ':declaration) (l.find stm ':loc) names input-file-string))
	((equal (car stm) 'expression-statement)
	 (let* ((stm (let* ((res (convert-expression (l.find stm ':expression) names input-file-string)))
		       (if (equal (car res) 'not-an-expression)
			   (coerce-expression-to-statements res)
			 (list 'not-a-statements (coerce-expression-to-expression res))))))
	 (pair.make stm names)))
	((equal (car stm) 'case-statement)
	 (error 'no-conversion :text (list 'case 'statement 'unsupported)))
	((equal (car stm) 'switch-statement)
	 (error 'no-conversion :text (list 'switch 'statement 'unsupported)))
	((equal (car stm) 'goto-statement)
	 (let* ((label (l.find stm ':label))
		(_ (if (not (equal (car label) 'name)) (error 'no-conversion :text (list 'expected 'name 'for 'label stm)) '_)))
	   (pair.make (list (list 'goto-statement ':label (cadr label) ':loc (l.find stm ':loc) ':source-code source-code))
		      names)))
	((equal (car stm) 'label-statement)
	 (let* ((label (l.find stm ':label))
		(_ (if (not (equal (car label) 'name)) (error 'no-conversion :text (list 'expected 'name 'for 'label stm)) '_))
		(label-stm (list 'label-statement ':label (cadr label) ':loc (l.find stm ':loc) ':source-code source-code))
		(other (if (not (equal (l.find stm ':nested) '_))
			   ()
			 ;; does this do scoping correct?
			 (coerce-expresion-to-statements (convert-expression (l.find stm ':nested) names input-file-string)))))
	   (pair.make (cons label-stm other) names))))))

