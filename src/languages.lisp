
#|

The language that is given from the Eclipse C Parser is

[a maybe] = '_ | a

program := (list 'program ':declarations [declaration list])
loc := (list 'loc :start-line int :end-line int :offset int :length int)

declaration := (list 'simple-declaration 
                     ':declarators [declarator list]
                     ':decl-specifier decl-specifier 
                     ':loc loc)
             | (list 'function-definition 
                     ':declarator declarator 
                     ':decl-specifier decl-specifier 
                     ':body statement 
                     ':loc loc)

binary-operator := 'assign 
                 | 'bin-and | 'assign-bin-and | 'bin-or | 'assign-bin-or | 'bin-xor | 'assign-bin-xor 
                 | '== | '!= 
                 | '>= | '> | '<= | '< 
                 | 'log-and | 'log-or 
                 | '+ | 'assign-+ | '- | 'assign-- | '* | 'assign-* | '/ | 'assign-/ 
                 | 'mod | 'assign-mod 
                 | '<< | '<<= | '>> | '>>=

unary-operator := 'reference | 'dereference 
                | '- | '+ 
                | 'log-not | 'bin-not 
                | 'decr-post | 'incr-post | 'decr-pre | 'incr-pre 
                | 'parens ; wrapping an expression in parens is treated as a unary operator

decl-type-base := 'auto | 'bool | 'char | 'int | 'float | 'double | 'void
decl-type := (list 'decl-type
		   ':base decl-type-base
		   ':modifiers [type-modifier list])

type-kind := 'bool | 'char | 'char16 | 'char32 | 'double | 'float | 'int | 'void

type-modifier := 'long | 'long-long | 'short | 'signed | 'unsigned | 'complex | 'imaginary

type-id = (list 'type-id
                ':abstract-declarator declarator
                ':decl-specifier decl-specifier)

type := (list 'basic-type
              :modifier [type-modifier list]
              :kind type-kind)
      | (list 'function-type
              ':input-types [type list]
              ':return-type type)
      | (list 'array-type 
              ':type type) 
      | (list 'pointer-type 
              ':type type) 

name := (list 'name symbolp) ; TODO: watch out for t / nil / other reserved?
      | (list 'unnamed)

literal-kind := 'char-constant
              | 'false-constant
              | 'float-constant
              | 'integer-constant
              | 'string-constant
              | 'true-constant

expression := (list 'literal-expression
                    :type type
                    :kind literal-kind
                    :value TODO
                    :loc loc)
            | (list 'id-expression
                    ':type type
                    ':name name
                    ':loc loc)
            | (list 'unary-expression
                    ':type type
                    ':operator unary-operator
                    ':operand expression
                    ':loc loc)
            | (list 'binary-expression
                    ':type type
                    ':operator binary-operator
                    ':operand1 expression
                    ':operand2 expression
                    ':loc loc)
            | (list 'function-call-expression
                    ':type type
                    ':function expression
                    ':arguments [expression list]
                    ':loc loc)
            | (list 'compound-statement-expression
                    ':type type
                    ':statements [statement list]
                    ':loc loc)
            | (list 'conditional-expression
                    ':type type
                    ':test expression
                    ':then expression
                    ':else expression
                    ':loc loc)
            | (list 'array-subscript-expression
                    ':type type
                    ':array expression
                    ':subscript [expression maybe]
                    ':arg [expression maybe]
                    ':loc loc)
            | (list 'cast-expression
                    ':type type
                    ':operator type-id
                    ':operand expression
                    ':loc loc)

statement := (list 'compound-statement
                   ':statements [statement list]
                   ':loc loc)
           | (list 'return-statement
                   ':expression [expression maybe]
                   ':loc loc)
           | (list 'if-statement
                   ':test expression
                   ':then statement
                   ':else [statement maybe]
                   ':loc loc)
           | (list 'while-statement
                   ':test expression
                   ':body statement
                   ':loc loc)
           | (list 'do-statement
                   ':test expression
                   ':body statement
                   ':loc loc)
           | (list 'for-statement
                   ':init statement
                   ':test expression
                   ':iter expression
                   ':body statement
                   ':loc loc)
           | (list 'break-statement
                   ':loc loc)
           | (list 'continue-statement
                   ':loc loc)
           | (list 'default-statement
                   ':loc loc)
           | (list 'null-statement
                   ':loc loc)
           | (list 'goto-statement
                   ':label name
                   ':loc loc)
           | (list 'declaration-statement
                   ':declaration declaration
                   ':loc loc)
           | (list 'expression-statement
                   ':expression expression
                   ':loc loc)
           | (list 'case-statement
                   ':expression expression
                   ':loc loc)
           | (list 'switch-statement
                   ':controller-expression expression
                   ':body statement
                   ':loc loc)
           | (list 'label-statement
                   ':label name
                   ':nested [expression maybe]
                   ':loc loc)
                    
initializer-clause := (list 'initializer-clause-expression 
                            ':expression expression
                            ':loc loc)

initializer := (list 'initializer
                     ':initializer-clause initializer-clause
                     ':loc loc)

declarator := (list 'function-declarator
                    ':name name
                    ':parameters [parameter-declaration list]
                    ':initializer [initializer maybe]
                    ':loc loc)
            | (list 'declarator
                    ':name name
                    ':initializer [initializer maybe]
                    ':loc loc)

parameter-declaration := (list 'parameter-declaration 
                               ':declarator declarator
                               ':decl-specifier decl-specifier
                               ':loc loc)

decl-specifier := (list 'decl-specifier 
                        ':decl-type decl-type
                        ':loc loc)

|#


#|

Future:
program := (list 'program ':declarations [declaration list]) 
declaration := (list 'simple-declaration 
                     ':declaring declaring
                     ':initializer expression
                     ':loc loc)
             | (list 'function-declaration
		     ':name name
                     ':parameters [declaring list]
                     ':return-type type
                     ':body statement 
                     ':loc loc)
expression 
| (list 'function-call-expression
        ':type type
        ':function name
        ':arguments [expression list]
        ':loc loc)
| (list 'compound-statement-expression
        ':type type
        ':statements [statement list]
        ':loc loc)
| (list 'array-subscript-expression
        ':type type
        ':array expression
        ':subscript expression
        ':loc loc)
| (list 'cast-expression
        ':type type
        ':operator type-id
        ':operand expression
        ':loc loc)

lvalue
| (list 'array-lvalue 
        ':lvalue array
        ':subscript expression)

statement
| (list 'expression-statement
        ':expression expression
        ':loc loc)

type
| (list 'array-type 
        ':type type) 




The IR language:

program := statement

loc := (list 'loc :start-line int :end-line int :offset int :length int)

declaring := (list 'declaring
		   ':var var
		   ':type type)

binary-operator := '== | '!= 
                 | '>= | '> | '<= | '< 
                 | 'log-and | 'log-or 
                 | 'bin-and | 'bin-or | 'bin-xor
                 | '+ | '- | '* | '/ 
                 | 'unsigned-+ | 'unsigned-- | 'unsigned-* | 'unsigned-/ 
                 | 'mod 

unary-operator := '- | 'log-not | 'bin-not

base-type := 'bool | 'char | 'int | 'uint

type := (list 'basic-type
              :kind base-type)

literal-kind := 'char-constant
              | 'false-constant
              | 'float-constant
              | 'integer-constant
              | 'string-constant
              | 'true-constant

expression := (list 'literal-expression
                    :type type
                    :value value
                    :loc loc)
            | (list 'id-expression
                    ':type type
                    ':var var
                    ':loc loc)
            | (list 'unary-expression
                    ':type type
                    ':operator unary-operator
                    ':operand expression
                    ':loc loc)
            | (list 'binary-expression
                    ':type type
                    ':operator binary-operator
                    ':operand1 expression
                    ':operand2 expression
                    ':loc loc)
	    | (list 'nondeterministic
                    ':type type
                    ':loc loc) 
            | (list 'conditional-expression
                    ':type type
                    ':test expression
                    ':then expression
                    ':else expression
                    ':loc loc)

statement := (list 'compound-statement
                   ':statements [statement list]
                   ':loc loc ':source-code source-code)
           | (list 'if-then-statement
                   ':test expression
                   ':then statement
                   ':loc loc ':source-code source-code)
           | (list 'if-then-else-statement
                   ':test expression
                   ':then statement
                   ':else statement
                   ':loc loc ':source-code source-code)
           | (list 'while-statement
                   ':test expression
                   ':body statement
                   ':loc loc ':source-code source-code)
           | (list 'do-statement
                   ':test expression
                   ':body statement
                   ':loc loc ':source-code source-code)
           | (list 'for-statement
                   ':init statement
                   ':test expression
                   ':iter statement
                   ':body statement
                   ':loc loc ':source-code source-code)
           | (list 'empty-statement
                   ':loc loc ':source-code source-code)
           | (list 'break-statement
                   ':loc loc ':source-code source-code)
           | (list 'continue-statement
                   ':loc loc ':source-code source-code)
           | (list 'declaration-statement
                   ':declaring declaring
		   ':initializer expression
                   ':loc loc ':source-code source-code)
           | (list 'assignment-statement
                   ':lvalue lvalue
                   ':expression expression
                   ':loc loc ':source-code source-code)
           | (list 'assert-statement
                   ':expression expression
                   ':loc loc ':source-code source-code)
           | (list 'goto-statement
                   ':label name
                   ':loc loc)
           | (list 'label-statement
                   ':label name
                   ':loc loc)

lvalue := (list 'var-lvalue 
                ':type type
                ':var var)

|#
