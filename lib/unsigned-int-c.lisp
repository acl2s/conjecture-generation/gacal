#|

 A book for reasoning about C unsigned integers.

 Only Pete should modify this file. 

 If you need to make changes, run them by me before doing so.
 
 You should be able to get what you want by loading this book and
 putting whatever wrapper you want around it. To make your life easy,
 I defined macros that provide the names you want, e.g.:

 (defabbrev c-uint-+ (x y) (c+ x y))

 Notes: I am using the current ACL2s default arithmetic theory, which
 was tuned based on this book, but it may be worth examining which of
 the expensive arithmetic rules make sense to enable/disable and more
 generally to come up with a good rewrite strategy, which is a hard
 proof engineering project.

 TODOS: Modify this so that we can provide a word size argument.  This
 will be needed for unsigned arithmetic over different word sizes,
 which will be useful for really reasoning about C programs.

|#

(in-package "ACL2S")

#|
 
 We are using a fixed word size, but we could use the utility for
 generating type-constrained constants to make *word-size* an element
 from some domain, while using a specific value for cgen. That is nice
 because it gives us flexibility. Eg, using small values may make
 testing faster & more likely to find counterexamples.

|#

(defconst *word-size* 32)
(defconst *c-umax* (expt 2 *word-size*))
(defconst *c-uint-max* *c-umax*)

(defnatrange c-uint *c-umax*)
(defintrange c-puint 1 *c-umax*)
(defdata-subtype-strict c-puint c-uint)

#|

 Previous definition.

 (definec cfix (i :int) :c-uint
   (mod i *c-umax*))

 The strategy here is to use cfix as much as possible. Cfix is really
 a helper function that is used to define unsigned arithmetic
 functions in a way that allows us to reason about them using a
 rewrite strategy that keeps cfix disabled and tries to get remove as
 many cfix's in a term as possible. We keep cfix disabled to avoid
 reasoning about mod, which may be expensive and may go off the
 rails. Most of the reasoning is done via the cfix theorems below and
 we only expand cfix when no more simplification is possible. By
 removing cfix's, we are left with mostly regular arithmetic terms
 which are handled by ACL2S and the libraries it includes.

 Notice that the input contract for cfix allows anything. That allows
 us to define theorems with less hypotheses than would otherwise be
 the case, which leads to proof speedups. The input contracts we need
 are still there for the top-level functions. See below.

|#

(definec cfix (i :all) :c-uint
  (mod (ifix i) *c-umax*))

(defthm cfix-nump
  (acl2-numberp (cfix x))
  :rule-classes :type-prescription)

(defthm cfix-natp
  (natp (cfix x))
  :rule-classes :type-prescription)

(local
 (encapsulate
  ()

  (defthm cfix-cfix
    (equal (cfix (cfix x))
           (cfix (ifix (double-rewrite x)))))

  (defthm cfix-c-uint
    (implies (c-uintp x)
	     (equal (cfix x) x))
    :rule-classes ((:rewrite :backchain-limit-lst 0)
		   (:forward-chaining :trigger-terms ((cfix x)))))

  (defthm cfix-+-cfix
    (equal (cfix (+ x (cfix y)))
	   (cfix (+ x (ifix (double-rewrite y))))))

  (defthm cfix-+-cfix2
    (equal (cfix (+ (cfix x) y))
	   (cfix (+ y (ifix (double-rewrite x))))))

  (defthm cfix-*-cfix
    (implies (intp x)
	     (equal (cfix (* x (cfix y)))
		    (cfix (* x (ifix (double-rewrite y))))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

  (defthm cfix-*-cfix2
    (implies (intp y)
	     (equal (cfix (* (cfix x) y))
		    (cfix (* y (ifix (double-rewrite x))))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

  (defthm cfix---cfix
    (equal (cfix (- x (cfix y)))
	   (cfix (- x (ifix (double-rewrite y))))))

  (defthm cfix---cfix2
    (equal (cfix (- (cfix x) y))
	   (cfix (- (ifix (double-rewrite x)) y))))

  (defthm cfix-/-cfix
    (implies (and (c-uintp x)
		  (posp y))
	     (equal (cfix (floor x y))
		    (floor x y)))
    :rule-classes ((:rewrite :backchain-limit-lst 1)))))

(in-theory (disable c-uintp))
(in-theory (disable cfix-definition-rule))

#|
 
 The stage-rule utility is new in ACL2s and provides a stage-like
 utility that supports definec/defunc functions.

|#

(add-default-hints!
 '((stage-rule cfix cfix-definition-rule 500))
 :at-end t)

(definec c+-bin (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (cfix (+ a b)))

(definec c*-bin (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (cfix (* a b)))

(make-n-ary-macro c+ c+-bin 0 t)

(make-n-ary-macro c* c*-bin 1 t)

(definec c- (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (cfix (- a b)))

#|

Since / with 0 is undefined, we decided to make it return 0


(definec c/ (a :c-uint b :c-puint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (floor a b))

|#

(definec c/ (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (if (= b 0) 
      0
    (floor a b)))

#|

Since mod with 0 is undefined, we decided to make it return 0


(definec c-mod (a :c-uint b :c-puint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (mod a b))

|#

(definec c-mod (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (if (= b 0) 
      0
    (mod a b)))

;;(defabbrev c-uint-+ (x y) (c+ x y))
;;(defabbrev c-uint-* (x y) (c* x y))
;;(defabbrev c-uint-- (x y) (c- x y))
;;(defabbrev c-uint-/ (x y) (c/ x y))

(definec c-uint-+ (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (c+ a b))

(definec c-uint-* (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (c* a b))

(definec c-uint-- (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (c- a b))

(definec c-uint-/ (a :c-uint b :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (c/ a b))

#|
 
 Using cfix, the definition of the unsigned arithmetic operations is
 easy. Notice that these functions are defined in a way that we expect
 is consistent with C semantics, e.g., c/ is not defined when the
 second argument is 0. So, contract checking is enough to establish
 that what the functions return is deterministic and consistent with
 the C specification, modulo the word size.

|#

(local
 (encapsulate
  ()
 
; This will not fire, unless we turn off forcing, due to
; cfix-plus-is-zero-int-conditional

  (defthm cfix-plus-is-zero-int-unconditional
    (equal (equal (cfix (+ a b)) b)
	   (or (and (equal b 0)
		    (not (intp a)))
	       (and (c-uintp b)
		    (intp (+ a b))
		    (equal (cfix a) 0)))))

  (defthm cfix-plus-is-zero-int-conditional
    (implies (intp a)
	     (equal (equal (cfix (+ a b)) b)
		    (and (c-uintp b)
		         (equal (cfix a) 0))))
    :rule-classes ((:rewrite :backchain-limit-lst 0)))

  (defthm cfix-plus-is-zero-c-uint
    (implies (c-uintp a)
	     (equal (equal (cfix (+ a b)) b)
		    (and (equal a 0)
		         (c-uintp b))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

  (defthm c-uintp-neg-zero-unconditional
    (equal (equal (cfix (- b)) 0)
	   (equal (cfix b) 0)))

  (defthm c-uintp-neg-zero
    (implies (c-uintp b)
	     (equal (equal (cfix (- b)) 0)
		    (equal b 0)))
    :rule-classes ((:rewrite :backchain-limit-lst 0)))

  (defthm cfix-neg-to-pos
    (implies (identity t)
	     (equal (equal (cfix (- x)) (cfix y))
		    (equal (cfix x) (cfix (- y)))))
    :rule-classes ((:rewrite :loop-stopper ())))
; without the loop-stopper, this rule never gets applied
; and without the trivial hypothesis it is used in preprocessing and
; leads to loops.

  (defthm cfix-plus-cancel
    (implies (and (intp a)
                  (intp b)
                  (intp c))
	     (equal (equal (cfix (+ a c))
			   (cfix (+ b c)))
		    (equal (cfix a) (cfix b))))
    :rule-classes ((:rewrite :backchain-limit-lst 0)))))

#|

 We want to control the order in which theorems are used.

|#

(defun simplify-nested-cfix-terms-fn (x)
  ;; If x has subexpressions of the form (cfix ...), replace all of
  ;; these with (ifix ...).
  (let ((simp-cfix (subst-fun-sym 'ifix 'cfix x)))
    (if (equal x simp-cfix)
        nil
      (list (cons 'simp-cfix simp-cfix)))))

; I think this is OK, but we need to test it.
(skip-proofs
 (defthm simplify-nested-cfix-terms
   (implies (bind-free (simplify-nested-cfix-terms-fn x)
                       (simp-cfix))
            (equal (cfix x) (cfix simp-cfix)))))

#|

 The above theorem avoids us writing theorem like this one.
 (defthm cfix-++-cfix
   (equal (cfix (+ x y (cfix z)))
          (cfix (+ x y (ifix z))))
   :hints (("goal"
            :use ((:instance cfix-+-cfix (x (+ x y)) (y z))))))

|#


#|

 floor related rules ordered

|#

(defthm floor-expt-linear
  (implies
   (and (c-uintp y)
        (c-uintp x)
        (< y (floor x y)))
   (< (expt y 2) *c-umax*))
  :rule-classes ((:linear :match-free :all)))

(defthm floor-expt-linear-gen 
  (implies
   (and (natp y)
        (< y (floor x y))
        (< x z))
   (< (expt y 2) z))
  :rule-classes ((:linear :match-free :all)))

(defthm floor-expt-c-uintp
  (implies
   (and (c-uintp y)
        (c-uintp x)
        (< y (floor x y)))
   (c-uintp (expt y 2)))
  :rule-classes ((:type-prescription :backchain-limit-lst 0)))

(defthm floor-expt-posp
  (implies
   (and (c-uintp y)
        (c-uintp x)
        (< y (floor x y)))
   (posp (expt y 2)))
  :rule-classes ((:type-prescription :backchain-limit-lst 0)))

(defthm floor-expt-c-puintp
  (implies
   (and (c-uintp y)
        (c-uintp x)
        (< y (floor x y)))
   (c-puintp (expt y 2)))
  :rule-classes ((:type-prescription :backchain-limit-lst 0)))


#|

 c-mod theorems ordered

|#

(in-theory (disable ACL2::|(mod x 2)|))

(defthm mod-on-2
  (implies (and (intp y)
                (equal (mod x 2) (mod y 2)))
           (equal (mod (cfix (+ x y)) 2) 0))
  :rule-classes ((:rewrite :backchain-limit-lst 1)))

(defthm c-mod-on-2-with-plus-1
  (implies
   (and (intp x)
        (equal (mod x 2) (mod y 2)))
   (equal (equal (mod (cfix (+ a x)) 2)
                 (mod (cfix (+ a y)) 2))
          t))
  :rule-classes ((:rewrite :backchain-limit-lst 1)))

(defthm c-mod-on-2-with-plus-2
  (implies
   (and (intp x)
        (equal (mod x 2) (mod y 2)))
   (equal (equal (mod (cfix (+ x a)) 2)
                 (mod (cfix (+ y a)) 2))
          t))
  :rule-classes ((:rewrite :backchain-limit-lst 1)))

#|
 
 c/ theorems ordered

|#

(defthm cfix-/-cfix
  (implies (and (c-uintp x)
                (posp y))
	   (equal (cfix (floor x y))
		  (floor x y)))
  :rule-classes ((:rewrite :backchain-limit-lst 1)))

#|
 
 c* theorems ordered

|#

(defthm cfix-*-cfix2
  (implies (intp y)
	   (equal (cfix (* (cfix x) y))
		  (cfix (* y (ifix (double-rewrite x))))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

(defthm cfix-*-cfix
  (implies (intp x)
	   (equal (cfix (* x (cfix y)))
		  (cfix (* x (ifix (double-rewrite y))))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

#|
 
 c- theorems ordered

|#

(defthm cfix-neg-to-pos
  (implies (identity t)
	   (equal (equal (cfix (- x)) (cfix y))
		  (equal (cfix x) (cfix (- y)))))
  :rule-classes ((:rewrite :loop-stopper ())))

(defthm c-uintp-neg-zero
  (implies (c-uintp b)
	   (equal (equal (cfix (- b)) 0)
		  (equal b 0)))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

(defthm c-uintp-neg-zero-unconditional
  (equal (equal (cfix (- b)) 0)
	 (equal (cfix b) 0)))

(defthm cfix---cfix2
  (equal (cfix (- (cfix x) y))
	 (cfix (- (ifix (double-rewrite x)) y))))

(defthm cfix---cfix
  (equal (cfix (- x (cfix y)))
	 (cfix (- x (ifix (double-rewrite y))))))


#|
 
 c+ theorems ordered

|#

(defthm cfix-plus-cancel
  (implies (and (intp a)
                (intp b)
                (intp c))
	   (equal (equal (cfix (+ a c))
			 (cfix (+ b c)))
		  (equal (cfix a) (cfix b))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

(defthm cfix-plus-is-zero-int-unconditional
  (equal (equal (cfix (+ a b)) b)
	 (or (and (equal b 0)
		  (not (intp a)))
	     (and (c-uintp b)
		  (intp (+ a b))
		  (equal (cfix a) 0)))))

(defthm cfix-plus-is-zero-int-conditional
  (implies (intp a)
	   (equal (equal (cfix (+ a b)) b)
		  (and (c-uintp b)
		       (equal (cfix a) 0))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

(defthm cfix-plus-is-zero-c-uint
  (implies (c-uintp a)
	   (equal (equal (cfix (+ a b)) b)
		  (and (equal a 0)
		       (c-uintp b))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

(defthm cfix-+-cfix2
  (equal (cfix (+ (cfix x) y))
	 (cfix (+ y (ifix (double-rewrite x))))))

(defthm cfix-+-cfix
  (equal (cfix (+ x (cfix y)))
	 (cfix (+ x (ifix (double-rewrite y))))))

#|

 cfix theorems

|#

(defthm cfix-in-range
  (implies (and (<= 0 x)
                (< x *c-umax*))
           (equal (cfix x)
                  (ifix (double-rewrite x))))
  :rule-classes ((:rewrite :backchain-limit-lst 0)))

(defthm cfix-cfix
  (equal (cfix (cfix x))
         (cfix (ifix (double-rewrite x)))))

(defthm cfix-c-uint
  (implies (c-uintp x)
	   (equal (cfix x) x))
  :rule-classes ((:rewrite :backchain-limit-lst 0)
		 (:forward-chaining :trigger-terms ((cfix x)))))

#|

 Theory adjustments

|#

(in-theory 
 #!acl2(enable reduce-additive-constant-<))

