(in-package "ACL2S")

(include-book "c-operations")

(in-theory (disable acl2::mod-cancel-*-const))

(defdata listof-c-uint (listof c-uint))
(defdata listof-c-int (listof c-int))
(defdata listof-c-bool (listof c-bool))
(defdata listof-c-char (listof c-char))
(defdata nondet (list listof-c-uint
		      listof-c-int
		      listof-c-bool
		      listof-c-char))

; definecd disables definitions

(definecd nondet-c-uint (nondet :nondet counter :nat) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (if (< counter (length (car nondet)))
      (nth counter (car nondet))
    (mod (* counter 47) 107)))

(definecd nondet-c-int (nondet :nondet counter :nat) :c-int
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (if (< counter (length (cadr nondet)))
      (nth counter (cadr nondet))
    (- (mod (* counter 91) 103) 60)))

(definecd nondet-c-bool (nondet :nondet counter :nat) :c-bool
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (if (< counter (length (caddr nondet)))
      (nth counter (caddr nondet))
    (equal (mod counter 2) 0)))

(definecd nondet-c-char (nondet :nondet counter :nat) :c-char
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (if (< counter (length (cadddr nondet)))
      (nth counter (cadddr nondet))
    #\R ; doesn't come up often, so don't need anything complex here?
    ))

(in-theory (disable acl2::mod-cancel-*-const))

(property ; provided by ben
  (implies
   (and (nondetp nondet) (natp counter) (c-uintp |var11|) (c-uintp |var9|) (c-uintp |var10|)
	(< |var11| 268435455)
	(equal (c-uint-+ |var9| |var9|) |var10|)
	(equal (c-uint-+ |var11| |var9|) (c-uint-+ |var10| |var10|))
	(equal (c-uint-+ |var10| |var9|) |var11|))
   (let ((|var9| (c-uint-+ |var9| 1)))
     (let ((|var10| (c-uint-+ |var10| 2)))
       (let ((|var11| (c-uint-+ |var11| 3)))
	 (equal (c-uint-+ |var10| |var9|) |var11|))))))
