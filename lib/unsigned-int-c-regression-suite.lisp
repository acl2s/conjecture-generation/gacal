(in-package "ACL2S")
(include-book "unsigned-int-c")

#|

 This is a collection of properties that we should be able to
 prove. Some of these came from verification examples and provide a
 nice regression suite.

 Add more must-fail examples to make sure the skip-proofs form is 
 OK.

|#

(property ; c+-commutative
  (implies (and (c-uintp x)
		(c-uintp y))
	   (equal (c+ x y) (c+ y x))))

(property ; c+-associative
  (implies (and (c-uintp x)
		(c-uintp y)
		(c-uintp z))
	   (equal (c+ x (c+ y z))
		  (c+ (c+ x y) z))))

(property ; c*-commutative
  (implies (and (c-uintp x)
		(c-uintp y))
	   (equal (c* x y) (c* y x))))

(property ; c*-associative
  (implies (and (c-uintp x)
		(c-uintp y)
		(c-uintp z))
	   (equal (c* x (c* y z))
		  (c* (c* x y) z))))

(property ; example from Ben
  (implies (and (c-uintp |var8|)
		(c-uintp |var7|)
		(equal (c+ 1 |var8|) |var7|))
	   (equal (c+ 1 (c+ |var7| |var8|))
		  (c+ |var7| |var7|))))

(property ; c+-c+
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c+ x (c+ y z))
		  (cfix (+ x y z)))))

(property ; c+-c*
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c+ x (c* y z))
		  (cfix (+ x (* y z))))))

(property ; c+-c-
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c+ x (c- y z))
		  (cfix (+ x (- y z))))))

(property ; c*-c+
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c* x (c+ y z))
		  (cfix (* x (+ y z))))))

(property ; c*-c*
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c* x (c* y z))
		  (cfix (* x y z)))))

(property ; c*-c-
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c* x (c- y z))
		  (cfix (* x (- y z))))))

(property ; c--c+
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c- x (c+ y z))
		  (cfix (- x (+ y z))))))

(property ; c--c*
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c- x (c* y z))
		  (cfix (- x (* y z))))))

(property  ; c--c-
  (implies (and (c-uintp x) (c-uintp y) (c-uintp z))
	   (equal (c- x (c- y z))
		  (cfix (- x (- y z))))))

(property ; c+ is + when ...
  (implies (and (natp x) (natp y)
		(< x (/ *c-umax* 2))
		(< y (/ *c-umax* 2)))
	   (equal (c+ x y)
		  (+ x y))))

(property ; c* is * when ...
  (implies (and (natp x) (natp y)
		(< x (expt 2 (/ *word-size* 2)))
		(< y (expt 2 (/ *word-size* 2))))
	   (equal (c* x y)
		  (* x y))))

(property ; c- is - when ...
  (implies (and (c-uintp x) (c-uintp y)
		(< y x))
	   (equal (c- x y)
		  (- x y))))

(property ; c/ is / when ...
  (implies (and (c-uintp x) (c-puintp y)
		(natp (/ x y)))
	   (equal (c/ x y)
		  (/ x y))))

(property ; c/ is <= / 
  (implies (and (c-uintp x) (c-puintp y))
	   (<= (c/ x y)
	       (/ x y))))

(property ;
 (implies (and (natp a) (c-puintp b))
	  (equal (cfix (c-mod a b))
		 (c-mod a b))))
		 
(property ;
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
	       (equal (c+ a c) (c+ b c)))
	  (equal a b)))

(property ;
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
	       (equal (c+ c a) (c+ c b)))
	  (equal a b)))

(property ;
 (implies (and (c-uintp a) (c-uintp b)
	       (< a b))
	  (<= (c+ a 1) b)))

(property ;
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
	       (equal (c- c a) (c- c b)))
	  (equal a b)))

(property ;
 (implies (and (c-uintp a) (c-uintp b)
	       (> a b))
	  (>= (c- a 1) b)))

(property
 (implies (and (intp x) (intp y))
	  (equal (equal (cfix (- x)) (cfix (- y)))
		 (equal (cfix x) (cfix y)))))

(property
  (implies (and (intp a) (intp b) (intp c))
	   (equal (equal (cfix (+ a (- c)))
			 (cfix (+ b (- c))))
		  (equal (cfix a) (cfix b)))))

(property
  (implies (and (intp a) (intp b) (intp c))
	   (equal (equal (cfix (+ c (- a)))
			 (cfix (+ c (- b))))
		  (equal (cfix a) (cfix b)))))

(property
  (implies (and (intp a) (intp b) (intp c))
	   (equal (equal (cfix (+ c (- a)))
			 (cfix (+ c (- b))))
		  (equal (cfix a) (cfix b)))))

(property
  (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
		(equal (c- a c) 
		       (c- b c)))
	   (equal a b)))

(property ; c-uintp-lemma
  (implies (and (c-uintp b)
		(equal (cfix (+ (- b) c)) c))
	   (equal b 0)))

(property ;
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
	       (equal (c- c a) (c- c b)))
	  (equal a b)))


(property ;
  (implies (and (intp x) (intp y))
	   (equal (equal (cfix (- x)) (cfix (- y)))
		  (equal (cfix x) (cfix y)))))

(property ;
 (implies (and (c-uintp a) (c-uintp b))
	  (implies (< a b)
		   (<= (c+ a 1) b))))

(property ;
 (implies (and (c-uintp a) (c-uintp b))
	  (implies (> a b)
		   (>= (c- a 1) b))))

(property ; moving c-uint- to the other side of an equality
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
	       (equal (c- a b) c))
	  (equal a (c+ b c))))

(property ; moving c-uint- to the other side of an equality
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c)
	       (equal c (c- a b)))
	  (equal (c+ b c) a)))

(property ;
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c))
	  (implies (equal (c+ a c) (c+ b c))
		   (equal a b))))

(property ; 
 (implies (and (c-uintp a) (c-uintp b) (c-uintp c))
	  (implies (equal (c- a c) (c- b c))
		   (equal a b))))

(must-fail
 (thm (equal (cfix (+ x y (cfix z)))
             (cfix (+ x y z)))))

(property
  (implies (intp z)
           (equal (cfix (+ x y (cfix z)))
                  (cfix (+ x y z)))))

(property ; provided by ben
  (implies (and (c-uintp |var5|)
                (c-uintp |var6|)
                (equal (c-uint-+ |var5| |var6|) 0))
	   (equal |var5| (c-uint-- 0 |var6|)))
  )

(property ;provided by ben 
  (implies (and (c-uintp |var5|) (c-uintp |var6|) 0
	        (equal (c-uint-+ |var5| |var6|) 0))
	   (equal |var5| (c-uint-- 0 |var6|)))
  )

(property ;provided by ben
  (implies (and (c-uintp |var5|) (c-uintp |var6|) 0
	        (equal (c-uint-+ |var5| |var6|) 0))
	   (equal |var5| (cfix (- |var6|))))
  )


