(in-package "ACL2S")

; Unsigned integer arithmetic

(include-book "unsigned-int-c")


#|
 
 Here are the definitions for unsigned arithmetic functions. See the
 above book.

 (definec c+ (a :c-uint b :c-uint) :c-uint
   (cfix (+ a b)))

 (definec c* (a :c-uint b :c-uint) :c-uint
   (cfix (* a b)))

 (definec c- (a :c-uint b :c-uint) :c-uint
   (cfix (- a b)))

 (definec c/ (a :c-uint b :c-puint) :c-uint
   (floor a b))

 (definec c-mod (a :c-uint b :c-puint) :c-uint
   (mod a b))

 (defabbrev c-uint-+ (x y) (c+ x y))
 (defabbrev c-uint-* (x y) (c* x y))
 (defabbrev c-uint-- (x y) (c- x y))
 (defabbrev c-uint-/ (x y) (c/ x y))

|#

; Signed integer arithmetic

(defdata-alias c-int integer)
(defdata-alias c-non-0-int non-0-integer)

(definec c-int-+ (x :c-int y :c-int) :c-int
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (+ x y))

(definec c-int-- (x :c-int y :c-int) :c-int
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (- x y))

(definec c-int-* (x :c-int y :c-int) :c-int
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (* x y))

(definec c-int-/ (x :c-int y :c-non-0-int) :c-int
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (floor x y))

(definec c-int-mod (x :c-int y :c-non-0-int) :c-int
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (mod x y))

(definec c-uint-bin-and (x :c-uint y :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (logand x y))

(definec c-uint-bin-or (x :c-uint y :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (logior x y))

(definec c-uint-bin-xor (x :c-uint y :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (logxor x y))

(definec c-uint-bin-not (x :c-uint) :c-uint
  :force-ic-hyps-in-definitionp nil
  :force-ic-hyps-in-contract-thmp nil
  (mod (lognot x) *c-uint-max*))

; want: (thm (implies (natp x) (= 5 (logand 5 (* 8 x)))))
; or rather: (thm (implies (c-uintp x) (equal 5 (c-uint-bin-and 5 (c-uint-+ (c-uint-* 8 x))))))



(defdata c-bool boolean)



(defdata c-char character)

(property
 (implies (and (c-uintp a)
	       (c-puintp b)
	       (c-uintp c)
	       (equal (c-int-mod a b) c))
	  (equal (c-int-mod c b) c)))










(defdata c-array-uint (listof c-uint))
(defdata c-array-int (listof c-int))
(defdata c-array-bool (listof c-bool))
(defdata c-array-char (listof c-char))

(definec c-array-uint-access (index :int arr :c-array-uint) :c-uint
  (if (and (<= 0 index) (< index (length arr)))
      (nth index arr)
    0))

(definec c-array-uint-update (index :int d :c-uint arr :c-array-uint) :c-array-uint
  (if (and (<= 0 index) (< index (length arr)))
      (update-nth index d arr)
    arr))

(definec c-array-int-access (index :int arr :c-array-int) :c-int
  (if (and (<= 0 index) (< index (length arr)))
      (nth index arr)
    0))

(definec c-array-int-update (index :int d :c-int arr :c-array-int) :c-array-int
  (if (and (<= 0 index) (< index (length arr)))
      (update-nth index d arr)
    arr))

(definec c-array-bool-access (index :int arr :c-array-bool) :c-bool
  (if (and (<= 0 index) (< index (length arr)))
      (nth index arr)
    t))

(definec c-array-bool-update (index :int d :c-bool arr :c-array-bool) :c-array-bool
  (if (and (<= 0 index) (< index (length arr)))
      (update-nth index d arr)
    arr))

(definec c-array-char-access (index :int arr :c-array-char) :c-char
  (if (and (<= 0 index) (< index (length arr)))
      (nth index arr)
    #\R))

(definec c-array-char-update (index :int d :c-char arr :c-array-char) :c-array-char
  (if (and (<= 0 index) (< index (length arr)))
      (update-nth index d arr)
    arr))

