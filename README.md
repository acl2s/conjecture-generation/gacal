
GACAL version 1.1

Instructions for SV-COMP 2020
=============================

0. Install the dependencies:

       apt-get install cmake git make python sbcl ant 

1. Obtain and install ACL2 and ACL2s. This can be done via

       ./scripts/install.sh 

2. Use benchexec tool to run the tests or use `run-gacal.py`
   The `--witness` option must be included.

   You can use the `run-gacal.py` script to verify each single test-case.

       run-gacal.py --witness WITNESS TASK

   WITNESS:      Path to a file, where the witness trace in XML will be written
   
   TASK:         Path to a C program to be verified

    
   For example,

       ./run-gacal.py --witness "test/test-program-1.graphml" "test/test-program-1.i"

       ./run-gacal.py --witness "test/test-program-2.graphml" "test/test-program-2.i"

---------------------------------------------

Instructions for building/testing:

0. Install the dependencies:

       apt-get install cmake git make python sbcl ant 

1. Obtain and install ACL2 and ACL2s. This can be done via

       ./scripts/install.sh 

2. Build the parser:

       (cd parser; ant)

3. Certify the libraries

       (cd lib; cert.pl "top.lisp")

4. Install submodule dependencies for testing

       git submodule init
       git submodule update

5. Run GACAL

       ./run-gacal.py --witness "test/test-program-1.graphml" "test/test-program-1.i"

   OR Run the tests

       ./run-tests.py

   with validation of results:

       ./run-tests.py --validate

   with parallelization

       ./run-tests.py --parallel

   with violation problems

       ./run-tests.py --do-violation

   these flags may be combined in any way