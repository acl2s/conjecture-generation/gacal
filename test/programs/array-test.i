extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

unsigned int __VERIFIER_nondet_uint();
int main() {
  unsigned int SIZE = __VERIFIER_nondet_uint();
  int arr[SIZE];
  for (int i=0; i<SIZE; i++) {
    arr[i] = i;
  }

  for (int i=0; i<SIZE; i++) {
    __VERIFIER_assert(arr[i] == i);
  }
  return 0;
}
