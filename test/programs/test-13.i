extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  int i = 0;
  while (i == 0) {
    if (__VERIFIER_nondet_uint()) {
      i = __VERIFIER_nondet_int();
    }
  }
  __VERIFIER_assert(i != 0);
  return 0;
}
