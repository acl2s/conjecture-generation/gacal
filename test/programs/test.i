extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int i=0;
  unsigned int n=__VERIFIER_nondet_uint();
  for (i=0; i<n; i++) ;
  __VERIFIER_assert(i == n);
  return 0;
}
