extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int i=11;
  while (i != 10) {
    i = i+1;
  }
  while (i != 4) {
    i = i+1;
  }

  __VERIFIER_assert(i == 4);
  return 0;
}
