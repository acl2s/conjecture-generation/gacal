extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int n=__VERIFIER_nondet_uint();
  if (!(n < 5 || 10 < n)) return 0;
  __VERIFIER_assert (n < 5 || 10 < n);
  return 0;
}
