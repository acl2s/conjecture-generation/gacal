extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int x=0;
  unsigned int y=0;
  unsigned int n=__VERIFIER_nondet_uint();
  for (unsigned int i=0; i<n; i++) {
    unsigned int r = __VERIFIER_nondet_uint();
    x = x + r;
    y = y + r;
  }
  __VERIFIER_assert(x == y);
  return 0;
}
