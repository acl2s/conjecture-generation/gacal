extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int i=0;
  unsigned int n=__VERIFIER_nondet_uint();
  while (i<n) {
    i = i+1;
    if (__VERIFIER_nondet_uint()) {
      break;
    }
  }
  __VERIFIER_assert(i <= n);
  return 0;
}
