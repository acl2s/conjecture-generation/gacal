extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int __VERIFIER_nondet_int();

int n = 100;
int main() {
  int i=0;
  while (i<n) {
    i++;
  }
  __VERIFIER_assert(i == n+1);
  return 0;
}
