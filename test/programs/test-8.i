extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

unsigned int n=200000;

int main() {
  unsigned int i=0;
  while (i<n) {
    i = i+1;
  }
  __VERIFIER_assert(i == n);
  return 0;
}
