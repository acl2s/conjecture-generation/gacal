extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int __VERIFIER_nondet_int();

int main() {
  int i=0;
  int n=__VERIFIER_nondet_int();
  while (i<n) {
    __VERIFIER_assert(i < 10);
    i++;
  }
  return 0;
}
