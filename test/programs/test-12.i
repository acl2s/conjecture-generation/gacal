extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int a = 0;
  unsigned int b = 0;
  while (__VERIFIER_nondet_uint()) {
    unsigned int i = 0;
    while (i < 10000000) {
      i = i + 1;
    }
    __VERIFIER_assert(a == b);
  }
  __VERIFIER_assert(a == b);
  return 0;
}
