(include-book "./lib/top")

:q

(load "src/load.lisp")

(in-package :acl2)

#|
(defun main (args)
  (cond ((< (len args) 2)
    (progn (print "Usage: gacal -file <hwk file> [output fd]") 
           (sb-ext:quit)))
        ((= (len args) 3) (prover::set-print2-fd (parse-integer (third args)))))
  (progn (prover::check-file-proofs (second args))
         (sb-ext:quit)))
|#

(save-exec "gacal"
            (format
             nil
             "GACAL Version 0.1.~
   ~%Copyright (C) 2019, Northeastern University.~
   ~%GACAL uses ACL2 and ACL2s.~
   ~%GACAL comes with ABSOLUTELY NO WARRANTY.~
   ~%This is free software with certain restrictions.~
   ~%See the LICENSE files distributed with ACL2s and ACL2.")
            )

#|
            :init-forms '((set-gag-mode nil)
                          (value :q))
            :toplevel-args "--eval '(declaim (sb-ext:muffle-conditions style-warning))' --eval '(acl2::main sb-ext:*posix-argv*)'")
|#
