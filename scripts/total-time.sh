#!/bin/bash

grep "TOTAL TRACE TIME" summary.log             | awk '{s+=$4}END{printf "TRACE TIME:             %10.2f\n", s}'
grep "TOTAL ITERATE AND PROVE TIME" summary.log | awk '{s+=$6}END{printf "ITERATE AND PROVE TIME: %10.2f\n", s}'
grep "TOTAL PROVE ASSERTIONS TIME" summary.log  | awk '{s+=$5}END{printf "PROVE ASSERTIONS:       %10.2f\n", s}'
grep "TOTAL PROVER TIME" summary.log            | awk '{s+=$4}END{printf "PROVER TIME:            %10.2f\n", s}'
grep "TOTAL CGEN EVAL TIME" summary.log         | awk '{s+=$5}END{printf "CGEN EVAL TIME:         %10.2f\n", s}'
grep "TOTAL TERM EVAL TIME" summary.log         | awk '{s+=$5}END{printf "TERM EVAL TIME:         %10.2f\n", s}'
grep "TIMEOUT" summary.log | wc                 | awk           '{printf "TIMEOUT TIME:           %10.2f\n", $1*60*15}'
