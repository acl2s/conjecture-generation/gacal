#!/usr/bin/python3

import sys
import os
import subprocess
import signal
import re
from datetime import datetime
import multiprocessing as mp
from functools import partial
import argparse
import tempfile
import time
import platform

# https://eli.thegreenplace.net/2012/01/04/shared-counter-with-pythons-multiprocessing
class Counter(object):
    def __init__(self, initval=0):
        self.val = mp.Value('i', initval)
        self.lock = mp.Lock()

    def increment(self):
        with self.lock:
            self.val.value += 1

    def value(self):
        with self.lock:
            return self.val.value

parser = argparse.ArgumentParser()
parser.add_argument("--validate", dest="validate", action="store_true")
parser.add_argument("--parallel", dest="parallel", action="store_true")
parser.add_argument("--do-violation", dest="do_violation", action="store_true")
parser.add_argument("--important", dest="important", action="store_true")
parser.add_argument("--threads", dest="threads", default = str(mp.cpu_count()))
parser.add_argument("--time-limit", dest="time_limit", default = str(15 * 60))
parser.add_argument("--memory-limit", dest="memory_limit", default = "8192")
parser.add_argument("--header", dest="header", default="")
parser.add_argument("--evaluate", dest="evaluate", action="store_true")
args = parser.parse_args()
validate = args.validate
parallel = args.parallel
do_violation = args.do_violation
important = args.important
threads = int(args.threads)
time_limit = args.time_limit
memory_limit = args.memory_limit
header = args.header
evaluate = args.evaluate

verbose = True

path = os.path.dirname(os.path.realpath(__file__))
base_log_dir = path + "/logs" if not important else path + "/important-logs"
test_path = path + "/test"
property_file = path + "/verify/sv-witnesses/PropertyUnreachCall.prp"
platform_info = " ".join (platform.uname())

def ensure_dir (directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

DO_ALL = True
DO_VERIABS = True

ensure_dir (base_log_dir)

if DO_ALL:

    log_dir = base_log_dir + "/" + datetime.now().strftime(platform.system() + "-%Y-%m-%d-%H:%M:%S")
    summary_log_file = log_dir + "/" + "summary.log"
    ensure_dir (log_dir)

    global_fout = open(summary_log_file, "w")
    global_fout.write(header + "\n")
    global_fout.close()

    true_property = "".join("  - property_file: ../properties/unreach-call.prp\n    expected_verdict: true".split())
    false_property = "".join("  - property_file: ../properties/unreach-call.prp\n    expected_verdict: false".split())
    directories = ["loop-acceleration", 
                   "loop-crafted", 
                   "loop-industry-pattern", 
                   "loop-invariants", 
                   "loop-invgen", 
                   "loop-lit", 
                   "loop-new", 
                   "loop-simple", 
                   "loops-crafted-1", 
                   "loops"
                   ]
    regex = re.compile('.*yml')
    all_files = []
    index = 0
    for dir in directories:
        ensure_dir (log_dir + "/" + dir)
        for root, dirs, files in os.walk(test_path + "/programs/sv-benchmarks/c/" + dir):
            for file in files:
                if regex.match(file):
                    with open(test_path + "/programs/sv-benchmarks/c/" + dir + "/" + file, "r") as f:
                        temp = "".join(f.read().split())
                        f.close()
                        if true_property in temp or (do_violation and false_property in temp):
                            all_files = all_files + [(dir, file, index)]
                            index = index + 1

    count_verify_C = Counter(0)
    count_falsify_C = Counter(0)
    parsed_verify_C = Counter(0)
    parsed_falsify_C = Counter(0)
    total_verify_C = Counter(0)
    total_falsify_C = Counter(0)
    write_lock = mp.Lock()
    tempfile_lock = mp.Lock()

    def runFile (pair):
        dir, file, index = pair
        temp = ""
        with open(test_path + "/programs/sv-benchmarks/c/" + dir + "/" + file, "r") as f:
            temp = "".join(f.read().split())
            f.close()
        if true_property in temp or (do_violation and false_property in temp):
            print_string = ""
            global_fout_string = ""
            global_eval_string = ""
            base_name = os.path.splitext(file)[0]
            base_name_file = test_path + "/programs/sv-benchmarks/c/" + dir + "/" + base_name
            log_file = log_dir + "/" + dir + "/" + base_name + ".log"
            
            input_file = base_name_file + ".i" if os.path.exists(base_name_file + ".i") else base_name_file + ".c"
                
            witness_file = log_dir + "/" + dir + "/" + base_name + ".graphml"
            
            call = [path + "/run-gacal.py", "--witness", witness_file, "--memory-limit", memory_limit, "--time-limit", time_limit]
            call = call + ["--verbose"] if verbose else call
            call = call + [input_file]
            
            print_string = print_string + "\n" + "===================================="
            print_string = print_string + "\n" + (dir + "/" + base_name)
            print_string = print_string + "\n" + ("====================================")
            if true_property in temp:
                print_string = print_string + "\n" + ("EXPECTED: TRUE")
            if false_property in temp:
                print_string = print_string + "\n" + ("EXPECTED: FALSE")

            result = 0
                        
            gacal_stdout_file = ""
            gacal_stderr_file = ""
            with tempfile_lock:
                gacal_stdout_file = tempfile.mkstemp()[1]
                gacal_stderr_file = tempfile.mkstemp()[1]
            stdout_file = open(gacal_stdout_file, 'w') 
            stderr_file = open(gacal_stderr_file, 'w') 
            result = subprocess.run (call,
                                     stdout=stdout_file,
                                     stderr=stderr_file)
            stdout_file.close()
            stderr_file.close()

            output = ""
            stdout_file = open(gacal_stdout_file, 'r') 
            output = stdout_file.read()
            stdout_file.close()
            stderr_file = open(gacal_stderr_file, 'r') 
            output = output + stderr_file.read()
            stderr_file.close()
            os.remove(gacal_stdout_file)
            os.remove(gacal_stderr_file)

            fout = open(log_file, "w")
            fout.write ("====================================" + "\n")
            fout.write (dir + "/" + base_name + "\n")
            fout.write ("===================================="+ "\n")
            if true_property in temp:
                fout.write ("EXPECTED: TRUE" + "\n")
            if false_property in temp:
                fout.write ("EXPECTED: FALSE" + "\n")
            fout.write(output)
            fout.close()                
            
            successful = False
            failed = False
            unknown = False
            error = "BAD ERROR" in output
            not_parsed = "NO CONVERSION" in output
            #print(output)
            summary_stats = []
            parsed = False
            if error:
                print_string = print_string + "\n" + ("BAD ERROR")
            elif not_parsed:
                print_string = print_string + "\n" + ("NOT PARSED")
            else:
                parsed = True
                global_fout_string = global_fout_string + ("====================================" + "\n")
                global_fout_string = global_fout_string + (dir + "/" + base_name + "\n")
                global_fout_string = global_fout_string + ("===================================="+ "\n")
                global_eval_string = global_eval_string + ("(" + dir + "/" + base_name + " ")
                if true_property in temp:
                    global_fout_string = global_fout_string + ("EXPECTED: TRUE" + "\n")
                if false_property in temp:
                    global_fout_string = global_fout_string + ("EXPECTED: FALSE" + "\n")
                print_stats = []
                for line in output.split("\n"):
                    if "TOTAL" in line:
                        print_string = print_string + "\n" + (line)
                        global_fout_string = global_fout_string + (line + "\n")
                    if "PROOF STATS" in line:
                        print_stats = print_stats + [line]
                    if "SUMMARY" in line:
                        summary_stats = summary_stats + [line]
                if print_stats != []:
                    line = print_stats[len(print_stats) - 1]
                    print_string = print_string + "\n" + line
                    global_fout_string = global_fout_string + (line + "\n")

                successful = "VERIFICATION_SUCCESSFUL" in output
                failed = "VERIFICATION_FAILED" in output
                unknown = "COULD NOT PROVE ALL ASSERTIONS" in output
                if successful or failed or unknown:
                    if successful or failed:
                        if successful:
                            print_string = print_string + "\n" + ("VERIFICATION SUCCESSFUL")
                            global_fout_string = global_fout_string + ("VERIFICATION SUCCESSFUL" + "\n")
                            global_eval_string = global_eval_string + "SUCCESS "
                        if failed:
                            print_string = print_string + "\n" + ("VERIFICATION FAILED")
                            global_fout_string = global_fout_string + ("VERIFICATION FAILED" + "\n")
                            global_eval_string = global_eval_string + "FAIL "
                            
                        if validate:
                            print_string = print_string + "\n" + ("ATTEMPTING TO VERIFY WITNESS")

                            if DO_VERIABS:
                                veriabs_call = [path + "/verify/VeriAbs/scripts/veriabs",
                                                "--property-file", property_file,
                                                "--validate", witness_file,
                                                input_file]
                                veriabs_stdout_file = ""
                                veriabs_stderr_file = ""
                                with tempfile_lock:
                                    veriabs_stdout_file = tempfile.mkstemp()[1]
                                    veriabs_stderr_file = tempfile.mkstemp()[1]

                                with open(veriabs_stdout_file, 'w') as stdout_file, \
                                     open(veriabs_stderr_file, 'w') as stderr_file:
                                    result = subprocess.run (call,
                                                             stdout=stdout_file,
                                                             stderr=stderr_file)
                                    stdout_file.close()
                                    stderr_file.close()
                                std_output = ""
                                err_output = ""
                                with open(veriabs_stdout_file, 'r') as file:
                                    std_output = file.read()
                                    file.close()
                                with open(veriabs_stderr_file, 'r') as file:
                                    err_output = file.read()
                                    file.close()
                                os.remove(veriabs_stdout_file)
                                os.remove(veriabs_stderr_file)
                                fout = open(log_file, "a")
                                fout.write ("====================================" + "\n")
                                fout.write("RUNNING VERIABS" + "\n")
                                fout.write ("====================================" + "\n")
                                fout.write ("stdout: " + "\n")
                                fout.write (std_output)
                                fout.write ("stderr: " + "\n")
                                fout.write (err_output)
                                fout.close()
                                
                                if "VERIABS_VERIFICATION_SUCCESSFUL" in std_output:
                                    print_string = print_string + "\n" + ("VERIFIED BY VERIABS")
                                    global_fout_string = global_fout_string + ("VERIFIED BY VERIABS")
                                elif "VERIABS_VERIFICATION_FAILED" in std_output:
                                    print_string = print_string + "\n" + ("VERIFIED FAILED BY VERIABS")
                                    global_fout_string = global_fout_string + ("VERIFIED FAILED BY VERIABS")
                                else:
                                    print_string = print_string + "\n" + ("VERIFIED UNKNOWN BY VERIABS")
                                    global_fout_string = global_fout_string + ("VERIFIED UNKNOWN BY VERIABS")
                    else: 
                        print_string = print_string + "\n" + ("COULD NOT PROVE ALL ASSERTIONS")
                        global_fout_string = global_fout_string + ("COULD NOT PROVE ALL ASSERTIONS" + "\n")
                        global_eval_string = global_eval_string + "COULDNT "
                else:
                    print_string = print_string + "\n" + ("TIMEOUT")
                    global_fout_string = global_fout_string + ("TIMEOUT" + "\n")
                    global_eval_string = global_eval_string + "TIMEOUT "
                   
            if summary_stats != []:
                line = summary_stats[len(summary_stats) - 1]
                global_eval_string = global_eval_string + line
            else:
                global_eval_string = global_eval_string + "?"
            global_eval_string = global_eval_string + ")\n"

            with write_lock:
                if true_property in temp:
                    total_verify_C.increment()
                if false_property in temp:
                    total_falsify_C.increment()
                        
                if successful:
                    count_verify_C.increment()
                if failed:
                    count_falsify_C.increment()
                if true_property in temp and not not_parsed and not error:
                    parsed_verify_C.increment()
                if false_property in temp and not not_parsed and not error:
                    parsed_falsify_C.increment()
                if not not_parsed and not error:
                    global_fout_string = global_fout_string + ("TOTAL PARSED CORRECTNESS TASKS: " + str(parsed_verify_C.value()) + "\n" +
                                                               "TOTAL PARSED VIOLATION TASKS: " + str(parsed_falsify_C.value()) + "\n" +
                                                               "TOTAL SOLVED CORRECTNESS TASKS: " + str(count_verify_C.value()) + "\n" +
                                                               "TOTAL SOLVED VIOLATION TASKS: " + str(count_falsify_C.value()) + "\n")

                print(print_string)
                global_fout = open(summary_log_file, "a")
                if evaluate and parsed:
                    global_fout.write(global_eval_string)
                else:
                    global_fout.write(global_fout_string)
                global_fout.close()    

    print(platform_info)
    limit_info = "Running with " + str(memory_limit) + " MB and " + str(time_limit) + " time out"
    print(limit_info)
    global_fout = open(summary_log_file, "a")
    global_fout.write(platform_info + "\n" + limit_info + "\n")
    global_fout.close()    

    if parallel:
        print("Running in parallel with " + str(threads) + " threads")
        global_fout = open(summary_log_file, "a")
        global_fout.write("Running in parallel with " + str(threads) + " threads" + "\n")
        global_fout.close()    

        pool = mp.Pool(threads)
        m = mp.Manager()
        pool.map (runFile, all_files)
        pool.close()
        pool.join()
    else:
        for pair in all_files:
            runFile(pair)

    count_verify = count_verify_C.value()
    count_falsify = count_falsify_C.value()
    parsed_verify = parsed_verify_C.value()
    parsed_falsify = parsed_falsify_C.value()
    total_verify = total_verify_C.value()
    total_falsify = total_falsify_C.value()

    results_str = "".join(["========== DONE ===========", "\n",
                           "TOTAL CORRECTNESS TASKS: ", str(total_verify), "\n",
                           "TOTAL VIOLATION TASKS: ", str(total_falsify), "\n",
                           "TOTAL PARSED CORRECTNESS TASKS: ", str(parsed_verify), "\n",
                           "TOTAL PARSED VIOLATION TASKS: ", str(parsed_falsify), "\n",
                           "TOTAL SOLVED CORRECTNESS TASKS: ", str(count_verify), "\n",
                           "TOTAL SOLVED VIOLATION TASKS: ", str(count_falsify)])
    print (results_str)

    global_fout = open(summary_log_file, "a")
    global_fout.write (results_str + "\n")
    global_fout.close()








programs = [
    (False, "programs/test-violation.i"), # 
    (False, "programs/test-violation-2.i"), # 
    (False, "programs/test-violation-3.i"), # 
    (False, "programs/test-violation-4.i"), # 


    (False, "programs/test.i"), # VERIFICATION_SUCCESSFUL 5s
    (False, "programs/test-1.i"), # 
    (False, "programs/test-2.i"), # VERIFICATION_SUCCESSFUL 2s
    (False, "programs/test-3.i"), # VERIFICATION_SUCCESSFUL 4s
    (False, "programs/test-4.i"), # VERIFICATION_SUCCESSFUL 6s
    (False, "programs/test-5.i"), # VERIFICATION_SUCCESSFUL 7s
    (False, "programs/test-6.i"), # VERIFICATION_SUCCESSFUL 
    (False, "programs/test-7.i"), # VERIFICATION_SUCCESSFUL 
    (False, "programs/test-8.i"), # VERIFICATION_SUCCESSFUL 
    (False, "programs/test-9.i"), # VERIFICATION_SUCCESSFUL 
    (False, "programs/test-10.i"), # VERIFICATION_SUCCESSFUL 
    (False, "programs/test-11.i"), # VERIFICATION_SUCCESSFUL 
   #(False, "programs/test-12.i"), # 
    (False, "programs/test-13.i"), # VERIFICATION_SUCCESSFUL 
   #(False, "programs/array-test.i"), # NO CONVERSION (array)
   #(False, "programs/loop-crafted/simple_array_index_value_1-1.c"), # NO CONVERSION (array)
   #(False, "programs/loop-crafted/simple_array_index_value_1-2.c"), # NO CONVERSION (array)
   #(False, "programs/loop-crafted/simple_array_index_value_2.c"), # NO CONVERSION (array)
   #(False, "programs/loop-crafted/simple_array_index_value_3.c"), # NO CONVERSION (array)
   #(False, "programs/loop-crafted/simple_array_index_value_4.c"), # NO CONVERSION (array)
    (False, "programs/loop-crafted/simple_vardep_1.c"), # VERIFICATION SUCCESSFUL 191s
    (False, "programs/loop-crafted/simple_vardep_2.c"), # VERIFICATION SUCCESSFUL 43s
    (False, "programs/loop-industry-pattern/mod3.c"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invariants/bin-suffix-5.c"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invariants/const.c"), # VERIFICATION SUCCESSFUL 2s 
    (False, "programs/loop-invariants/eq1.c"), # VERIFICATION SUCCESSFUL 4s 
    (False, "programs/loop-invariants/eq2.c"), # VERIFICATION SUCCESSFUL 2s
    (False, "programs/loop-invariants/even.c"), # VERIFICATION SUCCESSFUL 8s
    (False, "programs/loop-invariants/mod4.c"), # VERIFICATION SUCCESSFUL 11s
    (False, "programs/loop-invariants/odd.c"), # VERIFICATION SUCCESSFUL 4s
    (False, "programs/loop-invgen/down.i"), # VERIFICATION SUCCESSFUL 8s
    (False, "programs/loop-invgen/fragtest_simple.i"), # VERIFICATION SUCCESSFUL 50s
    (False, "programs/loop-invgen/half_2.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invgen/heapsort.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invgen/large_const.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invgen/nested6.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invgen/nested9.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-invgen/nest-if3.i"), # VERIFICATION SUCCESSFUL 93s
    (False, "programs/loop-invgen/seq-3.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS
   #(False, "programs/loop-invgen/SpamAssassin-loop.i"), # 
    (False, "programs/loop-lit/afnp2014.i"), # VERIFICATION SUCCESSFUL 10s
    (False, "programs/loop-lit/bhmr2007.i"), # VERIFICATION SUCCESSFUL 40s
    (False, "programs/loop-lit/cggmp2005.i"), # VERIFICATION SUCCESSFUL 14s
    (False, "programs/loop-lit/cggmp2005b.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/cggmp2005_variant.i"), # VERIFICATION SUCCESSFUL 14s
    (False, "programs/loop-lit/css2003.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/ddlm2013.i"), # VERIFICATION SUCCESSFUL 
   #(False, "programs/loop-lit/gcnr2008.i"), # NO CONVERSION (chained equalities)
    (False, "programs/loop-lit/gj2007.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/gj2007b.i"),  # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/gr2006.i"),  # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/gsv2008.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/hhk2008.i"), # VERIFICATION SUCCESSFUL 
    (False, "programs/loop-lit/jm2006.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
    (False, "programs/loop-lit/jm2006_variant.i"), # TIMEOUT / COULD NOT PROVE ALL ASSERTIONS (lack of traces?)
   #(False, "programs/loop-lit/mcmillan2006.i"), # NO CONVERSION (malloc)
    (False, "programs/loop-new/count_by_1.i"), # VERIFICATION SUCCESSFUL 31s
    (False, "programs/loop-new/count_by_1_variant.i"), # VERIFICATION SUCCESSFUL 38s
    (False, "programs/loop-new/count_by_2.i"), # VERIFICATION SUCCESSFUL 29s
    (False, "programs/loop-new/count_by_k.i"), # VERIFICATION SUCCESSFUL 47s
    (False, "programs/loop-new/count_by_nondet.i"), # VERIFICATION SUCCESSFUL 37s
    (False, "programs/loop-new/gauss_sum.i"), # VERIFICATION SUCCESSFUL 50s
    (False, "programs/loop-new/half.i"), # COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loop-new/nested-1.i"), # VERIFICATION SUCCESSFUL 90s
   #(False, "programs/loops/array-1.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/array-2.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/bubble_sort-1.c"), # NO CONVERSION (arrays)
    (False, "programs/loops/count_up_down-1.c"), # VERIFICATION SUCCESSFUL 37s
   #(False, "programs/loops/count_up_down-2.c"), # is not true
   #(False, "programs/loops/eureka_01-1.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/eureka_01-2.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/eureka_05.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/for_bounded_loop1.c"), # is not true
   #(False, "programs/loops/insertion_sort-1.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/insertion_sort-2.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/matrix-1.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/matrix-2.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/nec20.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/nec40.c"), # NO CONVERSION (arrays)
   #(False, "programs/loops/sum01-1.c"), # is not true
    (False, "programs/loops/sum01-2.c"), # VERIFICATION SUCCESSFUL 48s
    (False, "programs/loops/sum04-1.c"), # COULD NOT PROVE ALL ASSERTIONS
    (False, "programs/loops/sum04-2.c"), # VERIFICATION SUCCESSFUL 38s
    (False, "programs/loops-crafted-1/Mono1_1-1.c"), # VERIFICATION SUCCESSFUL 28s
    (False, "programs/loops-crafted-1/Mono1_1-2.c"), # VERIFICATION SUCCESSFUL 28s
    (False, "programs/loops-crafted-1/Mono3_1.c"), # VERIFICATION SUCCESSFUL 40s
    (False, "programs/loops-crafted-1/Mono4_1.c"), # VERIFICATION SUCCESSFUL 40s
    (False, "programs/loops-crafted-1/Mono5_1.c"), # VERIFICATION SUCCESSFUL 74s
    (False, "programs/loops-crafted-1/Mono6_1.c"), # VERIFICATION SUCCESSFUL 77s
    (False, "programs/sv-benchmarks/c/loop-acceleration/const_1-1.c"),
    (False, "programs/sv-benchmarks/c/loops-crafted-1/sumt3.c"),
    (False, "programs/sv-benchmarks/c/loop-acceleration/underapprox_1-2.c"),
    (False, "programs/sv-benchmarks/c/loop-acceleration/diamond_2-2.c"),
    (False, "programs/sv-benchmarks/c/loop-acceleration/diamond_1-1.c"),

    (False, "programs/sv-benchmarks/c/loop-acceleration/nested_1-1.c"), # good
    (False, "programs/sv-benchmarks/c/loop-acceleration/phases_1-1.c"), # good
    (False, "programs/sv-benchmarks/c/loop-invgen/nest-if3.c"), # good (although almost hitting 2min runtime)
    (False, "programs/sv-benchmarks/c/loop-invgen/fragtest_simple.c"), # good

    (False, "programs/sv-benchmarks/c/loop-lit/bhmr2007.c"), #


    (False, "programs/sv-benchmarks/c/loop-acceleration/underapprox_1-2.c"), # good

    (False, "programs/sv-benchmarks/c/loop-acceleration/const_1-1.c"), #
    (False, "programs/sv-benchmarks/c/loop-acceleration/phases_1-1.c"), #

    (False, "programs/sv-benchmarks/c/loop-acceleration/diamond_1-1.c"), #
    (False, "programs/sv-benchmarks/c/loop-acceleration/simple_1-2.c"), #
    (False, "programs/sv-benchmarks/c/loop-acceleration/nested_1-1.c"), #

    (False, "programs/sv-benchmarks/c/loops-crafted-1/nested5-1.c"), #
    (False, "programs/sv-benchmarks/c/loops/trex03-2.c"), #

    (False, "programs/sv-benchmarks/c/loop-invgen/half_2.i"), #
    (False, "programs/sv-benchmarks/c/loop-invgen/heapsort.i"), #
    (False, "programs/sv-benchmarks/c/loop-invgen/string_concat-noarr.i"), #
    (False , "programs/sv-benchmarks/c/loops-crafted-1/mono-crafted_11.c"), #
    (False, "programs/sv-benchmarks/c/loops-crafted-1/loopv3.c"), #
    (False, "programs/sv-benchmarks/c/loops-crafted-1/vnew1.c"), #

    ]

for program_pair in programs:
    do = program_pair[0]
    if do:
        program = program_pair[1]
        
        call = [path + "/run-gacal.py",
                "--witness", test_path + "/output/" + program + ".graphml"]
        if verbose:
            call = call + ["--verbose"]
        call = call + ["--memory-limit", "8192"]
        call = call + ["--time-limit", "120"]
        
        call = call + [test_path + "/" + program]
            
        #print("Running: " + " ".join(call))
        print ("====================================")
        print (program)
        print ("====================================")
        result = 0
        
        result = subprocess.run (call)

        veriabs_call = [path + "/verify/VeriAbs/scripts/veriabs",
                        "--property-file", property_file,
                        "--validate", test_path + "/output/" + program + ".graphml",
                        test_path + "/" + program]
        if verify:
            result = subprocess.run (veriabs_call)

        print("\n\n\n\n")

