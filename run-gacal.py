#!/usr/bin/python3

import sys
import os
import subprocess
import argparse
import tempfile
import hashlib
import threading
from datetime import datetime
import signal

parser = argparse.ArgumentParser()
parser.add_argument("-v","--version", action='version', version='1.0')
parser.add_argument("--verbose", dest="verbose", action="store_true")
parser.add_argument("--witness", dest="witness")
parser.add_argument("--memory-limit", dest="memory_limit", default="8192")
parser.add_argument("--time-limit", dest="time_limit", default="900")
parser.add_argument("--debug", dest="debug", action="store_true")
parser.add_argument("testcase")
args = parser.parse_args()

input_file = args.testcase
output_file = args.witness
memory_limit = args.memory_limit
time_limit = int(args.time_limit)
debug = args.debug

assert (input_file != None)
assert (output_file != None)

# hash the input
hash_value = ""
with open(input_file,"rb") as f:
    bytes = f.read() # read entire file as bytes
    hash_value = hashlib.sha256(bytes).hexdigest();
    f.close()
    
path = os.path.dirname(os.path.realpath(__file__))

# call the java program to parse the c program into an sexp 
sexp_output_file = tempfile.mkstemp()[1]

sexp_parser_call = [path + "/parser/scripts/sexp.sh", "-source", input_file, "-output", sexp_output_file]
if (sys.version_info[0] < 3) or (sys.version_info[0] == 3 and sys.version_info[1] < 5):
    subprocess.call (sexp_parser_call)
else:
    subprocess.run (sexp_parser_call)
    
# call Gacal to solve

gacal_call = ["./gacal",
              "--noprint",
              "--noinform",
              "-input-file", input_file,
              "-sexp-input-file", sexp_output_file,
              "-output-file", output_file,
              "-input-hash", hash_value]

sbcl_user_args = [os.environ.get("SBCL_USER_ARGS", ""), "--noinform"]
if memory_limit != None:
    sbcl_user_args = sbcl_user_args + ["--dynamic-space-size", memory_limit]

os.environ["SBCL_USER_ARGS"] = " ".join(sbcl_user_args)

if args.verbose:
   gacal_call = gacal_call + ["--verbose"]	
    
gacal_input = [path + "/src/start-script.lisp"]

gacal_call = gacal_call + ["<"] + gacal_input

def run_cmd(cmd, timeout_sec):
    proc = subprocess.Popen(" ".join(cmd), shell=True)
    timer = threading.Timer(timeout_sec, proc.kill)
    try:
        timer.start()
        stdout, stderr = proc.communicate()
    finally:
        timer.cancel()
        os.killpg(0, signal.SIGKILL)

if debug:
    os.system(" ".join(gacal_call))
else:
    os.setpgrp()

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("Current Time =", current_time)
    
    run_cmd(gacal_call, time_limit)
    
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("Current Time =", current_time)
    
    run_cmd(["sleep", "1"], 100)

